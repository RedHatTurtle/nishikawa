
 module gcr_module

 public  :: gcr_solver

! Constants used in GCR
  integer , parameter ::   dp = selected_real_kind(p=15)
  real(dp), parameter :: zero = 0.0_dp
  real(dp), parameter ::  one = 1.0_dp

 contains

!********************************************************************************
!
!      Generalized Conjugate Residual (GCR) with a variable preconditioner.
!
! This subroutine 'gcr_solver' solves the linear system: Ax = b, up to a specified
! tolerance by the GCR method with a variable preconditioner that approximately
! computes M^{-1} (e.g., by a relaxation scheme), where M is a preconditioning matrix.
!
! See, for example,
!
!  K. Abe and S.-L. Zhang,
!  A Variable Preconditioning Using The SOR Method for GCR-Like Methods,
!  International Journal of Numerical Analysis and Modeling , Vol.2, No.2,
!  pp.147-161, 2005.
!
!--------------------------------------------------------------------------------
!
!  Input: max_projection_gcr = Maximum GCR projections (search directions)
!                     mu_gcr = Tolerance for the linear residual reduction.
!
! Output: actual_projections = Actual projections performed.
!              actual_sweeps = Total # of relaxations (sweeps) to compute M^{-1}.
!                   rkr0_gcr = Final linear residual reduction
!                  status_pc = success(0), diverged(1 or 2)
!                 status_gcr = success(0), not converged (2)
!                          x = Solution copied to your code before exit.
!
!  Note: Success means that the tolerance is met within max_projection_gcr.
!
!--------------------------------------------------------------------------------
!
! To use 'gcr_solver' in your code:
!
!  (1)Write the following subroutines using your data in your code:
!
!      your_dimension()            : Return total # of discrete unknowns
!      your_rhs_vector()           : Compute b of Ax=b.
!      your_preconditioner()       : Compute p=M^{-1}*r for a given vector r.
!      your_compute_Ap()           : Compute Ap for a given vector p.
!      your_rms()                  : Compute RMS of a vector.
!      your_length()               : Compute the length of a vector.
!      your_dot_product()          : Compute dot product of two vectors.
!      your_store_x_in_your_array(): Copy x to your own soluition array.
!
!     and add them at the end of this module or make them accesible via use
!     statement where they are called.
!
!     These subroutines are the ones that use specific data structure and
!     subroutines/functions in your code. Only you can write them!
!
!  (2)Then, call gcr_solver() inside your code, e.g.,
!
!       call gcr_solver(10,0.5,.....)
!
!--------------------------------------------------------------------------------
!
!
!  Note: To solve a linear system well, e.g., mu_gcr=1.0e-06, but then you'll
!        need to set a large value for max_projection_gcr such as 50-100, or more.
!
!  Note: To solve a linear system in Newton's method for nonlinear equations,
!        it's perhaps enouigh to set mu_gcr=0.9 with max_projection_gcr = 4, for
!        example.
!
!  Note: Preconditioner is assumed to be an approximate matrix M approximately
!        inverted by a relaxation scheme (e.g., Gauss Seidel). 'actual_sweeps'
!        is the actual nummber of relaxations performed; it is accumulated over
!        the entire GCR projections. The number of relaxations can change every
!        time the preconditioning is performed; thus a variable preconditioner.
!
!  Note: Preconditioner does not have to be variable, and can be fixed.
!        If the preconditioner is of iLU-type, we'll always have status_pc = 0
!        and actual_sweeps=0.
!
!  Note: In CFD codes, a variable preconditioner is typically available as
!        an implicit solver: approximately solve M*dq = -Res(q), where M is an
!        approximate Jacobian. If you already have it, you can use your implicit
!        solver (as it is) as the preconditioner.
!
!
!--------
! Notes:
!
!  The purpose of this code is to give a beginner an opportunity to learn.
!  Hence, the focus here is on the simplicity. The code is not optimized for
!  efficiency; and no consideration for parallelization is given.
!
!  If the code is not simple enough to understand, please send questions to Hiro
!  at sunmasen(at)hotmail.com. He'll greatly appreciate it and revise the code.
!
!  If the code helps you understand how to write your own code that is more
!  efficient and has more features (possibly in a different programming language),
!  it'll have served its purpose.
!
!
!        written by Dr. Katate Masatsuka (info[at]cfdbooks.com),
!
! the author of useful CFD books, "I do like CFD" (http://www.cfdbooks.com).
!
! This is Version 3 (February 2018).
! This F90 code is written and made available for an educational purpose.
! This file may be updated in future
!
!********************************************************************************
 subroutine gcr_solver(max_projection_gcr, mu_gcr, actual_projections, &
                        actual_sweeps, rkr0_gcr, status_pc, status_gcr )

 implicit none

 integer , intent( in) :: max_projection_gcr !Maximum search directions
 real(dp), intent( in) :: mu_gcr             !Tolerance for GCR

 integer , intent(out) :: actual_projections !Actual GCR projections performed
 integer , intent(out) :: actual_sweeps      !Actual preconditioning relaxations
 real(dp), intent(out) :: rkr0_gcr           !Actual residual reduction
 integer , intent(out) :: status_pc          !Preconditioner status
 integer , intent(out) :: status_gcr         !GCR status

 real(dp), dimension(:)  , allocatable :: x    !Solution vector
 real(dp), dimension(:)  , allocatable :: r    !Residual vector
 real(dp), dimension(:)  , allocatable :: r0   !Initial residual vector
 real(dp), dimension(:)  , allocatable :: a, b !Temporary arrays
 real(dp), dimension(:,:), allocatable :: p    !Search directions
 real(dp), dimension(:,:), allocatable :: Ap   !Ap vectors

 real(dp) :: my_eps, alpha, beta, rms_initial, length_p, length_r
 integer  :: i, k, istat_preconditioner, istat_preconditioner_0
 integer  :: ndim, sweeps
 logical  :: stall_condition

             my_eps  = epsilon(one)
          status_pc  = 0
          status_gcr = 0
              alpha  = one
       actual_sweeps = 0
  actual_projections = 0

!------------------------------------------------------------------
! Give me the problem dimension, ndim = total number of discrete unknowns.

  call gcr_dimension(ndim)

!------------------------------------------------------------------
! Allocate gcr arrays:

  allocate(  x(ndim) )
  allocate(  r(ndim) )
  allocate( r0(ndim) )
  allocate(  a(ndim) )
  allocate(  b(ndim) )
  allocate(  p(ndim,max_projection_gcr) )
  allocate( Ap(ndim,max_projection_gcr) )

!------------------------------------------------------------------
!  Initialization

   x = zero
   r = zero
   p = zero

!  Initial residual r0 is b since x=0 (r = b-A*x = b).
!  If your linear system is (dRes/dq)*x=-Res(qk), then r0=-Res(qk).

   call gcr_rhs_vector(r0,ndim)
             r = r0
   rms_initial = gcr_rms(r0,ndim)

!   Preconditioning: p_1 = M^{-1}*r0.
    call gcr_preconditioner(r0,ndim, p(:,1),sweeps,istat_preconditioner_0)
    actual_sweeps = actual_sweeps + sweeps !Accumulate sweeps. 

    if (istat_preconditioner_0 == 100) then
     write(2000,*) ">>>>> Exit GCR: Preconditioner diverged at the beginning(roc > 100.0)."
     status_pc          = 2
     status_gcr         = 0
     actual_projections = 0
     actual_sweeps      = 0
     return
    endif

!   Compute Ap
    call gcr_compute_Ap(p(:,1),ndim, Ap(:,1))

!-----------------------------------------------------------------

!---------------------------------------------------------------------
!---------------------------------------------------------------------
! Beginning of GCR Projection
!---------------------------------------------------------------------
!---------------------------------------------------------------------
   i = 0
  gcr_projection : do

   i = i + 1
   actual_projections = i

!-----------------------------------------
!  Update x and r to minimize the residual in L2 norm.
!   Minimize (ri,ri): -> (r-alpha*Ap_i)*d(r-alpha*Ap_i)/d(alpha)
!                      = 0 -> alpha = (r,Ap_i)/(Ap_i,Ap_i).
!
!      alpha = (r,Ap_i)/(Ap_i,Ap_i)  <- inner products.
!            = (r/|r|,Ap_i/|Ap_i|)*|r|*|Ap_i| / |Ap_i|^2
!            = (r/|r|,Ap_i/|Ap_i|)*|r|/|Ap_i|
!          x = x + alpha*p_i
!          r = r - alpha*Ap_i
!
    length_r = gcr_length(r,ndim)
    length_p = gcr_length(Ap(:,i),ndim)
           a =       r/max(my_eps,length_r)
           b = Ap(:,i)/max(my_eps,length_p)
       alpha = gcr_dot_product(a,b,ndim)*( length_r/max(my_eps,length_p) )

        !---------------------------------------------------------------
        ! Check GCR stall for i > 1: Always perform the first iteration.
          if (i > 1) then

           !Best practice stall detection (Boris Diskin).
            stall_condition = maxval(alpha*Ap(:,i)) < gcr_rms(r,ndim)
 
          endif
        !---------------------------------------------------------------

      if ( .not.stall_condition ) then
        x(:) = x(:) + alpha* p(:,i)
        r(:) = r(:) - alpha*Ap(:,i)
      endif

!-----------------------------------------
!  Compute the convergence rate (residual reduction): rms(current r)/rms(r0).

      rkr0_gcr = gcr_rms(r,ndim) / rms_initial

!-----------------------------------------
!  Exit GCR if any of the following is true.

  !Exit if tolerance is met.
     if (rkr0_gcr < mu_gcr) then
      status_pc          = 0
      status_gcr         = 0
      exit gcr_projection
     endif

  !Exit if stalled.
     if ( stall_condition ) then
      status_pc          = 0
      status_gcr         = 2
      exit gcr_projection
     endif

  !Exit if max_projection is reached.
     if (i == max_projection_gcr) then
      status_pc          = 0
      status_gcr         = 2
      exit gcr_projection
     endif

!-----------------------------------------
!  Construct the next search direction.

!   Set the next search direction to be p_{i+1} = M^{-1}*r(i).
    call gcr_preconditioner(r,ndim, p(:,i+1),sweeps,istat_preconditioner)
    actual_sweeps = actual_sweeps + sweeps !Accumulate sweeps. 

    if (istat_preconditioner == 100) then
     status_pc          = 2
     status_gcr         = 0
     exit gcr_projection
    endif

!   Compute Ap
    call gcr_compute_Ap(p(:,i+1),ndim, Ap(:,i+1))

  !---------------------------------------------------------
  ! Orthogonalize p_{i+1} against all previous vectors: p_1, p_2, ..., p_{i}:
  !  p_{i+1} = A_approx^{-1}*r + sum_{k=0,i} beta*p_{k}
  !     beta = -(Ap_{i+1},Ap_{k})/(Ap_{k},Ap_{k})

    do k = 1, i

      length_p = gcr_length(Ap(:,k),ndim)
             a = Ap(:,i+1)/max(my_eps,length_p)
             b = Ap(:,k)  /max(my_eps,length_p)
          beta = -gcr_dot_product(a,b,ndim)
     Ap(:,i+1) = Ap(:,i+1) + Ap(:,k)*beta    !<- This was missing in Version 2.
      p(:,i+1) =  p(:,i+1) +  p(:,k)*beta

    end do
  !---------------------------------------------------------

  end do gcr_projection
!---------------------------------------------------------------------
!---------------------------------------------------------------------
! End of GCR Projection
!---------------------------------------------------------------------
!---------------------------------------------------------------------

!------------------------------------------------------------------
! Store the solution x(:) in your code.

  call gcr_store_x_in_your_array(x,ndim)

!------------------------------------------------------------------
! Deallocate GCR arrays

  deallocate( x, r, r0, a, b, p, Ap )

 end subroutine gcr_solver
!********************************************************************************

!********************************************************************************
! Assign ndim, which is the total number of discrete unknowns.
! For example, if you have a cell-centered code for 3D Navier-Stokes equations,
! ndim = (number of cells) x 5(equatins) = ncells*5.
!********************************************************************************
 subroutine gcr_dimension(ndim)

 implicit none

 integer, intent(out) :: ndim

 !Compute the total number of discrete unknowns in your code, and return it to GCR.

  call your_dimension(ndim)

 end subroutine gcr_dimension
!********************************************************************************

!********************************************************************************
! GCR works with a global 1D array of residuals. So, please store your RHS
! in a 1D array, r(:), if not in 1D array.
!
! Note: If you solve dRes/dq*dq = -Res, then the rhs is -Res.
!********************************************************************************
 subroutine gcr_rhs_vector(r,ndim)

 implicit none

 integer                  , intent( in) :: ndim  !dimension of r
 real(dp), dimension(ndim), intent(out) :: r

 !Store your rhs in the 1D array r(:), and return it to GCR.

  call your_rhs_vector(r,ndim)

 end subroutine gcr_rhs_vector
!********************************************************************************

!********************************************************************************
! Perform preconditioning: compute p = M^{-1}*r.
!
! The only important output is p = M^{-1}*r.
!
! 'sweeps' may be 0 if the preconditioner is not based on relaxations.
! 'istat' may always be 0 if failure is not defined for a preconditioner.
!
! If your preconditioner consists of
!
! (1)Approximate Jacobian, M.
! (2)Approximate inversion of M by a relaxation scheme (e.g., Gauss Seidel).
!
! You can return p and the residual reduction 'roc', which is the
! ratio of the final linear residual to the initial one for M.p=r,
! and also the number of relaxations 'sweeps'. In this case, istat can be nonzero
! to indicate that the relaxation diverged.
!
! If you use iLU-type preconditioner, just compute p and return sweeps=0, and 
! istat = 0.
!
!********************************************************************************
 subroutine gcr_preconditioner(r,ndim, p, sweeps,istat)

 implicit none

 integer                  , intent( in) :: ndim  !dimension of r and p
 real(dp), dimension(ndim), intent( in) :: r     !residual vector
 real(dp), dimension(ndim), intent(out) :: p     !M^{-1}.r
 integer ,                  intent(out) :: istat !status integer used in HANIM
 integer ,                  intent(out) :: sweeps

!Local
 real(dp) :: roc   !r(final)/r(0)

! Your preconditioner, which computes p as p = M^{-1}.r.

  call your_preconditioner(r,ndim, p,roc,sweeps)

! Status is defined as follows.

  if (    roc > 100.0_dp) then !<- This is bad.
   istat = 100
  elseif (roc > 10.0_dp) then  !<- This is acceptable.
   istat = 10
  elseif (roc > 1.0_dp) then   !<- This is acceptable.
   istat = 1
  else                         !<- This is acceptable.
   istat = 0
  endif

 end subroutine gcr_preconditioner
!********************************************************************************

!********************************************************************************
! Compute Ap.
!
! (1)If you have A, then perform a matrix-vector multiplicaiton.
! (2)If you don't, then Frechet: Ap = dRes/dU*p = [Res(u+epsilon*p)-Res(u)]/eps.
!
!********************************************************************************
 subroutine gcr_compute_Ap(p,ndim, Ap)

 implicit none

 integer                  , intent( in) :: ndim
 real(dp), dimension(ndim), intent( in) :: p
 real(dp), dimension(ndim), intent(out) :: Ap

 !Compute Ap and return it to GCR.

  call your_compute_Ap(p,ndim, Ap)

 end subroutine gcr_compute_Ap
!********************************************************************************

!********************************************************************************
! Copy the GCR solution x to your solution data.
!********************************************************************************
 subroutine gcr_store_x_in_your_array(x,ndim)

 implicit none

 integer                  , intent(in) :: ndim  !dimension of x
 real(dp), dimension(ndim), intent(in) :: x

 !Given a GCR solution x, copy it to the solution array in your code.

  call your_store_x_in_your_array(x,ndim)

 !Note: Do this in your code. No need to return anything to GCR.

 end subroutine gcr_store_x_in_your_array
!********************************************************************************

!********************************************************************************
!* This function returns the dot (or inner) product of two vectors.
!********************************************************************************
 function gcr_dot_product(a,b,n) result(dot_prod)

 implicit none

!Input
 integer               , intent(in) :: n
 real(dp), dimension(n), intent(in) :: a, b

!Output
 real(dp)                           :: dot_prod

! Call your subroutine to compute the rms of the vector of dimension n.

  call your_dot_product(a,b,n, dot_prod)

! which should perform the following:
!
!   dot_prod = zero
!  do i = 1, n
!   dot_prod = dot_prod + a(i)*b(i)
!  end do

 end function gcr_dot_product
!********************************************************************************

!********************************************************************************
!* This function returns the root-mean-square of a vector. 
!********************************************************************************
 function gcr_rms(vec,n) result(rms)

 implicit none

!Input
 integer               , intent(in) :: n
 real(dp), dimension(n), intent(in) :: vec

!Output
 real(dp)                           :: rms

! Call your subroutine to compute the rms of the vector of dimension n.

  call your_rms(vec,n, rms)

! which should perform the following:
!
!   rms = zero
!  do i = 1, n
!   rms = rms + vec(i)**2
!  end do
!   rms = sqrt( rms / real(n,dp) )

 end function gcr_rms
!********************************************************************************

!********************************************************************************
!* This function returns the length of a vector.
!********************************************************************************
 function gcr_length(vec,n) result(length)

 implicit none

!Input
 integer               , intent(in) :: n
 real(dp), dimension(n), intent(in) :: vec
!Output
 real(dp)                           :: length

! Call your subroutine to compute the length of the vector of dimension n.

  call your_length(vec,n, length)

! which should perform the following:
!
!   length = zero
!  do i = 1, n
!   length = length + vec(i)**2
!  end do
!   length = sqrt( length )

 end function gcr_length
!********************************************************************************

!********************************************************************************
!********************************************************************************
!********************************************************************************
! End of General GCR subroutines.
!********************************************************************************
!********************************************************************************
!********************************************************************************


 end module gcr_module
