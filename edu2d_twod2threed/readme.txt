# In each example directory, compile the program located in the directory above as

gfortran -O2 ../edu2d_twod2threed_v12.f90

#----------------------------------------------------------------------
# For binary .ugrid file, endianness will be automatically detected, but if you wish,
# you can specify endianness at compilation: e.g., as follows:
#  gfortran -O2 -fconvert=big-endian -o edu2d_twod2threed  ../edu2d_twod2threed_v12.f90
#     ifort -O2 -convert big_endian  -o edu2d_twod2threed  ../edu2d_twod2threed_v12.f90
#----------------------------------------------------------------------

# Now, run the code by typing 

./a.out


