!********************************************************************************
!* Kahan sum example program
!*
!*  --- kahan_sum_example
!*
!* Kahan's summation is an effective way of avoiding round-off errors
!* in computing the sum of finite precision floating point numbers,
!* Details can be found in the following webpage:
!*
!*   https://en.wikipedia.org/wiki/Kahan_summation_algorithm
!*
!*
!* This is Version 1 (December 26, 2017).
!*
!* This F90 code is written and made available for an educational purpose.
!* This file may be updated in future.
!*
!* Katate Masatsuka, http://www.cfdbooks.com
!********************************************************************************

program kahan_sum_example

implicit none

  integer , parameter :: pc = selected_real_kind(5)  ! Single precision
! integer , parameter :: pc = selected_real_kind(15) ! Double precision

!Some constants
 integer, parameter     ::     n = 10**8  != 100,000,000.
 real(pc)               ::  zero = 0.0_pc
 real(pc)               ::   one = 1.0_pc

 real(pc), dimension(n) :: dp
 real(pc)               :: dpc, sum, kahan_sum, temp, c, t
 integer                :: i

!-----------------------------------------------------------------
! We compute sum = 1.0 + sum_{i=1,n} dp(i).
! For simplicity, use a constant dp: dp = dpc, where

  dpc = 1.234567891234567e-02_pc
  dp  = dpc

! Note: This is a simplified example. In this case, the correct sum
!       should be 1+dpc*n, which we know because dp is a vecror of
!       the same constant value: dp = [dpc,dpc,...,dpc]. The correct sum
!       is used here to show how accurate the Kahan summation is.
!
!       In general, if dp(i) is different for each i, then we must
!       add up 1 and dp(i) over i=1,n as below; this is where the round-off
!       error can cause a loss of precision and Kahan's technique helps
!       keep the precision.

!-----------------------------------------------------------------
! (1)Simple summation

    sum = one

   do i = 1, n
    sum = sum + dp(i)
   end do

!-----------------------------------------------------------------
! (2)Summation by Kahan's technique

  kahan_sum =  one
          c = zero

  do i = 1, n
      temp = dp(i) - c              !Correction: Lost digits from previous is added.
      t    = kahan_sum + temp       !Here, if kahan_sum >> temp, then temp digits will be lost...
      c    = (t - kahan_sum) - temp !This is the lost part, keep it and use as correction in the next round.
      kahan_sum  = t                !Digits will be lost at i=n, but only at i=n.
  end do

!-----------------------------------------------------------------
! Compare the results with the correct sum.

 write(*,*)
 write(*,*) "            sum = ", sum
 write(*,*) "      Kahan sum = ", kahan_sum
 write(*,*) "    Correct sum = ", one + dpc*real(n,pc)
 write(*,*)


end program kahan_sum_example

