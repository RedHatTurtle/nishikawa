 close('all')
 clear

% Load the data

 dpts  = load('fort.100');
 spln  = load('fort.200');

% Plot the data

 figure(1)
 hold on
 box
 grid on

 % Discrerte data and spline

 plot( dpts(:,1), dpts(:,2), 'ko' ) % Discrete data
 plot( spln(:,1), spln(:,2), 'k-' )  % Spline

   % Flip the data to create a lower surface (in red)
 % - So we have a symmetric airfoil.
 plot( dpts(:,1), -dpts(:,2), 'ro' ) % Discrete data
 plot( spln(:,1), -spln(:,2), 'r-' )  % Spline

axis('equal')
axis([0, 1])

h = legend('Input data','Spline curve','Input data (fliped)','Spline data (flipped)'); 
legend('boxoff')
set(h,'FontSize',18,'Interpreter','latex')

 
