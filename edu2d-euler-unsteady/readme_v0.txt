######################################################################
# Running EDU2D-Euler-Unsteady for a shock diffraction problem.
#
# Follow the steps to run the shock diffraction test case.
# (Or just run this script by typing "source readme.txt".)
#
# NOTE: The following is for gfortran. If you use other compiler,
#       replace "gfortran" by your compiler (ifort, g95, etc).
#
#
# Katate Masatsuka, July 2015 (http://www.cfdbooks.com)
#####################################################################

#####################################################################
# 1. Compile the grid generation code.
#####################################################################

gfortran -O2 -o twod_rectangular_grid twod_rectangular_grid_v0.f90

#####################################################################
# 2. Run and generate grids (quads and triangles).
#    Dimensions are defined in the program: 401x401 grid.
#    It will generate the following files:
#     quad_grid_tecplot.dat - for viewing the quad grid
#     tria_grid_tecplot.dat - for viewing the tria grid
#     quad.grid             - for running EDU2D-Euler-Unsteady
#     tria.grid             - for running EDU2D-Euler-Unsteady
#     project.bcmap         - for running EDU2D-Euler-Unsteady
#####################################################################

./twod_rectangular_grid

#####################################################################
# 3. Compile EDU2D-Euler-Unsteady
#####################################################################

gfortran -O2 -c edu2d_euler_unsteady_basic_package_v0.f90
gfortran -O2 -c edu2d_euler_unsteady_solver_v0.f90
gfortran -O2 -c edu2d_euler_unsteady_main_v0.f90
gfortran -O2 -o edu2d_euler_unsteady edu2d_euler_unsteady_basic_package_v0.o edu2d_euler_unsteady_solver_v0.o edu2d_euler_unsteady_main_v0.o

# Or just type 'make', which uses Makefile to compile.

#####################################################################
# 4. Rename the quad grid file
#    (NOTE: EDU2D-Euler-Unsteady reads project.grid and project.bcmap)
#####################################################################

cp quad.grid   project.grid

#####################################################################
# 5. Run EDU2D-Euler-Unsteady to compute the solution on the quad grid 
#    (NOTE: All input parametetrs are specified inside the program.)
#    It will stop at t = 0.18 (16503 steps). It takes a while.
#    Go get a cup of coffee, and relax.
#
# See 'project_quad_screen_out.txt', which is what you should see on the screen.
#####################################################################

./edu2d_euler_unsteady

#####################################################################
# 6. Save the output file
#    (NOTE: The file 'project_tecplot.dat' will be overwritten by a
#           subsequent run.)
#####################################################################

cp project_tecplot.dat project_quad_result_tecplot.dat

# And open it with Tecplot to view the solution.

#####################################################################
# Move on to Step 7 if you want to run it also on the triangular grid.
#
# 7. Rename the triangular grid file
#    (NOTE: bcmap file is the same for quad and triangular grids)
#####################################################################

cp tria.grid project.grid

#####################################################################
# 8. Run EDU2D-Euler-Unsteady to compute the solution on the triangular grid
#    It will stop at t = 0.18 (2214 steps).
#
# See 'project_tria_screen_out.txt', which is what you should see on the screen.
#####################################################################

./edu2d_euler_unsteady

#####################################################################
# 9. Save the output file if you wish.
#####################################################################

cp project_tecplot.dat project_tria_result_tecplot.dat

# And open it with Tecplot to view the solution.
