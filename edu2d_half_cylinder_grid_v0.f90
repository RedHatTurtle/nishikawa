!********************************************************************************
! This program generates 2D quad and triangular grids for a half-cylinder domain,
! typical in a hypersomnic cylinder case.
!
!  y ^
!    |
!    ---> x
!
!                  .|
!               .   |
!             .     | <- Outflow upper
!    Outer  .       |
!          .        |
!          .       (<- Inner boundary (half cylinder)
!          .        |
!           .       |
!             .     | <- Outflow lower
!               .   |
!                  .|
!
!
!
! Inuput: 
!
!    thetamin !Location of upper outflow boundary
!    thetamax !Location of lower outflow boundary
!    rmin     !Radius of the inner boundary 
!    dc       !Center location of the outer boundary circle.
!    Rc       !Radius of the outer boundary circle.
!    sf       !Stretching factor (towards the inner boundary)
!    nx       !Number of nodes along the inner boundary
!    ny       !Number of nodes in the radial direction.
!
!        (NOTE: All input parameters are defined inside the program.)
!
! Output:
!
!            tria_grid_tec.dat = tecplot file of the triangular grid
!            quad_grid_tec.dat = tecplot file of the quadrilateral grid
!                    tria.grid = triangular-grid file for EDU2D-Euler solver
!                    quad.grid = quadrilateral-grid file for EDU2D-Euler solver
!                project.bcmap = file that contains boundary condition info
!                    tria.su2  = triangular-grid file for SU2
!                    quad.su2  = quadrilateral-grid file for SU2
!                    tria.vtk  = triangular-grid file in .vtk.
!                    quad.vtk  = quadrilateral-grid file in .vtk.
!    tria_tec_b.dat_boundary.k.dat, k=1,4 : boundary grid Tecplot data file.
!    quad_tec_b.dat_boundary.k.dat, k=1,4 : boundary grid Tecplot data file.
!
!
!
!        written by Dr. Katate Masatsuka (info[at]cfdbooks.com),
!
! the author of useful CFD books, "I do like CFD" (http://www.cfdbooks.com).
!
!
! This is Version 0 (July 2019).
!
! This F90 code is written and made available for an educational purpose as well
! as for generating grids for the EDU2D-Euler code.
!
! This file may be updated in future.
!
! Katate Masatsuka, July 2019. http://www.cfdbooks.com.
!********************************************************************************
 program edu2d_half_cylinder_grid

 implicit none

!Local parameters
  integer, parameter :: sp = kind(1.0)
  integer, parameter :: dp = selected_real_kind(2*precision(1.0_sp))
  real(dp) :: zero=0.0_dp, one=1.0_dp, two=2.0_dp, pi=3.141592653589793238_dp

!Input  - domain size and grid dimensions
 real(dp) :: thetamin !Location of upper outflow boundary
 real(dp) :: thetamax !Location of lower outflow boundary
 real(dp) :: rmin     !Radius of the inner boundary 
 real(dp) :: dc       !Center location of the outer boundary circle.
 real(dp) :: Rc       !Radius of the outer boundary circle.
 real(dp) :: sf       !Stretching factor (towards the inner boundary)
 integer  :: nx       !Number of nodes along the inner boundary
 integer  :: ny       !Number of nodes in the radial direction.

!Output - grid files
 character(80) :: filename_bcmap
 character(80) :: filename_grid
 character(80) :: filename_su2
 character(80) :: filename_vtk
 character(80) :: filename_tec
 character(80) :: filename_tec_b

!Local variables
 real(dp), dimension(:,:), allocatable :: xs, ys
 integer                               :: nnodes, ntria, nquad, inode
 integer , dimension(:,:), allocatable :: tria, quad
 real(dp), dimension(:)  , allocatable :: x , y

 integer  :: i, j
 real(dp) :: s, r, theta, rmax, dtheta, dr
 real(dp) :: a, b, c, sol1, sol2

 !- Boundary data requied by a solver.
 !   These are needed for .grid and .su2 files.
 integer                                :: ib, ic
 integer                                :: nb      !# of boundaries
 integer      , dimension(:  ), pointer :: nbnodes !# of nodes in each boundary
 integer      , dimension(:,:), pointer :: bnode   !List of boundary nodes
 character(80), dimension(:  ), pointer :: bnames  !Boundary names

!------------------------------------------------------------------
! Input parameters
!------------------------------------------------------------------

 ! Grid size

       nx = 17  !# of nodes along the inner cylinder.
       ny = 17  !# of nodes in the radial direction.

 ! Outflow location (top=thetamin and bottom=thetamax).

 thetamin = pi*1.0_dp/2.0_dp
 thetamax = pi*3.0_dp/2.0_dp

 !Inner boudnary: circle centered at the origin and the radius rmin.

     rmin = 0.5_dp

 !Outer boudnary: circle centered at (-dc,0) and the radius Rc.

       dc = 2.0_dp
       Rc = 3.0_dp

 !Stretching factor

       sf = 1.3_dp

!------------------------------------------------------------------
! End of Input parameters
!------------------------------------------------------------------

!  Allocate arrays.
   allocate( xs(nx+1,ny+1), ys(nx+1,ny+1) )

!--------------------------------------------------------------------------------
! Generate a Uniform 2D grid, using (i,j) data: go up in y-direction!

   write(*,*) "Generating structured data..."

   dtheta = (thetamax-thetamin)/real(nx-1,dp)

 do i = 1, nx

    theta = thetamax - dtheta*real(i-1,dp)

       a = one
       b = -two*dc*cos(theta)
       c = dc**2 - Rc**2
      sol1 = (-b+sqrt(b**2-4.0_dp*a*c))/(two*a)
      sol2 = (-b-sqrt(b**2-4.0_dp*a*c))/(two*a)
      rmax = max(sol1,sol2)
       dr = (rmax-rmin)/real(ny-1,dp)

  do j = 1, ny
   r = rmin + dr*real(j-1,dp)
   s = (r-rmin)/(rmax-rmin)
   s = (one-exp(sf*s))/(one-exp(sf))
   r = rmin + s*(rmax-rmin)
    xs(i,j) = r*cos(theta)
    ys(i,j) = r*sin(theta)
  end do

 end do  

!--------------------------------------------------------------------------------
! Generate unstructured data:

   write(*,*) "Generating unstructured data..."
   nnodes = nx*ny
   allocate(x(nnodes),y(nnodes))

  ! Node data

   do j = 1, ny
    do i = 1, nx
     inode = i + (j-1)*nx
       x(inode) =   xs(i,j)
       y(inode) =   ys(i,j)
    end do
   end do

!--------------------------------------------------------------------------------
! Boundary information

  !Node data
   nnodes = nx*ny

  !Boundary data
   nb = 4
   allocate( bnode( max(ny,nx), nb) )
   allocate( nbnodes(nb) )
   allocate(  bnames(nb) )

  !(1) Inner boundary: j=1

    ib = 1
    bnames( ib) = "inner"
    nbnodes(ib) = nx
              j = 1
             ic = 0

    do i = 1, nx
            inode = i + (j-1)*nx
               ic = ic + 1
     bnode(ic,ib) = inode
    end do

  !(2) Upper outflow boundary: i=nx

    ib = 2
    bnames( ib) = "outflow_upper"
    nbnodes(ib) = ny
              i = nx
             ic = 0

    do j = 1, ny
            inode = i + (j-1)*nx
               ic = ic + 1
     bnode(ic,ib) = inode
    end do

  !(3) Outer boundary: j=ny

    ib = 3
    bnames( ib) = "outer"
    nbnodes(ib) = nx
              j = ny
             ic = 0

    do i = nx, 1, -1
            inode = i + (j-1)*nx
               ic = ic + 1
     bnode(ic,ib) = inode
    end do

  !(4) Lower outflow boundary: i=1

    ib = 4
    bnames( ib) = "outflow_lower"
    nbnodes(ib) = ny
              i = 1
             ic = 0

    do j = ny, 1, -1
            inode = i + (j-1)*nx
               ic = ic + 1
     bnode(ic,ib) = inode
    end do

   filename_bcmap = "quad.bcmap"
   call write_bcmap_file(filename_bcmap, nb, bnames)

   filename_bcmap = "tria.bcmap"
   call write_bcmap_file(filename_bcmap, nb, bnames)

!--------------------------------------------------------------------------------
! Generate unstructured data:

  allocate(tria(2*(nx-1)*(ny-1),3), quad((nx-1)*(ny-1),4))

 !-------------------------------------------------------------------------
 ! Triangular grid

   write(*,*) "Generating triangular grid..."
   call generate_tri_grid(tria, quad, ntria, nquad, nx, ny)

   nquad = 0

   filename_grid  = "tria.grid"
   call write_grid_file(filename_grid,nnodes,tria,ntria,quad,nquad,x,y, &
                                                       nb,nbnodes,bnode )
   filename_su2   = "tria.su2"
   call write_su2_file(filename_su2,nnodes,tria,ntria,quad,nquad,x,y, &
                                              nb,nbnodes,bnode,bnames )
   filename_vtk   = "tria.vtk"
   call write_vtk_file(filename_vtk, nnodes,x,y, ntria,tria, &
                                                 nquad,quad  )
   filename_tec   = "tria_tec.dat"
   call write_tecplot_file(filename_tec, nnodes,x,y, ntria,tria, &
                                                     nquad,quad  )
   filename_tec_b = "tria_tec_b.dat"
   call write_tecplot_boundary_file(filename_tec_b,x,y,nb,nbnodes,bnode)

 !-------------------------------------------------------------------------
 !  Quadrilateral grid

   write(*,*) "Generating quad grid..."
   call generate_quad_grid(tria, quad, ntria, nquad, nx, ny)

   ntria = 0

   filename_grid  = "quad.grid"
   call write_grid_file(filename_grid,nnodes,tria,ntria,quad,nquad,x,y, &
                                                       nb,nbnodes,bnode )
   filename_su2   = "quad.su2"
   call write_su2_file(filename_su2,nnodes,tria,ntria,quad,nquad,x,y, &
                                              nb,nbnodes,bnode,bnames )
   filename_vtk   = "quad.vtk"
   call write_vtk_file(filename_vtk, nnodes,x,y, ntria,tria, &
                                                 nquad,quad  )
   filename_tec   = "quad_tec.dat"
   call write_tecplot_file(filename_tec, nnodes,x,y, ntria,tria, &
                                                     nquad,quad  )
   filename_tec_b = "quad_tec_b.dat"
   call write_tecplot_boundary_file(filename_tec_b,x,y,nb,nbnodes,bnode)

!--------------------------------------------------------------------------------

  write(*,*) " nx=",nx, " ny=", ny
  write(*,*) " nx*ny=",nx*ny
  write(*,*) " nnodes=",nnodes
  write(*,*) "Successfully completed. Stop."

 stop

 contains

!********************************************************************************
! Generate a triangular grid
!********************************************************************************
 subroutine generate_tri_grid(tri, quad, ntri, nquad, nx, ny)
 implicit none
 integer,                 intent( in) ::   nx, ny 
 integer,                 intent(out) :: ntri, nquad
 integer, dimension(:,:), intent(out) ::  tri, quad
!Local variables
 integer  :: i, j, inode, i1, i2, i3, i4
 real(dp) :: rn, rn2

! No quads
 nquad = 0
  quad = 0

! Trianguler grid with right-up diagonals (i.e., / ).

 ntri = 0

 do j = 1, ny-1
  do i = 1, nx-1

   inode = i + (j-1)*nx
      i1 = inode
      i2 = inode + 1
      i3 = inode + nx + 1
      i4 = inode + nx

   call random_number(rn)
   rn2 = 1.0d0 !2.0d0*rn-1.0d0

   if (i < (nx-1)/2+1 ) then
!! /
           ntri = ntri + 1
    tri(ntri,1) = i1
    tri(ntri,2) = i2
    tri(ntri,3) = i3

           ntri = ntri + 1
    tri(ntri,1) = i1
    tri(ntri,2) = i3
    tri(ntri,3) = i4

   else

!! \
           ntri = ntri + 1
    tri(ntri,1) = i1
    tri(ntri,2) = i2
    tri(ntri,3) = i4

           ntri = ntri + 1
    tri(ntri,1) = i2
    tri(ntri,2) = i3
    tri(ntri,3) = i4

   endif

  end do
 end do

 end subroutine generate_tri_grid
!********************************************************************************

!********************************************************************************
! Generate a quadrialteral grid
!********************************************************************************
 subroutine generate_quad_grid(tri, quad, ntri, nquad, nx, ny)
 implicit none
 integer,                 intent( in) ::   nx, ny 
 integer,                 intent(out) :: ntri, nquad
 integer, dimension(:,:), intent(out) ::  tri, quad
!Local variables
 integer :: i, j, inode, i1, i2, i3, i4

! No triangles
  ntri = 0
   tri = 0

! Quadrilateral grid

 nquad = 0

 do j = 1, ny-1
  do i = 1, nx-1

   inode = i + (j-1)*nx
      i1 = inode
      i2 = inode + 1
      i3 = inode + nx + 1
      i4 = inode + nx

!  Order the quad counterclockwise:
          nquad = nquad + 1
   quad(nquad,1) = i1
   quad(nquad,2) = i2
   quad(nquad,3) = i3
   quad(nquad,4) = i4

  end do
 end do

 end subroutine generate_quad_grid
!********************************************************************************

!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------


!*******************************************************************************
! This subroutine writes a Tecplot file for the grid.
!*******************************************************************************
 subroutine write_tecplot_file(filename, nnodes,x,y, ntria,tria, nquad,quad  )

  implicit none

  character(80),                 intent(in) :: filename
  integer      ,                 intent(in) :: nnodes, ntria, nquad
  real(dp)     , dimension(:  ), intent(in) :: x, y
  integer      , dimension(:,:), intent(in) :: tria
  integer      , dimension(:,:), intent(in) :: quad

!Local variables
  integer :: i, os

 write(*,*)
 write(*,*) ' Writing a Tecplot file = ', trim(filename)
 write(*,*)

  open(unit=9, file=filename, status="unknown", iostat=os)

  write(9,*) 'TITLE = "Grid"'
  write(9,*) 'VARIABLES = "x","y"'
!---------------------------------------------------------------------------
! zone

   write(9,*) 'zone t=', '"grid"'
   write(9,*)'  n=', nnodes,',e=', ntria+nquad,' , et=quadrilateral, f=fepoint'

   do i = 1, nnodes
     write(9,'(2es25.15)') x(i), y(i)
   end do

  if (ntria > 0) then
   do i = 1, ntria
    write(9,'(4i10)') tria(i,1), tria(i,2), tria(i,3), tria(i,3)
   end do
  endif

  if (nquad > 0) then
   do i = 1, nquad
    write(9,'(4i10)') quad(i,1), quad(i,2), quad(i,3), quad(i,4)
   end do
  endif

!---------------------------------------------------------------------------

 close(9)

 end subroutine write_tecplot_file
!********************************************************************************


!********************************************************************************
! This subroutine writes boundary grid files.
!********************************************************************************
 subroutine write_tecplot_boundary_file(filename,x,y,nb,nbnodes,bnode )

 implicit none

 character(80),                 intent(in) :: filename
 real(dp)     , dimension(:)  , intent(in) :: x, y
 integer                      , intent(in) :: nb
 integer      , dimension(:  ), intent(in) :: nbnodes
 integer      , dimension(:,:), intent(in) :: bnode

!Local variables
 character(80) :: filename_loc
 character(80) :: bid
 integer       :: ib, os

  do ib = 1, nb

     write( bid  , '(i0)' ) ib
     filename_loc = trim(filename) // trim("_boundary") // trim(".") // trim(bid) // '.dat'

   write(*,*)
   write(*,*) ' Writing a Tecplot file for boundary = ', ib , trim(filename_loc)
   write(*,*)

   open(unit=10, file=filename_loc, status="unknown", iostat=os)

    write(10,*) 'variables = "x", "y"'
    write(10,*) 'ZONE t="', trim("Boundary.") // trim(bid) ,'" I =', nbnodes(ib), ', DATAPACKING=POINT'

    do i = 1, nbnodes(ib)
     write(10,*) x( bnode(i,ib) ), y( bnode(i,ib) )
    end do

   close(10)

  end do

 end subroutine write_tecplot_boundary_file

!********************************************************************************
! This subroutine writes a grid file to be read by a solver.
! NOTE: Unlike the tecplot file, this files contains boundary info.
!********************************************************************************
 subroutine write_grid_file(filename,nnodes,tria,ntria,quad,nquad,x,y, &
                                                      nb,nbnodes,bnode )

 implicit none

 character(80),                 intent(in) :: filename
 integer                      , intent(in) :: nnodes, ntria, nquad
 integer      , dimension(:,:), intent(in) :: tria, quad
 real(dp)     , dimension(:)  , intent(in) :: x, y
 integer                      , intent(in) :: nb
 integer      , dimension(:  ), intent(in) :: nbnodes
 integer      , dimension(:,:), intent(in) :: bnode

!Local variables
 integer  :: os, i, j

   write(*,*)
   write(*,*) " Writing .grid file: ", trim(filename)

!--------------------------------------------------------------------------------
 open(unit=1, file=filename, status="unknown", iostat=os)

!--------------------------------------------------------------------------------
! Grid size: # of nodes, # of triangles, # of quadrilaterals
  write(1,*) nnodes, ntria, nquad

!--------------------------------------------------------------------------------
! Node data
  do i = 1, nnodes
   write(1,*) x(i), y(i)
  end do

!--------------------------------------------------------------------------------
! Triangle connectivity
  if (ntria > 0) then
   do i = 1, ntria
    write(1,*) tria(i,1), tria(i,2), tria(i,3)
   end do
  endif

  if (nquad > 0) then
   do i = 1, nquad
    write(1,*) quad(i,1), quad(i,2), quad(i,3), quad(i,4)
   end do
  endif

!--------------------------------------------------------------------------------
! Boundary data:
!
! The number of boundary segments
  write(1,*) nb

! The number of nodes in each segment:
  do i = 1, nb
   write(1,*) nbnodes(i)
  end do

  write(1,*)

! List of boundary nodes
  do i = 1, nb

   write(*,*) " write_grid_file: nbnodes(i) = ", nbnodes(i), i

   do j = 1, nbnodes(i)
    write(1,*) bnode(j,i)
   end do
    write(1,*)

  end do

!--------------------------------------------------------------------------------
 close(1)

 end subroutine write_grid_file
!********************************************************************************



!*******************************************************************************
! This subroutine writes a .bcmap for the grid.
!*******************************************************************************
 subroutine write_bcmap_file(filename, nb, bnames)

  implicit none

 character(80),                 intent(in) :: filename
 integer                      , intent(in) :: nb
 character(80),   dimension(:), intent(in) :: bnames

!Local variables
  integer :: i, os

 write(*,*)
 write(*,*) ' Writing a .bcmap file = ', trim(filename)
 write(*,*)

  open(unit=9, file=filename, status="unknown", iostat=os)


  write(9,*) "      Boundary Part          Boundary Condition"

  do i = 1, nb
   write(9,'(i20,a28)') i, '"' // trim(bnames(i)) // '"'
  end do

  close(9)

 end subroutine write_bcmap_file
!********************************************************************************

!*******************************************************************************
! This subroutine writes a su2 grid file.
!
! Note: Nodes -> i = 0,1,2,...; Elements -> i = 0,1,2,...
!
!
!  Identifier:
!  Line 	 3
!  Triangle 	 5
!  Quadrilateral 9
!  Tetrahedral 	10
!  Hexahedral 	12
!  Prism 	13
!  Pyramid 	14
!
!
!*******************************************************************************
 subroutine write_su2_file(filename,nnodes,tria,ntria,quad,nquad,x,y, &
                                              nb,nbnodes,bnode,bnames )

 character(80),                 intent(in) :: filename
 integer                      , intent(in) :: nnodes, ntria, nquad
 integer      , dimension(:,:), intent(in) :: tria, quad
 real(dp)     , dimension(:)  , intent(in) :: x, y
 integer                      , intent(in) :: nb
 integer      , dimension(:  ), intent(in) :: nbnodes
 integer      , dimension(:,:), intent(in) :: bnode
 character(80), dimension(:)  , intent(in) :: bnames

!Local variables
 integer :: i, ib, j, os

   write(*,*)
   write(*,*) " Writing .su2 file: ", trim(filename)

  open(unit=7, file=filename, status="unknown", iostat=os)

  write(7,*) "%"
  write(7,*) "% Problem dimension"
  write(7,*) "%"
  write(7,5) 2
5 format('NDIME= ',i12)

   write(7,*) "%"
   write(7,*) "% Inner element connectivity"
   write(7,10) ntria + nquad
10 format('NELEM= ',i12)

 !-------------------------------------------------------------------------
 ! Elements

  if (ntria > 0) then
   do i = 1, ntria
    write(7,'(4i20)') 5, tria(i,1)-1, tria(i,2)-1, tria(i,3)-1
   end do
  endif

  if (nquad > 0) then
   do i = 1, nquad
    write(7,'(5i20)') 9, quad(i,1)-1, quad(i,2)-1, quad(i,3)-1, quad(i,4)-1
   end do
  endif

 !--------------------------------------------------------------------------
 ! Nodes

   write(7,*) "%"
   write(7,*) "% Node coordinates"
   write(7,*) "%"
   write(7,20) nnodes
20 format('NPOIN= ', i12)

  ! Nodes
    do i = 1, nnodes
     write(7,'(2es26.15)') x(i), y(i)
    end do

 !--------------------------------------------------------------------------
 ! Boundary

30  format('NMARK= ',i12)
40  format('MARKER_TAG= ',a)
50  format('MARKER_ELEMS= ', i12)

    write(7,*) "%"
    write(7,*) "% Boundary elements"
    write(7,*) "%"
    write(7,30) nb !# of boundary parts.

   do ib = 1, nb

   !-------------------------
   !Just to print on screen
    write(*,*)
    write(*,40) trim(bnames(ib)) !ib-th boundary-part name, e.g., "farfield".
    write(*,50) nbnodes(ib)-1    !# of boundary elements (edges)
   !-------------------------

    write(7,40) trim(bnames(ib)) !ib-th boundary-part name, e.g., "farfield".
    write(7,50) nbnodes(ib)-1    !# of boundary elements (edges)

    do j = 1, nbnodes(ib)-1
     write(7,'(3i20)') 3, bnode(j,ib)-1, bnode(j+1,ib)-1
    end do

   end do

  close(7)

 end subroutine write_su2_file
!********************************************************************************

!*******************************************************************************
! This subroutine writes a .vtk file for the grid whose name is defined by
! filename_vtk.
!
! Use Paraview to read .vtk and visualize it.  https://www.paraview.org
!
! Search in Google for 'vkt format' to learn .vtk file format.
!*******************************************************************************
 subroutine write_vtk_file(filename, nnodes,x,y, ntria,tria, nquad,quad  )

  implicit none

  character(80),                 intent(in) :: filename
  integer      ,                 intent(in) :: nnodes, ntria, nquad
  real(dp)     , dimension(:  ), intent(in) :: x, y
  integer      , dimension(:,:), intent(in) :: tria
  integer      , dimension(:,:), intent(in) :: quad

!Local variables
  integer :: i, j, os

 !------------------------------------------------------------------------------
 !------------------------------------------------------------------------------
 !------------------------------------------------------------------------------

  write(*,*)
  write(*,*) ' Writing .vtk file = ', trim(filename)
  write(*,*)

 !Open the output file.
  open(unit=8, file=filename, status="unknown", iostat=os)

!---------------------------------------------------------------------------
! Header information

  write(8,'(a)') '# vtk DataFile Version 3.0'
  write(8,'(a)') filename
  write(8,'(a)') 'ASCII'
  write(8,'(a)') 'DATASET UNSTRUCTURED_GRID'

!---------------------------------------------------------------------------
! Nodal information
!
! Note: These nodes i=1,nnodes are interpreted as i=0,nnodes-1 in .vtk file.
!       So, later below, the connectivity list for tria and quad will be
!       shifted by -1.

   write(8,*) 'POINTS ', nnodes, ' double'

   do j = 1, nnodes
    write(8,'(3es25.15)') x(j), y(j), zero
   end do

!---------------------------------------------------------------------------
! Cell information.

  !CELLS: # of total cells (tria+quad), total size of the cell list.

  write(8,'(a,i12,i12)') 'CELLS ',ntria+nquad, (3+1)*ntria + (4+1)*nquad

  ! Note: The latter is the number of integer values written below as data.
  !           4 for triangles (# of vertices + 3 vertices), and
  !           5 for quads     (# of vertices + 4 vertices).

  !---------------------------------
  ! 2.1 List of triangles (counterclockwise vertex ordering)

   if (ntria > 0) then
                         ! (# of vertices = 3), 3 vertices in counterclockwise
    do i = 1, ntria
     write(8,'(a,4i12)') '3', tria(i,1)-1, tria(i,2)-1, tria(i,3)-1
                         ! -1 since VTK reads the nodes as 0,1,2,3,..., not 1,2,3,..
    end do
   endif

  !---------------------------------
  ! 2.2 List of quads (counterclockwise vertex ordering)

   if (nquad > 0) then
                         ! (# of vertices = 4), 4 vertices in counterclockwise
    do i = 1, nquad
     write(8,'(a,4i12)') '4', quad(i,1)-1, quad(i,2)-1, quad(i,3)-1, quad(i,4)-1
                         ! -1 since VTK reads the nodes as 0,1,2,3,..., not 1,2,3,..
    end do
   endif

!---------------------------------------------------------------------------
! Cell type information.

                                   !# of all cells
  write(8,'(a,i11)') 'CELL_TYPES ', ntria+nquad

  !Triangle is classified as the cell type 5 in the .vtk format.

  if (ntria > 0) then
   do i = 1, ntria
    write(8,'(i3)') 5
   end do
  endif

  !Triangle is classified as the cell type 9 in the .vtk format.

  if (nquad > 0) then
   do i = 1, nquad
    write(8,'(i3)') 9
   end do
  endif

!--------------------------------------------------------------------------------
! NOTE: Commented out because there are no solution data. This part should be
!       uncommented if this is used in a solver or if solution data are available
!       and one would like to plot them.
!
! Field data (e.g., density, pressure, velocity)are added here for visualization.

!   write(8,*) 'POINT_DATA   ',nnodes

!  !            FIELD  dataName    # of arrays (variables to plot) <--This is a commnet.
!   write(8,*) 'FIELD  FlowField ', 4

!   write(8,*) 'Density   ',  1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,1)
!   end do

!   write(8,*) 'X-velocity ', 1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,2)
!   end do

!   write(8,*) 'Y-veloiity ', 1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,3)
!   end do

!   write(8,*) 'Pressure   ', 1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,4)
!   end do

!---------------------------------------------------------------------------

 !Close the output file. <--This is a commnet.
  close(8)


 end subroutine write_vtk_file
!********************************************************************************

 end program edu2d_half_cylinder_grid
