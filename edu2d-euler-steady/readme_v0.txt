######################################################################
# Running EDU2D-Euler-Steady for a flow over a bump.
#
# Follow the steps to run the bump test case.
# (Or just run this script by typing "source readme_v0.txt".)
#
# NOTE: The following is for gfortran. If you use other compiler,
#       replace "gfortran" by your compiler (ifort, g95, etc).
#
#
# Katate Masatsuka, July 2015 (http://www.cfdbooks.com)
#####################################################################

#####################################################################
# 1. Compile the grid generation code.
#####################################################################

gfortran -O2 -o twod_bump_irregular_grid twod_bump_irregular_grid_v0.f90

#####################################################################
# 2. Run and generate a grid
#    Dimensions are defined in the program: 40x320 grid.
#    It will generate the following files:
#     bump_tecplot.dat - for viewing the grid
#     bump.grid        - for running EDU2D-Euler-Steady code
#     bump.bcmap       - for running EDU2D-Euler-Steady code
#####################################################################

./twod_bump_irregular_grid

#####################################################################
# 3. Compile EDU2D-Euler-Steady
#####################################################################

gfortran -O2 -c edu2d_euler_steady_basic_package_v0.f90
gfortran -O2 -c edu2d_euler_jacobian_v1.f90
gfortran -O2 -c edu2d_euler_linear_solve_v0.f90
gfortran -O2 -c edu2d_euler_steady_solver_v0.f90
gfortran -O2 -c edu2d_euler_steady_main_v0.f90
gfortran -O2 -o edu2d_euler_steady edu2d_euler_steady_basic_package_v0.o edu2d_euler_jacobian_v1.o edu2d_euler_linear_solve_v0.o edu2d_euler_steady_solver_v0.o edu2d_euler_steady_main_v0.o 

#####################################################################
# 4. Run EDU2D-Euler-Steady to compute the solution.
#    (NOTE: All input parameters are specified inside the program.)
#    With the default parameters, it will converge to machine zero
#    at 336 iterations for the implicit scheme and 13,322 iterations
#    for the explicit scheme.
#
#    See 'bump_screen_out.txt', which is what you should see on the screen.
#####################################################################

./edu2d_euler_steady


#####################################################################
# Explore other parameters, or compare the results with the explicit
# scheme by modifying the input parameters in main_v0.f90. 
#####################################################################
