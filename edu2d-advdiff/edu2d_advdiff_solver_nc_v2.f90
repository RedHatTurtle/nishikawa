 module edu2d_advdiff_solver_nc
!********************************************************************************
! Educationally-Designed Unstructured 2D (EDU2D) Code
!
!
!    This module belongs to EDU2D-AdvDiff.
!
!
! This module contains a subroutine that computes a steady state solution or 
! advance the solution over a physical time step (by solving the residual equation):
!
!   edu2d_advdiff_solver_jfnk
!
! which is the only subroutine used outside this module.
!
!
! Contained subroutines/functions:
! -----------------------------------------------------------------------------
!
! - Main solver -
!
! edu2d_advdiff_solver_nc  : Main solver - Jacobian-Free Newton-Krylov with GCR and
!                            a defect-correction implicit preconditioner.
!
! compute_residual         : Compute the residual.
! compute_jacobian         : Compute residual Jacobian to be used in DC solver
! interface_flux           : Driver for a numerical flux at face (edge-midpoint)
! interface_jac            : Driver for flux Jacobians at face (edge-midpoint)
! upwind_flux              : Upwind flux for advection and hyperbolic diffusion
! get_right_state          : Set up the right state at a boundary node
! residual_norm            : Compute the residual norms
! compute_pseudo_time_step : Compute pseudo time step
! report_res_norm          : Print out the residuals in the screen
! update_solution_du       : Update the solution
!
! - Defect-Correction (DC) implicit solver -
!   Note: This is am implicit solver with 1st-order Jacobian (exact derivative of 1st-order scheme).
!   Note: This is used as a preconditioner for the Newton-Krylov solver.
!         Every Newton-Krylov solver requires a good preconditioner.
!         Without preconditioners, Newton-Krylov takes forever to converge...
!
! gs_sequential            : Perform Gauss-Seidel relaxation for a linear system
! gewp_solve               : Gauss elimination used to invert the diagonal blocks.
! And other related functions...
!
! - Jacobian-Free Newton-Krylov with GCR -
!   Note: This is basically a Newton solver with the exact Jacobian.
!         But the exact Jacobian is not computed nor stored: Jacobian-Free.
!         Linear system is solved (without the Jacobian) by GCR + preconditioner.
!
! jfnk_gcr_solver          : Jacobian-Free Newton-Krylov GCR solver
! preconditioner           : Call a perconditioner (gs_sequential)
! vector_u                 : Store the solution in a single vector
! vector2du                : Store the correction in the grid data type, node%du.
! vector_dt_term           : Store a pseudo time term in a single vector
! And other related functions...
!
!
! Note: The solver is Jacobian-Free, but a Jacobian is constructed for the
!       DC implicit solver (preconditioner).
!
! -----------------------------------------------------------------------------
!
!
!        written by Dr. Katate Masatsuka (info[at]cfdbooks.com),
!
! the author of useful CFD books, "I do like CFD" (http://www.cfdbooks.com).
!
! This is Version 0 (July 2015).
! This F90 code is written and made available for an educational purpose.
! This file may be updated in future.
!
!********************************************************************************

 private

 public :: edu2d_advdiff_solver_jfnk

 contains

!********************************************************************************
! EDU2D-AdvDiff Newton-Krylov Solver
!
!   - Newton-Krylov solver with GCR
!   - A defect-correction implicit solver employed as a variable preconditioner.
!
!   - 2nd-order node-centered edge-based finite-volume discretization
!   - Linear LSQ gradients
!   - Upwind flux for advection and upwind flux for hyperbolic diffusion system
!
! ------------------------------------------------------------------------------
!  Input:  node(:)%u - Initial solution
!
! Output:  node(:)%u - Solution to a system of residual equations, which can be
!                      a steady solution or a unsteady solution at the next
!                      physical time step.
! ------------------------------------------------------------------------------
!
!********************************************************************************
 subroutine edu2d_advdiff_solver_jfnk

 use edu2d_constants   , only : p2, one
 use edu2d_my_main_data, only : nq, max_iterations, i_iteration    , &
                                CFLp, CFLp1, CFLp2, CFLp_ramp_steps, &
                                tolerance, max_projection_gcr, gradient_type
 use edu2d_lsq_grad_nc , only : compute_gradient_nc

!Local variables
 real(p2), dimension(nq) :: res_norm1_initial
 real(p2), dimension(nq) :: res_norm1_pre

 real(p2), dimension(0:max_iterations,nq,3) :: res_norm !Residual norms(L1,L2,Linf)
 integer                                    :: sweeps_actual, gcr_actual, total_steps
 real(p2)                                   :: s, roc, CFLp_previous, exp_factor

 write(*,*) "   --------------------------------------------------------------------"
 write(*,*) "          Beginning of Jacobian-Free Newton-Krylov Solver "
 write(*,*)

!--------------------------------------------------------------------------------
! Iteration towards a pseudo steady state: tau -> infinity.
!--------------------------------------------------------------------------------

   i_iteration = 0

  iteration : do
 
!----------------------------------------------------------
!   Implicit: Exponential CFL ramping between CFLp1 and CFLp2 over ramp_steps

      CFLp_previous = CFLp
 
    if (i_iteration <= CFLp_ramp_steps) then
      exp_factor = 0.5_p2
        s = real(i_iteration, p2) / real(CFLp_ramp_steps, p2)
      CFLp = CFLp1 + (CFLp2-CFLp1)*( one-exp(-s*exp_factor) )/ ( one-exp(-exp_factor) )
     endif

!   Compute Res(u^n)
     call compute_residual

!   Compute residual norms.
    call residual_norm(res_norm(i_iteration,:,:))

!   Set the initial residual after the first iteration.
    if (i_iteration==1) then
      res_norm1_initial = res_norm(1,:,1)
      res_norm1_pre     = res_norm(1,:,1)
    endif

!   Display the residual norm from the previous iteration : i_iteration.
    if (i_iteration==0) write(*,'(a88)') "    u-equation   p-equation  q-equation"
    if (i_iteration>=0) then
     call report_res_norm(CFLp_previous,res_norm(i_iteration,:,:),res_norm1_pre, sweeps_actual,gcr_actual,roc)

     if (i_iteration==1) write(*,*)
     if (i_iteration==1) write(*,'(a)') "    Note: We take these residuals (after the 1st iteration)"
     if (i_iteration==1) write(*,'(a)') "          as the initial residual to measure the reduction."
     if (i_iteration==1) write(*,*)

     write(*,'(a86)') "   ----------------------------------------------------------------------------------"
    endif

!   Stop if the L1 residual norm drops below the specified tolerance or if we reach the max iteration.
!   Note: Machine zero residual level is very hard to define. Here, I just set it to be 1.0e-14.
!         It may be larger or smaller for different problems.
    if (i_iteration >= 1) then

     if (     maxval(res_norm(i_iteration,:,1)/res_norm1_initial(:)) < tolerance & !Tolerance met
         .or. maxval(res_norm(i_iteration,:,1)) < 1.0e-14                        & !Machine zero(?)
                                                                                 ) then
       total_steps = i_iteration
       write(*,*)
       write(*,'(a15,i10,a12,3es12.2,a10)') "    steps=", total_steps, &
                                       " L1(res)=",res_norm(i_iteration,:,1), " Converged"
       write(*,*)
       exit iteration
     endif

     if (i_iteration == max_iterations) exit iteration

    endif

!   Proceed to the next iteration
    res_norm1_pre = res_norm(i_iteration,:,1)
    i_iteration   = i_iteration + 1

!   Compute local pseudo time steps at nodes.
    call compute_pseudo_time_step(CFLp)

!   Construct the residual Jacobian matrix (based on 1st-order scheme: Defect Correction)
     call compute_jacobian

!   Compute du (Correction) by Defect-correction iteration or Newton-Krylov.

    !------------------------------------------------
    !(1)Defect correction: Relax the linear system by Gauss-Seidel to get du (Correction)
     if (max_projection_gcr == 0) then

      call gs_sequential(sweeps_actual,roc)

    !----------------------------------------------------
    !(2)GCR with defect-correction as a preconditioner
     elseif (max_projection_gcr > 0) then

      !For GCR, sweeps_actual=actual_projections, cr_gs=l1norm_ratio
       call jfnk_gcr_solver(gcr_actual,sweeps_actual,roc)

     endif
    !------------------------------------------------

!   Use the correction, du, to update the solution: u_new = u_old + du
     call update_solution_du

   total_steps = i_iteration

  end do iteration
!--------------------------------------------------------------------------------
! End of Iteration
!--------------------------------------------------------------------------------

  write(*,*)
  write(*,*)
  write(*,*) "             End of Jacobian-Free Newton-Krylov Solver "
  write(*,*) "   --------------------------------------------------------------------"

  call compute_gradient_nc(1,gradient_type)

!--------------------------------------------------------------------------------

  write(*,*)

 end subroutine edu2d_advdiff_solver_jfnk


!********************************************************************************
!* This subroutine computes the residual (RHS) at all nodes.
!*
!* Note: This is a second-order node-centered FV method.
!*
!* ------------------------------------------------------------------------------
!*  Input:  node(:)%u   - solution vector at nodes
!*
!* Output:  node(:)%res - residual vector at nodes
!* ------------------------------------------------------------------------------
!********************************************************************************
 subroutine compute_residual

 use edu2d_constants   , only : p2, zero, one, two, half, pi
 use edu2d_my_main_data, only : nnodes, node, nedges, edge, elm, nbound, bound, &
                                nu, gradient_type, scheme_type,                 &
                                problem_type, i_time_step, dtn, dtnm1
 use edu2d_lsq_grad_nc , only : compute_gradient_nc

 implicit none

!Local variables
 real(p2), dimension(3)   :: num_flux       !Numerical flux
 real(p2)                 :: mag_e12        !Magnitude of the edge vector
 real(p2)                 :: mag_n12        !Magnitude of the face-normal vector
 integer                  :: node1, node2   !Left and right nodes of each edge
 integer                  :: i, j, n1, n2, boundary_elm, i2, i3
 integer                  :: ix=1, iy=2, iu=1, ip=2, iq=3

 real(p2)                 :: nx, ny, ex, ey, wsn
 real(p2)                 :: Tr, Lr
 real(p2)                 :: temp

 real(p2), dimension(3)   :: bfluxR, bfluxL
 real(p2), dimension(3)   :: ub
 real(p2), dimension(3,3) :: dummy3x3

 logical                  :: interior_edge
 logical                  :: boundary_edge

 real(p2)                 :: cnp1, cnm1, cn

  interior_edge = .true.
  boundary_edge = .false.

!-------------------------------------------------------------------------
! LSQ gradient computation at nodes.

!  Initialization
   do i = 1, nnodes
     node(i)%gradu = zero
   end do

!  Gradient computation at nodes for each variable:

   if (trim(scheme_type)=="SchemeI") then
    call compute_gradient_nc(1,gradient_type) !Gradient of u(1) = u
   endif

    call compute_gradient_nc(2,gradient_type) !Gradient of u(2) = p
    call compute_gradient_nc(3,gradient_type) !Gradient of u(3) = q

!-------------------------------------------------------------------------
! Residual computation: interior fluxes

! Initialization
  nodes : do i = 1, nnodes
   node(i)%res = zero
   node(i)%wsn = zero
  end do nodes

!-------------------------------------------------------------------------
! Flux computation across internal edges (to be accumulated in res(:))
!
!   node2              1. Extrapolate the solutions to the edge-midpoint
!       o                 from the nodes, n1 and n2.
!        \   face      2. Compute the numerical flux
!         \ -------c2  3. Add it to the residual for n1, and subtract it from
!        / \              the residual for n2.
!   face/   \ edge
!      /     o         Directed area is the sum of the left and the right faces.
!    c1    node1       Left/right face is defined by the edge-midpoint and
!                      the centroid of the left/right element.
!                      Directed area is positive in n1 -> n2
!
! (c1, c2: element centroids)
!
  edges : do i = 1, nedges
!--------------------------------------------------------------------------------

! Left and right nodes of the i-th edge

     node1 = edge(i)%n1  ! Left node of the edge
     node2 = edge(i)%n2  ! Right node of the edge
   mag_n12 = edge(i)%da  ! Magnitude of the directed area vector
   mag_e12 = edge(i)%e   ! Magnitude of the edge vector (Length of the edge)

        !The directed area vector (unit vector)
        nx = edge(i)%dav(ix)
        ny = edge(i)%dav(iy)

        !The vector along the edge (unit vector)
        ex = edge(i)%ev(ix)
        ey = edge(i)%ev(iy)

!  Numerical flux (with solution reconstruction to the edge-midpoint)

   call interface_flux(node(node1)%u    , node(node2)%u    , &
                       node(node1)%gradu, node(node2)%gradu, &
                       node(node1)%ar   , node(node2)%ar   , &
                        ex, ey, mag_e12, nx,ny, num_flux   , &
                                         wsn, interior_edge  )

    node(node1)%res = node(node1)%res  + num_flux *mag_n12
    node(node2)%res = node(node2)%res  - num_flux *mag_n12

    node(node1)%wsn = node(node1)%wsn  +       wsn*mag_n12
    node(node2)%wsn = node(node2)%wsn  +       wsn*mag_n12

!--------------------------------------------------------------------------------
  end do edges
!--------------------------------------------------------------------------------

!-------------------------------------------------------------------------
! Close with the boundary flux using the element-based formula that is
! exact for linear fluxes (See Nishikawa AIAA2010-5093 for boundary weights
! that ensure the linear exactness for 2D/3D elements).
!
!      |  Interior Domain          |
!      |        .........          |
!      |        .       .          |
!      |        .       .          |
!      o--o--o-----o---------o--o--o  <- Boundary segment
!                  n1   |   n2
!                       v
!                     n12 (unit face normal vector)
!
! NOTE: We visit each boundary face, defined by the nodes n1 and n2,
!       and compute the flux across the boundary face: left half for node1,
!       and the right half for node2. In the above figure, the dots indicate
!       the control volume around the node n1. Clearly, the flux across the
!       left half of the face contributes to the node n1. Similarly for n2.
!
!
!--------------------------------------------------------------------------------
  bc_loop : do i = 1, nbound
!--------------------------------------------------------------------------------

    bfaces : do j = 1, bound(i)%nbfaces

         n1 = bound(i)%bnode(j  )  !Left node
         n2 = bound(i)%bnode(j+1)  !Right node
         nx = bound(i)%bfnx(j)     !x-component of the unit face normal vector
         ny = bound(i)%bfny(j)     !y-component of the unit face normal vector
    mag_n12 = bound(i)%bfn(j)*half !Half length of the boundary face, j.

!   Node 1
    call get_right_state(node(n1)%u,node(n1)%uexact,nx,ny,node(n1)%x,node(n1)%y, &
                                                    ub,dummy3x3,bound(i)%bc_type )
    call interface_flux( node(n1)%u, ub, node(n1)%gradu, node(n1)%gradu         , &
                         node(n1)%ar, node(n1)%ar, nx,ny,two*mag_n12/node(n1)%ar, &
                                               nx,ny, num_flux,wsn, boundary_edge )

          bfluxL  = num_flux

!   Node 2
    call get_right_state(node(n2)%u,node(n2)%uexact,nx,ny,node(n2)%x,node(n2)%y, &
                                                    ub,dummy3x3,bound(i)%bc_type )
    call interface_flux( node(n2)%u, ub, node(n2)%gradu, node(n2)%gradu         , &
                         node(n2)%ar, node(n2)%ar, nx,ny,two*mag_n12/node(n2)%ar, &
                                              nx,ny, num_flux,wsn, boundary_edge  )

          bfluxR  = num_flux

!---------------------------------------------------------------------------------------------
!   3. Add contributions to the two nodes (See Nishikawa AIAA2010-5093)
      boundary_elm = bound(i)%belm(j)

     if     (elm(boundary_elm)%nvtx == 3) then !Triangle

     ! This is a second-order boudnary flux quadrature formula, but not very accurate.
     ! node(n1)%res = node(n1)%res + (5.0_p2*bfluxL + bfluxR)/6.0_p2*mag_n12
     ! node(n2)%res = node(n2)%res + (5.0_p2*bfluxR + bfluxL)/6.0_p2*mag_n12

     ! Below is just a one-point quadrature formula, but extremely accurate.
     ! It achieves 3rd-order accuracy on a straight boundary with uniform spacing
     ! in the boundary grid in the case of triangular grids (interior can be fully irregular).
     !
     ! See this paper for details:
     !   "Accuracy-preserving boundary flux quadrature for finite-volume discretization
     !    on unstructured grids", JCP, Vol.281, pp. 518-555, 2015.
     !    http://www.hiroakinishikawa.com/My_papers/nishikawa_jcp2015v281pp518-555_preprint.pdf
     !
     ! Note: For general grids, a general 3rd-order formula should be implemented,
     !       which can be found in the above paper.

      node(n1)%res = node(n1)%res + bfluxL*mag_n12
      node(n2)%res = node(n2)%res + bfluxR*mag_n12

     elseif (elm(boundary_elm)%nvtx == 4) then !Quadrilateral

      node(n1)%res = node(n1)%res + bfluxL*mag_n12
      node(n2)%res = node(n2)%res + bfluxR*mag_n12

     else

      write(*,*) " Error: Element is neither tria nor quad. Stop. "; stop

     endif
!---------------------------------------------------------------------------------------------

    end do bfaces

!------------------------------------------------

!--------------------------------------------------------------------------------
  end do bc_loop
!--------------------------------------------------------------------------------


!---------------------------------------------------------------------------------
!------------------------------------------------------
! Point source integration

     do i = 1, nnodes
      node(i)%res(ip) = node(i)%res(ip) + node(i)%u(ip) * node(i)%vol
      node(i)%res(iq) = node(i)%res(iq) + node(i)%u(iq) * node(i)%vol
          node(i)%wsn = node(i)%wsn + node(i)%vol
     end do

!------------------------------------------------
!  Perform local preconditioning.
!  - Multiply the residual by P -> P*( Fx + Gy )
!    P is a diagonal matrix: P = diag(1, 1/Tr, 1/Tr)

  do i = 1, nnodes

     Lr = one/(two*pi*node(i)%ar)
     Tr = Lr*Lr/nu
     node(i)%res(ip) = node(i)%res(ip)/Tr
     node(i)%res(iq) = node(i)%res(iq)/Tr
     node(i)%wsn     = node(i)%wsn    /Tr

  end do
!---------------------------------------------------------------------------------


!---------------------------------------------------------------------------------
!------------------------------------------------------
! Unsteady problems only:
!
! Add a physical time derivative term discretized by BDF1/BDF2.
!
! Note: Don't forget to multiply it by the volume.
!       It is treated as a source term, and the residual is the integrated
!       value of the governing equations.
!
! Note: Right, the first step is BDF1, which is first-order accurate in time.
!       If you want to minimize the effect of the low-order accuracy by BDF1,
!       consider implementing variable-step BDF1 and BDF2 and use a very small
!       time step at the beginning for BDF1.
!
! Time step does not have to be constant.
! o-------o----------o-------------o----o-----o  -----> time
!        n-1  dtnm1  n     dtn    n+1
!
!  dtnm1 = t_{n} - t_{n-1},  dtn = t_{n+1} - t_{n}
!

  unsteady : if ( trim(problem_type)=="unsteady") then

   !BDF1 for the first time step : (u^{n+1}-u^{n})/dtn
   if (i_time_step == 0) then

     cnp1  =  one/dtn
     cn    = -one/dtn
     cnm1  =  zero

   !BDF2 thereafter : (3*u^{n+1}-4*u^n+u^{n-1})/dtn if dtn=dtnm1
   else

     !This is BDF2 with variable time steps.
     !( To derive it, construct a quadratic polynomial for u over n-1, n and n+1,
     !  and differentiate and evaluate it at n+1 to get du/dt. )
     cnp1  = ( one + dtn/(dtnm1+dtn) )/dtn
     cnm1  =  dtn/(dtnm1+dtn)/dtnm1
     cn    = -(cnp1+cnm1)

   endif

   !Now add the BDF term to the residual at all nodes.
   do i = 1, nnodes

    !Physical time derivative
    node(i)%dudt    = cnp1*node(i)%u + cn*node(i)%un + cnm1*node(i)%unm1

    !Add the physical time derivative to the first equation: ut + a*ux+b*uy = nu*(px+qy)
    !Note: Second and third equations do not have physical time derivatives.
    node(i)%res(iu) = node(i)%res(iu) + node(i)%dudt(iu)*node(i)%vol

   end do

  endif unsteady


!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
!------------------------------------------------
!  BC: Dirichlet conditions
!
!  NOTE: It is good to enforce Dirichlet condition after everything else has been done.
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
  bc_dirichlet_bc : do i = 1, nbound

  !---------------------------------------------------------
  ! Dirichlet condition on all vairables.
  !---------------------------------------------------------
   if (trim(bound(i)%bc_type) == "dirichlet_all") then

   ! We will keep the solution at boundary nodes.
   ! So, we solve the equations of U-Uexact = 0 at these nodes.
    do j = 1, bound(i)%nbnodes

                  n1 = bound(i)%bnode(j)
       node(n1)%res(iu) = node(n1)%u(iu) - node(n1)%uexact(iu)
       node(n1)%res(ip) = node(n1)%u(ip) - node(n1)%uexact(ip)
       node(n1)%res(iq) = node(n1)%u(iq) - node(n1)%uexact(iq)

    end do

  !---------------------------------------------------------
  ! Dirichlet condition on u and a tangential gradient (p,q)*tangent
  ! the normal gradient will be computed by the scheme.
  !---------------------------------------------------------
   elseif (trim(bound(i)%bc_type) == "dirichlet_u") then

    !We will fix u, but compute (p,q) by solving two equations for them.

    do j = 1, bound(i)%nbnodes

     n1 = bound(i)%bnode(j)

     ! At corner nodes in a square domain, if we know u, then we know p and q.
     ! So, we solve the equations of U-Uexact = 0 at these nodes.
     if (node(n1)%nbmarks > 1) then ! Corner node; Specify all because we know all.

       node(n1)%res(iu) = node(n1)%u(iu) - node(n1)%uexact(iu)
       node(n1)%res(ip) = node(n1)%u(ip) - node(n1)%uexact(ip)
       node(n1)%res(iq) = node(n1)%u(iq) - node(n1)%uexact(iq)

     !Otherwise, we let the scheme predict the normal gradient.
     else

         nx = bound(i)%bnx(j) !x-component of the unit face normal vector
         ny = bound(i)%bny(j) !y-component of the unit face normal vector
         ex = - ny            !x-component of the unit boundary tangent vector
         ey =   nx            !y-component of the unit boundary tangent vector

       !Arrange the residual vector as below to avoid zero diagonal in the residual Jacobian.

         !The case when the boundary edge is aligned more with x-axis.
         if (abs(ex) > abs(ey)) then
           i2 = 2
           i3 = 3
         !The case when the boundary edge is aligned more with y-axis.
         else
           i2 = 3
           i3 = 2
         endif

       !Equation 1: Solve u-uexact = 0, (to specify u)
       node(n1)%res(1)  = node(n1)%u(iu) - node(n1)%uexact(iu)

       !Equation 2: Solve a residual for (ux-p)/Tr*nx + (uy-q)/Tr*ny = (un-pn)/Tr,
       !            which is the equation for the normal gradient.
                   temp = node(n1)%res(ip)*nx + node(n1)%res(iq)*ny
       node(n1)%res(i3) = temp

       !Equation 3: Solve (p,q)*(ex,ey) - du/ds = 0, where du/ds is a derivative along
       !            the boudnary, which is known becasue u is known on the boundary.
       node(n1)%res(i2) =  (      node(n1)%u(ip)*ex +      node(n1)%u(iq)*ey) &  !<- (p,q)*(ex,ey) 
                         - ( node(n1)%uexact(ip)*ex + node(n1)%uexact(iq)*ey)    !<- du/ds

       !Note: So, as you can see from the above, this is Dirichlet condition for u and
       !      the tangential gradient. The normal gradient will be predicted by the scheme.
       !      For Neumann condition, you can specify the normal gradient and let the scheme
       !      predict the solution value (you can modify the code for that, can you?).

     endif

    end do

   else

    write(*,*) " Invalid boundary condition: bound(i)%bc_type = ", trim(bound(i)%bc_type)
    write(*,*) "  i = ", i
    write(*,*) " Stop..."
    stop

   endif

  end do bc_dirichlet_bc
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------


!---------------------------------------------------------------------------------
! Bring the residual to the right hand side: Res = 0  --->  0 = -Res.
!
! Note: Res = Integral of Fx + Gy, where the target equation is Ut + Fx + Gy = 0.
!---------------------------------------------------------------------------------

  do i = 1, nnodes
     node(i)%res = -node(i)%res
  end do

 end subroutine compute_residual
!--------------------------------------------------------------------------------


!********************************************************************************
!********************************************************************************
!********************************************************************************
!* Compute Jacobian over edge loop
!*
!* Note: This Jacobian is the exact derivative of the first-order residual,
!*       which is defined as the residual with zero LSQ gradients.
!*
!* Note: Newton-Krylov solver is Jacobian free, and so it does not require
!*       the computation and storage of a Jacobian (exact derivatibe of the
!*       second-order residual). But every Newton-Krylov solver requires 
!*       a good preconditioner in order to be efficient.
!*       This Jacobian subroutine compute and store a good approximation
!*       to the true Jacobian: exact derivative of 1st-order accurate residual.
!*       1st-order scheme has a compact stencil (involves only neighbors), and
!*       so the size of the Jacobian is reasonable.
!*
!* ------------------------------------------------------------------------------
!*  Input:  node(:)%u   - solution vector at nodes
!*
!* Output:  jac(:)%diag - diagonal blocks at nodes
!*          jac(:)%off  - off-diagonal blocks at nodes
!* ------------------------------------------------------------------------------
!*
!********************************************************************************
 subroutine compute_jacobian

 use edu2d_constants   , only : p2, zero, one, two, half, pi
 use edu2d_my_main_data, only : nu, nnodes, node, nedges, edge, elm, &
                                nbound, bound, jac,                  &
                                problem_type, i_time_step, dtn, dtnm1

 implicit none

!Local variables
 real(p2), dimension(2)   :: n12            !Unit face normal vector
 real(p2), dimension(2)   :: e12            !Unit edge vector
 real(p2)                 :: mag_e12        !Magnitude of the edge vector
 real(p2)                 :: mag_n12        !Magnitude of the face-normal vector
 integer                  :: node1, node2   !Left and right nodes of each edge
 integer                  :: n1, n2         !Left and right nodes of boundary face
 integer                  :: i, j, k, boundary_elm, i2,i3

 real(p2), dimension(3,3) :: dFnducL , dFnducR

 real(p2)                 :: nx, ny, ex, ey
 real(p2)                 :: Lr, Tr
 integer                  :: ix=1, iy=2, iu = 1, ip=2, iq=3

 real(p2), dimension(3,3) :: bjac1, bjac2 !Boundary flux diagonal Jacobian at nodes 1 and 2
 real(p2), dimension(3,3) :: duRduL
 real(p2), dimension(3)   :: temp3, ub

 logical                  :: interior_edge
 logical                  :: boundary_edge

 real(p2)                 :: cnp1

  interior_edge = .true.
  boundary_edge = .false.

!-------------------------------------------------------------------------
! Initialization

  do i = 1, nnodes
   jac(i)%diag         = zero
   jac(i)%diag_inverse = zero
   jac(i)%off          = zero
  end do

!-------------------------------------------------------------------------
! Jacobian computation across internal edges (to be accumulated in res(:))
!
!   node2              1. Extrapolate the solutions to the edge-midpoint
!       o                 from the nodes, n1 and n2.
!        \   face      2. Compute the numerical flux
!         \ -------c2  3. Add it to the residual for n1, and subtract it from
!        / \              the residual for n2.
!   face/   \ edge
!      /     o         Directed area is the sum of the left and the right faces.
!    c1    node1       Left/right face is defined by the edge-midpoint and
!                      the centroid of the left/right element.
!                      Directed area is positive in n1 -> n2
!
! (c1, c2: element centroids)
!
!--------------------------------------------------------------------------------
  edges : do i = 1, nedges
!--------------------------------------------------------------------------------

! Left and right nodes of the i-th edge

    node1 = edge(i)%n1  ! Left node of the edge
    node2 = edge(i)%n2  ! Right node of the edge
      n12 = edge(i)%dav ! This is the directed area vector (unit vector)
  mag_n12 = edge(i)%da  ! Magnitude of the directed area vector
      e12 = edge(i)%ev  ! This is the vector along the edge (uniti vector)
  mag_e12 = edge(i)%e   ! Magnitude of the edge vector (Length of the edge)

       nx = edge(i)%dav(ix)
       ny = edge(i)%dav(iy)
       ex = edge(i)%ev( ix)
       ey = edge(i)%ev( iy)

  dFnducL = zero
  dFnducR = zero

  call interface_jac(node(node1)%u    , node(node2)%u    , &
                     node(node1)%gradu, node(node2)%gradu, &
                     node(node1)%ar   , node(node2)%ar   , &
                                     ex,ey,mag_e12, nx,ny, &
                            dFnducL,dFnducR, interior_edge )

!  Add the flux multiplied by the magnitude of the directed area vector to node1,
!  and accumulate the max wave speed quantity for use in the time step calculation.

     jac(node1)%diag        = jac(node1)%diag        + dFnducL * mag_n12
     k = edge(i)%kth_nghbr_of_1
     jac(node1)%off(:,:, k) = jac(node1)%off(:,:, k) + dFnducR * mag_n12
 
!  Subtract the flux multiplied by the magnitude of the directed area vector from node2,
!  and accumulate the max wave speed quantity for use in the time step calculation.
!
!  NOTE: Subtract because the outward face normal is -n12 for the node2.

     jac(node2)%diag        = jac(node2)%diag        - dFnducR * mag_n12
     k = edge(i)%kth_nghbr_of_2
     jac(node2)%off(:,:, k) = jac(node2)%off(:,:, k) - dFnducL * mag_n12

!--------------------------------------------------------------------------------
  end do edges
!--------------------------------------------------------------------------------

!-------------------------------------------------------------------------
! Close with the boundary flux using the element-based formula that is
! exact for linear fluxes (See Nishikawa AIAA2010-5093 for boundary weights
! that ensure the linear exactness for 2D/3D elements).
!
!      |  Interior Domain          |
!      |        .........          |
!      |        .       .          |
!      |        .       .          |
!      o--o--o-----o---------o--o--o  <- Boundary segment
!                  n1   |   n2
!                       v
!                     n12 (unit face normal vector)
!
! NOTE: We visit each boundary face, defined by the nodes n1 and n2,
!       and compute the flux across the boundary face: left half for node1,
!       and the right half for node2. In the above figure, the dots indicate
!       the control volume around the node n1. Clearly, the flux across the
!       left half of the face contributes to the node n1. Similarly for n2.
!
!
!--------------------------------------------------------------------------------
  bc_loop : do i = 1, nbound
!--------------------------------------------------------------------------------

    bfaces : do j = 1, bound(i)%nbfaces

         n1 = bound(i)%bnode(j  )  !Left node
         n2 = bound(i)%bnode(j+1)  !Right node
         nx = bound(i)%bfnx(j)     !x-component of the unit face normal vector
         ny = bound(i)%bfny(j)     !y-component of the unit face normal vector
    mag_e12 = bound(i)%bfn(j)
    mag_n12 = bound(i)%bfn(j)*half !Half length of the boundary face, j.

!   1. Left node
    call get_right_state(node(n1)%u,node(n1)%uexact,nx,ny,node(n1)%x,node(n1)%y, &
                                                      ub,duRduL,bound(i)%bc_type )
    call interface_jac(node(n1)%u, ub, node(n1)%gradu, node(n1)%gradu         , &
                       node(n1)%ar, node(n1)%ar, nx,ny,two*mag_n12/node(n1)%ar, &
                                          nx,ny, dFnducL,dFnducR, boundary_edge )

    bjac1  = dFnducL  + matmul(dFnducR ,duRduL)

!   2. Right node
    call get_right_state(node(n2)%u,node(n2)%uexact,nx,ny,node(n2)%x,node(n2)%y, &
                                                      ub,duRduL,bound(i)%bc_type )
    call interface_jac(node(n2)%u, ub, node(n2)%gradu, node(n2)%gradu  , &
                node(n2)%ar, node(n2)%ar, nx,ny,two*mag_n12/node(n2)%ar, &
                                   nx,ny, dFnducL,dFnducR, boundary_edge )

    bjac2 =  dFnducL  + matmul(dFnducR ,duRduL)

!---------------------------------------------------------------------------------------------
!   3. Add contributions to the two nodes (See Nishikawa AIAA2010-5093)
      boundary_elm = bound(i)%belm(j)

     if     (elm(boundary_elm)%nvtx == 3) then !Triangle

     ! This is a second-order boudnary flux quadrature formula, but not very accurate.
     ! jac(n1)%diag = jac(n1)%diag + (5.0_p2*bjac1)/6.0_p2*mag_n12
     ! jac(n2)%diag = jac(n2)%diag + (5.0_p2*bjac2)/6.0_p2*mag_n12
     ! k = bound(i)%kth_nghbr_of_1(j)
     ! jac(n1)%off(:,:,k) = jac(n1)%off(:,:,k) + bjac2/6.0_p2*mag_n12
     ! k = bound(i)%kth_nghbr_of_2(j)
     ! jac(n2)%off(:,:,k) = jac(n2)%off(:,:,k) + bjac1/6.0_p2*mag_n12

     ! Below is just a one-point quadrature formula, but extremely accurate.
     ! It achieves 3rd-order accuracy on a straight boundary with uniform spacing
     ! in the boundary grid in the case of triangular grids (interior can be fully irregular).
     !
     ! See this paper for details:
     !   "Accuracy-preserving boundary flux quadrature for finite-volume discretization
     !    on unstructured grids", JCP, Vol.281, pp. 518-555, 2015.
     !    http://www.hiroakinishikawa.com/My_papers/nishikawa_jcp2015v281pp518-555_preprint.pdf
     !
     ! Note: For general grids, a general 3rd-order formula should be implemented,
     !       which can be found in the above paper.

      jac(n1)%diag = jac(n1)%diag + bjac1*mag_n12
      jac(n2)%diag = jac(n2)%diag + bjac2*mag_n12

     elseif (elm(boundary_elm)%nvtx == 4) then !Quad

      jac(n1)%diag = jac(n1)%diag + bjac1*mag_n12
      jac(n2)%diag = jac(n2)%diag + bjac2*mag_n12

     else

      write(*,*) " Error: Element is neither tria nor quad. Stop. "; stop

     endif
!---------------------------------------------------------------------------------------------

    end do bfaces

!--------------------------------------------------------------------------------
  end do bc_loop
!--------------------------------------------------------------------------------

!---------------------------------------------------------------------------------
!------------------------------------------------------
! Point source integration

     do i = 1, nnodes
      jac(i)%diag(2,2) = jac(i)%diag(2,2) + node(i)%vol
      jac(i)%diag(3,3) = jac(i)%diag(3,3) + node(i)%vol
     end do

!------------------------------------------------
!  Perform preconditioning

     do i = 1, nnodes

       Lr = one/(two*pi*node(i)%ar)
       Tr = Lr*Lr/nu
       jac(i)%diag(2,:)  = jac(i)%diag(2,:)  / Tr
       jac(i)%diag(3,:)  = jac(i)%diag(3,:)  / Tr
       jac(i)%off(2,:,:) = jac(i)%off(2,:,:) / Tr
       jac(i)%off(3,:,:) = jac(i)%off(3,:,:) / Tr

     end do

!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------


!---------------------------------------------------------------------------------
!------------------------------------------------------
! Unsteady problems only:
!
! Add Jacobian contribution from a physical time derivative term discretized by BDF1/BDF2.
!
! o-------o----------o-------------o----o-----o  -----> time
!        n-1  dtnm1  n     dtn    n+1
!
!  dtnm1 = t_{n} - t_{n-1},  dtn = t_{n+1} - t_{n}
!

  unsteady : if ( trim(problem_type)=="unsteady") then

   !BDF1 for the first time step : (u^{n+1}-u^{n})/dtn
   if (i_time_step == 0) then

     cnp1  =  one/dtn

   !BDF2 thereafter : (3*u^{n+1}-4*u^n+u^{n-1})/dtn if dtn=dtnm1
   else

     cnp1  = ( one + dtn/(dtnm1+dtn) )/dtn

   endif

   !Now add the BDF term contribution, d(BDF)/du, at all nodes.
   do i = 1, nnodes
    jac(i)%diag(iu,iu) = jac(i)%diag(iu,iu) + cnp1 * node(i)%vol
   end do

  endif unsteady

!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------
!------------------------------------------------
!  BC: Dirichlet on all variables.
!
!  NOTE: It is good to enforce Dirichlet condition after everything else has been done.
!---------------------------------------------------------------------------------
  bc_dirichlet_bc : do i = 1, nbound

  !---------------------------------------------------------
  ! Dirichlet condition on all vairables.
  !---------------------------------------------------------
   if (trim(bound(i)%bc_type) == "dirichlet_all") then

    bnodes : do j = 1, bound(i)%nbnodes

                      n1 = bound(i)%bnode(j)
      jac(n1)%diag       = zero
      jac(n1)%diag(iu,iu)  = one
      jac(n1)%diag(ip,ip)  = one
      jac(n1)%diag(iq,iq)  = one

     do k = 1, node(n1)%nnghbrs
      jac(n1)%off(:,:,k) = zero
     end do

    end do bnodes

  !---------------------------------------------------------
  ! Dirichlet condition on u (not on p and q)
  !---------------------------------------------------------
   elseif (trim(bound(i)%bc_type) == "dirichlet_u") then

    bnodes2 : do j = 1, bound(i)%nbnodes

     n1 = bound(i)%bnode(j)

     if (node(n1)%nbmarks > 1) then ! Corner node; Specify all because we know all.

       jac(n1)%diag       = zero
       jac(n1)%diag(iu,iu)  =  one
       jac(n1)%diag(ip,ip)  =  one
       jac(n1)%diag(iq,iq)  =  one

      do k = 1, node(n1)%nnghbrs
       jac(n1)%off(:,:,k) = zero
      end do

     else

         nx = bound(i)%bnx(j)     !x-component of the unit face normal vector
         ny = bound(i)%bny(j)     !y-component of the unit face normal vector
         ex = - ny
         ey =   nx

       !Arrange the residual vector as below to avoid zero diagonal in the residual Jacobian.

         if (abs(ex) > abs(ey)) then
           i2 = 2
           i3 = 3
         else
           i2 = 3
           i3 = 2
         endif

!      Derivative of node(n1)%res(iu) = (node(n1)%u(iu) - node(n1)%uexact(iu))
        jac(n1)%diag(iu,iu)  =  one
        jac(n1)%diag(iu,ip)  = zero
        jac(n1)%diag(iu,iq)  = zero

       do k = 1, node(n1)%nnghbrs
        jac(n1)%off(iu,:,k) = zero
       end do

!      Derivative of temp = node(n1)%res(ip)*nx + node(n1)%res(iq)*ny
!                    node(n1)%res(i3) = temp
        temp3 = jac(n1)%diag(ip,:)*nx  + jac(n1)%diag(iq,:)*ny
        jac(n1)%diag(i3,:)  = temp3

       do k = 1, node(n1)%nnghbrs
        temp3 = jac(n1)%off(ip,:,k)*nx + jac(n1)%off(iq,:,k)*ny
        jac(n1)%off(i3,:,k) = temp3
       end do

!      Derivative of node(n1)%res(i2) = ( node(n1)%u(ip)*ex      +      node(n1)%u(iq)*ey) &
!                                       ( node(n1)%uexact(ip)*ex + node(n1)%uexact(iq)*ey)
        jac(n1)%diag(i2,iu)  = zero
        jac(n1)%diag(i2,ip)  =   ex
        jac(n1)%diag(i2,iq)  =   ey

       do k = 1, node(n1)%nnghbrs
        jac(n1)%off(i2,:,k) = zero
       end do

     endif

    end do bnodes2

   endif

  end do bc_dirichlet_bc
!---------------------------------------------------------------------------------
!---------------------------------------------------------------------------------

 end subroutine compute_jacobian



!********************************************************************************
! This subroutine reconstructs the solution at the midpoint, and computes
! a numerical flux for a given set of reconstructed left and right states.
!
! ------------------------------------------------------------------------------
!  Input:
!          u1, gradu1 - Solution and gradient   at node 1
!                 ar1 - local cell aspect ratio at node 1
!
!          u2, gradu2 - Solution and gradient   at node 2
!                 ar2 - local cell aspect ratio at node 2
!
!    (ex,ey), mag_e12 - Unit edge vector and its magnitude
!             (nx,ny) - Unit directed-area vector
!       interior_edge - .true.  if this is an internal edge
!                       .false. if this is not an edge, e.g., left and right
!                               values at a boudnary node.
!
!
! Output:  num_flux   - Numerical flux
!          wsn        - Wave speed (to be used to compute a pseudo time step)
! ------------------------------------------------------------------------------
!
!********************************************************************************

 subroutine interface_flux(u1,u2,gradu1,gradu2,ar1,ar2,ex,ey,mag_e12,nx,ny, &
                                                num_flux,wsn, interior_edge )

 use edu2d_constants          , only : p2
 use edu2d_derivative_data_df3

 implicit none
 
 real(p2), dimension(3  ), intent( in) ::     u1,    u2
 real(p2), dimension(3,2), intent( in) :: gradu1,gradu2
 real(p2),                 intent( in) :: ex,ey,mag_e12,nx,ny,ar1,ar2
 logical                 , intent( in) :: interior_edge

 real(p2), dimension(3)  , intent(out) :: num_flux
 real(p2),                 intent(out) :: wsn

!Local variables
 real(p2), dimension(3,3) :: dummy3x3
 real(p2)                 :: ar

 type(derivative_data_type_df3), dimension(3) :: u1ddt, u2ddt

      ar = max(ar1, ar2)
   u1ddt = u1
   u2ddt = u2

!---------------------------------------------------------------------------
! Advection-diffusion flux
!---------------------------------------------------------------------------

    call upwind_flux(u1ddt,u2ddt,gradu1,gradu2,ar,ex,ey,mag_e12, &
                      nx,ny, num_flux,wsn,dummy3x3,interior_edge )

 end subroutine interface_flux

!********************************************************************************

!********************************************************************************
! This subroutine computes interface flux Jacobians.
!
! ------------------------------------------------------------------------------
!  Input:
!          u1, gradu1 - Solution and gradient   at node 1
!                 ar1 - local cell aspect ratio at node 1
!
!          u2, gradu2 - Solution and gradient   at node 2
!                 ar2 - local cell aspect ratio at node 2
!
!    (ex,ey), mag_e12 - Unit edge vector and its magnitude
!             (nx,ny) - Unit directed-area vector
!       interior_edge - .true.  if this is an internal edge
!                       .false. if this is not an edge, e.g., left and right
!                               values at a boudnary node.
!
!
! Output:  dFnducL    - Left  Jacobian: d(numerical flux)/du1
!          dFnducR    - Right Jacobian: d(numerical flux)/du2
! ------------------------------------------------------------------------------
!
!********************************************************************************

 subroutine interface_jac(u1,u2,gradu1,gradu2,ar1,ar2,ex,ey,mag_e12,nx,ny, &
                                            dFnducL,dFnducR, interior_edge )

 use edu2d_derivative_data_df3
 use edu2d_constants              , only : p2, zero

 implicit none

 real(p2), dimension(3  ), intent( in) ::     u1,    u2
 real(p2), dimension(3,2), intent( in) :: gradu1,gradu2
 real(p2),                 intent( in) :: ex,ey,mag_e12,nx,ny,ar1,ar2
 logical                 , intent( in) :: interior_edge

 real(p2), dimension(3,3), intent(out) :: dFnducL,dFnducR

!Local variables
 type(derivative_data_type_df3), dimension(3) :: u1ddt, u2ddt
 real(p2)                      , dimension(3) :: dummy3
 real(p2)                                     :: ar, wsn

       ar = max(ar1, ar2)
  dFnducL = zero
  dFnducR = zero

!*********************************************************************************
! Left Jacobian
!*********************************************************************************

  u1ddt = u1
  u2ddt = u2
  call ddt_seed(u1ddt) !meaning that the matrix u1ddt(i)%df(j) is set to be diag(1,1,1).

  call upwind_flux(u1ddt,u2ddt,gradu1,gradu2,ar,ex,ey,mag_e12,nx,ny, &
                                     dummy3,wsn,dFnducL,interior_edge )

!*********************************************************************************
! Right Jacobian
!*********************************************************************************

  u1ddt = u1
  u2ddt = u2
  call ddt_seed(u2ddt) !meaning that the matrix u2ddt(i)%df(j) is set to be diag(1,1,1).

  call upwind_flux(u1ddt,u2ddt,gradu1,gradu2,ar,ex,ey,mag_e12,nx,ny, &
                                    dummy3,wsn,dFnducR,interior_edge )

 end subroutine interface_jac

!********************************************************************************

!********************************************************************************
! Upwind advection-diffusion flux (SchemeI or SchemeII)
!
! Note: The flux is implemented with Automatic Differentiation, so that
!       the flux jacobian is also computed automatically by going through
!       the flux computation. Hence, this subroutine is used for computing
!       the numerical flux as well as for computing the Jacobian matrix.
!       
! ------------------------------------------------------------------------------
!  Input:
!          U1, gradU1 - Solution and gradient   at node 1
!                 ar1 - local cell aspect ratio at node 1
!
!          U2, gradU2 - Solution and gradient   at node 2
!                 ar2 - local cell aspect ratio at node 2
!
!    (ex,ey), mag_e12 - Unit edge vector and its magnitude
!             (nx,ny) - Unit directed-area vector
!       interior_edge - .true.  if this is an internal edge
!                       .false. if this is not an edge, e.g., left and right
!                               values at a boudnary node.
!
!
! Output:    num_flux - Nuemerical flux
!                 wsn - Wave speed to be used for computing a pseudo time step
!                dFdU - Flux Jacobian: d(num_flux)/dU1 or d(num_flux)/dU2
!
! Note: dF/dU is d(num_flux)/dU1 if the matrix U1(i)%df(j) is set to be
!       the indentity matrix when this subroutine is called. Likewise for U2.
! ------------------------------------------------------------------------------
!
!********************************************************************************
 subroutine upwind_flux(U1,U2,gradU1,gradU2,ar,ex,ey,mag_e12,nx,ny, &
                                          num_flux,wsn,dFdU,interior_edge )

 use edu2d_derivative_data_df3
 use edu2d_constants          , only : p2, zero, half, one, two, pi
 use edu2d_my_main_data       , only : nu, adv_speed_x, adv_speed_y, scheme_type

 implicit none

 type(derivative_data_type_df3), dimension(3)  , intent( in) :: U1, U2
 real(p2)                      , dimension(3,2), intent( in) :: gradU1, gradU2
 real(p2)                      ,                 intent( in) :: ar,ex,ey,mag_e12,nx,ny
 logical                       ,                 intent( in) :: interior_edge

 real(p2)                      , dimension(3)  , intent(out) :: num_flux
 real(p2)                      ,                 intent(out) :: wsn
 real(p2)                      , dimension(3,3), intent(out) :: dFdU

 type(derivative_data_type_df3)               ::  uL, uR
 type(derivative_data_type_df3)               ::  pL, pR
 type(derivative_data_type_df3)               ::  qL, qR
 type(derivative_data_type_df3)               :: pnL, pnR
 type(derivative_data_type_df3)               :: fnL, fnR
 type(derivative_data_type_df3), dimension(3) :: flux_ddt
 type(derivative_data_type_df3), dimension(3) ::  wL, wR

 real(p2) :: an, Lr, Tr, av
 integer  :: ix=1, iy=2, iu=1, ip=2, iq=3, i, j

  !Normal advection speed
     an = adv_speed_x*nx + adv_speed_y*ny
  !Length scale. 'ar' is a local aspect ratio.
     Lr = one/(two*pi*ar)
  !Relaxation time
     Tr = Lr*Lr/nu
  !Diffusion wave speed
     av = sqrt(nu/Tr)

!--------------------------------------------------------------------------------------
! 1. Define the left and right states (that will be used to evaluate a numerical flux):

  !----------------------------------------------------------------------------
  !(1)Linear extrapolation for numerical flux at interior edge-midpoint.
   if (interior_edge) then

    !Face value of u

     if (trim(scheme_type)=="SchemeII") then

      wL(iu) = U1(iu) + ( U1(ip)*ex + U1(iq)*ey ) * half*mag_e12
      wR(iu) = U2(iu) - ( U2(ip)*ex + U2(iq)*ey ) * half*mag_e12

     elseif (trim(scheme_type)=="SchemeI") then

      wL(iu) = U1(iu) + ( gradU1(iu,ix)*ex + gradU1(iu,iy)*ey ) * half*mag_e12
      wR(iu) = U2(iu) - ( gradU2(iu,ix)*ex + gradU2(iu,iy)*ey ) * half*mag_e12

     endif

    !Face value of p
     wL(ip) = U1(ip) + ( gradU1(ip,ix)*ex + gradU1(ip,iy)*ey ) * half*mag_e12
     wR(ip) = U2(ip) - ( gradU2(ip,ix)*ex + gradU2(ip,iy)*ey ) * half*mag_e12

    !Face value of q
     wL(iq) = U1(iq) + ( gradU1(iq,ix)*ex + gradU1(iq,iy)*ey ) * half*mag_e12
     wR(iq) = U2(iq) - ( gradU2(iq,ix)*ex + gradU2(iq,iy)*ey ) * half*mag_e12

  !----------------------------------------------------------------------------
  !(2)For numerical flux at a boudnary node (if interior_edge = .false.), where edge-length is zero.
  !   No face value reconstruction if the numerical flux is computed at a boundary node.
   else

     wL = U1
     wR = U2

   endif

        uL = wL(iu)
        pL = wL(ip)
        qL = wL(iq)

        uR = wR(iu)
        pR = wR(ip)
        qR = wR(iq)

       pnL =  pL*nx + qL*ny
       pnR =  pR*nx + qR*ny

!--------------------------------------------------------------------------------------
! 2. Compute a numerical flux for advection diffusion as a sum of upwind advection flux
!    and upwind hyperbolic diffusion flux:

! Upwind advective flux: : (1/2)*( (FL+FR) - |an|*dU ), where an=a*nx+b*ny.
          fnL = an*uL
          fnR = an*uR
  flux_ddt(1) = half*( fnL + fnR   - abs(an)*(uR-uL) )
  flux_ddt(2) = zero
  flux_ddt(3) = zero

! Then, add an upwind hyperbolic-diffusion flux: (1/2)*( (FL+FR) - P^{-1}*|P*An|*dU ) 
          fnL = nu*pnL
          fnR = nu*pnR
  flux_ddt(1) = flux_ddt(1) - half*( fnL + fnR   +    av*(     uR-uL               )    )
  flux_ddt(2) = flux_ddt(2) - half*( (uL+uR)*nx  + Tr*av*( nx*(pR-pL) + ny*(qR-qL) )*nx )
  flux_ddt(3) = flux_ddt(3) - half*( (uL+uR)*ny  + Tr*av*( nx*(pR-pL) + ny*(qR-qL) )*ny )

!--------------------------------------------------------------------------------------
! 3. Output: wsn, num_flux, and dFdU.

 !Wave speed used later for computing a time step.
  wsn = half*( abs(an) + av )

  do i = 1, 3

    !Numerical flux
    num_flux(i) = flux_ddt(i)%f

   do j = 1, 3

    !Flux Jacobian (derivative of the numerical flux)
      dFdU(i,j) = flux_ddt(i)%df(j)

   end do

  end do

 end subroutine upwind_flux
!********************************************************************************

!********************************************************************************
! Get the right state: Boundary condition
!
! ------------------------------------------------------------------------------
!  Input:
!          uL_real  - Real valued array of solution at a boundary node (L).
!      uexact_real  - Real valued array of exact solution at the boundary node (L).
!           (nx,ny) - Unit surface normal vector at the boudnary node (L)
!             (x,y) - Coordinates of the boundary node (L).
!           bc_type - Boudnary condition type to be applied at the boudnary node (L)
!
! Output:  
!            uR_real  - Right (ghost or BC) solution, which will be used to compute
!                       a numerical flux as a funciton of uL and uR.
!        duRduL_real  - Derivative, duR/duL, which will be used to compute Jacobian.
! ------------------------------------------------------------------------------
!
!********************************************************************************
 subroutine get_right_state(uL_real,uexact_real,nx,ny,x,y,    &
                            uR_real,duRduL_real,bc_type )

 use edu2d_derivative_data_df3
 use edu2d_constants          , only : p2, zero
 implicit none

 real(p2), dimension(  3), intent( in) :: uL_real, uexact_real
 real(p2)                , intent( in) :: nx, ny, x, y
 character(80),            intent( in) :: bc_type


 real(p2), dimension(  3), intent(out) :: uR_real
 real(p2), dimension(3,3), intent(out) :: duRduL_real

!Local variables
 type(derivative_data_type_df3), dimension(3) :: uL, uR
 integer                                      :: i, j
 real(p2)                                     :: temp

  temp = nx
  temp = ny
  temp = x
  temp = y

  uR = zero
  uL = uL_real
  call ddt_seed(uL)

  !(1)Dirichlet condition on all (u,p,q):
  if     (trim(bc_type)=='dirichlet_all') then

   uR    = uexact_real

  !(2)Dirichet condition on u only:
  elseif (trim(bc_type)=='dirichlet_u') then

   uR(1) = uexact_real(1)
   uR(2) = uL(2)         !<- Just copy from uL because don't know what to specify for p.
   uR(3) = uL(3)         !<- Just copy from uL because don't know what to specify for q.

  endif


!--------------
! Output

  do i = 1, 3

   !Right state
   uR_real(i) = uR(i)%f

   !Derivative duR/duL
   do j = 1, 3
     duRduL_real(i,j) = uR(i)%df(j)
   end do

  end do
 
!--------------

 end subroutine get_right_state
!********************************************************************************







!********************************************************************************
!* This subroutine computes the residual norms: L1, L2, L_infty
!*
!* ------------------------------------------------------------------------------
!*  Input:  node(:)res = the residuals
!*
!* Output:  res_norm   = residual norms (L1, L2, Linf)
!* ------------------------------------------------------------------------------
!*
!* NOTE: It is not done here, but I advise you to keep the location of the
!*       maximum residual (L_inf).
!*
!********************************************************************************
 subroutine residual_norm(res_norm)

 use edu2d_constants   , only : p2, zero, one
 use edu2d_my_main_data, only : node, nnodes, nq

 implicit none

 real(kind=p2), dimension(nq,nq), intent(out) :: res_norm
 integer                                      :: i, L1, L2, Li, k

   L1 = 1
   L2 = 2
   Li = 3 ! This is the infinity norm (Max in magnitude).

   res_norm(:,L1) =  zero
   res_norm(:,L2) =  zero
   res_norm(:,Li) = - one

!--------------------------------------------------------------------------------
  nodes : do i = 1, nnodes
!--------------------------------------------------------------------------------

   !For L1 norm
   res_norm(:,L1) = res_norm(:,L1)    + abs( node(i)%res )

   !For L2 norm
   res_norm(:,L2) = res_norm(:,L2)    + abs( node(i)%res )**2

   !L_infinity norm
   do k = 1, nq

    if ( abs( node(i)%res(k) ) > res_norm(k,Li) ) then

     res_norm(k,Li) = abs( node(i)%res(k) )

    endif

   end do

!--------------------------------------------------------------------------------
  end do nodes
!--------------------------------------------------------------------------------

   res_norm(:,L1) =      res_norm(:,L1)/real(nnodes,p2)  !L1 norm
   res_norm(:,L2) = sqrt(res_norm(:,L2)/real(nnodes,p2)) !L2 norm

 end subroutine residual_norm
!--------------------------------------------------------------------------------


!********************************************************************************
!* This subroutine computes a local pseudo-time-step.
!*
!* Note: It is a local time step becasue dtau can be different at different nodes,
!*       while CFL number is kept constant over all nodes.
!*
!* ------------------------------------------------------------------------------
!*  Input: node(i)%vol  = Dual volume
!*         node(i)%wsn  = Sum of the max wave speed multiplied by the face length
!*                CFLp  = CFL number for the pseudo time step
!*
!* Output: node(:)%dtau = a local pseudo time step
!* ------------------------------------------------------------------------------
!*
!* NOTE: Local pseudo time step is computed and stored at every node.
!*
!********************************************************************************
 subroutine compute_pseudo_time_step(CFLp)

 use edu2d_constants   , only : p2
 use edu2d_my_main_data, only : nnodes, node

 implicit none

 real(p2), intent( in) :: CFLp

!Local variables
 integer  :: i


  nodes : do i = 1, nnodes

   !Local pseudo time step: dtau = CFLp*volume/sum(0.5*max_wave_speed*face_area).
    node(i)%dtau = CFLp * node(i)%vol / node(i)%wsn

  end do nodes


 end subroutine compute_pseudo_time_step
!--------------------------------------------------------------------------------

!********************************************************************************
!* This subroutine print out in the screen the residual history and associated
!* data.
!*
!********************************************************************************
 subroutine report_res_norm(CFLp,res_norm,res_norm1_pre,sweeps,gcr,roc)

 use edu2d_constants   , only : p2
 use edu2d_my_main_data, only : i_iteration, nq, max_projection_gcr

 implicit none
 real(p2)                 , intent(in) :: CFLp, roc
 real(p2), dimension(nq,3), intent(in) :: res_norm
 real(p2), dimension(nq)  , intent(in) :: res_norm1_pre

 integer                               :: k, sweeps, gcr
 real(p2), dimension(nq)               :: cr

 !Below, we print out only the first 3 components of res_norm and cr
 !because the equations we consider in this code has 3 equations.

   write(*,'(a10,es9.2, a9,i10,a12,4es12.2)') &
           "CFLp=", CFLp, "steps=", i_iteration, " L1(res)=",res_norm(:,1)

 !Compute the convergence rate:
 ! The ratio of the current residual norm to the previous one for each equation.
 ! So, it should be less than 1.0 if converging.

   do k = 1, nq

    if (res_norm1_pre(k) > 1.0e-15) then
     cr(k) = res_norm(k,1)/res_norm1_pre(k)
    else
     cr(k) = res_norm(k,1) !To avoid zero division.
    endif

   end do

 !Print convergence rate ( = the ratio of the current residual norm to the previous one for each
 !                             equation. So, it should be less than 1.0 if converging.)
  if (i_iteration > 1) write(*,'(33x,a15,3f12.4)') "    c.r.: ", cr(1:nq)

 !Print the number of sweeps/projections and the convergence rate.

   !(1)Defect-Correction case (The case when max_projection_gcr  =0)
   if (max_projection_gcr == 0) then

    if (i_iteration > 0) write(*,'(27x,a14,i5,a,es8.2)') "GS(sweeps:cr)=", sweeps,":",roc

   !(2)Newton-Krylov(GCR) case (The case when max_projection_gcr > 0)
   elseif (max_projection_gcr > 0) then

    if (i_iteration > 0) write(*,'(27x,a28,i5,a,i12,a,es8.2)') &
                         "GCR(projections:sweeps:cr)=", gcr,":",sweeps,":",roc
   else

    ! max_projection_gcr < 0 has no meaning... Stop.

    write(*,*) " Invalid value: max_projection_gcr = ", max_projection_gcr
    write(*,*) " Set zero or positive integer, and try again. Stop..."
    stop

   endif

 end subroutine report_res_norm
!--------------------------------------------------------------------------------


!********************************************************************************

!********************************************************************************
!* This subroutine updates the solution.
!*
!* ------------------------------------------------------------------------------
!*  Input:   node(:)du = correction
!*
!* Output:   node(:)u  = updated variables 
!* ------------------------------------------------------------------------------
!*
!********************************************************************************
 subroutine update_solution_du

 use edu2d_my_main_data, only : nnodes, node

 implicit none

!Local variables
 integer :: i

!--------------------------------------------------------------------------------
  nodes : do i = 1, nnodes
!--------------------------------------------------------------------------------

   node(i)%u  = node(i)%u + node(i)%du

!--------------------------------------------------------------------------------
  end do nodes
!--------------------------------------------------------------------------------

 end subroutine update_solution_du
!--------------------------------------------------------------------------------



!********************************************************************************
!********************************************************************************
!********************************************************************************
!********************************************************************************
!********************************************************************************
!* This subroutine relaxes the linear system, Jac*du = -Res,
!* by the sequential Gauss-Seidel relaxation scheme.
!*
!* ------------------------------------------------------------------------------
!*  Input:         jac = Jacobian matirx (left hand side)
!*         node(:)%res = Residual (right hand side)
!*              sweeps = The number of GS sweeps to be performed
!*    tolerance_linear = Tolerance on the linear residual
!*
!*
!* Output: node(:)%du  = Correction
!*       sweeps_actual = Number of sweeps performed
!*                 roc = Rate of convergence (the ratio of the final linear residual
!*                       to the initial linear residual), showing how much it is
!*                       reduced. Tolerance is typically 1 order of magnitude.
!* ------------------------------------------------------------------------------
!*
!********************************************************************************
 subroutine gs_sequential(sweeps_actual,roc)

 use edu2d_constants   , only : p2, zero
 use edu2d_my_main_data, only : nq, nnodes, node, jac, i_iteration, sweeps, &
                                tolerance_linear

 implicit none

 integer , intent(out)      :: sweeps_actual
 real(p2), intent(out)      :: roc

!Local variables
 real(p2), dimension(nq,nq) :: inverse
 real(p2), dimension(nq)    :: b, x
 integer                    :: i, k, idestat

 integer                    :: ii, kth_nghbr
 real(p2), dimension(nq,3)  :: linear_res_norm !Residual norms(L1,L2,Linf) for the linear system
 real(p2), dimension(nq,3)  :: linear_res_norm_initial
 real(p2), dimension(nq,3)  :: linear_res_norm_previous

 real(p2)                   :: omega           !Relaxation parameter

            omega = 0.95_p2

 write(1000,*) "DC Iteration = ", i_iteration

 sweeps_actual = 0

!--------------------------------------------------------------------------------
! 1. Initialize the correction

  nodes1 : do i = 1, nnodes
   node(i)%du = zero
  end do nodes1

!--------------------------------------------------------------------------------
! 2. Invert the diagonal block and store it (replace jac(i)%diag by its inverse)

   nodes2 : do i = 1, nnodes

!   b is just a dummy variable here.

     b = zero

!   Invert the diagonal block at node i by Gauss elimination with pivoting.
!   Note: gewp_solve() actually solves a linear system, Ax=b, but here
!         we use it only to obtain the inverse of A. So, b is a dummy.

!             Ax=b ->          A  b     x   A^{-1}
    call gewp_solve( jac(i)%diag, b, nq, x, inverse, idestat )

!   Report errors

    if (idestat/=0) then
     write(*,*) " Error in inverting the diagonal block... Stop..."
     stop
    endif

!   Save the inverse of the diagonal block.

    jac(i)%diag_inverse = inverse

   end do nodes2

!--------------------------------------------------------------------------------
! 3. Linear Relaxation (Sweep)

  relax : do ii = 1, sweeps

!----------------------------------------------------
!    Sequential Gauss-Seidel Relaxation (sweep)

   nodes3 : do i = 1, nnodes

!   Form the right hand side of GS: [ sum( off_diagonal_block*du ) - residual ]

     b = node(i)%res ! Residual has already been given the minus sign.

    do k = 1, node(i)%nnghbrs

     kth_nghbr = node(i)%nghbr(k)
     b = b - matmul(jac(i)%off(:,:,k), node(kth_nghbr)%du)

    end do

!   Update du by the GS relaxation (with a relaxation parameter, omega)

              b = matmul( jac(i)%diag_inverse, b ) - node(i)%du
     node(i)%du = node(i)%du + omega * b

   end do nodes3

!    End of 1 Sequential Gauss-Seidel Relaxation(sweep)
!----------------------------------------------------

!----------------------------------------------------
!  Check convergence.

   !Compute the L1 norm of the residuals (for the linear system): res = A^{-1}*b-x.
   linear_res_norm(:,1) = zero

   nodes4 : do i = 1, nnodes
     b = node(i)%res
    do k = 1, node(i)%nnghbrs
     kth_nghbr = node(i)%nghbr(k)
     b = b - matmul(jac(i)%off(:,:,k), node(kth_nghbr)%du)
    end do
     b = matmul( jac(i)%diag_inverse, b ) - node(i)%du

    linear_res_norm(:,1) = linear_res_norm(:,1) + abs( b(:) )

   end do nodes4

    linear_res_norm(:,1) = linear_res_norm(:,1) / real(nnodes, p2)

   !Skip the first relaxation
    if (ii==1) then

     linear_res_norm_initial = linear_res_norm
     write(1000,'(a,i10,a,es12.5)') " relax ", ii, " max(L1 norm) = ", maxval(linear_res_norm(:,1))

   !Check convergence from the second sweep.
    else

      roc = maxval(linear_res_norm(:,1)/linear_res_norm_initial(:,1))

     write(1000,'(a,i10,a,3es12.5,2x,2f10.3)') " relax ", ii, &
           " max(L1 norm) = ", linear_res_norm(:,1), roc, omega

       !Tolerance met. Good!
       if (maxval(linear_res_norm(:,1)/linear_res_norm_initial(:,1)) < tolerance_linear .or. &
           maxval(linear_res_norm(:,1)) < 1.0e-17 ) then
       write(1000,*) " Tolerance met. Exit GS relaxation. Total sweeps = ", ii, tolerance_linear
       sweeps_actual = ii
       exit relax

       !Maximum sweep reached... Not converged...
       else

        if (ii == sweeps) then
         write(1000,*) " Tolerance not met... sweeps = ", sweeps
         sweeps_actual = sweeps
        endif

       endif

    endif

!----------------------------------------------------------------------------------------

    linear_res_norm_previous = linear_res_norm

  end do relax

!    End of Linear Relaxation (Sweep)
!--------------------------------------------------------------------------------


    write(1000,*)

 end subroutine gs_sequential
!--------------------------------------------------------------------------------


!********************************************************************************
!********************************************************************************
!********************************************************************************
!********************************************************************************
!********************************************************************************
!********************************************************************************
!* This subroutine performs one iteration of a Jabobian-Free Newton-Krylov method
!* based on GCR with a variable preconditioner.
!*
!* Note: This is a nonlinear solver, equivalent to Newton's mehtod.
!*       But the Jacobian is not computed nor stored.
!*       This is a Jacobian-Free Newton-Krylov method.
!*       The linear solver is a Krylov method (GCR).
!*       It requries a good preconditioner, for which we employ
!*       the implicit DC solver. This is a variable preconditioner
!*       because the preconditioning is not exactly the same every time.
!*       (A popular preconditioner is ILU, which is exactly the same every time.)
!*
!*
!* We solve the following linear system:
!*
!*    dRes/dU*dU = -Res    (A*x=b)
!*
!* The left hand side can be formed without computing and storing dRes/dU:
!*
!*  dRes/dU*dU ~ [ Res(U+epsilon*dU/|dU|) - Res(U) ] / [ epsilon*|dU| ]
!*
!* What is required in the GCR is the computation of dRes/dU*p, and so
!*
!*  dRes/dU*p ~ [ Res(U+epsilon*p/|p|) - Res(U) ] / [ epsilon*|p| ]
!*
!* ------------------------------------------------------------------------------
!*  Input:  node(:)%u   - Solution
!*          node(:)%res - Residual
!*
!*
!* Output:  node(:)%du  - Correction to be applied to the solution:
!*                        node(:)%u = node(:)%u + node(:)%du
!* ------------------------------------------------------------------------------
!*
!* Written by Katate Masatsuka (http://www.cfdbooks.com)
!********************************************************************************
 subroutine jfnk_gcr_solver(actual_projections,actual_sweeps,l2norm_ratio)

 use edu2d_constants   , only : p2, zero
 use edu2d_my_main_data, only : nq, nnodes, my_eps, max_projection_gcr, &
                                tolerance_gcr, sweeps_actual_gcr

 integer , intent(out) :: actual_projections, actual_sweeps
 real(p2), intent(out) :: l2norm_ratio
 
 !Max dimension is set here (65 is too many, and so large enough!).
 integer , parameter                            :: max_projections=65
 real(p2), dimension(nnodes*nq)                 :: x, r, r0, a, b, Apip1
 real(p2), dimension(nnodes*nq,max_projections) :: p, Ap 

 real(p2) :: alpha, beta, l2norm_initial, length_p, length_r
 integer  :: i, k, ndim, istat_preconditioner

              alpha = 1.0_p2
  sweeps_actual_gcr = 0

  write(2000,*) ">>>"
  write(2000,*) ">>> GCR begins"

  if (max_projection_gcr > max_projections) then 
   write(*,*) "Too large max_projection_gcr..., it should be <", max_projections
   write(*,*) "Try again with a smaller number, or modify the code. Stop."
   stop
  endif

  ndim = nnodes*nq

! Initialization

   x = zero
   r = zero
   p = zero

!  Initial residual is the nonlinear residual since x=0 (r = b-A*x = b = -Res).

   r0 = vector_res(nnodes,nq) ! This is b=-Res.
   l2norm_initial = elltwo_norm(r0,ndim)
   write(2000,*) "i = ", 0, " L1(residual) = ", l2norm_initial

    ! p_{i+1} = A_approx^{-1}*r, where A_approx is an approximate Jacobian.
    call preconditioner(r0,ndim, p(:,1),istat_preconditioner)
    Ap(:,1) = compute_Ap(p(:,1),r0,ndim) !Ap by the Frechet derivative

     r  = r0

!---------------------------------------------------------------------
! Beginning of GCR Projection
!---------------------------------------------------------------------
   i = 0
  gcr_projection : do
   i = i + 1

    actual_projections = i

!   Here, for i > 1, alpha = beta = -(Ap_{i+1},Ap_{k})/(Ap_{k},Ap_{k})
    if (abs(alpha) < my_eps) then
     write(2000,*) ">>>>> Exit GCR: GCR Stall - Residual stalls with beta=", alpha
      x(:) = p(:,1) ! Cancel the update and exit: Revert to the defect correction.
     exit gcr_projection
    endif

!-----------------------------------------
!  Update x and r:
!
!      alpha = (r,Ap_i)/(Ap_i,Ap_i)  <- inner products.
!          x = x + alpha*p_i
!          r = r - alpha*Ap_i
!
!  Computation of alpha is implemented with normalized vectors as below:

    length_r = length(r,ndim)
    length_p = length(Ap(:,i),ndim)
           a =       r/max(my_eps,length_r)
           b = Ap(:,i)/max(my_eps,length_p)
       alpha = dot_product(a,b)*(length_r/length_p)
        x(:) = x(:) + alpha* p(:,i)
        r(:) = r(:) - alpha*Ap(:,i)

!-----------------------------------------
!  Check the convergence. If not converged, construct a new search direction.

     l2norm_ratio = elltwo_norm(r,ndim) / l2norm_initial
      write(2000,*) " i = ", i, " L1(residual) = ", l2norm_initial, " roc = ", l2norm_ratio

     if (l2norm_ratio < tolerance_gcr) then
      write(2000,*) ">>>>> Exit GCR: Converged..."
      exit gcr_projection
     endif

     if (l2norm_ratio > 100.0_p2) then
      write(2000,*) ">>>>> Exit GCR: Diverged over two orders of magnitude..."
      exit gcr_projection
     endif

     if (i == max_projection_gcr) then
      write(2000,*) ">>>>> Exit GCR: max_projection_gcr reached... max_projection_gcr = ", max_projection_gcr
      exit gcr_projection
     endif

     if (i == max_projections) then
      write(2000,*) ">>>>> Exit GCR: max_projections reached... max_projections = ", max_projections
      write(2000,*) ">>>>> This is the maximum projections specified inside the code..."
      exit gcr_projection
     endif

     if (l2norm_ratio > 0.999_p2) then
      write(2000,*) ">>>>> Exit GCR: l2norm_ratio = 1.0 (stall)... Revert to the defect correction."
      x(:) = p(:,1) ! Cancel the update and exit: Revert to the defect correction.
      exit gcr_projection
     endif

     if (l2norm_ratio > 0.99_p2 .and. i == 5) then
      write(2000,*) ">>>>> Exit GCR: l2norm_ratio = 0.99 after 5 projections. Revert to the defect correction."
      x(:) = p(:,1) ! Cancel the update and exit: Revert to the defect correction.
      exit gcr_projection
     endif

     if (istat_preconditioner > 0) then
      write(2000,*) ">>>>> Exit GCR: Preconditioner diverged."
      exit gcr_projection
     endif

!-----------------------------------------
!  Construct the next search direction.
!
!    p_{i+1} = A_approx^{-1}*r
!   Ap_{i+1} = [ Res(U+epsilon*p_{i+1}/|p_{i+1}|) - Res(U) ] / [ epsilon*|p_{i+1}| ]

    call preconditioner(r,ndim, p(:,i+1),istat_preconditioner)
    Ap(:,i+1) = compute_Ap(p(:,i+1),r0,ndim) !Ap by the Frechet derivative
        Apip1 = Ap(:,i+1)
    do k = 1, i

!      beta = (Ap_{i+1},Ap_{k})/(Ap_{k},Ap_{k})
!   p_{i+1} = p_{i+1} - beta*p_{k}
!
!   Computation of beta is done with normalized Ap vectors:

       length_p = length(Ap(:,k),ndim)
              a = Apip1  /max(my_eps,length_p)
              b = Ap(:,k)/max(my_eps,length_p)
           beta = dot_product(a,b)
      Ap(:,i+1) = Ap(:,i+1) - Ap(:,k)*beta !dot_product(a,b)
       p(:,i+1) =  p(:,i+1) -  p(:,k)*beta !dot_product(a,b)

    end do

  end do gcr_projection
!---------------------------------------------------------------------
! End of GCR Projection
!---------------------------------------------------------------------

  write(2000,*) " GCR ",i," ends.", " # of p vectors = ", actual_projections

! Store the solution x(:) in node(:)%du
  call vector2du(x,nnodes,nq)

  actual_sweeps = sweeps_actual_gcr

  write(2000,*) ">>> GCR ends."
  write(2000,*) ">>>"

 end subroutine jfnk_gcr_solver
!********************************************************************************


!********************************************************************************
!* This subroutine computes the Frechet derivative
!*
!* Ap = dRes/dU*p ~ [Res(u+epsilon*p) - Res(u)]/epsilon
!* Note: compute_residual_ncfv computes %res = -Res.
!*       So, don't forget the minus sign to compute Ap.
!*
!* Written by Katate Masatsuka (http://www.cfdbooks.com)
!********************************************************************************
 function compute_Ap(p,r0,ndim) result(Ap)

 use edu2d_constants               , only : p2, one, zero
 use edu2d_my_main_data            , only : nq, nnodes, node, my_eps

 implicit none

!Input
 integer                  , intent( in) :: ndim
 real(p2), dimension(ndim), intent( in) :: p, r0
!Output
 real(p2), dimension(ndim) :: Ap

!Local variables
 real(p2), dimension(ndim) :: r_ep, v_over_dt
 real(p2)                  :: eps, length_p
 integer                   :: i, k

 Ap = zero

!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
! Frechet derivative: Newton's Method

!--------------------------------------------------------------------------------
! Save the current solution and residual

   do i = 1, nnodes
     node(i)%u_temp = node(i)%u
     node(i)%r_temp = node(i)%res ! This is -Res.
   end do

!--------------------------------------------------------------------------------
! Store (u + epsilon*p) into the solution vector.

   length_p = length(p,ndim)

   call vector_u(r_ep) !Store the solution temporarily in r_ep(:).
   eps = max( one, rms(r_ep,ndim) )*sqrt(my_eps)

   do i = 1, nnodes
    do k = 1, nq
     node(i)%u(k) = node(i)%u(k) + eps* p( nq*(i-1) + k )/max(my_eps,length_p)
    end do
   end do

!--------------------------------------------------------------------------------
! Compute the residual vector with u + epsilon*p.

     call compute_residual

!--------------------------------------------------------------------------------
! Store the computed residual into the residual vector, r_ep.

   do i = 1, nnodes
    do k = 1, nq
     r_ep( nq*(i-1) + k ) = node(i)%res(k) ! This is -Res.
    end do
   end do

!--------------------------------------------------------------------------------
! Compute the Frechet derivative (finite-difference approximation).

   Ap = ( (-r_ep) - (-r0) ) / eps * length_p ! = [Res(u+epsilon*p) - Res(u)]/epsilon

!--------------------------------------------------------------------------------
! Compute the pseudo-time term

   call vector_dt_term(v_over_dt)

   do i = 1, ndim
    Ap(i) = v_over_dt(i)*p(i) + Ap(i)  ! Ap <- (V/dt + A)*p
   end do

!--------------------------------------------------------------------------------
! Copy back the current solution and residual

   do i = 1, nnodes
     node(i)%u   = node(i)%u_temp
     node(i)%res = node(i)%r_temp ! This is -Res.
   end do

!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------

 end function compute_Ap
!********************************************************************************





!********************************************************************************
!* This subroutine performs preconditioning by the implicit DC solver.
!*
!*
!* linear_solve with right hand side "r"
!*
!* Written by Katate Masatsuka (http://www.cfdbooks.com)
!********************************************************************************
 subroutine preconditioner(r,ndim, p,istat)

 use edu2d_constants   , only : p2
 use edu2d_my_main_data, only : nq, nnodes, node, sweeps_actual_gcr

 implicit none

!Input
 integer                  , intent( in) :: ndim
 real(p2), dimension(ndim), intent( in) :: r
!Output
 real(p2), dimension(ndim), intent(out) :: p
 integer ,                  intent(out) :: istat

!Local variables
 integer                                :: i, k, sweeps_actual
 real(p2)                               :: roc

!--------------------------------------------------------------------------------
! Save the current residual

   do i = 1, nnodes
     node(i)%r_temp = node(i)%res
   end do

!--------------------------------------------------------------------------------
! Store the input residual into the residual vector.

  do i = 1, nnodes
   do k = 1, nq
    node(i)%res(k) = r( nq*(i-1) + k )
   end do
  end do

!--------------------------------------------------------------------------------
! Linear relaxation: Relax the linear system of Jac*dU = r,
!                    which is the implciit Defect Correction solver.

! Relax Jac*du = r by sequential/multicolor GS.
  call gs_sequential(sweeps_actual,roc) !This will compute node(:)%du(:).
  write(2000,*) "preconditioning:", sweeps_actual,":",roc
  write(*,'(27x,a32,i5,a,es8.2,a5,es10.2,a5,es10.2)') " - preconditioner: GS(sweeps:cr)=", &
  sweeps_actual,":",roc
  sweeps_actual_gcr = sweeps_actual_gcr + sweeps_actual

!--------------------------------------------------------------------------------
! Store the solution into p(:):
! ->   p(:) = -(Jac)^{-1}*r, where the inverse is approximate.

  do i = 1, nnodes
   do k = 1, nq
    p( nq*(i-1) + k ) = node(i)%du(k)
   end do
  end do

!--------------------------------------------------------------------------------
! Copy back the current residual.

   do i = 1, nnodes
     node(i)%res = node(i)%r_temp
   end do

!--------------------------------------------------------------------------------
  if (roc > 1.0_p2) then
   istat = 1
  else
   istat = 0
  endif

 end subroutine preconditioner
!********************************************************************************


!********************************************************************************
!* This function returns a global vector of residual.
!********************************************************************************
 function vector_res(nnodes,nq) result(vector)

 use edu2d_constants   , only : p2
 use edu2d_my_main_data, only : node

 implicit none

!Input
 integer          , intent( in) :: nnodes, nq

!Output
 real(p2), dimension(nnodes*nq) :: vector

!local variables
 integer                        :: i, k

  do i = 1, nnodes
   do k = 1, nq
    vector( nq*(i-1) + k ) = node(i)%res(k)
   end do
  end do  

 end function vector_res
!********************************************************************************

!********************************************************************************
!* This function returns a global vector of residual.
!********************************************************************************
 subroutine vector_u(vector)

 use edu2d_constants   , only : p2
 use edu2d_my_main_data, only : node, nnodes, nq

 implicit none

!Output
 real(p2), dimension(nnodes*nq), intent(out) :: vector

!local variables
 integer                                     :: i, k

  do i = 1, nnodes
   do k = 1, nq
    vector( nq*(i-1) + k ) = node(i)%u(k)
   end do
  end do  

 end subroutine vector_u
!********************************************************************************


!********************************************************************************
!* This function returns a global vector of residual.
!********************************************************************************
 subroutine vector2du(vector,nnodes,nq)

 use edu2d_constants   , only : p2
 use edu2d_my_main_data, only : node

 implicit none

!Input
 integer                       , intent( in) :: nnodes, nq
 real(p2), dimension(nnodes*nq), intent( in) :: vector

!local variables
 integer                                     :: i, k

  do i = 1, nnodes
   do k = 1, nq
    node(i)%du(k) = vector( nq*(i-1) + k )
   end do
  end do  

 end subroutine vector2du
!********************************************************************************

!********************************************************************************
!* This function returns a global vector of residual.
!********************************************************************************
 subroutine vector_dt_term(vector)

 use edu2d_constants   , only : p2
 use edu2d_my_main_data, only : node, nnodes, nq, CFLp_frechet, CFLp

 implicit none

!Output
 real(p2), dimension(nnodes*nq), intent(out) :: vector

!local variables
 integer                                     :: i, k
 real(p2) :: temp

  CFLp_frechet = CFLp

  do i = 1, nnodes
   temp = node(i)%vol / ( CFLp_frechet*node(i)%dtau )

   do k = 1, nq
    vector( nq*(i-1) + k ) = temp
   end do
  end do  

 end subroutine vector_dt_term
!********************************************************************************



!********************************************************************************
!* This function returns a global vector of residual.
!********************************************************************************
 function elltwo_norm(vec,n)

 use edu2d_constants, only : p2, zero

 implicit none

!Input
 integer               , intent(in) :: n
 real(p2), dimension(n), intent(in) :: vec

!Output
 real(p2)                           :: elltwo_norm

!Local variable
 integer :: i

   elltwo_norm = zero

  do i = 1, n
   elltwo_norm = elltwo_norm + abs(vec(i))**2
  end do

   elltwo_norm = sqrt( elltwo_norm / real(n,p2) )

 end function elltwo_norm
!********************************************************************************


!********************************************************************************
!* This function returns a global vector of residual.
!********************************************************************************
 function rms(vec,n)

 use edu2d_constants, only : p2, zero

 implicit none

!Input
 integer               , intent(in) :: n
 real(p2), dimension(n), intent(in) :: vec
!Output
 real(p2)                           :: rms
!Local variable
 integer :: i

   rms = zero

  do i = 1, n
   rms = rms + vec(i)**2
  end do

   rms = sqrt( rms / real(n,p2) )

 end function rms
!********************************************************************************


!********************************************************************************
!* This function returns a global vector of residual.
!********************************************************************************
 function length(vec,n)

 use edu2d_constants, only : p2, zero

 implicit none

!Input
 integer               , intent(in) :: n
 real(p2), dimension(n), intent(in) :: vec

!Output
 real(p2)                           :: length

!Local variable
 integer :: i

   length = zero

  do i = 1, n
   length = length + vec(i)**2
  end do

   length = sqrt( length )

 end function length
!********************************************************************************


!****************************************************************************
!****************************************************************************
!****************************************************************************
!****************************************************************************
!****************************************************************************
!****************************************************************************
!* ------------------ GAUSS ELIMINATION WITH PIVOTING ---------------------
!*
!*  This computes the inverse of an (Nm)x(Nm) matrix "ai".
!*
!*  IN :       ai = an (Nm)x(Nm) matrix whoise inverse is sought.
!*             bi = right hand side of a linear system: ai*x=bi.
!*             nm = the size of the matrix "ai"
!*
!* OUT :  inverse = the inverse of "ai".
!*            sol = solution to the linear system, ai*x=bi
!*       idetstat = 0 -> inverse successfully computed
!*                  1 -> THE INVERSE DOES NOT EXIST (det=0).
!*                  2 -> No unique solutions exist.
!*
!* Katate Masatsuka, April 2015. http://www.cfdbooks.com
!*****************************************************************************
 subroutine gewp_solve(ai,bi,nm,sol,inverse,idetstat)

  use edu2d_constants, only : p2, zero, one

  implicit none

! Input
  integer , intent( in) :: nm
  real(p2), intent( in) :: ai(nm,nm),bi(nm)

! Output
  real(p2), intent(out) :: sol(nm),inverse(nm,nm)
  integer , intent(out) :: idetstat

! Local variables
  real(p2) :: a(nm,nm+1),x(nm)
  integer  :: i,j,k,pp,nrow(nm),m

 do m=1,nm
!*****************************************************************************
!*****************************************************************************
       do j=1,nm
        do i=1,nm
          a(i,j) = ai(i,j)
        end do
       end do
       do k=1,nm
          a(k,nm+1)=zero; nrow(k)=k
       end do
          a(m,nm+1)=one
!*****************************************************************************
       do j=1,nm-1
!*****************************************************************************
!* find smallest pp for a(pp,j) is maximum in jth column.
!***************************************************************************** 
      call findmax(nm,j,pp,a,nrow)
!*****************************************************************************
!* if a(nrow(p),j) is zero, there's no unique solutions      
!*****************************************************************************
      if ( abs(a(nrow(pp),j) - zero) < 1.0e-15 ) then
       write(6,*) 'the inverse does not exist.'
        idetstat = 1
        return
      else
      endif
!*****************************************************************************
!* if the max is not a diagonal element, switch those rows       
!*****************************************************************************
      if (nrow(pp) .ne. nrow(j)) then
      call switch(nm,j,pp,nrow)
      else
      endif  
!*****************************************************************************
!* eliminate all the entries below the diagonal one
!***************************************************************************** 
      call eliminate_below(nm,j,a,nrow)

      end do
!*****************************************************************************
!* check if a(nrow(n),n)=0.0 .
!*****************************************************************************
      if ( abs(a(nrow(nm),nm) - zero) < 1.0e-15 ) then
        write(6,*) 'no unique solution exists!';  idetstat = 2
        return 
      else
      endif
!*****************************************************************************
!* backsubstitution!
!*****************************************************************************
      call backsub(nm,x,a,nrow)
!*****************************************************************************
!* store the solutions, you know they are inverse(i,m) i=1...
!*****************************************************************************
      do i=1,nm
         inverse(i,m)=x(i)
      end do
!*****************************************************************************
 end do
!*****************************************************************************
!* solve
!*****************************************************************************
    do i=1,nm; sol(i)=zero;
     do j=1,nm
       sol(i) = sol(i) + inverse(i,j)*bi(j)
     end do
    end do

    idetstat = 0;
    return

 end subroutine gewp_solve


!Four subroutines below are used in gewp above.


!*****************************************************************************
!* Find maximum element in jth column 
!***************************************************************************** 
 subroutine findmax(nm,j,pp,a,nrow)

  use edu2d_constants   , only : p2

  implicit none

! Input
  integer , intent( in) :: nm
  real(p2), intent( in) :: a(nm,nm+1)
  integer , intent( in) :: j,nrow(nm)

! Output
  integer , intent(out) :: pp

! Local variables
  real(p2) :: max
  integer :: i

            max=abs(a(nrow(j),j)); pp=j
           do i=j+1,nm
             if (max < abs(a(nrow(i),j))) then
                  pp=i; max=abs(a(nrow(i),j))
             endif
           end do

  return

 end subroutine findmax

!*****************************************************************************
!* Switch rows       
!*****************************************************************************
 subroutine switch(nm,j,pp,nrow)

 implicit none

! Input
  integer, intent(   in) :: nm,j,pp

! Output
  integer, intent(inout) :: nrow(nm)

! Local
  integer :: ncopy

      if (nrow(pp).ne.nrow(j)) then
         ncopy=nrow(j)
         nrow(j)=nrow(pp)
         nrow(pp)=ncopy
      endif  

  return

 end subroutine switch

!*****************************************************************************
!* Eliminate all the entries below the diagonal one
!* (give me j, the column you are working on now)
!***************************************************************************** 
 subroutine eliminate_below(nm,j,a,nrow)

  use edu2d_constants   , only : p2, zero

  implicit none
  
! Input
  integer , intent(   in) :: nm
  integer , intent(   in) :: j,nrow(nm)

! Output
  real(p2), intent(inout) :: a(nm,nm+1)

! Local
  real(p2) :: m
  integer  :: k,i

      do i=j+1,nm
        m=a(nrow(i),j)/a(nrow(j),j)
        a(nrow(i),j)=zero
          do k=j+1,nm+1
            a(nrow(i),k)=a(nrow(i),k)-m*a(nrow(j),k)
          end do
      end do

  return

 end subroutine eliminate_below

!*****************************************************************************
!* Backsubstitution!
!*****************************************************************************
 subroutine backsub(nm,x,a,nrow)

  use edu2d_constants   , only : p2, zero

  implicit none

! Input
  integer , intent( in) :: nm
  real(p2), intent( in) :: a(nm,nm+1)
  integer , intent( in) :: nrow(nm)

! Output
  real(p2), intent(out) :: x(nm)

! Local
  real(p2) :: sum
  integer :: i,k

      x(nm)=a(nrow(nm),nm+1)/a(nrow(nm),nm)
      do i=nm-1,1,-1
         sum=zero
           do k=i+1,nm
              sum=sum+a(nrow(i),k)*x(k)
           end do
      x(i)=(a(nrow(i),nm+1)-sum)/a(nrow(i),i)
      end do

  return

 end subroutine backsub
!*********************************************************************



 end module edu2d_advdiff_solver_nc

