######################################################################
# Running EDU2D-AdvDIff code
#
# Follow the steps to run the code.
# (Or just run this script by typing "source readme_v0.txt".)
#
# NOTE: The following is for gfortran. If you use other compiler,
#       replace "gfortran" by your compiler (ifort, g95, etc).
#
#
# Katate Masatsuka, http://www.cfdbooks.com
#####################################################################

#####################################################################
# 1. Steady: Compile and run the grid generation code.
#    It will generate the following files:
#     steady.grid        - Input EDU2D-AdvDiff code (steady case)
#     steady.bcmap       - Input EDU2D-AdvDiff code (steady case)
#     other files        - Not needed by EDU2D-AdvDiff code
#####################################################################

gfortran -O2 -o generate_grids_for_edu2d_steady_v0 generate_grids_for_edu2d_steady_v0.f90

./generate_grids_for_edu2d_steady_v0

#####################################################################
# 2. Unsteady: Compile and run the grid generation code.
#    It will generate the following files:
#     steady.grid        - Input EDU2D-AdvDiff code (unsteady case)
#     steady.bcmap       - Input EDU2D-AdvDiff code (unsteady case)
#     other files        - Not needed by EDU2D-AdvDiff code
#####################################################################

gfortran -O2 -o generate_grids_for_edu2d_unsteady_v0 generate_grids_for_edu2d_unsteady_v0.f90

./generate_grids_for_edu2d_unsteady_v0


#####################################################################
# 3. Compile EDU2D-AdvDiff code
#    Use the following, or just type 'make' which uses Makefile.
#####################################################################

gfortran -O2 -c edu2d_advdiff_basic_package_v0.f90
gfortran -O2 -c edu2d_ddt3_v0.f90
gfortran -O2 -c edu2d_advdiff_lsq_grad_nc_v0.f90
gfortran -O2 -c edu2d_advdiff_solver_nc_v2.f90
gfortran -O2 -c edu2d_advdiff_main_v1.f90
gfortran -O2 -o edu2d_advdiff  edu2d_advdiff_basic_package_v0.o edu2d_ddt3_v0.o edu2d_advdiff_lsq_grad_nc_v0.o edu2d_advdiff_solver_nc_v2.o edu2d_advdiff_main_v1.o

#####################################################################
# 4. Run EDU2D-AdvDiff code for steady case
#    See 'steady_screen_out.txt', which is what you should see on the screen.
#####################################################################

./edu2d_advdiff steady 



# Plot the solution by Tecplot (if you have Tecplot): 

tec360 steady_plot_solution.lay          &  #This opens steady_nc_tecplot.dat 
tec360 steady_plot_boundary_solution.lay &  #This opens steady_nc_tecplot_b.dat 

#####################################################################
# 5. Run EDU2D-AAdvDiff code for unsteady case
#    See 'unsteady_screen_out.txt', which is what you should see on the screen.
#####################################################################

./edu2d_advdiff unsteady 



# Plot the solutions by Tecplot (if you have Tecplot): 

tec360 unsteady_initial_solution.lay         &  #This opens unsteady_nc_tecplot_0.dat
tec360 unsteady_final_solution.lay           &  #This opens unsteady_nc_tecplot.dat
tec360 unsteady_initial_final_solutions.lay  &  #This plots both in one place.

#####################################################################
# Enjoy!
#####################################################################


