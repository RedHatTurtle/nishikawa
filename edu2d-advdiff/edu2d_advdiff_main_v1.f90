!********************************************************************************
!* --- Educationally-Designed Unstructured 2D (EDU2D) Code
!*
!*
!*
!*                     --- This is EDU2D-AdvDiff ---
!*
!*
!* EDU2D-AdvDiff: A 2D unstructured hyperbolic advection-diffusion solver with
!*
!*    - Node-centered finite-volume discretization (2nd-order)
!*    - Hyperbolic method for diffusion
!*    -   Steady solver: Jacobian-Free Newton-Krylov solver
!*    - Unsteady solver: The 2nd-order Backward-Difference Formula (BDF2)
!*                       with the implicit (steady) solver used to solve the residual
!*                       equations over each physical time step.
!*
!* Target equation: Time-dependent advection-diffusion equation,
!*
!*                  du/dt + a*ux + b*uy = nu*(uxx+uyy),
!*
!* where (a,b) and nu are constants, in a square domain:
!*
!*
!*                               Boundary 4
!*                       1 --------------------
!*                         |                  |
!*                         |                  |
!*             Boundary 1  |                  | Boundary 3
!*                         |                  | 
!*                         |                  |
!*            y ^          |                  |
!*              |          |                  |
!*              |          --------------------
!*              |---> x   0     Boundary 2     1
!*                              
!*
!* We solve the advection-diffusion equation in the hyperbolic-diffusion form:
!*
!*     du/dtau + a*ux + b*uy = nu*(px + qy) - du/dt,
!*     dp/dtau               = (ux-p)/Tr,
!*     dq/dtau               = (uy-q)/Tr,
!*
!* where tau is a pseudo time, Tr is an arbitrary relaxation time, and du/dt
!* is discretized in time by implicit BDF1/BDF2 (Backward Difference Formulas).
!* In the pseudo steady state, we obtain the solution at the next physical time level.
!* This is an implicit time-stepping scheme, which is unconditionally stable.
!* So, you can use any size of time step.
!*
!*
!* Note: We specify all variables on Boundary 1, 3, and 4 while
!*       on Boundary 2, we specify u and p, but predict q by the scheme.
!*
!*
!* Note: Solving this way using the hyperbolic method, we get the following advantages:
!*       - Same second-order accruacy in u, p(=ux), and q(=uy) at every time step.
!*       - High-quality and high-accurate solution gradients on fully irregular grids.
!*       - Accelerated convergence when the stiffiness due to diffusion dominates.
!*
!* Note: You can try an extremely small diffusion coefficient, e.g., nu=1.0e-12.
!*       It will work fine. In such a case, the solver is a pure advection solver.
!*       But the negligibly small hyperbolic diffusion system will produce
!*       accurate (2nd-order) gradients on irregular grids. This is what is called
!*       'artificial hyperbolic diffusion'. See JCP2014 below, and also AIAA2015-2451.
!*
!* References:
!*
!*  - H. Nishikawa, "First, Second, and Third Order Finite-Volume Schemes for Advection-Diffusion",
!*    Journal of Computational Physics, Vol. 273, September 2014, Pages 287-309, 2014.
!*    http://www.hiroakinishikawa.com/My_papers/nishikawa_jcp2014v273pp287-309_preprint.pdf
!*
!*  - H. Nishikawa, " Accuracy-preserving boundary flux quadrature for finite-volume discretization
!*    on unstructured grids", Journal of Computational Physics, Vol. 281,
!*    January 2015, Pages 518-555, 2015.
!*    http://www.hiroakinishikawa.com/My_papers/nishikawa_jcp2015v281pp518-555_preprint.pdf
!*
!*
!*
!* Program Files (Five Fortran90 files):
!*
!* ------------------------------------------
!*  1. edu2d_advdiff_main.f90          <- Main program file (This file), which contains
!*
!*      -- edu2d_advdiff          : Main prgoram
!*      -- other subroutines
!*
!* ------------------------------------------
!*  2. edu2d_advdiff_basic_package.f90 <- Basic EDU2D package file, which contains
!*
!*      -- edu2d_constants        : Numerical values defined
!*      -- edu2d_grid_data_type   : Grid data types defined
!*      -- edu2d_main_data        : Main grid data and parameters declared
!*      -- edu2d_grid_data        : Read/construct/check grid data
!*
!* ------------------------------------------
!*  3. edu2d_ddt3_v0.f90                <- Automatic Differentiation(AD), which contains
!*
!*      -- AD-related subroutines for arrays of dimension 3.
!*
!* ------------------------------------------
!*  4. edu2d_advdiff_lsq_grad_nc_v0.f90 <- LSQ gradient methods file, which contains
!*
!*      -- compute_lsq_coeff_nc : Compute and store LSQ gradient coefficients at nodes
!*      --   check_lsq_coeff_nc : Check the computed LSQ coefficients
!*      --  compute_gradient_nc : Compute gradients at nodes
!*
!* ------------------------------------------
!*  5. edu2d_advdiff_solver_nc.f90      <- Solver file, which contains
!*
!*      -- edu2d_advdiff_solver_nc : Jacobian-Free Newton-Krylov solver
!*
!*
!* ------------------------------------------------------------------------------
!*
!* Notes:
!*
!*  The purpose of this code is to give a beginner an opportunity to learn how to
!*  write an unstructured CFD code. Hence, the focus here is on the simplicity.
!*  The code is not optimized for efficiency.
!*
!*  If the code is not simple enough to understand, please send questions to Hiro
!*  at sunmasen(at)hotmail.com. I'll greatly appreciate it and revise the code.
!*
!*  If the code helps you understand how to write your own code that is more
!*  efficient and has more features (possibly in a different programming language),
!*  it'll have served its purpose.
!*
!*
!*
!*        written by Dr. Katate Masatsuka (info[at]cfdbooks.com),
!*
!* the author of useful CFD books, "I do like CFD" (http://www.cfdbooks.com).
!*
!* This is Version 0 (July 2015).
!* This F90 code is written and made available for an educational purpose.
!* This file may be updated in future.
!*
!* Katate Masatsuka, http://www.cfdbooks.com
!********************************************************************************

!********************************************************************************
!* Main progam for EDU2D-AdvDiff
!*
!* Input -------------------------------------------------------
!*
!*  (1)Parameters to be specified inside the main program:
!*
!*   project_name = Project name that determines the names of grid and BC files to read,
!*                  as well as the names of output data files.
!*   nq = The number of equations/variables (it is set to be 3 here).
!*   gradient_type     = LSQ gradient type ('linear' or 'quadratic2')
!*   gradient_weight   = LSQ gradient weight ('none' or 'inverse_distance')
!*   gradient_weight_p = Power of LSQ weight (any positive real number or zero)
!*
!*   And many others. See below.
!*
!*  (2)Files that should be available before running this code:
!*
!*   [project name].grid  = grid file containing boundary information
!*   [project name].bcmap = file that specifies boundary conditions
!*
!*   (Note: Compile and run generate_grids_for_edu2d_v0.f90 to generate
!*       these input files for an exmaple square domain grid. )
!*
!*   (Note: See the subroutine "read_grid" in edu2d_grid_data.f90
!*          or generate_grids_edu2d_v0.f90 for the format of these files.)
!*
!* Output ------------------------------------------------------
!*
!*  - "[project name]_nc_tecplot_0.dat" = Tecplot file containing the initial solution.
!*  - "[project name]_nc_tecplot.dat"   = Tecplot file containing the grid and the final solution.
!*  - "[project name]_nc_tecplot_b.dat" = Tecplot file for boundary data.
!*
!*  Note: If you want to create a movie for an unsteady simulation, add a subroutine
!*        that writes out unsteady data by yourself. You can do it!
!*
!*
!* Katate Masatsuka, http://www.cfdbooks.com
!********************************************************************************
 program edu2d_template_main

 use edu2d_constants   , only : p2, zero
 use edu2d_my_main_data, only : nq, gradient_type, gradient_weight, gradient_weight_p,          &
                                tolerance, max_iterations, tolerance_linear, node, jac, nnodes, &
                                max_projection_gcr, CFLp1, CFLp2, CFLp_ramp_steps, sweeps,      &
                                adv_speed_x, adv_speed_y, nu, scheme_type, problem_type,        &
                                i_time_step, time_step_max, dtn, dtnm1, t_final
 use edu2d_grid_data   , only : read_grid, construct_grid_data, check_grid_data
 use edu2d_lsq_grad_nc , only : compute_lsq_coeff_nc, check_lsq_coeff_nc

 use edu2d_advdiff_solver_nc, only : edu2d_advdiff_solver_jfnk

 implicit none

!Input grid data files
 character(80) :: datafile_grid_in  !Grid file
 character(80) :: datafile_bcmap_in !Boundary condition file

!Output data file for NC scheme
 character(80) :: datafile_nc_tec    !Tecplot file for viewing the result: Entire domain
 character(80) :: datafile_nc_tec_b  !Tecplot file for viewing the result: Boundary
 character(80) :: datafile_nc_tec_0  !Tecplot file for viewing the initial solution: Entire domain

!Local
 character(80) :: project_name
 real(p2)      :: time, temp
 integer       :: i

! Set input paramters (These variables are declared in the module 'edu2d_my_main_data'
!                      in edu2d_advdiff_basic_package.f90.)

  write(*,*)
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  write(*,*) ">>> Setting up parameters..."
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  write(*,*)

   !Read problem_type from a command line. So, you run the code as follows:
   !% ./edu2d_advdiff steady        <- To run a steady case
   !% ./edu2d_advdiff unsteady      <- To run an unsteady case

   call get_command_argument(1,problem_type)

   write(*,*) " problem_type = ", problem_type

   project_name       = problem_type ! Project name: The code reads grid/BC files with this name.
                                     ! Yes, we use "steady" and "unsteady" as a project name.

   write(*,*) " Project name = ", project_name

   !Target equation parameters
   nq                 = 3          ! Number of equtaions/variables: U=[u,p,q], where p=du/dx and q=du/dy.
   adv_speed_x        = 0.51       ! Advection speed in x-direction:  a of a*ux+b*uy
   adv_speed_y        = 0.45       ! Advection speed in y-direction:  b of a*ux+b*uy
   nu                 = 0.002      ! Diffusion coefficient         : nu of nu*(uxx+uyy)

   !LSQ gradients
   gradient_type      = "linear"   ! LSQ gradients: "none"/"linear"/"quadratic2"
   gradient_weight    = "none"     ! LSQ Grad weight: "none"/"inverse_distance".
   gradient_weight_p  =  zero      ! LSQ grad weight power: zero or positive real value

   !Newton-Krylov solver
   tolerance          = 1.0e-06    ! Tolerance on the solver (residual reduction): Set later.
   max_iterations     = 1000000    ! Max iterations
   max_projection_gcr = 4          ! Max projections for GCR (larger->more expensive/memory)
   CFLp1              = 1.0e+02    ! Starting CFL for pseudo time step
   CFLp2              = 1.0e+08    !    Final CFL for pseudo time step
   CFLp_ramp_steps    = 50         ! CFL is ramped over the first CFLp_ramp_steps

   !Linear solver parameters
   tolerance_linear   = 0.1        ! Tolerance for a linear solver (linear residual reduction)
   sweeps             = 150        ! Maximum number of linear relaxaitons (sweeps)

   !Hyperbolic diffusion scheme type
   scheme_type        = "SchemeII" ! "SchemeI"/"SchemeII". SchemeII should be superior.

   !Unsteady problem parameters
   if     (trim(problem_type)=="unsteady") then

    time_step_max = 10000    ! Maximum physical time steps
    t_final       = 1.0_p2   ! Final time for unsteady problem
    dtn           = 0.01_p2  ! Physical time step
    tolerance     = 1.0e-02  ! Tolerance on the solver (residual reduction)

   elseif (trim(problem_type)=="steady"  ) then

    write(*,*)
    write(*,*) ">>> This is a steady problem -> One time step with dt=infinity."
    write(*,*)

    time_step_max = 1         ! Maximum physical time steps
    t_final       = 1.0e+15   ! Final time for unsteady problem
    dtn           = 1.0e+15   ! Physical time step
    tolerance     = 1.0e-06   ! Tolerance on the solver (residual reduction)

   endif


! Read grid files
  write(*,*)
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  write(*,*) ">>> Reading the grid and boundary condition files... "
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"

  datafile_grid_in  = trim(project_name) // '.grid'             !<= project.grid
  datafile_bcmap_in = trim(project_name) // '.bcmap'            !<= project.bcmap
  datafile_nc_tec   = trim(project_name) // '_nc_tecplot.dat'   !<= project_nc_tecplot.dat
  datafile_nc_tec_b = trim(project_name) // '_nc_tecplot_b.dat' !<= project_nc_tecplot_b.dat
  datafile_nc_tec_0 = trim(project_name) // '_nc_tecplot_0.dat' !<= project_nc_tecplot_0.dat

  call read_grid( datafile_grid_in, datafile_bcmap_in )

! Construct grid data
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  write(*,*) ">>> Constructing grid data... "
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  write(*,*)

  call construct_grid_data

! Allocate arrays
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  write(*,*) ">>> Allocating arrays... "
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  write(*,*)

  !Allocate arrays within 'node' array
    do i = 1, nnodes
     allocate( node(i)%u(nq)       ) !To store solution: u = [u,p,q]
     allocate( node(i)%uexact(nq)  ) !To store exact solution
     allocate( node(i)%du(nq)      ) !To store corrections: u_new = u_old + du
     allocate( node(i)%gradu(nq,2) ) !To store solution gradient: (du/dx,du./y)
     allocate( node(i)%res(nq)     ) !To store residuals
     allocate( node(i)%r_temp(nq)  ) !To store residuals temporarily (used in GCR)
     allocate( node(i)%u_temp(nq)  ) !To store solution  temporarily (used in GCR)

     allocate( node(i)%un(nq)      ) !To store solution at previous time step "n"
     allocate( node(i)%unm1(nq)    ) !To store solution at time step "n-1"
     allocate( node(i)%dudt(nq)    ) !To store physical time derivatives

    end do

  !For implicit method, allocate the arrays that store a Jacobian matrix
    allocate(jac(nnodes))
    do i = 1, nnodes
     allocate( jac(i)%diag(nq,nq)                  ) ! Diagonal blocks
     allocate( jac(i)%diag_inverse(nq,nq)          ) ! Inverse of Diagonal blocks
     allocate( jac(i)%off( nq,nq, node(i)%nnghbrs) ) ! Off-diagonal blocks
    end do

! Check the grid data (It is always good to check them before use!)
  write(*,*)
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  write(*,*) ">>> Checking the grid data... "
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"

  call check_grid_data

! Compute and store LSQ gradient coefficients: linear LSQ and quadratic LSQ
  write(*,*)
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  write(*,*) ">>> Computing LSQ coefficients (at nodes)... "
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"

  call compute_lsq_coeff_nc

! Check LSQ gradient coefficients: linear LSQ and quadratic LSQ
  write(*,*)
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  write(*,*) ">>> Checking the LSQ coefficients... "
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"

  call check_lsq_coeff_nc

! Store the initial and exact solutions for unsteady/steady problems.
  write(*,*)
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  write(*,*) ">>> Setting initial and exact solutions... "
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"

  call initial_and_exact_solutions_nc(t_final)

  !Write a Tecplot file for the initial solution.
  call write_tecplot_file(datafile_nc_tec_0)
  write(*,*) " Writing an initial solution Tecplot file", trim(datafile_nc_tec_0), " ..."

  write(*,*)
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  write(*,*) ">>> Starting physical time stepping... "
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  write(*,*)

!---------------------------------------------------------------------------
!---------------------------------------------------------------------------
! Beginning of Physical time step loop
!---------------------------------------------------------------------------
!---------------------------------------------------------------------------

  i_time_step = 0
         time = zero
         temp = dtn

  physical_time_step : do

  !Stop if the final time is reached.
   if ( trim(problem_type)=="unsteady" .and. abs(time-t_final) < 1.0e-15_p2 ) exit physical_time_step

  !Stop if the max is reached.
   if (i_time_step == time_step_max) exit physical_time_step

  !Set a time step (It doesn't have to be a constant.)
   if (trim(problem_type)=="unsteady" .and. i_time_step == 0) then
     dtn = 1.0e-05_p2 !Use small one for BDF1 to minimize the effect of the low-order accuracy
   else
     dtn = temp       !Constant time step for BDF2
   endif

  !Adjust dtn to finish exactly at the final time
   if (time + dtn > t_final) dtn = t_final - time

  !Save the solutions as previous solutions to move onto the next physical time.
   do i = 1, nnodes
    node(i)%unm1 = node(i)%un
    node(i)%un   = node(i)%u
   end do

  write(*,*) "***********************************************************************************"
  write(*,*)
  write(*,*) " Time step = ", i_time_step + 1, ":   dt = ", dtn
  write(*,*)
  write(*,*) "      We're now going to compute the solution at t = ", time + dtn
  write(*,*)

  !Sub-Iteration (a pseudo-steady solver / nonlinear solver)
  !  Compute the solution at the next physical time step: U at time = time + dt,
  !  by forming a system of unsteady-residual equations and solving it for U,
  !  which is equivalent to solving a pseudo steady problem.

   call edu2d_advdiff_solver_jfnk !Jacobian-Free Newton-Krylov(NK) with GCR + DC-preconditioner 

  !Update time (this is the time at which we've just computed the solution.)
   i_time_step = i_time_step + 1
          time = time + dtn

         dtnm1 = dtn !Set dtn as previous time step, and move onto the next step.

  end do physical_time_step

!---------------------------------------------------------------------------
!---------------------------------------------------------------------------
! End of Physical time step loop
!---------------------------------------------------------------------------
!---------------------------------------------------------------------------

 if (trim(problem_type)=="unsteady") then

  if (i_time_step==time_step_max .and. abs(time-t_final) > 1.0e-14_p2 ) then
   write(*,*) " Final time not reached... Sorry..."
   write(*,*) "   Max time step reached... time_step_max=", time_step_max
   write(*,*) "   Increase time_step_max, and try again."
   write(*,*) " ... Stop"
   stop
  endif

  write(*,*) " Congratulations. Final time is reached."
  write(*,*) " Final time_step      = ", i_time_step
  write(*,*) " Final time           = ", time
  write(*,*) " Final time requested = ", t_final
  write(*,*)
  write(*,*)

 elseif (trim(problem_type)=="steady") then

  write(*,*)
  write(*,*) " Finished a steady solver..."
  write(*,*)

 endif

  write(*,*)
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  write(*,*) ">>> Computing error norms... "
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"

  call compute_error

  write(*,*)
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  write(*,*) ">>> Writing Tecplot files... "
  write(*,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"

  write(*,*) " Writing ", trim(datafile_nc_tec), " ..."
  call write_tecplot_file(datafile_nc_tec)

  write(*,*) " Writing ", trim(datafile_nc_tec_b), " ..."
  call write_tecplot_file_b(datafile_nc_tec_b)



  write(*,*)
  write(*,*) "Finished the EDU2D-AdvDiff solver... Bye!"
  write(*,*)

  stop

 contains

!********************************************************************************
! This subroutine defines the initial and exact solutions.
!
! Note: Initial solution is a physical initial solution.
!       For steady problems, it is set as the exact solution.
!       Exact solution can be steady or unsteady.
!
! ------------------------------------------------------------------------------
!  Input:  nu                        - Diffusion coefficient
!          (adv_speed_x,adv_speed_y) - Advection vector
!
! Output:  node(:)%u                 - Solution
!          node(:)%uexact            - Exact solution
! ------------------------------------------------------------------------------
!
! Note: For unsteady problems, the exact solution is defined at the final time.
!
!********************************************************************************
 subroutine initial_and_exact_solutions_nc(t_final)

 use edu2d_constants   , only : p2, pi, zero, one, two, four
 use edu2d_my_main_data, only : nnodes, node, nu, adv_speed_x, adv_speed_y

 implicit none

!Input
 real(p2), intent(in) :: t_final

!Local variables
 integer              :: i, iu, ip, iq
 real(p2)             :: x, y, c1, c2, eta, xi, temp, A, C

   iu = 1
   ip = 2
   iq = 3

 !------------------------------------------------------------------------
  do i = 1, nnodes
 !------------------------------------------------------------------------

   x = node(i)%x
   y = node(i)%y


  !------------------------------------------------------------------------
  ! Unsteady problem:
   unsteady_or_steady : if (trim(problem_type)=="unsteady") then

      c1 = 0.2
      c2 = 0.2
       C = 0.25
    temp = four*t_final + c1

    !Exact unsteady solution (See "I do like CFD, VOL.1", available at http://cfdbooks.com)
    node(i)%uexact(iu) = (C/temp)*exp( - (x - adv_speed_x*t_final - c2)**2/nu/temp &
                                       - (y - adv_speed_y*t_final - c2)**2/nu/temp )

    node(i)%uexact(ip) = node(i)%uexact(iu) * ( -two*(x - adv_speed_x*t_final - c2)/nu/temp )

    node(i)%uexact(iq) = node(i)%uexact(iu) * ( -two*(y - adv_speed_y*t_final - c2)/nu/temp )

    !Initial solution at t=0.0 (set t_final = zero)
    temp = four*zero + c1

    node(i)%u(iu) = (C/temp)*exp(        - (x - adv_speed_x*zero - c2)**2/nu/temp &
                                         - (y - adv_speed_y*zero - c2)**2/nu/temp )

    node(i)%u(ip) = node(i)%u(iu) * ( -two*(x - adv_speed_x*zero - c2)   /nu/temp )

    node(i)%u(iq) = node(i)%u(iu) * ( -two*(y - adv_speed_y*zero - c2)   /nu/temp )

  !------------------------------------------------------------------------
  ! Steady problem:
   elseif (trim(problem_type)=="steady"  ) then

   !A smooth exact steady solution:

         A = two
         C = one
       eta = adv_speed_y * x - adv_speed_x * y !bx-ay = const along the characteristics
        xi = adv_speed_x * x + adv_speed_y * y
      temp = A*A* pi*pi

    !Soluton u:
    node(i)%uexact(iu) = C*cos(A*pi*eta)*exp( -two*temp*nu /(one+sqrt(one+four*temp*nu*nu))*xi )

    !Solution p(=du/dx):
    node(i)%uexact(ip) = ( -C*sin(A*pi*eta)*A*pi*adv_speed_y                             &
            - C*cos(A*pi*eta)*two*temp*nu /(one+sqrt(one+four*temp*nu*nu))*adv_speed_x ) &
            * exp( -two*temp*nu /(one+sqrt(one+four*temp*nu*nu))*xi )

    !Solution q(=du/dy):
    node(i)%uexact(iq) = (  C*sin(A*pi*eta)*A*pi*adv_speed_x                             &
            - C*cos(A*pi*eta)*two*temp*nu /(one+sqrt(one+four*temp*nu*nu))*adv_speed_y ) &
            * exp( -two*temp*nu /(one+sqrt(one+four*temp*nu*nu))*xi )

    !Use the exact solution as an initial solution.
    node(i)%u = node(i)%uexact

   else
    write(*,*) " Invalid input: 'problem_type' = ", trim(problem_type)
    stop
   endif unsteady_or_steady

 !------------------------------------------------------------------------
  end do
 !------------------------------------------------------------------------

 end subroutine initial_and_exact_solutions_nc

!********************************************************************************
!* This subroutine computes and prints errors in the solution:
!* ( which is often called the Discretization Error (DE) in contrast to
!*   Truncation Error(TE). )
!*
!* Note: Here, we simply compute errors and print them in the screen.
!*
! ------------------------------------------------------------------------------
!  Input:  node(:)%u      - Solution
!          node(:)%uexact - Exact solution
!
! Output:  Error norms printed in the screen
! ------------------------------------------------------------------------------
!
!********************************************************************************
 subroutine compute_error

 use edu2d_constants   , only : p2, zero
 use edu2d_my_main_data, only : node, nnodes, nbound, bound

 implicit none

 real(p2), dimension(3,3) :: error_norm
 real(p2), dimension(3)   :: error
 integer                  :: i, L1=1, L2=2, Li=3
 integer                  :: i_max(3), k, nnodes_actual, n1, j

   nnodes_actual = 0
   error_norm(:,L1)   = zero
   error_norm(:,L2)   = zero
   error_norm(:,Li)   = -1.0_p2

  write(*,*)

!--------------------------------------------------------------------------------
! Interior nodes
!--------------------------------------------------------------------------------
  nodes : do i = 1, nnodes

   if (node(i)%bmark/=0) cycle nodes !Skip boundary nodes

!--------------------------------------------------------------------------------
   error = abs( node(i)%u - node(i)%uexact )
   error_norm(:,L1) = error_norm(:,L1) + error
   error_norm(:,L2) = error_norm(:,L2) + error*error

    do k = 1, 3
     if (error_norm(k,Li) < error(k)) then
      error_norm(k,Li) = error(k)
      i_max(k) = i
     endif
    end do

   nnodes_actual = nnodes_actual + 1
!--------------------------------------------------------------------------------

  end do nodes

   error_norm(:,L1) = error_norm(:,L1)/real(nnodes_actual, p2)
   error_norm(:,L2) = sqrt( error_norm(:,L2)/real(nnodes_actual, p2) )

  write(*,*) "------------------------------------------------------------------"
  write(*,*) " Error norms computed over the interior nodes "
  write(*,*)

  write(*,'(a50)') "                  L1,      L2,       Linf"
  write(*,'(a18,3es12.3)') "ERRORS for u: ", error_norm(1,:)
  write(*,'(a18,3es12.3)') "ERRORS for p: ", error_norm(2,:)
  write(*,'(a18,3es12.3)') "ERRORS for q: ", error_norm(3,:)
  write(*,*)

   write(*,*) " Max error location"
   write(*,'(a,es12.3,a,2es15.5)') &
   "  Max error(u) = ", error_norm(1,Li), " (x,y) = ", node(i_max(1))%x, node(i_max(1))%y
   write(*,'(a,es12.3,a,2es15.5)') &
   "  Max error(p) = ", error_norm(2,Li), " (x,y) = ", node(i_max(2))%x, node(i_max(2))%y
   write(*,'(a,es12.3,a,2es15.5)') &
   "  Max error(q) = ", error_norm(3,Li), " (x,y) = ", node(i_max(3))%x, node(i_max(3))%y

  write(*,*)

!--------------------------------------------------------------------------------
! Boundary nodes
!--------------------------------------------------------------------------------

   bc_loop : do i = 1, nbound

   nnodes_actual = 0
   error_norm(:,L1)   = zero
   error_norm(:,L2)   = zero
   error_norm(:,Li)   = -1.0_p2

     bnodes : do j = 1, bound(i)%nbnodes

                   n1 = bound(i)%bnode(j)

!--------------------------------------------------------------------------------
   error = abs( node(n1)%u - node(n1)%uexact )
   error_norm(:,L1) = error_norm(:,L1) + error
   error_norm(:,L2) = error_norm(:,L2) + error*error

    do k = 1, 3
     if (error_norm(k,Li) < error(k)) then
      error_norm(k,Li) = error(k)
      i_max(k) = n1
     endif
    end do

   nnodes_actual = nnodes_actual + 1
!--------------------------------------------------------------------------------

     end do bnodes

   error_norm(:,L1) = error_norm(:,L1)/real(nnodes_actual, p2)
   error_norm(:,L2) = sqrt( error_norm(:,L2)/real(nnodes_actual, p2) )

  write(*,*) "------------------------------------------------------------------"
  write(*,*) " Error norms computed over boundary nodes:"
  write(*,*)
  write(*,*) "  - Boundary number = ", i
  write(*,*) "  - Number of nodes = ", nnodes_actual
  write(*,*)

  write(*,'(a50)') "                  L1,      L2,       Linf"
  write(*,'(a18,3es12.3)') "ERRORS for u: ", error_norm(1,:)
  write(*,'(a18,3es12.3)') "ERRORS for p: ", error_norm(2,:)
  write(*,'(a18,3es12.3)') "ERRORS for q: ", error_norm(3,:)
  write(*,*)

   write(*,*) " Max error location"
   write(*,'(a,es12.3,a,2es15.5)') &
   "  Max error(u) = ", error_norm(1,Li), " (x,y) = ", node(i_max(1))%x, node(i_max(1))%y
   write(*,'(a,es12.3,a,2es15.5)') &
   "  Max error(p) = ", error_norm(2,Li), " (x,y) = ", node(i_max(2))%x, node(i_max(2))%y
   write(*,'(a,es12.3,a,2es15.5)') &
   "  Max error(q) = ", error_norm(3,Li), " (x,y) = ", node(i_max(3))%x, node(i_max(3))%y

  write(*,*)

   end do bc_loop

!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------

  write(*,*) "-------------------------------------------------------------------"
  write(*,*)

 end subroutine compute_error


!********************************************************************************
!* Write a tecplot file: grid and solution
!*
!*  Written by Katate Masatsuka, http://www.cfdbooks.com
!********************************************************************************
 subroutine write_tecplot_file(datafile_tec)

 use edu2d_my_main_data, only : nnodes, node, elm, nelms

 implicit none

!Input
 character(80), intent(in) :: datafile_tec

 integer :: i, k, os

!--------------------------------------------------------------------------------
 open(unit=1, file=datafile_tec, status="unknown", iostat=os)

  write(1,*) 'title = "grid"'
  write(1,'(a120)') 'variables = "x","y","u","p","q","LSQ(ux)","LSQ(uy)","uexact","pexact","qexact"'
  write(1,*) 'zone n=',nnodes,' e =', nelms,' et=quadrilateral, f=fepoint'

! Nodal quantities: x, y, u1, u2, u3

  do i = 1, nnodes
   write(1,*) node(i)%x, node(i)%y, (node(i)%u(k),k=1,3), node(i)%gradu(1,1), node(i)%gradu(1,2), &
              (node(i)%uexact(k),k=1,3)
  end do

! Both quad and tria elements in quad format:

  do i = 1, nelms

   !Triangles
   if (elm(i)%nvtx == 3) then

    write(1,*) elm(i)%vtx(1), elm(i)%vtx(2), elm(i)%vtx(3), elm(i)%vtx(3)

   !Quadrilaterals
   elseif (elm(i)%nvtx == 4) then

    write(1,*) elm(i)%vtx(1), elm(i)%vtx(2), elm(i)%vtx(3), elm(i)%vtx(4)

   else

    !Impossible
    write(*,*) " Error in elm%vtx data... Stop..: elm(i)%nvtx=",elm(i)%nvtx
    stop

   endif

  end do

!--------------------------------------------------------------------------------
 close(1)
 end subroutine write_tecplot_file
!********************************************************************************


!********************************************************************************
!* Write a tecplot file: grid and solution on bourdaries
!*
!*  Written by Katate Masatsuka, http://www.cfdbooks.com
!********************************************************************************
 subroutine write_tecplot_file_b(datafile_tec)

 use edu2d_my_main_data           , only : node, bound, nbound

 implicit none

!Input
 character(80), intent(in) :: datafile_tec

 character(80) :: boundary_number_char, temp
 integer       :: i, k, os, j, ibn

!--------------------------------------------------------------------------------
 open(unit=1, file=datafile_tec, status="unknown", iostat=os)

 write(1,*) 'title = "boundary data"'

  write(1,'(a)') 'variables = "x","y","u","p","q","LSQ(ux)","LSQ(uy)","uexact","pexact","qexact"'

 bc_loop : do i = 1, nbound

  write(boundary_number_char,'(i0)') i
  temp = trim(bound(i)%bc_type) // '_' // trim(boundary_number_char)
  write(1,*) 'zone t=', temp, &
             ' I=',bound(i)%nbnodes, ' F=POINT'

  bnodes : do j = 1, bound(i)%nbnodes

  ibn = bound(i)%bnode(j)

    write(1,*) node(ibn)%x, node(ibn)%y    , & !<- x, y
               (node(ibn)%u(k),k=1,3)      , & !<- u, p, q
                node(ibn)%gradu(1,1)       , & !<- LSQ ux
                node(ibn)%gradu(1,2)       , & !<- LSQ uy
               (node(ibn)%uexact(k) ,k=1,3)    !<- Exact u, p ,q

  end do bnodes

 end do bc_loop

!--------------------------------------------------------------------------------
 close(1)
 end subroutine write_tecplot_file_b
!********************************************************************************


 end program edu2d_template_main




