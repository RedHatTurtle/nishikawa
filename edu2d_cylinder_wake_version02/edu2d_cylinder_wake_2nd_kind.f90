!********************************************************************************
!
! This program generates a grid over a cylinder with grid liness clustered
! in the wake region, suitable for vortex shedding simulations.
!
!
!        written by Dr. Katate Masatsuka (info[at]cfdbooks.com),
!
! the author of useful CFD books, "I do like CFD" (http://www.cfdbooks.com).
!
! This is Version 1 (2021).
!
!
! Version 1, 02/019/2021
!
! These F90 routines were written and made available for download
! for an educational purpose. Also, this is intended to provide all CFD
! students and researchers with a tool for their code verification.
!
! This file may be updated in future.
!
! Katate Masatsuka, http://www.cfdbooks.com
!********************************************************************************

!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!
!  Input parameter module
!
!-------------------------------------------------------------------------------
!
!  Sample input file: 'input.nml' to generate a sample grid.
!  ------------------------------------------------------
!   &input_parameters
!                      debug_mode = F       ! =T: write extra data, =F: do not write.
!                      igrid_type = 1       ! 1:quad, 2:tria, 3:mixed
!                      R_cylinder = 0.5     ! Radius of cylinder
!                             dr1 = 1.0e-03 ! The first-off-the-wall mesh spacing at cylinder
!                              r1 = 10.0    ! Distance to the outer of the inner part.!
!                             nr1 = 200     ! # of elements in the radial direction (inner region)
!                             nc  = 200     ! # of elements around cylinder
!                             nw  = 135     ! # of elements in the wake region.
!                             no  = 25      ! # of elements in the outer region.
!                        xoutflow = 100.0   ! X-coordinate of the outlfow boundary.
!                          offset = 25      ! # of nods to skip from the shoulders.
!                 radius_to_outer = 100.0   ! Distance from cylinder to farfield.
!             distance_horizontal = 115.0   ! Distance of the horizontal part of farfield 
!           factor4sf2_along_wake = 3.0     ! Factor1 for streatching of streatchign factor
!                  sf2_along_wake = 3.0     ! Factor2
!              generate_grid_file = T       ! T: write a .grid file for edu2d solvers, F: Not write
!              generate_su2_file  = F       ! T: write a .su2 grid for SU2, F: Not write
!              generate_tec_file  = T       ! T: write a Tecplot file, F: Not write
!              generate_vtk_file  = F       ! T: write a .vtk file, F: Not write
!   /
!  ------------------------------------------------------
!
!   Note: No need to specify all namelist variables.
!         Those not specified are given their default values
!         as defined below.
!
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
 module input_parameter_module

  implicit none

  integer , parameter ::    dp = selected_real_kind(P=15)
  real(dp), parameter ::  zero = 0.0_dp
  real(dp), parameter ::   one = 1.0_dp
  real(dp), parameter ::   two = 2.0_dp
  real(dp), parameter :: three = 3.0_dp
  real(dp), parameter ::  half = 0.5_dp
  real(dp), parameter ::    pi = 3.14159265358979323846_dp

  public

!----------------------------
! Default input values
!----------------------------

!----------------------------
! debug_mode = T: for debug use only.
!              F: Default
!
  logical :: debug_mode = .false.

!----------------------------
! The first-off-the-wall mesh spacing at cylinder.

  real(dp) ::       dr1 = 1.0e-03_dp

  real(dp) ::        r1 = 1.0
  real(dp) ::  xoutflow = 1.0
  integer  ::    offset = 0

  real(dp) ::   distance_horizontal = 12.0
  real(dp) ::       radius_to_outer = 10.0
  real(dp) :: factor4sf2_along_wake = 5.0
  real(dp) ::        sf2_along_wake = 2.0

!----------------------------
! igrid_type = Element type as listed below.
!
!                           1 = Quads
!                           2 = Trias
!                           3 = Mixed
!
! Mixed grid will have quads in the inner region and
! triangles in the outer region.

  integer :: igrid_type = 1

!----------------------------
! Radius of the cylinder

  real(dp) :: R_cylinder   = 0.5_dp

!----------------------------
! # of elements in different directions.
!
  integer :: nc       = 40
  integer :: nw       = 40
  integer :: no       = 40
  integer :: nin      = 40
  integer :: nout     = 40
  integer :: ntop     = 40
  integer :: nbottom  = 40
  integer :: nr1      = 40

!----------------------------
! generate_tec_file = T to write a Tecplot file.

  logical :: generate_grid_file = .true.

!----------------------------
! generate_su2_file = T to write .su2 file
!                         F not to write.

  logical :: generate_su2_file = .true.

!----------------------------
! generate_tec_file = T to write a Tecplot file.

  logical :: generate_tec_file = .true.

!----------------------------
! generate_vtk_file = T towWrite a .vtk file.

  logical :: generate_vtk_file = .true.

!----------------------------
! End of Default input values
!----------------------------

! Below is the list of all input parameters available:

  namelist / input_parameters /   &
                      debug_mode, &
                             dr1, &
                              r1, &
                        xoutflow, &
                          offset, &
             distance_horizontal, &
                 radius_to_outer, &
           factor4sf2_along_wake, & 
                  sf2_along_wake, &
                      igrid_type, &
                      R_cylinder, &
                             nc , &
                             nw , &
                             no , &
                            nin , &
                           nout , &
                           ntop , &
                        nbottom , &
                             nr1, &
              generate_grid_file, &
              generate_su2_file , &
              generate_tec_file , &
              generate_vtk_file

 contains

!*****************************************************************************
!* Read input_parameters in the input file: file name = namelist_file
!*****************************************************************************
  subroutine read_nml_input_parameters(namelist_file)

  implicit none
  character(9), intent(in) :: namelist_file
  integer :: os

  write(*,*) "**************************************************************"
  write(*,*) " List of namelist variables and their values"
  write(*,*)

  open(unit=10,file=namelist_file,form='formatted',status='old',iostat=os)
  read(unit=10,nml=input_parameters)

  write(*,nml=input_parameters) ! Print the namelist variables.
  close(10)

  write(*,*) "**************************************************************"
  write(*,*)

  end subroutine read_nml_input_parameters

 end module input_parameter_module
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!
!  End of input parameter module
!
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!********************************************************************************
! Main program begins here.
!
!
!********************************************************************************

 program edu2d_cgrid_generation_cylinder

 use input_parameter_module

 implicit none

!Output file names
 character(80) :: filename_tecplot0
 character(80) :: filename_tecplot1
 character(80) :: filename_tecplot2
 character(80) :: filename_grid
 character(80) :: filename_su2
 character(80) :: filename_tec
 character(80) :: filename_vtk
 character(80) :: filename_temp
 character(80) :: filename_bcmap

!Grid data to be constructed:

 !- Basic grid data enough for visualizing the grid.
 !   These are needed by all output files.
 integer                                :: ntria, nquad
 integer      , dimension(:,:), pointer ::  tria,  quad
 integer                                :: nnodes
 real(dp)     , dimension(:), pointer   :: x, y

 !- Boundary data requied by a solver.
 !   These are needed for .grid and .su2 files.
 integer                                :: nb      !# of boundaries
 integer      , dimension(:  ), pointer :: nbnodes !# of nodes in each boundary
 integer      , dimension(:,:), pointer :: bnode   !List of boundary nodes
 character(80), dimension(:  ), pointer :: bnames  !Boundary names

!Temporary variables.
 integer                           :: i, ib, j, k, itr, stat, nnodes1, nnodes2
 integer                           :: i1, i2, i3,i4
 real(dp)                          :: theta, dtheta, dr_last

 real(dp)                          :: sf, sf_initial, xi, final_x, xis
 real(dp)                          :: sf2

 real(dp)                          :: nx, ny
 real(dp)                          :: nx2, ny2

 integer                           :: nmax

 real(dp), dimension(:,:), allocatable :: x1, y1
 integer , dimension(:)  , allocatable :: left_nodes
 integer                               :: n_left_nodes

 integer , dimension(:)  , allocatable :: outer_bnode
 integer , dimension(:)  , allocatable :: inner_bnode
 integer                               :: n_inner_bnodes, n_outer_bnodes
 real(dp), dimension(:)  , allocatable :: xouter, youter
 real(dp)                              :: xL, yL, radius_o, theta0, width_theta
 integer                               :: n_theta, i0, in, itemp, io1, io2
 real(dp)                              :: mag, mag2, xx, yy
 real(dp), dimension(:)  , allocatable :: node_maker

 real(dp)                              :: area, area1, area2
 real(dp)                              :: total_area_sum, total_area_sum_gg, total_area_gg
 real(dp)                              :: xm, ym, bx, by
 real(dp)                              :: sum_bfaces_x, sum_bfaces_y, total_bfaces_mag, n_bfaces
 real(dp)                              :: bcheckx, bchecky, b_length(7)

 integer :: nedges, ncells, nbedges

 real(dp)                              :: erx, ery
 real(dp)                              :: xc, yc, xx1, xx2, xx3,xx4,  yy1,yy2,yy3,yy4

!-------------------------------------------------------------------------------
!
! Read the input parameters, defined in the file named as 'input.nml'.
!
!-------------------------------------------------------------------------------

   write(*,*) "Reading the input file: input.nml..... "
   write(*,*)
   call read_nml_input_parameters('input.nml')
   write(*,*)

  if    (igrid_type == 1) then

   filename_tec  = "grid_quad_tec.dat"
   filename_grid = "grid_quad.grid"
   filename_su2  = "grid_quad.su2"
   filename_vtk  = "grid_quad.vtk"
   filename_bcmap = "grid_quad.bcmap"

  elseif(igrid_type == 2) then

   filename_tec  = "grid_tria_tec.dat"
   filename_grid = "grid_tria.grid"
   filename_su2  = "grid_tria.su2"
   filename_vtk  = "grid_tria.vtk"
   filename_bcmap = "grid_tria.bcmap"

  elseif(igrid_type == 3) then

   filename_tec  = "grid_mixd3_tec.dat"
   filename_grid = "grid_mixd3.grid"
   filename_su2  = "grid_mixd3.su2"
   filename_vtk  = "grid_mixd3.vtk"
   filename_bcmap = "grid_mixd3.bcmap"

  elseif(igrid_type == 4) then

   filename_tec  = "grid_mixd4_tec.dat"
   filename_grid = "grid_mixd4.grid"
   filename_su2  = "grid_mixd4.su2"
   filename_vtk  = "grid_mixd4.vtk"
   filename_bcmap = "grid_mixd4.bcmap"

  endif

   filename_tecplot0 = "debug_grid0_tec.dat"
   filename_tecplot1 = "debug_grid1_tec.dat"
   filename_tecplot2 = "debug_grid2_tec.dat"

!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!
! Estimate the grid size and allocate arrays.
!
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------

      nb = 7
    nmax = max(nc, nin+nout+ntop+nbottom)

    allocate(bnode(2*nw+nc+1,nb))
    allocate(nbnodes(nb))
    allocate(bnames(nb))

  nnodes = (nr1*nc) + (nc*nw) + (2*nw+nc)*no
  allocate( x(nnodes), y(nnodes) )

  if    (igrid_type == 1) then

    ntria = 0
    nquad = (nr1*nc) + (nc*nw) + (2*nw+nc)*no

    allocate( tria(1    ,3) )
    allocate( quad(nquad,4) )

  elseif(igrid_type == 2) then

    nquad = 2 * ( (nr1*nc) + (nc*nw) + (2*nw+nc)*no )
    nquad = 0

    allocate( tria(ntria,3) )
    allocate( quad(1    ,4) )

  elseif(igrid_type == 3 .or. igrid_type == 4) then

    ntria = 2 * ( (nr1*nc) + (nc*nw) + (2*nw+nc)*no )
    nquad =       (nr1*nc) + (nc*nw) + (2*nw+nc)*no

    allocate( tria(ntria,3) )
    allocate( quad(nquad,4) )

  else

   write(*,*) "Invalid input: igrid_type = ", igrid_type
   stop

  endif

    allocate( x(nnodes), y(nnodes) )

    allocate( node_maker(nnodes) )
    node_maker = 0

  write(*,*) "  Predicted dimension:"
  write(*,*) "      nnodes = ", nnodes
  write(*,*) "      nquad  = ", nquad
  write(*,*) "      ntria  = ", ntria

  nnodes = 0
  nquad  = 0
  ntria  = 0

!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!
! Generate a grid around the cylinder: inner region part I.
!
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------

  write(*,*)
  write(*,*) "  Generating the inner-region grid part I....."

    allocate( x1(nc,nr1+1), y1(nc,nr1+1) )

    write(*,*)
    write(*,*) "      nc  = ", nc
    write(*,*) "      nr1 = ", nr1
    write(*,*)

 !---------------------------------------------------------------------------- 
 ! Compute the stretching factor sf to satisfy the specified first spacing.

    sf_initial = 5.0_dp
            xi = 1.0_dp/nr1

    call compute_stretching_factor(dr1,r1,xi,sf_initial, final_x,sf,itr,stat )

    write(*,*)
    write(*,*) "      r1 = ", r1 
    write(*,*) "     itr = ", itr
    write(*,*) " final_x = ", final_x 
    write(*,*) "     sf  = ", sf

    write(*,*) " xi=1/n: (r1)*(one-exp(sf*xi    ))/(one-exp(sf)) = ", &
                         (r1)*(one-exp(sf*xi   ))/(one-exp(sf))
    write(*,*) " xi=1  : (r1)*(one-exp(sf*1.0_dp))/(one-exp(sf)) = ", &
                         (r1)*(one-exp(sf*1.0_dp))/(one-exp(sf))
 !---------------------------------------------------------------------------- 

!- Generate nodes:

    dtheta = two*pi/real(nc,dp)

  do k = 1, nr1+1
   do i = 1, nc

     theta = 2.0_dp*pi - dtheta*real(i-1,dp)

           xi = real(k-1,dp)/real(nr1,dp)

     x1(i,k) = ( R_cylinder + (r1)*(one-exp(sf*xi))/(one-exp(sf)) ) * cos(theta)
     y1(i,k) = ( R_cylinder + (r1)*(one-exp(sf*xi))/(one-exp(sf)) ) * sin(theta)

   end do
  end do

  dr_last = sqrt( (x1(1,nr1+1)-x1(1,nr1))**2 + (y1(1,nr1+1)-y1(1,nr1))**2 )

  write(*,*)
  write(*,*) " dr_last = ", dr_last
  write(*,*)

 !Store them in the lexicographic ordering (reverse order):

    nnodes = 0

   do k = 1, nr1+1
    do i = 1, nc

    nnodes = nnodes + 1
 
     j = i + (k-1)*(nc)

     x(j) = x1(i,k) 
     y(j) = y1(i,k)

   end do
  end do

  deallocate( x1, y1 )

!Boundary nodes (oriented so as to see the interior domain on the left).

   ib = 1
   bnames( ib) = "cylinder"
   nbnodes(ib) = nc+1
  do i = 1, nc
   bnode(i,ib) = i
  end do
   bnode(nc+1  ,ib) = 1 !Add the first node as the last to close the boundary.


!- Generate elements:

  do k = 1, nr1
   do i = 1, nc

  !Counterclockwise ordering

    if (i==nc) then
     i1 =  i  +(k-1)*(nc)
     i2 =  1  +(k-1)*(nc) !First line at i=1.
     i3 =  1  +(k  )*(nc) !First line at i=1.
     i4 =  i  +(k  )*(nc)
    else
     i1 =  i  +(k-1)*(nc)
     i2 =  i+1+(k-1)*(nc)
     i3 =  i+1+(k  )*(nc)
     i4 =  i  +(k  )*(nc)
    endif

   !Triangular grid
    if    (igrid_type == 2) then

     if ( i < nc/2+1 ) then

             ntria = ntria + 1
     tria(ntria,1) = i1
     tria(ntria,2) = i2
     tria(ntria,3) = i4

             ntria = ntria + 1
     tria(ntria,1) = i4
     tria(ntria,2) = i2
     tria(ntria,3) = i3

     else

             ntria = ntria + 1
     tria(ntria,1) = i1
     tria(ntria,2) = i2
     tria(ntria,3) = i3

             ntria = ntria + 1
     tria(ntria,1) = i1
     tria(ntria,2) = i3
     tria(ntria,3) = i4

     endif

   !Quadrilateral/Mixed grid
    else

             nquad = nquad + 1
     quad(nquad,1) = i1
     quad(nquad,2) = i2
     quad(nquad,3) = i3
     quad(nquad,4) = i4

    endif

   end do
  end do

  if (debug_mode) call write_tecplot_file(filename_tecplot0, nnodes,x,y, ntria,tria, &
                                                                         nquad,quad  )

  write(*,*)
  write(*,*) " After the first section:"
  write(*,*) "      nnodes = ", nnodes
  write(*,*) "      nquad  = ", nquad
  write(*,*) "      ntria  = ", ntria

  nnodes1 = nnodes

!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!
! Generate a grid in the wake region: inner region part II.
!
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------

  write(*,*)
  write(*,*) "  Generating the inner-region grid part I....."

 !Count the # of nodes on the right outside of the region part I.

   n_left_nodes = 0

  do i = nc-nc/4+1 + offset, nc
   n_left_nodes = n_left_nodes + 1
  end do

  do i = 1, nc/4 - (offset-1)
   n_left_nodes = n_left_nodes + 1
  end do

 !Allocate

  allocate( left_nodes(n_left_nodes) )

 !Stote the nodes: from the bottom to the top.

   n_left_nodes = 0

  do i = nc/4 - (offset-1), 1, -1
              n_left_nodes  = n_left_nodes + 1
   left_nodes(n_left_nodes) = i + nr1*nc
  end do

  do i = nc, nc-nc/4+1 + offset, -1
              n_left_nodes  = n_left_nodes + 1
   left_nodes(n_left_nodes) = i + nr1*nc
  end do

  write(*,*) "      n_left_nodes = ", n_left_nodes 
  write(*,*) " nc - n_left_nodes = ", nc - n_left_nodes 
 
 !Generate nodes in this rectangular region.

  do k = 1, n_left_nodes

   !--------------------------------------------------------------------
   ! Compute the stretching factor sf for a smooth transition.

    sf_initial = 5.0_dp
            xi = 1.0_dp/real(nw,dp)

    call compute_stretching_factor(dr_last,xoutflow-R_cylinder,xi,sf_initial, final_x,sf,itr,stat )

   !--------------------------------------------------------------------

   do i = 1, nw

       nnodes = nnodes + 1

           xi = i/real(nw,dp)
          sf2 = 1.0_dp + factor4sf2_along_wake*(one-exp(sf2_along_wake*xi))/(one-exp(sf2_along_wake))  
  
    !Stretched parameter function
    ! xis = [0,0.01,0.015,0.02,...,0.5,0.8,1.0]
      xis = (one-exp(sf2*sf*xi))/(one-exp(sf2*sf)) 

    ! Straight distance function
      xx = xis*( xoutflow - x(left_nodes(k)) )
      yy = 0.0_dp !xis*(         - y(left_nodes(k)) )
     mag = sqrt( xx**2 + yy**2  )

    ! Direction
            nx2 = abs( xoutflow - x(left_nodes(k)) )
            ny2 = 0.0_dp
            mag2 = sqrt( nx2**2 + ny2**2 )
            nx2 = nx2/mag2
            ny2 = ny2/mag2

    ! Select (nx2,ny2).

       nx = nx2
       ny = ny2

    x(nnodes) = x(left_nodes(k)) + mag*nx
    y(nnodes) = y(left_nodes(k)) + mag*ny

   end do
  end do

!- Generate elements:

  write(*,*) " Before the wake region: nquad = ", nquad

  do k = 1, n_left_nodes-1
   do i = 0, nw-1

     if (i==0) then
     i1 =  left_nodes(k)
     i2 =  nnodes1 + i+1 + (k-1)*(nw)
     i3 =  nnodes1 + i+1 + (k  )*(nw)
     i4 =  left_nodes(k+1)
     else
     i1 =  nnodes1 + i   + (k-1)*(nw)
     i2 =  nnodes1 + i+1 + (k-1)*(nw)
     i3 =  nnodes1 + i+1 + (k  )*(nw)
     i4 =  nnodes1 + i   + (k  )*(nw)
     endif

   !Triangular grid or mixed0
    if    (igrid_type == 2 .or. igrid_type == 3) then

     if ( k < n_left_nodes/2+1 ) then

             ntria = ntria + 1
     tria(ntria,1) = i1
     tria(ntria,2) = i2
     tria(ntria,3) = i3

             ntria = ntria + 1
     tria(ntria,1) = i1
     tria(ntria,2) = i3
     tria(ntria,3) = i4

     else

             ntria = ntria + 1
     tria(ntria,1) = i1
     tria(ntria,2) = i2
     tria(ntria,3) = i4

             ntria = ntria + 1
     tria(ntria,1) = i4
     tria(ntria,2) = i2
     tria(ntria,3) = i3

     endif

   !Quadrilateral/Mixed grid
    else

             nquad = nquad + 1
     quad(nquad,1) = i1
     quad(nquad,2) = i2
     quad(nquad,3) = i3
     quad(nquad,4) = i4

    endif

   end do
  end do

  write(*,*) " After the wake region: nquad = ", nquad

! Boundary nodes (oriented so as to see the interior domain on the left).
! The middle outflow part.

    ib = 2
    bnames( ib) = "outflow_middle"
    nbnodes(ib) = n_left_nodes

   do k = 1, n_left_nodes
    bnode(k,ib) = nnodes1 + (nw) + (k-1)*(nw)
   end do


 !-----------------------------------------------------------------------------------
 !-----------------------------------------------------------------------------------
  if (debug_mode) call write_tecplot_file(filename_tecplot1, nnodes,x,y, ntria,tria, &
                                                                         nquad,quad  )

  if (debug_mode) call write_tecplot_boundary_file(filename_tec,x,y,2,nbnodes,bnode )
 !-----------------------------------------------------------------------------------
 !-----------------------------------------------------------------------------------

  nnodes2 = nnodes

!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!
! Generate a grid in the outer region.
!
! NOTE: The last point may not land precisely on the outer circle.
!
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------

  write(*,*)
  write(*,*) "  Generating the outer-region grid....."


! First define the inner_bnodes: top of the part II. 
  
  n_inner_bnodes = 2*(nw) + (nc-n_left_nodes)
  allocate( inner_bnode(n_inner_bnodes) )

   n_inner_bnodes = 0
  do i = nw, 1, -1
                  n_inner_bnodes = n_inner_bnodes + 1
   inner_bnode( n_inner_bnodes ) = nnodes1 + i + (n_left_nodes-1)*(nw)
  end do

! Second, add nodes of the left part of the cylinder.

  itemp = 0

   do i = nc-nc/4+1 + offset, nc/4 - (offset-1), -1
                           itemp = itemp + 1
                  n_inner_bnodes = n_inner_bnodes + 1
   inner_bnode( n_inner_bnodes ) = i + nr1*nc
  end do

  write(*,*)
  write(*,*) " rest of cylinder nodes = ", itemp
  write(*,*) "      nc - n_left_nodes = ", nc - n_left_nodes 
  write(*,*)

! Finally add the bottom of the part II. 

  do i = 1, nw
                  n_inner_bnodes = n_inner_bnodes + 1
   inner_bnode( n_inner_bnodes ) = nnodes1 + i
  end do

   write(*,*) " n_inner_bnodes = ", n_inner_bnodes

 !-----------------------------------------------------------------------------------
 ! Visualization just for debugging.
 !------------------------------------------------------------------------
  if (debug_mode) then

    ib = 3
    bnames( ib) = "Inner boundary (just for debugging)"
    nbnodes(ib) = n_inner_bnodes

   do k = 1, n_inner_bnodes
      bnode(k,ib) = inner_bnode(k)
   end do

    filename_temp = trim(filename_tec) // "_debug"
    write(*,*) " filename_temp = ", trim( filename_temp )
    call write_tecplot_boundary_file(filename_temp,x,y,3,nbnodes,bnode )
   
!    call euler_check(nnodes,tria,ntria,quad,nquad,x,y, 3,nbnodes,bnode )!

  endif
 !------------------------------------------------------------------------
 !-----------------------------------------------------------------------------------

! Next, define the actual outer boundary nodes.

  n_outer_bnodes = n_inner_bnodes
  allocate( outer_bnode(n_outer_bnodes) )
  allocate(      xouter(n_outer_bnodes) )
  allocate(      youter(n_outer_bnodes) )

! Straight part at the top.

  do i = nw, 1, -1
   xouter((nw+1)-i) = xoutflow + (distance_horizontal)*real(i-nw,dp) /real(nw,dp)
   youter((nw+1)-i) = radius_to_outer
  end do

            xL = xouter(nw)
            yL = youter(nw)
      radius_o = sqrt( xL**2 + yL**2 )
        theta0 = atan2(yL,xL)
   width_theta = two*pi - two*theta0

    write(*,*) "       theta0 = ", theta0/pi*180.0_dp
    write(*,*) "  width_theta = ", width_theta /pi*180.0_dp
    write(*,*) "  sector area = ", (pi*radius_to_outer*radius_to_outer) * (width_theta/(2.0_dp*pi))

   n_outer_bnodes = nw

   io1 = nw+1

   i0 = nc/4 - (offset-1)  ! + 1
   in = nc-nc/4+1 + offset ! - 1

   n_theta = in - i0 + 1 !(nc-nc/4+1 + offset - 1) - (nc/4 - (offset-1) + 1) + 1

! Inflow boundary

 !(1) Circular arc
  do i = 1, n_theta
           n_outer_bnodes = n_outer_bnodes + 1
                   dtheta = width_theta/real(n_theta+1,dp)
                    theta = theta0 + dtheta*real(i,dp)
   xouter(n_outer_bnodes) = radius_o*cos(theta)
   youter(n_outer_bnodes) = radius_o*sin(theta)
  end do

 !(2) Straight line
 ! do i = 1, n_theta
 !          n_outer_bnodes = n_outer_bnodes + 1
 !  xouter(n_outer_bnodes) = -25.0_dp
 !  youter(n_outer_bnodes) = 100.0_dp - real(i-1,dp)*( 200.0_dp/real(n_theta-1,dp) )
 ! end do

   io2 = n_outer_bnodes !+ 1

! Straight part at the bottom.

  do i = 1, nw
   n_outer_bnodes = n_outer_bnodes + 1
   xouter(n_outer_bnodes) = (xoutflow-distance_horizontal) + (distance_horizontal)*real(i,dp) /real(nw,dp)
   youter(n_outer_bnodes) =-radius_to_outer
  end do

   write(*,*)
   write(*,*) "  n_outer_bnodes = ", n_outer_bnodes
   write(*,*) "  n_inner_bnodes = ", n_inner_bnodes
   write(*,*)

! Now generate nodes in between the inner and outer boundaries.

    nnodes = nnodes2

  do k = 1, no

   do i = 1, n_inner_bnodes
       nnodes = nnodes + 1 ! = nnodes2 + i + (k-1)*n_inner_bnodes

   !--------------------------------------------------------------------
   ! Compute the stretching factor sf for a smooth transition.

     sf_initial = 5.0_dp
             xi = 1.0_dp/real(no,dp)

     mag = sqrt( ( xouter(i)-x(inner_bnode(i)) )**2 + ( youter(i)-y(inner_bnode(i)) )**2 )
    call compute_stretching_factor(dr_last,mag,xi,sf_initial, final_x,sf,itr,stat )

   !--------------------------------------------------------------------

          xi  = real(k,dp)/real(no,dp)
          xis = (one-exp(sf*xi))/(one-exp(sf)) 
    x(nnodes) = x(inner_bnode(i)) + ( xouter(i)-x(inner_bnode(i)) )*xis
    y(nnodes) = y(inner_bnode(i)) + ( youter(i)-y(inner_bnode(i)) )*xis

    if (k==no) outer_bnode(i) = nnodes

   end do
  end do

! Generate elements.

  do k = 1, no
   do i = 1, n_inner_bnodes-1

    if (k==1) then
     i4 = inner_bnode(i  )
     i3 = inner_bnode(i+1)
     i2 = nnodes2 + i+1+(k-1)*n_inner_bnodes
     i1 = nnodes2 + i  +(k-1)*n_inner_bnodes
    else
     i4 = nnodes2 + i  +(k-2)*n_inner_bnodes
     i3 = nnodes2 + i+1+(k-2)*n_inner_bnodes
     i2 = nnodes2 + i+1+(k-1)*n_inner_bnodes
     i1 = nnodes2 + i  +(k-1)*n_inner_bnodes
    endif

   !Triangular grid
    if    (igrid_type == 2 .or. igrid_type == 3 .or. igrid_type == 4) then

     if ( i < n_inner_bnodes/2+1 ) then

             ntria = ntria + 1
     tria(ntria,1) = i1
     tria(ntria,2) = i2
     tria(ntria,3) = i3

             ntria = ntria + 1
     tria(ntria,1) = i1
     tria(ntria,2) = i3
     tria(ntria,3) = i4

     else

             ntria = ntria + 1
     tria(ntria,1) = i1
     tria(ntria,2) = i2
     tria(ntria,3) = i4

             ntria = ntria + 1
     tria(ntria,1) = i4
     tria(ntria,2) = i2
     tria(ntria,3) = i3

     endif

   !Quadrilateral grid
    else

             nquad = nquad + 1
     quad(nquad,1) = i1
     quad(nquad,2) = i2
     quad(nquad,3) = i3
     quad(nquad,4) = i4

    endif

   end do
  end do

! Boundary part: outer boundary except outflow

    ib = 3
    bnames( ib) = "farfield_top"
    nbnodes(ib) = io1

   do k = 1, io1
      bnode(k,ib) = outer_bnode(k)
   end do

! Boundary part: outer boundary except outflow

    ib = 4
    bnames( ib) = "farfield_inflow"
    nbnodes(ib) = io2-io1+1

   do k = io1, io2
      bnode(k-io1+1,ib) = outer_bnode(k)
   end do

! Boundary part: outer boundary except outflow

    ib = 5
    bnames( ib) = "farfield_bottom"
    nbnodes(ib) = io1

   do k = io2, n_outer_bnodes
      bnode(k-io2+1,ib) = outer_bnode(k)
   end do

! Boundary part: out flow top

    ib = 6
    bnames( ib) = "outflow_top"
    nbnodes(ib) = no+1

      bnode(1  ,ib) = inner_bnode(1)
   do k = 1, no
      bnode(k+1,ib) = nnodes2 + 1 + (k-1)*n_inner_bnodes
   end do

! Boundary part: out flow bottom

    ib = 7
    bnames( ib) = "outflow_bottom"
    nbnodes(ib) = no+1

   do k = no, 1, -1
      bnode(no-k+1   ,ib) = nnodes2 + n_inner_bnodes + (k-1)*n_inner_bnodes
   end do
      bnode(no+1,ib) = inner_bnode(n_inner_bnodes)

 !-----------------------------------------------------------------------------------
 !-----------------------------------------------------------------------------------
  if (debug_mode) call write_tecplot_file(filename_tecplot2, nnodes,x,y, ntria,tria, &
                                                                         nquad,quad  )

  if (debug_mode) call write_tecplot_boundary_file(filename_tec,x,y,7,nbnodes,bnode )
 !-----------------------------------------------------------------------------------
 !-----------------------------------------------------------------------------------

  write(*,*)
  write(*,*) "  Final dimension:"
  write(*,*) "      nnodes = ", nnodes
  write(*,*) "      nquad  = ", nquad
  write(*,*) "      ntria  = ", ntria
  write(*,*)

!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!
! Check areas.
!
! 35718.27839369060
!
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------

 !-----------------------------------------------------------------------------------
 ! Check duplicated nodes.

 if (1==0) then

   do i = 1, nnodes
    do k = 1, nnodes
     if (i==k) cycle
     if ( sqrt( (x(i)-x(k))**2 + (y(i)-y(k))**2 ) < 1.0e-10_dp ) then
       write(*,*) sqrt( (x(i)-x(k))**2 + (y(i)-y(k))**2 )
       write(*,*) " Same points  i: ", i, x(i), y(i)
       write(*,*) " Same points  k: ", k, x(k), y(k)
       stop
     endif
    end do
   end do

  endif

 !-----------------------------------------------------------------------------------
 ! Check Euler characteristics.

   call euler_check(nnodes,tria,ntria,quad,nquad,x,y, nb,nbnodes,bnode )

 !-----------------------------------------------------------------------------------
 ! Check the cell and total areas.

  write(*,*) " Checking the area..."

 total_area_sum = 0.0_dp

 if (ntria > 0) then

  do i  = 1, ntria

      i1 = tria(i,1)
      i2 = tria(i,2)
      i3 = tria(i,3)

    area = 0.5_dp*( x(i1)*(y(i2)-y(i3)) + x(i2)*(y(i3)-y(i1)) + x(i3)*(y(i1)-y(i2))  )

    if (area < 1.0e-16_dp ) then
     write(*,*) " Negative area triangle = ", i, area
!     stop
    endif

    total_area_sum = total_area_sum + area

  end do

 endif
 
 if (nquad > 0) then

  do i  = 1, nquad
      i1 = quad(i,1)
      i2 = quad(i,2)
      i3 = quad(i,3)
      i4 = quad(i,4)

     area1 = 0.5_dp*( x(i1)*(y(i2)-y(i3)) + x(i2)*(y(i3)-y(i1)) + x(i3)*(y(i1)-y(i2))  )
    if (area1 < 1.0e-16_dp ) then
     write(*,*) " Negative area1 quad = ", i, area1
     write(*,*) "             (x1,y1) = ", x(i1), y(i1)
     write(*,*) "             (x2,y2) = ", x(i2), y(i2)
     write(*,*) "             (x3,y3) = ", x(i3), y(i3)
     write(*,*) "             (x4,y4) = ", x(i4), y(i4)
!     stop
    endif

     area2 = 0.5_dp*( x(i1)*(y(i3)-y(i4)) + x(i3)*(y(i4)-y(i1)) + x(i4)*(y(i1)-y(i3))  ) 

    if (area2 < 1.0e-16_dp ) then
     write(*,*) " Negative area2 quad = ", i, area2
     write(*,*) "             (x1,y1) = ", x(i1), y(i1)
     write(*,*) "             (x2,y2) = ", x(i2), y(i2)
     write(*,*) "             (x3,y3) = ", x(i3), y(i3)
     write(*,*) "             (x4,y4) = ", x(i4), y(i4)
!     stop
    endif

      area = area1 + area2 
     total_area_sum = total_area_sum + area

  end do

 endif
 
   write(*,*) " Total area from sum of cell areas    = ",  total_area_sum

 !-----------------------------------------------------------------------

 total_area_sum_gg = 0.0_dp

 if (ntria > 0) then

  do i  = 1, ntria

      i1 = tria(i,1)
      i2 = tria(i,2)
      i3 = tria(i,3)

      xc = ( x(i1) + x(i2) + x(i3) )/3.0_dp
      yc = ( y(i1) + y(i2) + y(i3) )/3.0_dp

     xx1 = x(i1) - xc
     xx2 = x(i2) - xc
     xx3 = x(i3) - xc

     yy1 = y(i1) - yc
     yy2 = y(i2) - yc
     yy3 = y(i3) - yc

    area = 0.5_dp*( xx1*(yy2-yy3) + xx2*(yy3-yy1) + xx3*(yy1-yy2)  )

    if (area < 1.0e-16_dp ) then
     write(*,*) " Negative area triangle = ", i, area
!     stop
    endif

    total_area_sum_gg = total_area_sum_gg + area

  end do

 endif
 
 if (nquad > 0) then

  do i  = 1, nquad
      i1 = quad(i,1)
      i2 = quad(i,2)
      i3 = quad(i,3)
      i4 = quad(i,4)

      xc = ( x(i1) + x(i2) + x(i3) + x(i4) )/4.0_dp
      yc = ( y(i1) + y(i2) + y(i3) + y(i4) )/4.0_dp

     xx1 = x(i1) - xc
     xx2 = x(i2) - xc
     xx3 = x(i3) - xc
     xx4 = x(i4) - xc

     yy1 = y(i1) - yc
     yy2 = y(i2) - yc
     yy3 = y(i3) - yc
     yy4 = y(i4) - yc

     area1 = 0.5_dp*( xx1*(yy2-yy3) + xx2*(yy3-yy1) + xx3*(yy1-yy2)  )
    if (area1 < 1.0e-16_dp ) then
     write(*,*) " Negative area1 quad = ", i, area1
!     stop
    endif

     area2 = 0.5_dp*( xx1*(yy3-yy4) + xx3*(yy4-yy1) + xx4*(yy1-yy3)  )

    if (area2 < 1.0e-16_dp ) then
     write(*,*) " Negative area2 quad = ", i, area2
!     stop
    endif

      area = area1 + area2 
     total_area_sum_gg = total_area_sum_gg + area

  end do

 endif
 
   write(*,*) " Total area from sum of cell areas LC = ",  total_area_sum_gg

 !-----------------------------------------------------------------------

  total_area_gg = 0.0_dp
 
  do ib = 1, 7

   do j = 1, nbnodes(ib)-1

    xm = 0.5_dp*( x( bnode(j  ,ib) ) + x( bnode(j+1,ib) ) )
    ym = 0.5_dp*( y( bnode(j  ,ib) ) + y( bnode(j+1,ib) ) )

    bx =   (      y( bnode(j+1,ib) ) - y( bnode(j  ,ib) ) )
    by = - (      x( bnode(j+1,ib) ) - x( bnode(j  ,ib) ) )

    total_area_gg = total_area_gg + 0.5_dp * ( xm*bx + ym*by )

   end do

  end do

   write(*,*) " Total area from GG on boundaries     = ",  total_area_gg


   write(*,*) "                      Relative error  = ",  abs(total_area_sum-total_area_gg)/total_area_sum

 !----------------------------------------------------------------------

  sum_bfaces_x     = 0.0_dp
  sum_bfaces_y     = 0.0_dp
  total_bfaces_mag = 0.0_dp
  n_bfaces         = 0.0_dp

   write(*,*)

  do ib = 1, 7

    b_length(ib) = 0.0_dp

   do j = 1, nbnodes(ib)-1

    bx =   (      y( bnode(j+1,ib) ) - y( bnode(j  ,ib) ) )
    by = - (      x( bnode(j+1,ib) ) - x( bnode(j  ,ib) ) )

    sum_bfaces_x = sum_bfaces_x + bx
    sum_bfaces_y = sum_bfaces_y + by

    total_bfaces_mag = total_bfaces_mag + sqrt( bx**2 + by**2 )
            n_bfaces = n_bfaces + 1.0_dp

    b_length(ib) = b_length(ib) + sqrt( bx**2 + by**2 )

   end do

   write(*,*) " ib = ", ib, " length = ", b_length(ib)

  end do

   write(*,*) "            length (2+6+7) = ", b_length(2) + b_length(6) + b_length(7)

   write(*,*)
   write(*,*) " Total sum of outward bface vector: sum bx/total_length = ", sum_bfaces_x/total_bfaces_mag
   write(*,*) " Total sum of outward bface vector: sum by/total_length = ", sum_bfaces_y/total_bfaces_mag
   write(*,*)
  
!--------------------

  bcheckx = 0.0_dp
  bchecky = 0.0_dp

  do ib = 1, 1

   do j = 1, nbnodes(ib)-1

    bcheckx = bcheckx +  x( bnode(j+1,ib) ) - x( bnode(j  ,ib) )
    bchecky = bchecky +  y( bnode(j+1,ib) ) - y( bnode(j  ,ib) )

   end do

  end do

   write(*,*)
   write(*,*) " Boundary 1:"
   write(*,*) " Total sum of outward bedge vector: sum bx/total_length = ", bcheckx/total_bfaces_mag
   write(*,*) " Total sum of outward bedge vector: sum by/total_length = ", bchecky/total_bfaces_mag
   write(*,*)
  

!--------------------

  bcheckx = 0.0_dp
  bchecky = 0.0_dp

  do ib = 2, 7

   do j = 1, nbnodes(ib)-1

    bcheckx = bcheckx +  x( bnode(j+1,ib) ) - x( bnode(j  ,ib) )
    bchecky = bchecky +  y( bnode(j+1,ib) ) - y( bnode(j  ,ib) )

   end do

  end do

   write(*,*)
   write(*,*) " Boundaries 2-7:"
   write(*,*) " Total sum of outward bedge vector: sum bx/total_length = ", bcheckx/total_bfaces_mag
   write(*,*) " Total sum of outward bedge vector: sum by/total_length = ", bchecky/total_bfaces_mag
   write(*,*)

!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!
! Generate a .bcmap file for a 2D solver.
!
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------

  write(*,*)
  write(*,*) " Writing a .bcmap file: ", trim(filename_bcmap)

  open(unit=100, file=filename_bcmap, status="unknown" )

  write(100,*) nb
  do k = 1, nb
  write(100,*) k, trim( bnames(k) )
  end do

!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!
! Generate a .grid file for a 2D solver.
!
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------

  write(*,*)
  write(*,*) "      nnodes = ", nnodes
  write(*,*) "      nquad  = ", nquad
  write(*,*) "      ntria  = ", ntria
  write(*,*)

  if (generate_grid_file) then

   call write_grid_file(filename_grid,nnodes,tria,ntria,quad,nquad,x,y, &
                                                       nb,nbnodes,bnode )
  endif

!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!
! Generate a .grid file for a 2D solver.
!
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------

  if (generate_su2_file) then

   call write_su2_file(filename_su2,nnodes,tria,ntria,quad,nquad,x,y, &
                                              nb,nbnodes,bnode,bnames )
  endif

!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!
! Generate a Tecplot file.
!
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------

  if (generate_tec_file) then

   call write_tecplot_file(filename_tec, nnodes,x,y, ntria,tria, &
                                                     nquad,quad  )

   call write_tecplot_boundary_file(filename_tec,x,y,nb,nbnodes,bnode )

  endif

!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!
! Generate a .vtk file
!
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------

  if (generate_vtk_file) then

   call write_vtk_file(filename_vtk, nnodes,x,y, ntria,tria, &
                                                 nquad,quad  )
  endif

!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------

  write(*,*)
  write(*,*) " A 2D grid successfully generated.... Stop."
  write(*,*)

 contains

!********************************************************************************
 subroutine euler_check(nnodes,tria,ntria,quad,nquad,x,y, nb,nbnodes,bnode )

 implicit none

 integer                      , intent(in) :: nnodes, ntria, nquad
 integer      , dimension(:,:), intent(in) :: tria, quad
 real(dp)     , dimension(:)  , intent(in) :: x, y
 integer                      , intent(in) :: nb
 integer      , dimension(:  ), intent(in) :: nbnodes
 integer      , dimension(:,:), intent(in) :: bnode

!Local variables
 integer  :: i, ncells, nedges, nbedges

   nedges = 0
   ncells = 0
 
 if (ntria > 0) then
  do i = 1, ntria
   ncells = ncells + 1
   nedges = nedges + 3  !Mark 3 edges of the triangle.
  end do
 endif

 if (nquad > 0) then
  do i = 1, nquad
   ncells = ncells + 1
   nedges = nedges + 4  !Mark 4 edges of the quad.
  end do
 endif

   nbedges = 0
  do ib = 1, 7
   do j = 1, nbnodes(ib)-1
    nbedges = nbedges + 1  !Count the # of boundary edges.
   end do
  end do

 !Total # of edges. Note that internal edges have been marked twice, and
 !boudary edges have been marked only once.

            !# of interior edges + # of boundary edges
   nedges = (nedges - nbedges)/2 + nbedges

 write(*,*)
 write(*,*) "  nnodes = ", nnodes
 write(*,*) "  ncells = ", ncells
 write(*,*) "  nedges = ", nedges
 write(*,*)
 write(*,*) "  Check Euler's formula: vertices-edges+cells=2, cells+(cylinder)+(outside)"
 write(*,*) "  nnodes - nedges + (ncells+2) = ", nnodes - nedges + ncells + 2
 write(*,*)

 end subroutine euler_check


!*******************************************************************************
! This subroutine writes a Tecplot file for the grid.
!*******************************************************************************
 subroutine write_tecplot_file(filename, nnodes,x,y, ntria,tria, nquad,quad  )

  implicit none

  character(80),                 intent(in) :: filename
  integer      ,                 intent(in) :: nnodes, ntria, nquad
  real(dp)     , dimension(:  ), intent(in) :: x, y
  integer      , dimension(:,:), intent(in) :: tria
  integer      , dimension(:,:), intent(in) :: quad

!Local variables
  integer :: i, os

 write(*,*)
 write(*,*) ' Writing a Tecplot file = ', trim(filename)
 write(*,*)

  open(unit=9, file=filename, status="unknown", iostat=os)

  write(9,*) 'TITLE = "Grid"'
  write(9,*) 'VARIABLES = "x","y"'
!---------------------------------------------------------------------------
! zone

   write(9,*) 'zone t=', '"grid"'
   write(9,*)'  n=', nnodes,',e=', ntria+nquad,' , et=quadrilateral, f=fepoint'

   do i = 1, nnodes
     write(9,'(2es25.15)') x(i), y(i)
   end do

  if (ntria > 0) then
   do i = 1, ntria
    write(9,'(4i10)') tria(i,1), tria(i,2), tria(i,3), tria(i,3)
   end do
  endif

  if (nquad > 0) then
   do i = 1, nquad
    write(9,'(4i10)') quad(i,1), quad(i,2), quad(i,3), quad(i,4)
   end do
  endif

!---------------------------------------------------------------------------

 close(9)

 end subroutine write_tecplot_file
!********************************************************************************


!********************************************************************************
! This subroutine writes boundary grid files.
!********************************************************************************
 subroutine write_tecplot_boundary_file(filename,x,y,nb,nbnodes,bnode )

 implicit none

 character(80),                 intent(in) :: filename
 real(dp)     , dimension(:)  , intent(in) :: x, y
 integer                      , intent(in) :: nb
 integer      , dimension(:  ), intent(in) :: nbnodes
 integer      , dimension(:,:), intent(in) :: bnode

!Local variables
 character(80) :: filename_loc
 character(80) :: bid
 integer       :: ib, os

  do ib = 1, nb

     write( bid  , '(i0)' ) ib
     filename_loc = trim(filename) // trim("_boundary_") // trim(".") // trim(bid) // '.dat'

   write(*,*) ' Writing a Tecplot file for boundary = ', ib , trim(filename_loc)

   open(unit=10, file=filename_loc, status="unknown", iostat=os)

    write(10,*) 'variables = "x", "y"'
    write(10,*) 'ZONE t="', trim("Boundary.") // trim(bid) ,'" I =', nbnodes(ib), ', DATAPACKING=POINT'

    do i = 1, nbnodes(ib)
     write(10,*) x( bnode(i,ib) ), y( bnode(i,ib) )
    end do

   close(10)

  end do

 end subroutine write_tecplot_boundary_file

!********************************************************************************
! This subroutine writes a grid file to be read by a solver.
! NOTE: Unlike the tecplot file, this files contains boundary info.
!********************************************************************************
 subroutine write_grid_file(filename,nnodes,tria,ntria,quad,nquad,x,y, &
                                                      nb,nbnodes,bnode )

 implicit none

 character(80),                 intent(in) :: filename
 integer                      , intent(in) :: nnodes, ntria, nquad
 integer      , dimension(:,:), intent(in) :: tria, quad
 real(dp)     , dimension(:)  , intent(in) :: x, y
 integer                      , intent(in) :: nb
 integer      , dimension(:  ), intent(in) :: nbnodes
 integer      , dimension(:,:), intent(in) :: bnode

!Local variables
 integer  :: os, i, j

   write(*,*)
   write(*,*) " Writing a .grid file: ", trim(filename)

!--------------------------------------------------------------------------------
 open(unit=1, file=filename, status="unknown", iostat=os)

!--------------------------------------------------------------------------------
! Grid size: # of nodes, # of triangles, # of quadrilaterals
  write(1,*) nnodes, ntria, nquad

!--------------------------------------------------------------------------------
! Node data
  do i = 1, nnodes
   write(1,*) x(i), y(i)
  end do

!--------------------------------------------------------------------------------
! Triangle connectivity
  if (ntria > 0) then
   do i = 1, ntria
    write(1,*) tria(i,1), tria(i,2), tria(i,3)
   end do
  endif

  if (nquad > 0) then
   do i = 1, nquad
    write(1,*) quad(i,1), quad(i,2), quad(i,3), quad(i,4)
   end do
  endif

!--------------------------------------------------------------------------------
! Boundary data:
!
! The number of boundary segments
  write(1,*) nb

! The number of nodes in each segment:
  do i = 1, nb
   write(1,*) nbnodes(i)
  end do

  write(1,*)

! List of boundary nodes
  do i = 1, nb

   if (debug_mode) write(*,*) " write_grid_file: nbnodes(i) = ", nbnodes(i), i

   do j = 1, nbnodes(i)
    write(1,*) bnode(j,i)
   end do
    write(1,*)

  end do

!--------------------------------------------------------------------------------
 close(1)

 end subroutine write_grid_file
!********************************************************************************

!*******************************************************************************
! This subroutine writes a su2 grid file.
!
! Note: Nodes -> i = 0,1,2,...; Elements -> i = 0,1,2,...
!
!
!  Identifier:
!  Line 	 3
!  Triangle 	 5
!  Quadrilateral 9
!  Tetrahedral 	10
!  Hexahedral 	12
!  Prism 	13
!  Pyramid 	14
!
!
!*******************************************************************************
 subroutine write_su2_file(filename,nnodes,tria,ntria,quad,nquad,x,y, &
                                              nb,nbnodes,bnode,bnames )

 character(80),                 intent(in) :: filename
 integer                      , intent(in) :: nnodes, ntria, nquad
 integer      , dimension(:,:), intent(in) :: tria, quad
 real(dp)     , dimension(:)  , intent(in) :: x, y
 integer                      , intent(in) :: nb
 integer      , dimension(:  ), intent(in) :: nbnodes
 integer      , dimension(:,:), intent(in) :: bnode
 character(80), dimension(:)  , intent(in) :: bnames

!Local variables
 integer :: i, ib, j, os

   write(*,*)
   write(*,*) " Writing .su2 file: ", trim(filename)

  open(unit=7, file=filename, status="unknown", iostat=os)

  write(7,*) "%"
  write(7,*) "% Problem dimension"
  write(7,*) "%"
  write(7,5) 2
5 format('NDIME= ',i12)

   write(7,*) "%"
   write(7,*) "% Inner element connectivity"
   write(7,10) ntria + nquad
10 format('NELEM= ',i12)

 !-------------------------------------------------------------------------
 ! Elements

  if (ntria > 0) then
   do i = 1, ntria
    write(7,'(4i20)') 5, tria(i,1)-1, tria(i,2)-1, tria(i,3)-1
   end do
  endif

  if (nquad > 0) then
   do i = 1, nquad
    write(7,'(5i20)') 9, quad(i,1)-1, quad(i,2)-1, quad(i,3)-1, quad(i,4)-1
   end do
  endif

 !--------------------------------------------------------------------------
 ! Nodes

   write(7,*) "%"
   write(7,*) "% Node coordinates"
   write(7,*) "%"
   write(7,20) nnodes
20 format('NPOIN= ', i12)

  ! Nodes
    do i = 1, nnodes
     write(7,'(2es26.15)') x(i), y(i)
    end do

 !--------------------------------------------------------------------------
 ! Boundary

30  format('NMARK= ',i12)
40  format('MARKER_TAG= ',a)
50  format('MARKER_ELEMS= ', i12)

    write(7,*) "%"
    write(7,*) "% Boundary elements"
    write(7,*) "%"
    write(7,30) nb !# of boundary parts.

   do ib = 1, nb

   !-------------------------
   !Just to print on screen
    write(*,*)
    write(*,40) trim(bnames(ib)) !ib-th boundary-part name, e.g., "farfield".
    write(*,50) nbnodes(ib)-1    !# of boundary elements (edges)
   !-------------------------

    write(7,40) trim(bnames(ib)) !ib-th boundary-part name, e.g., "farfield".
    write(7,50) nbnodes(ib)-1    !# of boundary elements (edges)

    do j = 1, nbnodes(ib)-1
     write(7,'(3i20)') 3, bnode(j,ib)-1, bnode(j+1,ib)-1
    end do

   end do

  close(7)

 end subroutine write_su2_file
!********************************************************************************

!*******************************************************************************
! This subroutine writes a .vtk file for the grid whose name is defined by
! filename_vtk.
!
! Use Paraview to read .vtk and visualize it.  https://www.paraview.org
!
! Search in Google for 'vkt format' to learn .vtk file format.
!*******************************************************************************
 subroutine write_vtk_file(filename, nnodes,x,y, ntria,tria, nquad,quad  )

  implicit none

  character(80),                 intent(in) :: filename
  integer      ,                 intent(in) :: nnodes, ntria, nquad
  real(dp)     , dimension(:  ), intent(in) :: x, y
  integer      , dimension(:,:), intent(in) :: tria
  integer      , dimension(:,:), intent(in) :: quad

!Local variables
  integer :: i, j, os

 !------------------------------------------------------------------------------
 !------------------------------------------------------------------------------
 !------------------------------------------------------------------------------

  write(*,*)
  write(*,*) ' Writing .vtk file = ', trim(filename)
  write(*,*)

 !Open the output file.
  open(unit=8, file=filename, status="unknown", iostat=os)

!---------------------------------------------------------------------------
! Header information

  write(8,'(a)') '# vtk DataFile Version 3.0'
  write(8,'(a)') filename
  write(8,'(a)') 'ASCII'
  write(8,'(a)') 'DATASET UNSTRUCTURED_GRID'

!---------------------------------------------------------------------------
! Nodal information
!
! Note: These nodes i=1,nnodes are interpreted as i=0,nnodes-1 in .vtk file.
!       So, later below, the connectivity list for tria and quad will be
!       shifted by -1.

   write(8,*) 'POINTS ', nnodes, ' double'

   do j = 1, nnodes
    write(8,'(3es25.15)') x(j), y(j), zero
   end do

!---------------------------------------------------------------------------
! Cell information.

  !CELLS: # of total cells (tria+quad), total size of the cell list.

  write(8,'(a,i12,i12)') 'CELLS ',ntria+nquad, (3+1)*ntria + (4+1)*nquad

  ! Note: The latter is the number of integer values written below as data.
  !           4 for triangles (# of vertices + 3 vertices), and
  !           5 for quads     (# of vertices + 4 vertices).

  !---------------------------------
  ! 2.1 List of triangles (counterclockwise vertex ordering)

   if (ntria > 0) then
                         ! (# of vertices = 3), 3 vertices in counterclockwise
    do i = 1, ntria
     write(8,'(a,4i12)') '3', tria(i,1)-1, tria(i,2)-1, tria(i,3)-1
                         ! -1 since VTK reads the nodes as 0,1,2,3,..., not 1,2,3,..
    end do
   endif

  !---------------------------------
  ! 2.2 List of quads (counterclockwise vertex ordering)

   if (nquad > 0) then
                         ! (# of vertices = 4), 4 vertices in counterclockwise
    do i = 1, nquad
     write(8,'(a,4i12)') '4', quad(i,1)-1, quad(i,2)-1, quad(i,3)-1, quad(i,4)-1
                         ! -1 since VTK reads the nodes as 0,1,2,3,..., not 1,2,3,..
    end do
   endif

!---------------------------------------------------------------------------
! Cell type information.

                                   !# of all cells
  write(8,'(a,i11)') 'CELL_TYPES ', ntria+nquad

  !Triangle is classified as the cell type 5 in the .vtk format.

  if (ntria > 0) then
   do i = 1, ntria
    write(8,'(i3)') 5
   end do
  endif

  !Triangle is classified as the cell type 9 in the .vtk format.

  if (nquad > 0) then
   do i = 1, nquad
    write(8,'(i3)') 9
   end do
  endif

!--------------------------------------------------------------------------------
! NOTE: Commented out because there are no solution data. This part should be
!       uncommented if this is used in a solver or if solution data are available
!       and one would like to plot them.
!
! Field data (e.g., density, pressure, velocity)are added here for visualization.

!   write(8,*) 'POINT_DATA   ',nnodes

!  !            FIELD  dataName    # of arrays (variables to plot) <--This is a commnet.
!   write(8,*) 'FIELD  FlowField ', 4

!   write(8,*) 'Density   ',  1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,1)
!   end do

!   write(8,*) 'X-velocity ', 1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,2)
!   end do

!   write(8,*) 'Y-veloiity ', 1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,3)
!   end do

!   write(8,*) 'Pressure   ', 1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,4)
!   end do

!---------------------------------------------------------------------------

 !Close the output file. <--This is a commnet.
  close(8)


 end subroutine write_vtk_file
!********************************************************************************


!********************************************************************************
! This solves target_x - cx*(one-exp(sf*xi))/(one-exp(sf)) for sf.
!
! E.g., If one wants to find the stretching factor sf such that
!        (rmax-rmin)*(one-exp(sf*xi))/(one-exp(sf)) = desired spacing,
!       where xi = 1*(rmax-rmin)/n (=1st increment in the uniform spacing),
!       Set cx = rmax-rmin, and target_x=desired spacing.
!
!********************************************************************************
 subroutine compute_stretching_factor(target_x,cx,xi,sf_initial, final_x,sf,itr,stat )
 implicit none

 real(dp), intent( in) :: target_x, cx, xi, sf_initial
 real(dp), intent(out) :: final_x, sf
 integer , intent(out) :: itr, stat

 real(dp)              :: res, dres, gr

 
  stat = 0
   itr = 0
    sf = sf_initial

 do

   !Equation to sovle: res = 0.0.

       gr = (one-exp(sf*xi))/(one-exp(sf))
      res =  target_x - cx*gr

      final_x = gr*cx

     if (debug_mode) write(1000,'(a,i6,a,es10.2,a,es10.2,a,es10.2,a,es10.2)') &
       "   itr=", itr, " current_x = ", final_x, " target_x = ", target_x, &
       " sf=",sf,"  res=", abs(res/target_x)

     if (abs(res)/target_x < 1.0e-08_dp) exit !<--Smaller the better.

   ! The following doesn't work. This creates a trivial solution sf=0
   ! and Newton's method converges to it.
   !     res =  dr1*(one-exp(sf)) - rmax*(one-exp(sf*xi))
   !    dres = -dr1*     exp(sf)  +   xi*rmax*exp(sf*xi)

   !So, we solve directly: target_x - cx*(one-exp(sf*xi))/(one-exp(sf))
   !Its derivative is 
    dres = - cx*( -xi*exp(sf*xi)*(one-exp(sf)) + exp(sf)*(one-exp(sf*xi)) )/(one-exp(sf))**2

   !Newton iteration:
      sf = sf - res/dres

  itr = itr + 1
  if (itr > 500) then
    stat = 1
    exit
  endif

  if (abs(sf) > 1.0e+04) then
   write(1000,*) " Too large sf ... = ", sf 
   stop
  endif

 end do

   if (sf < 0.0_dp) stat = 2 !Negative stretching factor.

 end subroutine compute_stretching_factor
!********************************************************************************


 end program edu2d_cgrid_generation_cylinder
