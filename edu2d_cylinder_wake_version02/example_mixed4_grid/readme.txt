######################################################################
#
# This is an instruction of how to generate a 2D grid and then a 3D grid.
#
######################################################################
 
# Compile teh 2d grid generation code

gfortran -o edu2d_cylinder_wake_2nd_kind ../edu2d_cylinder_wake_2nd_kind.f90
 
# Run it to generate a 2D grid. 
# This will read the input file: input.nml.

./edu2d_cylinder_wake_2nd_kind
 
######################################################################
# If you want, you can generate a 3D grid by extending the 2D grid.
# Compile the 2D-to-3D grid generation code.

gfortran -o edu2d_twod2threed_v9 ../edu2d_twod2threed_v9.f90

# Run the code. This will read the input file: input_twod2threed.nml.

./edu2d_twod2threed_v9


