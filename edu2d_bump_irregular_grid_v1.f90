!********************************************************************************
!* --- Grid Generation for a Bump in a Channel ---
!*
!*        written by Dr. Katate Masatsuka (info[at]cfdbooks.com),
!*
!* the author of useful CFD books, "I do like CFD" (http://www.cfdbooks.com).
!*
!* This program generates a triangular grid for a small bump in a channel.
!*
!* Input ---------------------------------------------------
!*              nx = Number of nodes in x-direction.
!*              ny = Number of nodes in y-direction.
!*            xmin = Left end of the rectangular domain
!*            xmax = Right end of the domain
!*            ymin = Top end of the domain
!*            ymax = Bottom end of the domain (except the bump)
!*
!* Output --------------------------------------------------
!*  "bump.grid"         = A grid data file containing boundary-node information.
!*                        This file is read by the EDU2D-Euler solver.
!*  "bump.bcmap"        = Boundary condition file for the EDU2D-Euler solver.
!*  "bump_grid_tec.dat" = Tecplot file for viewing the grid.
!*  "bump.su2"          = SU2 grid file
!*  "bump.vtk"          = VTK file
!*
!* This is Version 1 (July 2019).
!* This F90 code is written and made available for an educational purpose.
!* This file may be updated in future.
!*
!* Katate Masatsuka, January 2013. http://www.cfdbooks.com
!********************************************************************************
 program twod_bump_grid

 implicit none

!Local parameters
  integer, parameter :: sp = kind(1.0)
  integer, parameter :: p2 = selected_real_kind(2*precision(1.0_sp))
  real(p2) :: zero=0.0_p2, half=0.5_p2, one=1.0_p2, pi=3.141592653589793238_p2

!Local variables

!Input Parameters
 integer  ::   nx,   ny ! Number of nodes in x and y
 real(p2) :: xmin, xmax ! Define left end and right end of the domain
 real(p2) :: ymin, ymax ! Define top and bottom of the domain

!Grid file names
 character(80) :: datafile_tria     = "bump.grid"         ! Grid file for EDU2D-Euler
 character(80) :: datafile_bcmap    = "bump.bcmap"        !   BC file for EDU2D-Euler
 character(80) :: datafile_tria_tec = "bump_grid_tec.dat" ! Tecplot file

 character(80) :: filename_su2 = "bump.su2"
 character(80) :: filename_vtk = "bump.vtk"

!Local variables
 real(p2), dimension(:,:), allocatable :: xs, ys ! Structured grid data
 integer                               :: nnodes ! Number of total nodes
 integer                               :: ntri   ! Number of triangles
 integer , dimension(:,:), allocatable :: tri    ! Triangle data
 real(p2), dimension(:)  , allocatable :: x, y   ! 1D arrays for x and y
 integer  :: i, j  !
 integer  :: inode !
 real(p2) :: dx    ! Grid spacing in x-direction (constant)
 real(p2) :: dy    ! Grid spacing in y-direction (variable)
 real(p2) :: rn    ! Random number for nodal perturbation
 real(p2) :: sf, s, smin, smax ! Parameters for grid stretching in y

 integer                                :: ib, ic
 integer                                :: nb      !# of boundaries
 integer      , dimension(:  ), pointer :: nbnodes !# of nodes in each boundary
 integer      , dimension(:,:), pointer :: bnode   !List of boundary nodes
 character(80), dimension(:  ), pointer :: bnames  !Boundary names

 integer                               :: nquad
 integer , dimension(:,:), allocatable :: quad

 nquad = 0
 allocate( quad(1,1) ) 

 write(*,*) "***************************************************************"
 write(*,*) " Start generating a grid for a bump in a channel."
 write(*,*) "***************************************************************"
 write(*,*)

!--------------------------------------------------------------------------------
! 1. Input parameters
!
! ymax --------------------------------------
!      .                                    .
!      .                                    .
!      .                                    .
!      .                                    .
!      .                                    .
!      .               Bump                 .
! ymin -----------------/\-------------------
!    xmin                                  xmax
!
     nx   = 86     ! Number of nodes in x-direction
     ny   = 16     ! Number of nodes in y-direction
     xmin = -2.0_p2 ! x-coordinate of the left end
     xmax =  3.0_p2 ! x-coordinate of the right end
     ymin =  0.0_p2 ! y-coordinates of the bottom
     ymax =  2.0_p2 ! y-coordinates of the top

!--------------------------------------------------------------------------------
! 2. Allocate arrays (temporary structured (i,j) data arrays)

     allocate(xs(nx,ny),ys(nx,ny))

!--------------------------------------------------------------------------------
! 3. Generate a Uniform 2D grid, using (i,j) data: go up in y-direction!

     write(*,*) "Generating structured data..."

!    Initial spacing (dy will be modified later)

        dx = (xmax-xmin)/real(nx-1) ! Uniform spacing in x
        dy = (ymax-ymin)/real(ny-1) ! This will be changed below.

!    Generate nodes in (i,j) format (uniform spacing here also)

     x_direction : do i = 1, nx
      y_direction : do j = 1, ny

      xs(i,j) = xmin + dx*real(i-1)

      if ( j == 1 ) then ! Bottom

       if ( xs(i,j) >= 0.3_p2 .and. xs(i,j) <=1.2_p2 ) then

 !      Here is a bump (same as the one in http://turbmodels.larc.nasa.gov).
        ys(i,j) = ymin + 0.05_p2*(sin(pi*xs(i,j)/0.9_p2-(pi/3.0_p2)))**4
             dy = (ymax - ys(i,j)) / real(ny-1) ! Unoform spacing from the bump

       else

             dy = (ymax - ymin) / real(ny-1)    ! Uniform spacing from y=ymin
        ys(i,j) = ymin + dy*real(j-1)

       endif

      else

        ys(i,j) = ys(i,1) + dy*real(j-1)

      endif

      end do y_direction
     end do x_direction

!--------------------------------------------------------------------------------
! 4. Stretching in y-direction: clustering towards the bottom.

     write(*,*) "Stretching in y-direction..."

     sf = 3.5_p2 ! 1.0 for no stretching

     do i = 1, nx
       smin = ys(i, 1)
       smax = ys(i,ny)
      do j = 1, ny
             s = (ys(i,j)-smin)/(smax-smin)    ! Transform ys into s = [0,1].
             s = (one-exp(sf*s))/(one-exp(sf)) ! Stretching in [0,1]
       ys(i,j) = s*(smax-smin) + smin          ! Transform back to ys.
      end do
     end do

!--------------------------------------------------------------------------------
! 5. Perturb the nodal coordinates to generate an irregular grid

     write(*,*) "Perturbing nodal coordinates..."

     do j = 2, ny-1
      do i = 2, nx-1
       call random_number(rn) ! rn = [0,1]
            dx = half*( xs(i+1,j  ) - xs(i-1,j  ) )
            dy = half*( ys(i  ,j+1) - ys(i  ,j-1) )
       xs(i,j) = xs(i,j) + (rn-half)* dx*0.2d0
       ys(i,j) = ys(i,j) - (rn-half)* dy*0.2d0
      end do
     end do

!--------------------------------------------------------------------------------
! 6. Generate unstructured data (1D arrays for x and y, and triangles):

     write(*,*) "Generating unstructured data..."
     nnodes = nx*ny
     allocate(x(nnodes),y(nnodes))

!    Node data (1D array) in lexcographic ordering

     do j = 1, ny
      do i = 1, nx
       inode = i + (j-1)*nx
       x(inode) =   xs(i,j)
       y(inode) =   ys(i,j)
      end do
     end do

!    Deallocate the structured data: xs and ys, which are not needed any more.
!    - You guys helped me create the 1D array. Thanks!

     deallocate(xs, ys)

!--------------------------------------------------------------------------------
! 7. Generate unstructured data: up to this point, we have a quadrilateral grid.

     allocate( tri(2*(nx-1)*(ny-1),3) )

!    Triangular grid (create triangles by randomly inserting diagonals into quads)

     write(*,*) "Generating triangular grid..."
     call generate_tri_grid(tri, ntri, nx, ny)


!    Write a Tecplot file

     write(*,*) "Writing a Tecplot file...", datafile_tria_tec
     call write_tecplot_file(datafile_tria_tec, nnodes, tri, ntri, x, y)


!    Write a grid file for the EDU2D-Euler solver

     write(*,*) "Writing a grid file for the EDU2D-Euler solver...", datafile_tria
     call write_grid_file(datafile_tria, nnodes, tri, ntri, x, y)


!    Write bcmap file for the EDU2D-Euler solver

     write(*,*) "Writing a BC file for the EDU2D-Euler solver...", datafile_bcmap
     open(unit=1, file=datafile_bcmap, status="unknown")
     write(1,*) '  Boundary Segment              Boundary Condition'
     write(1,*) '              1                 "slip_wall_weak"  '
     write(1,*) '              2                 "freestream"      '
     write(1,*) '              3                 "freestream"      '
     write(1,*) '              4                 "freestream"      '
     close(1)



!-------------------------------------------------------------------------------
!
! Boundary info
!
!-------------------------------------------------------------------------------

  !Boundary data
   nb = 4
   allocate( bnode( max(ny,nx), nb) )
   allocate( nbnodes(nb) )
   allocate(  bnames(nb) )

! Bottom
      ib = 1
      bnames( ib) = "bottom"
      nbnodes(ib) = nx
                j = 1
               ic = 0
    do i = 1, nx
            inode = i + (j-1)*(nx)
               ic = ic + 1
     bnode(ic,ib) = inode
    end do

! Right (outflow)
      ib = 2
      bnames( ib) = "outflow"
      nbnodes(ib) = ny
                i = nx
               ic = 0
    do j = 1, ny
            inode = i + (j-1)*(nx)
               ic = ic + 1
     bnode(ic,ib) = inode
    end do

! Top
      ib = 3
      bnames( ib) = "top"
      nbnodes(ib) = nx
                j = ny
               ic = 0
    do i = nx, 1, -1
            inode = i + (j-1)*(nx)
               ic = ic + 1
     bnode(ic,ib) = inode
    end do

! Left (inflow)
      ib = 4
      bnames( ib) = "inflow"
      nbnodes(ib) = ny
                i = 1
               ic = 0
    do j = ny, 1, -1
            inode = i + (j-1)*(nx)
               ic = ic + 1
     bnode(ic,ib) = inode
    end do

!-------------------------------------------------------------------------------
!
! Generate a .grid file for a 2D solver.
!
!-------------------------------------------------------------------------------

   call write_su2_file(filename_su2,nnodes,tri,ntri,quad,nquad,x,y, &
                                            nb,nbnodes,bnode,bnames )

!-------------------------------------------------------------------------------
!
! Generate a .vtk file
!
!-------------------------------------------------------------------------------

   call write_vtk_file(filename_vtk, nnodes,x,y, ntri,tri, &
                                               nquad,quad  )

!-------------------------------------------------------------------------------


!--------------------------------------------------------------------------------

     write(*,*) " nx=",nx, " ny=", ny
     write(*,*) " nx*ny=",nx*ny
     write(*,*) " nnodes=",nnodes
     write(*,*) " Successfully completed grid generation. Stop."
     write(*,*)
     write(*,*)
     write(*,*) "***************************************************************"
     write(*,*) " End of grid generation."
     write(*,*) "***************************************************************"

     stop

 contains

!********************************************************************************
 subroutine generate_tri_grid(tri, ntri, nx, ny)
 implicit none
 integer,                 intent( in) ::   nx, ny 
 integer,                 intent(out) :: ntri
 integer, dimension(:,:), intent(out) ::  tri
!Local variables
 integer  :: i, j, inode, i1, i2, i3, i4
 real(p2) :: rn, rn2

! Trianguler grid with random diagonals, i.e., / or \.

 ntri = 0

 do j = 1, ny-1
  do i = 1, nx-1

   inode = i + (j-1)*nx
      i1 = inode
      i2 = inode + 1
      i3 = inode + nx + 1
      i4 = inode + nx

   call random_number(rn)
   rn2 = 2.0d0*rn-1.0d0

   if (rn2 > 0.0d0 ) then
!! /
           ntri = ntri + 1
    tri(ntri,1) = i1
    tri(ntri,2) = i2
    tri(ntri,3) = i3

           ntri = ntri + 1
    tri(ntri,1) = i1
    tri(ntri,2) = i3
    tri(ntri,3) = i4

   else

!! \
           ntri = ntri + 1
    tri(ntri,1) = i1
    tri(ntri,2) = i2
    tri(ntri,3) = i4

           ntri = ntri + 1
    tri(ntri,1) = i2
    tri(ntri,2) = i3
    tri(ntri,3) = i4

   endif

  end do
 end do

 end subroutine generate_tri_grid
!********************************************************************************

!********************************************************************************
 subroutine write_tecplot_file(datafile_tec,nnodes,tri,ntri,x,y)
 implicit none
 character(80),            intent(in) :: datafile_tec
 integer ,                 intent(in) :: ntri, nnodes
 integer , dimension(:,:), intent(in) ::  tri
 real(p2), dimension(:)  , intent(in) :: x , y
 integer :: os
!--------------------------------------------------------------------------------
 open(unit=1, file=datafile_tec, status="unknown", iostat=os)
 write(1,*) 'title = "grid"'
 write(1,*) 'variables = "x","y",'
 write(1,*) 'zone N=',nnodes,',E=',ntri, ',ET=quadrilateral,F=FEPOINT'
!--------------------------------------------------------------------------------
 do i = 1, nnodes
  write(1,*) x(i),y(i)
 end do
!--------------------------------------------------------------------------------
! Both elements in quad format:
 do i = 1, ntri
  write(1,*)  tri(i,1),  tri(i,2), tri (i,3),  tri(i,3) !The last one is a dummy.
 end do
!--------------------------------------------------------------------------------
 close(1)
 end subroutine write_tecplot_file
!********************************************************************************

!********************************************************************************
!* This generates a grid file to be read by EDU2D-Euler solver.
!********************************************************************************
 subroutine write_grid_file(datafile_tec,nnodes,tri,ntri,x,y)
 implicit none
 character(80),            intent(in) :: datafile_tec
 integer ,                 intent(in) :: ntri, nnodes
 integer , dimension(:,:), intent(in) :: tri
 real(p2), dimension(:)  , intent(in) :: x, y
 integer :: os
!--------------------------------------------------------------------------------
 open(unit=1, file=datafile_tec, status="unknown", iostat=os)
!--------------------------------------------------------------------------------
! Grid size: # of nodes, # of triangles, # of quadrilaterals
  write(1,*) nnodes, ntri, 0 !<- 0 indicates there are no quadrilaterals.
!--------------------------------------------------------------------------------
! Node data
  do i = 1, nnodes
   write(1,*) x(i), y(i)
  end do
!--------------------------------------------------------------------------------
! Triangles: 3 node numbers that forms a triangle
   do i = 1, ntri
    write(1,*) tri(i,1), tri(i,2), tri(i,3) 
   end do

! Boundary nodes are ordered counterclockwise (seeing the interior domain on the left)
  write(1,*) 4  !4 boundary segments
  write(1,*) nx !Number of nodes on the Bottom
  write(1,*) ny !Number of nodes on the Right
  write(1,*) nx !Number of nodes on the Top
  write(1,*) ny !Number of nodes on the Left

! Bottom
  j = 1
   do i = 1, nx
    inode = i + (j-1)*nx
    write(1,*) inode, 0
   end do
! Right
  i = nx
  do j = 1, ny
    inode = i + (j-1)*nx
    write(1,*) inode, 0
  end do
! Top
  j = ny
   do i = nx, 1, -1
    inode = i + (j-1)*nx
    write(1,*) inode, 0
   end do
! Left
  i = 1
  do j = ny, 1, -1
    inode = i + (j-1)*nx
    write(1,*) inode, 0
  end do

 close(1)

 end subroutine write_grid_file
!********************************************************************************







!*******************************************************************************
! This subroutine writes a su2 grid file.
!
! Note: Nodes -> i = 0,1,2,...; Elements -> i = 0,1,2,...
!
!
!  Identifier:
!  Line 	 3
!  Triangle 	 5
!  Quadrilateral 9
!  Tetrahedral 	10
!  Hexahedral 	12
!  Prism 	13
!  Pyramid 	14
!
!
!*******************************************************************************
 subroutine write_su2_file(filename,nnodes,tria,ntria,quad,nquad,x,y, &
                                              nb,nbnodes,bnode,bnames )

 character(80),                 intent(in) :: filename
 integer                      , intent(in) :: nnodes, ntria, nquad
 integer      , dimension(:,:), intent(in) :: tria, quad
 real(p2)     , dimension(:)  , intent(in) :: x, y
 integer                      , intent(in) :: nb
 integer      , dimension(:  ), intent(in) :: nbnodes
 integer      , dimension(:,:), intent(in) :: bnode
 character(80), dimension(:)  , intent(in) :: bnames

!Local variables
 integer :: i, ib, j, os

   write(*,*)
   write(*,*) " Writing .su2 file: ", trim(filename)

  open(unit=7, file=filename, status="unknown", iostat=os)

  write(7,*) "%"
  write(7,*) "% Problem dimension"
  write(7,*) "%"
  write(7,5) 2
5 format('NDIME= ',i12)

   write(7,*) "%"
   write(7,*) "% Inner element connectivity"
   write(7,10) ntria + nquad
10 format('NELEM= ',i12)

 !-------------------------------------------------------------------------
 ! Elements

  if (ntria > 0) then
   do i = 1, ntria
    write(7,'(4i20)') 5, tria(i,1)-1, tria(i,2)-1, tria(i,3)-1
   end do
  endif

  if (nquad > 0) then
   do i = 1, nquad
    write(7,'(5i20)') 9, quad(i,1)-1, quad(i,2)-1, quad(i,3)-1, quad(i,4)-1
   end do
  endif

 !--------------------------------------------------------------------------
 ! Nodes

   write(7,*) "%"
   write(7,*) "% Node coordinates"
   write(7,*) "%"
   write(7,20) nnodes
20 format('NPOIN= ', i12)

  ! Nodes
    do i = 1, nnodes
     write(7,'(2es26.15)') x(i), y(i)
    end do

 !--------------------------------------------------------------------------
 ! Boundary

30  format('NMARK= ',i12)
40  format('MARKER_TAG= ',a)
50  format('MARKER_ELEMS= ', i12)

    write(7,*) "%"
    write(7,*) "% Boundary elements"
    write(7,*) "%"
    write(7,30) nb !# of boundary parts.

   do ib = 1, nb

   !-------------------------
   !Just to print on screen
    write(*,*)
    write(*,40) trim(bnames(ib)) !ib-th boundary-part name, e.g., "farfield".
    write(*,50) nbnodes(ib)-1    !# of boundary elements (edges)
   !-------------------------

    write(7,40) trim(bnames(ib)) !ib-th boundary-part name, e.g., "farfield".
    write(7,50) nbnodes(ib)-1    !# of boundary elements (edges)

    do j = 1, nbnodes(ib)-1
     write(7,'(3i20)') 3, bnode(j,ib)-1, bnode(j+1,ib)-1
    end do

   end do

  close(7)

 end subroutine write_su2_file
!********************************************************************************


!*******************************************************************************
! This subroutine writes a .vtk file for the grid whose name is defined by
! filename_vtk.
!
! Use Paraview to read .vtk and visualize it.  https://www.paraview.org
!
! Search in Google for 'vkt format' to learn .vtk file format.
!*******************************************************************************
 subroutine write_vtk_file(filename, nnodes,x,y, ntria,tria, nquad,quad  )

  implicit none

  character(80),                 intent(in) :: filename
  integer      ,                 intent(in) :: nnodes, ntria, nquad
  real(p2)     , dimension(:  ), intent(in) :: x, y
  integer      , dimension(:,:), intent(in) :: tria
  integer      , dimension(:,:), intent(in) :: quad

!Local variables
  integer :: i, j, os

 !------------------------------------------------------------------------------
 !------------------------------------------------------------------------------
 !------------------------------------------------------------------------------

  write(*,*)
  write(*,*) ' Writing .vtk file = ', trim(filename)
  write(*,*)

 !Open the output file.
  open(unit=8, file=filename, status="unknown", iostat=os)

!---------------------------------------------------------------------------
! Header information

  write(8,'(a)') '# vtk DataFile Version 3.0'
  write(8,'(a)') filename
  write(8,'(a)') 'ASCII'
  write(8,'(a)') 'DATASET UNSTRUCTURED_GRID'

!---------------------------------------------------------------------------
! Nodal information
!
! Note: These nodes i=1,nnodes are interpreted as i=0,nnodes-1 in .vtk file.
!       So, later below, the connectivity list for tria and quad will be
!       shifted by -1.

   write(8,*) 'POINTS ', nnodes, ' double'

   do j = 1, nnodes
    write(8,'(3es25.15)') x(j), y(j), zero
   end do

!---------------------------------------------------------------------------
! Cell information.

  !CELLS: # of total cells (tria+quad), total size of the cell list.

  write(8,'(a,i12,i12)') 'CELLS ',ntria+nquad, (3+1)*ntria + (4+1)*nquad

  ! Note: The latter is the number of integer values written below as data.
  !           4 for triangles (# of vertices + 3 vertices), and
  !           5 for quads     (# of vertices + 4 vertices).

  !---------------------------------
  ! 2.1 List of triangles (counterclockwise vertex ordering)

   if (ntria > 0) then
                         ! (# of vertices = 3), 3 vertices in counterclockwise
    do i = 1, ntria
     write(8,'(a,4i12)') '3', tria(i,1)-1, tria(i,2)-1, tria(i,3)-1
                         ! -1 since VTK reads the nodes as 0,1,2,3,..., not 1,2,3,..
    end do
   endif

  !---------------------------------
  ! 2.2 List of quads (counterclockwise vertex ordering)

   if (nquad > 0) then
                         ! (# of vertices = 4), 4 vertices in counterclockwise
    do i = 1, nquad
     write(8,'(a,4i12)') '4', quad(i,1)-1, quad(i,2)-1, quad(i,3)-1, quad(i,4)-1
                         ! -1 since VTK reads the nodes as 0,1,2,3,..., not 1,2,3,..
    end do
   endif

!---------------------------------------------------------------------------
! Cell type information.

                                   !# of all cells
  write(8,'(a,i11)') 'CELL_TYPES ', ntria+nquad

  !Triangle is classified as the cell type 5 in the .vtk format.

  if (ntria > 0) then
   do i = 1, ntria
    write(8,'(i3)') 5
   end do
  endif

  !Triangle is classified as the cell type 9 in the .vtk format.

  if (nquad > 0) then
   do i = 1, nquad
    write(8,'(i3)') 9
   end do
  endif

!--------------------------------------------------------------------------------
! NOTE: Commented out because there are no solution data. This part should be
!       uncommented if this is used in a solver or if solution data are available
!       and one would like to plot them.
!
! Field data (e.g., density, pressure, velocity)are added here for visualization.

!   write(8,*) 'POINT_DATA   ',nnodes

!  !            FIELD  dataName    # of arrays (variables to plot) <--This is a commnet.
!   write(8,*) 'FIELD  FlowField ', 4

!   write(8,*) 'Density   ',  1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,1)
!   end do

!   write(8,*) 'X-velocity ', 1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,2)
!   end do

!   write(8,*) 'Y-veloiity ', 1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,3)
!   end do

!   write(8,*) 'Pressure   ', 1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,4)
!   end do

!---------------------------------------------------------------------------

 !Close the output file. <--This is a commnet.
  close(8)


 end subroutine write_vtk_file
!********************************************************************************



 end program twod_bump_grid
