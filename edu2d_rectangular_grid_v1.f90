!********************************************************************************
! This program generates 2D quad and triangular grids in a rectangular domain.
!
!
! 2 boundary configurations:
!
! (1)4 boundary parts: n_boundary = 4.
!
!
!                                 Top(4)
!                         --------------------
!                         |                  |
!                         |                  |  
!                         |                  |
!                 Left(1) |                  | Right(3)
!                         |                  |
!                         |                  |
!                         |                  |
!                         --------------------
!                               Bottom(2)
!
!
! (2)5 boundary parts: n_boundary = 5.
!
!    E.g., One can use this grid with left_top=inflow and left_bottom=wall
!          for shock diffraction, backward facing step, etc.
!
!                               Top(5)
!                         --------------------
!                         |                  |     o:Corner node
!             Left_top(1) |                  |
!                         |                  |
!                  .......o                  |Right(4)
!                         |                  |
!          Left_bottom(2) |                  |
!                         |                  |
!                         --------------------
!                               Bottom(3)
!
! 3-Step Generation:
!
! 1. Generate a temporary structured grid data for nodes: xs(i,j) and ys(i,j)
! 2. Generate a 1D node array: x(1:nnodes), y(1:nnodes)
! 3. Generate element connectivity data: tria(1:ntria,3), quad(1:nquad,4)
!
!
! Inuput: 
!        xmin, xmax = x-coordinates of the left and right ends
!        ymin, ymax = y-coordinates of the bottom and top ends
!                nx = number of nodes in x-direction
!                ny = number of nodes in y-direction
!        n_boundary = 4 or 5
!
!        (NOTE: All input parameters are defined inside the program.)
!
! Output:
!        tria_grid_tecplot.dat = tecplot file of the triangular grid
!        quad_grid_tecplot.dat = tecplot file of the quadrilateral grid
!                    tria.grid = triangular-grid file for EDU2D-Euler solver
!                    quad.grid = quadrilateral-grid file for EDU2D-Euler solver
!                project.bcmap = file that contains boundary condition info
!                    tria.su2  = triangular-grid file for SU2
!                    quad.su2  = quadrilateral-grid file for SU2
!                    tria.vtk  = triangular-grid file in .vtk.
!                    quad.vtk  = quadrilateral-grid file in .vtk.
!
!
!        written by Dr. Katate Masatsuka (info[at]cfdbooks.com),
!
! the author of useful CFD books, "I do like CFD" (http://www.cfdbooks.com).
!
! This is Version 1 (July 2019).
!
! This F90 code is written and made available for an educational purpose as well
! as for generating grids for the EDU2D-Euler code.
! This file may be updated in future.
!
! Katate Masatsuka, July 2019. http://www.cfdbooks.com
!********************************************************************************
 program twod_rectangular_grid

 implicit none

!Parameters
  integer, parameter :: sp = kind(1.0)
  integer, parameter :: p2 = selected_real_kind(2*precision(1.0_sp))
  real(p2) :: zero=0.0_p2, one=1.0_p2

!Input  - domain size and grid dimensions
 real(p2) :: xmin, xmax !Minimum x and Max x.
 real(p2) :: ymin, ymax !Minimum y and Max y
 integer  :: nx         !Number of nodes in x-direction
 integer  :: ny         !Number of nodes in y-direction
 integer  :: n_boundary !Number of boundary parts

!Output - grid files
 character(80) :: filename_bcmap
 character(80) :: filename_grid
 character(80) :: filename_su2
 character(80) :: filename_vtk
 character(80) :: filename_tec
 character(80) :: filename_tec_b

!Local variables
 real(p2), dimension(:,:), allocatable :: xs, ys !Structured grid data

 integer :: nnodes !Total number of nodes
 integer ::  ntria !Total number of triangles
 integer ::  nquad !Total number of quadrilaterals
 integer ::  inode !Local variables used in the 1D nodal array

 integer , dimension(:,:), allocatable :: tria !Triangle connectivity data
 integer , dimension(:,:), allocatable :: quad !Quad connectivity data
 real(p2), dimension(:)  , allocatable :: x, y !Nodal coordinates, 1D array

 real(p2) :: dx !Uniform grid spacing in x-direction = (xmax-xmin)/nx
 real(p2) :: dy !Uniform grid spacing in y-direction = (ymax-ymin)/ny
 integer  :: i, j

 !- Boundary data requied by a solver.
 !   These are needed for .grid and .su2 files.
 integer                                :: ib, ic
 integer                                :: nb      !# of boundaries
 integer      , dimension(:  ), pointer :: nbnodes !# of nodes in each boundary
 integer      , dimension(:,:), pointer :: bnode   !List of boundary nodes
 character(80), dimension(:  ), pointer :: bnames  !Boundary names

!--------------------------------------------------------------------------------
! 0. Define the grid size and allocate the structured grid data array.
!
! ymax --------------------------------------
!      .                                    .
!      .                                    .
!      .                                    .
!      .                                    .
!      .                                    .
!      .                                    .
!      .                                    .
!      .                                    .
!      .                                    .
!      .                                    .
!      .                                    .
!      .                                    .
!      .                                    .
!      .                                    .
!      .                                    .
! ymin --------------------------------------
!    xmin                                  xmax

!------------------------------------------------------------------
! Input parameters
!------------------------------------------------------------------

! # of Boundary parts = 4 or 5.

    n_boundary  =  4

!  Define the domain: here we define a unit square.

      xmin = zero
      xmax = one

      ymin = zero
      ymax = one

!  Define the grid size: the number of nodes in each direction.
!  NOTE: "ny" is better to be an odd number to place a node at the midpoint
!        on the left boundary.
    nx = 11
    ny = 11 !<- Better to be an odd number.

!------------------------------------------------------------------
! End of Input parameters
!------------------------------------------------------------------

! Let me stop when ny is an even number...
   if (mod(ny,2)==0) then
    write(*,*) " ny must be an odd number... ny = ", ny
    write(*,*) " Stop. Try again with an odd number."
    stop
   endif

!  Allocate arrays.
   allocate(xs(nx,ny),ys(nx,ny))

!--------------------------------------------------------------------------------
! 1. Generate a structured 2D grid data, (i,j) data: go up in y-direction!
!
! j=5 o--------o--------o--------o--------o
!     |        |        |        |        |
!     |        |        |        |        |   On the left is an example:
!     |        |        |        |        |         nx = 5
! j=4 o--------o--------o--------o--------o         ny = 5
!     |        |        |        |        |
!     |        |        |        |        |
!     |        |        |        |        |
! j=3 o--------o--------o--------o--------o
!     |        |        |        |        |
!     |        |        |        |        |
!     |        |        |        |        |
! j=2 o--------o--------o--------o--------o
!     |        |        |        |        |
!     |        |        |        |        |
!     |        |        |        |        |
! j=1 o--------o--------o--------o--------o
!     i=1      i=2      i=3      i=4      i=5

     write(*,*) "Generating structured data..."

!  Compute the grid spacing in x-direction
    dx = (xmax-xmin)/real(nx-1)

!  Compute the grid spacing in y-direction
    dy = (ymax-ymin)/real(ny-1)

!  Generate nodes in the domain.

   do j = 1, ny  ! Go up in y-direction.
    do i = 1, nx ! Go to the right in x-direction.

     xs(i,j) = xmin + dx*real(i-1)
     ys(i,j) = ymin + dy*real(j-1)

    end do
   end do

!--------------------------------------------------------------------------------
! 2. Generate unstructured data: 1D array to store the node information.
!
!    - The so-called lexcographic ordering -
!
!   21       22       23       24       25
!     o--------o--------o--------o--------o
!     |        |        |        |        |
!     |        |        |        |        |   On the left is an example:
!   16|      17|      18|      19|      20|           nx = 5
!     o--------o--------o--------o--------o           ny = 5
!     |        |        |        |        |
!     |        |        |        |        |   nnodes = 5x5 = 25
!   11|      12|      13|      14|      15|
!     o--------o--------o--------o--------o
!     |        |        |        |        |
!     |        |        |        |        |
!    6|       7|       8|       9|      10|
!     o--------o--------o--------o--------o
!     |        |        |        |        |
!     |        |        |        |        |
!    1|       2|       3|       4|       5|
!     o--------o--------o--------o--------o
!
   write(*,*) "Generating 1D node array for unstructured grid data..."

!  Total number of nodes
   nnodes = nx*ny

!  Allocate the arrays
   allocate(x(nnodes),y(nnodes))

! Node data: the nodes are ordered in 1D array.

  do j = 1, ny   !Go up in y-direction.
   do i = 1, nx  !Go to the right in x-direction.

    inode = i + (j-1)*nx   !<- Node number in the lexcographic ordering
      x(inode) =   xs(i,j)
      y(inode) =   ys(i,j)

   end do
  end do

! Deallocate the structured data: xs and ys, which are not needed any more.
! - You guys helped me create the 1D array. Thanks!
  deallocate(xs, ys)

 write(*,*)
 write(*,*) " Nodes have been generated:"
 write(*,*) "       nx  =", nx
 write(*,*) "       ny  =", ny
 write(*,*) "    nx*ny  =", nx*ny
 write(*,*) "    nnodes =", nnodes
 write(*,*)
 write(*,*)

!-----------------------------------------------------------
!-----------------------------------------------------------
!-----------------------------------------------------------
!-----------------------------------------------------------

  !Node data
   nnodes = nx*ny

  !Boundary data
   nb = n_boundary
   allocate( bnode( max(ny,nx), nb) )
   allocate( nbnodes(nb) )
   allocate(  bnames(nb) )

 !------------------------------------------------------
 ! Left boundary
 !------------------------------------------------------

 !------------------------------------------
  if     (n_boundary==4) then

  !(1)Left boundary: i=1

      ib = 1
      bnames( ib) = "left"
      nbnodes(ib) = ny
                i = 1
               ic = 0

    do j = ny, 1, -1
            inode = i + (j-1)*nx
               ic = ic + 1
     bnode(ic,ib) = inode
    end do

 !------------------------------------------
  elseif (n_boundary==5) then

  !(1)Left boundary: i=1

      ib = 1
      bnames( ib) = "left_top"
      nbnodes(ib) = (ny-1)/2+1
                i = 1
               ic = 0

    do j = ny, (ny-1)/2+1, -1
            inode = i + (j-1)*nx
               ic = ic + 1
     bnode(ic,ib) = inode
    end do

  !(2)Wall boundary: i=1

      ib = 2
      bnames( ib) = "left_bottom"
      nbnodes(ib) = (ny-1)/2+1
                i = 1
               ic = 0

    do j = (ny-1)/2+1, 1, -1
            inode = i + (j-1)*nx
               ic = ic + 1
     bnode(ic,ib) = inode
    end do

  else

   write(*,*) "Invalid input: n_boundary=",n_boundary
   write(*,*) "  n_boundary must be 4 or 5. Stop..."
   stop

  endif
 !------------------------------------------

 !------------------------------------------------------
 ! Other boundaries
 !------------------------------------------------------

  !Bottom boundary: j = 1

      ib = ib + 1
      bnames( ib) = "bottom"
      nbnodes(ib) = nx
                j = 1
               ic = 0

    do i = 1, nx
            inode = i + (j-1)*nx
               ic = ic + 1
     bnode(ic,ib) = inode
    end do

  !Right boundary: i = nx

      ib = ib + 1
      bnames( ib) = "right"
      nbnodes(ib) = ny
                i = nx
               ic = 0

    do j = 1, ny
            inode = i + (j-1)*nx
               ic = ic + 1
     bnode(ic,ib) = inode
    end do

  !Top boundary: j = ny+1

      ib = ib + 1
      bnames( ib) = "top"
      nbnodes(ib) = nx
                j = ny
               ic = 0

    do i = nx, 1, -1
            inode = i + (j-1)*nx
               ic = ic + 1
     bnode(ic,ib) = inode
    end do

   filename_bcmap = "quad.bcmap"
   call write_bcmap_file(filename_bcmap, nb, bnames)

   filename_bcmap = "tria.bcmap"
   call write_bcmap_file(filename_bcmap, nb, bnames)

!--------------------------------------------------------------------------------
! 3. Generate unstructured element data:
!
!    We generate both quadrilateral and triangular grids.
!    Both grids are constructed in the unstructured (finite-element) data.
!

! Allocate arrays of triangular and quad connectivity data.

!   Number of quadrilaterals = (nx-1)(ny-1)

    allocate( quad((nx-1)*(ny-1)  ,4) )

!   Number of triangles = 2*(nx-1)*(ny-1)

    allocate(  tria(2*(nx-1)*(ny-1),3) )

!-----------------------------------------------------------------------------
! (1)Genearte a triangular grid
!-----------------------------------------------------------------------------

  write(*,*) "Generating triangular grid..."
  call generate_tria_grid
  write(*,*)
  write(*,*) " Number of triangles =", ntria
  write(*,*)

   nquad = 0

   filename_grid  = "tria.grid"
   call write_grid_file(filename_grid,nnodes,tria,ntria,quad,nquad,x,y, &
                                                       nb,nbnodes,bnode )
   filename_su2   = "tria.su2"
   call write_su2_file(filename_su2,nnodes,tria,ntria,quad,nquad,x,y, &
                                              nb,nbnodes,bnode,bnames )
   filename_vtk   = "tria.vtk"
   call write_vtk_file(filename_vtk, nnodes,x,y, ntria,tria, &
                                                 nquad,quad  )
   filename_tec   = "tria_tec.dat"
   call write_tecplot_file(filename_tec, nnodes,x,y, ntria,tria, &
                                                     nquad,quad  )
   filename_tec_b = "tria_tec_b.dat"
   call write_tecplot_boundary_file(filename_tec_b,x,y,nb,nbnodes,bnode)

  write(*,*)

!-----------------------------------------------------------------------------
! (2)Generate a quadrilateral grid
!-----------------------------------------------------------------------------

  write(*,*) "Generating quad grid..."
  call generate_quad_grid
  write(*,*)
  write(*,*) " Number of quads =", nquad
  write(*,*)

   ntria = 0

   filename_grid  = "quad.grid"
   call write_grid_file(filename_grid,nnodes,tria,ntria,quad,nquad,x,y, &
                                                       nb,nbnodes,bnode )
   filename_su2   = "quad.su2"
   call write_su2_file(filename_su2,nnodes,tria,ntria,quad,nquad,x,y, &
                                              nb,nbnodes,bnode,bnames )
   filename_vtk   = "quad.vtk"
   call write_vtk_file(filename_vtk, nnodes,x,y, ntria,tria, &
                                                 nquad,quad  )
   filename_tec   = "quad_tec.dat"
   call write_tecplot_file(filename_tec, nnodes,x,y, ntria,tria, &
                                                     nquad,quad  )
   filename_tec_b = "quad_tec_b.dat"
   call write_tecplot_boundary_file(filename_tec_b,x,y,nb,nbnodes,bnode)

! (3)Generate a mixed grid. (not implemented. I'll leave it to you! You can do it!)
!

!--------------------------------------------------------------------------------

 write(*,*)
 write(*,*) "Successfully completed. Stop."

 stop

 contains



!********************************************************************************
! This subroutine generates triangles by constructing the connectivity data.
!********************************************************************************
 subroutine generate_tria_grid
 implicit none
!Local variables
 integer  :: i, j, inode, i1, i2, i3, i4

! No quads
 nquad = 0
  quad = 0

! Trianguler grid with right-up diagonals (i.e., / ).
!
!  inode+nx   inode+nx+1     i4      i3
!       o--------o           o--------o
!       |     .  |           |     .  |
!       |   .    |     or    |   .    |
!       | .      |           | .      |
!       o--------o           o--------o
!    inode    inode+1        i1      i2
!
! Triangle is defined by the counterclockwise ordering of nodes.

 ntria = 0

 do j = 1, ny-1
  do i = 1, nx-1

   inode = i + (j-1)*nx

!     Define the local numbers (see figure above)
      i1 = inode
      i2 = inode + 1
      i3 = inode + nx + 1
      i4 = inode + nx

           ntria = ntria + 1
    tria(ntria,1) = i1
    tria(ntria,2) = i2
    tria(ntria,3) = i3

           ntria = ntria + 1
    tria(ntria,1) = i1
    tria(ntria,2) = i3
    tria(ntria,3) = i4

  end do
 end do

 end subroutine generate_tria_grid
!********************************************************************************


!********************************************************************************
! This subroutine generates quads by constructing the connectivity data.
!********************************************************************************
 subroutine generate_quad_grid
 implicit none
!Local variables
 integer :: i, j, inode, i1, i2, i3, i4

! No triangles
  ntria = 0
   tria = 0

!
!  inode+nx   inode+nx+1     i4      i3
!       o--------o           o--------o
!       |        |           |        |
!       |        |     or    |        |
!       |        |           |        |
!       o--------o           o--------o
!     inode   inode+1        i1      i2
!
! Quad is defined by the counterclockwise ordering of nodes.

! Quadrilateral grid

 nquad = 0

 do j = 1, ny-1
  do i = 1, nx-1

   inode = i + (j-1)*nx
!     Define the local numbers (see figure above)
      i1 = inode
      i2 = inode + 1
      i3 = inode + nx + 1
      i4 = inode + nx

!  Order the quad counterclockwise:
          nquad = nquad + 1
   quad(nquad,1) = i1
   quad(nquad,2) = i2
   quad(nquad,3) = i3
   quad(nquad,4) = i4

  end do
 end do

 end subroutine generate_quad_grid
!********************************************************************************

!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------


!*******************************************************************************
! This subroutine writes a Tecplot file for the grid.
!*******************************************************************************
 subroutine write_tecplot_file(filename, nnodes,x,y, ntria,tria, nquad,quad  )

  implicit none

  character(80),                 intent(in) :: filename
  integer      ,                 intent(in) :: nnodes, ntria, nquad
  real(p2)     , dimension(:  ), intent(in) :: x, y
  integer      , dimension(:,:), intent(in) :: tria
  integer      , dimension(:,:), intent(in) :: quad

!Local variables
  integer :: i, os

 write(*,*)
 write(*,*) ' Writing a Tecplot file = ', trim(filename)
 write(*,*)

  open(unit=9, file=filename, status="unknown", iostat=os)

  write(9,*) 'TITLE = "Grid"'
  write(9,*) 'VARIABLES = "x","y"'
!---------------------------------------------------------------------------
! zone

   write(9,*) 'zone t=', '"grid"'
   write(9,*)'  n=', nnodes,',e=', ntria+nquad,' , et=quadrilateral, f=fepoint'

   do i = 1, nnodes
     write(9,'(2es25.15)') x(i), y(i)
   end do

  if (ntria > 0) then
   do i = 1, ntria
    write(9,'(4i10)') tria(i,1), tria(i,2), tria(i,3), tria(i,3)
   end do
  endif

  if (nquad > 0) then
   do i = 1, nquad
    write(9,'(4i10)') quad(i,1), quad(i,2), quad(i,3), quad(i,4)
   end do
  endif

!---------------------------------------------------------------------------

 close(9)

 end subroutine write_tecplot_file
!********************************************************************************


!********************************************************************************
! This subroutine writes boundary grid files.
!********************************************************************************
 subroutine write_tecplot_boundary_file(filename,x,y,nb,nbnodes,bnode )

 implicit none

 character(80),                 intent(in) :: filename
 real(p2)     , dimension(:)  , intent(in) :: x, y
 integer                      , intent(in) :: nb
 integer      , dimension(:  ), intent(in) :: nbnodes
 integer      , dimension(:,:), intent(in) :: bnode

!Local variables
 character(80) :: filename_loc
 character(80) :: bid
 integer       :: ib, os

  do ib = 1, nb

     write( bid  , '(i0)' ) ib
     filename_loc = trim(filename) // trim("_boundary") // trim(".") // trim(bid) // '.dat'

   write(*,*)
   write(*,*) ' Writing a Tecplot file for boundary = ', ib , trim(filename_loc)
   write(*,*)

   open(unit=10, file=filename_loc, status="unknown", iostat=os)

    write(10,*) 'variables = "x", "y"'
    write(10,*) 'ZONE t="', trim("Boundary.") // trim(bid) ,'" I =', nbnodes(ib), ', DATAPACKING=POINT'

    do i = 1, nbnodes(ib)
     write(10,*) x( bnode(i,ib) ), y( bnode(i,ib) )
    end do

   close(10)

  end do

 end subroutine write_tecplot_boundary_file

!********************************************************************************
! This subroutine writes a grid file to be read by a solver.
! NOTE: Unlike the tecplot file, this files contains boundary info.
!********************************************************************************
 subroutine write_grid_file(filename,nnodes,tria,ntria,quad,nquad,x,y, &
                                                      nb,nbnodes,bnode )

 implicit none

 character(80),                 intent(in) :: filename
 integer                      , intent(in) :: nnodes, ntria, nquad
 integer      , dimension(:,:), intent(in) :: tria, quad
 real(p2)     , dimension(:)  , intent(in) :: x, y
 integer                      , intent(in) :: nb
 integer      , dimension(:  ), intent(in) :: nbnodes
 integer      , dimension(:,:), intent(in) :: bnode

!Local variables
 integer  :: os, i, j

   write(*,*)
   write(*,*) " Writing .grid file: ", trim(filename)

!--------------------------------------------------------------------------------
 open(unit=1, file=filename, status="unknown", iostat=os)

!--------------------------------------------------------------------------------
! Grid size: # of nodes, # of triangles, # of quadrilaterals
  write(1,*) nnodes, ntria, nquad

!--------------------------------------------------------------------------------
! Node data
  do i = 1, nnodes
   write(1,*) x(i), y(i)
  end do

!--------------------------------------------------------------------------------
! Triangle connectivity
  if (ntria > 0) then
   do i = 1, ntria
    write(1,*) tria(i,1), tria(i,2), tria(i,3)
   end do
  endif

  if (nquad > 0) then
   do i = 1, nquad
    write(1,*) quad(i,1), quad(i,2), quad(i,3), quad(i,4)
   end do
  endif

!--------------------------------------------------------------------------------
! Boundary data:
!
! The number of boundary segments
  write(1,*) nb

! The number of nodes in each segment:
  do i = 1, nb
   write(1,*) nbnodes(i)
  end do

  write(1,*)

! List of boundary nodes
  do i = 1, nb

   write(*,*) " write_grid_file: nbnodes(i) = ", nbnodes(i), i

   do j = 1, nbnodes(i)
    write(1,*) bnode(j,i)
   end do
    write(1,*)

  end do

!--------------------------------------------------------------------------------
 close(1)

 end subroutine write_grid_file
!********************************************************************************



!*******************************************************************************
! This subroutine writes a .bcmap for the grid.
!*******************************************************************************
 subroutine write_bcmap_file(filename, nb, bnames)

  implicit none

 character(80),                 intent(in) :: filename
 integer                      , intent(in) :: nb
 character(80),   dimension(:), intent(in) :: bnames

!Local variables
  integer :: i, os

 write(*,*)
 write(*,*) ' Writing a .bcmap file = ', trim(filename)
 write(*,*)

  open(unit=9, file=filename, status="unknown", iostat=os)


  write(9,*) "      Boundary Part          Boundary Condition"

  do i = 1, nb
   write(9,'(i20,a28)') i, '"' // trim(bnames(i)) // '"'
  end do

  close(9)

 end subroutine write_bcmap_file
!********************************************************************************

!*******************************************************************************
! This subroutine writes a su2 grid file.
!
! Note: Nodes -> i = 0,1,2,...; Elements -> i = 0,1,2,...
!
!
!  Identifier:
!  Line 	 3
!  Triangle 	 5
!  Quadrilateral 9
!  Tetrahedral 	10
!  Hexahedral 	12
!  Prism 	13
!  Pyramid 	14
!
!
!*******************************************************************************
 subroutine write_su2_file(filename,nnodes,tria,ntria,quad,nquad,x,y, &
                                              nb,nbnodes,bnode,bnames )

 character(80),                 intent(in) :: filename
 integer                      , intent(in) :: nnodes, ntria, nquad
 integer      , dimension(:,:), intent(in) :: tria, quad
 real(p2)     , dimension(:)  , intent(in) :: x, y
 integer                      , intent(in) :: nb
 integer      , dimension(:  ), intent(in) :: nbnodes
 integer      , dimension(:,:), intent(in) :: bnode
 character(80), dimension(:)  , intent(in) :: bnames

!Local variables
 integer :: i, ib, j, os

   write(*,*)
   write(*,*) " Writing .su2 file: ", trim(filename)

  open(unit=7, file=filename, status="unknown", iostat=os)

  write(7,*) "%"
  write(7,*) "% Problem dimension"
  write(7,*) "%"
  write(7,5) 2
5 format('NDIME= ',i12)

   write(7,*) "%"
   write(7,*) "% Inner element connectivity"
   write(7,10) ntria + nquad
10 format('NELEM= ',i12)

 !-------------------------------------------------------------------------
 ! Elements

  if (ntria > 0) then
   do i = 1, ntria
    write(7,'(4i20)') 5, tria(i,1)-1, tria(i,2)-1, tria(i,3)-1
   end do
  endif

  if (nquad > 0) then
   do i = 1, nquad
    write(7,'(5i20)') 9, quad(i,1)-1, quad(i,2)-1, quad(i,3)-1, quad(i,4)-1
   end do
  endif

 !--------------------------------------------------------------------------
 ! Nodes

   write(7,*) "%"
   write(7,*) "% Node coordinates"
   write(7,*) "%"
   write(7,20) nnodes
20 format('NPOIN= ', i12)

  ! Nodes
    do i = 1, nnodes
     write(7,'(2es26.15)') x(i), y(i)
    end do

 !--------------------------------------------------------------------------
 ! Boundary

30  format('NMARK= ',i12)
40  format('MARKER_TAG= ',a)
50  format('MARKER_ELEMS= ', i12)

    write(7,*) "%"
    write(7,*) "% Boundary elements"
    write(7,*) "%"
    write(7,30) nb !# of boundary parts.

   do ib = 1, nb

   !-------------------------
   !Just to print on screen
    write(*,*)
    write(*,40) trim(bnames(ib)) !ib-th boundary-part name, e.g., "farfield".
    write(*,50) nbnodes(ib)-1    !# of boundary elements (edges)
   !-------------------------

    write(7,40) trim(bnames(ib)) !ib-th boundary-part name, e.g., "farfield".
    write(7,50) nbnodes(ib)-1    !# of boundary elements (edges)

    do j = 1, nbnodes(ib)-1
     write(7,'(3i20)') 3, bnode(j,ib)-1, bnode(j+1,ib)-1
    end do

   end do

  close(7)

 end subroutine write_su2_file
!********************************************************************************

!*******************************************************************************
! This subroutine writes a .vtk file for the grid whose name is defined by
! filename_vtk.
!
! Use Paraview to read .vtk and visualize it.  https://www.paraview.org
!
! Search in Google for 'vkt format' to learn .vtk file format.
!*******************************************************************************
 subroutine write_vtk_file(filename, nnodes,x,y, ntria,tria, nquad,quad  )

  implicit none

  character(80),                 intent(in) :: filename
  integer      ,                 intent(in) :: nnodes, ntria, nquad
  real(p2)     , dimension(:  ), intent(in) :: x, y
  integer      , dimension(:,:), intent(in) :: tria
  integer      , dimension(:,:), intent(in) :: quad

!Local variables
  integer :: i, j, os

 !------------------------------------------------------------------------------
 !------------------------------------------------------------------------------
 !------------------------------------------------------------------------------

  write(*,*)
  write(*,*) ' Writing .vtk file = ', trim(filename)
  write(*,*)

 !Open the output file.
  open(unit=8, file=filename, status="unknown", iostat=os)

!---------------------------------------------------------------------------
! Header information

  write(8,'(a)') '# vtk DataFile Version 3.0'
  write(8,'(a)') filename
  write(8,'(a)') 'ASCII'
  write(8,'(a)') 'DATASET UNSTRUCTURED_GRID'

!---------------------------------------------------------------------------
! Nodal information
!
! Note: These nodes i=1,nnodes are interpreted as i=0,nnodes-1 in .vtk file.
!       So, later below, the connectivity list for tria and quad will be
!       shifted by -1.

   write(8,*) 'POINTS ', nnodes, ' double'

   do j = 1, nnodes
    write(8,'(3es25.15)') x(j), y(j), zero
   end do

!---------------------------------------------------------------------------
! Cell information.

  !CELLS: # of total cells (tria+quad), total size of the cell list.

  write(8,'(a,i12,i12)') 'CELLS ',ntria+nquad, (3+1)*ntria + (4+1)*nquad

  ! Note: The latter is the number of integer values written below as data.
  !           4 for triangles (# of vertices + 3 vertices), and
  !           5 for quads     (# of vertices + 4 vertices).

  !---------------------------------
  ! 2.1 List of triangles (counterclockwise vertex ordering)

   if (ntria > 0) then
                         ! (# of vertices = 3), 3 vertices in counterclockwise
    do i = 1, ntria
     write(8,'(a,4i12)') '3', tria(i,1)-1, tria(i,2)-1, tria(i,3)-1
                         ! -1 since VTK reads the nodes as 0,1,2,3,..., not 1,2,3,..
    end do
   endif

  !---------------------------------
  ! 2.2 List of quads (counterclockwise vertex ordering)

   if (nquad > 0) then
                         ! (# of vertices = 4), 4 vertices in counterclockwise
    do i = 1, nquad
     write(8,'(a,4i12)') '4', quad(i,1)-1, quad(i,2)-1, quad(i,3)-1, quad(i,4)-1
                         ! -1 since VTK reads the nodes as 0,1,2,3,..., not 1,2,3,..
    end do
   endif

!---------------------------------------------------------------------------
! Cell type information.

                                   !# of all cells
  write(8,'(a,i11)') 'CELL_TYPES ', ntria+nquad

  !Triangle is classified as the cell type 5 in the .vtk format.

  if (ntria > 0) then
   do i = 1, ntria
    write(8,'(i3)') 5
   end do
  endif

  !Triangle is classified as the cell type 9 in the .vtk format.

  if (nquad > 0) then
   do i = 1, nquad
    write(8,'(i3)') 9
   end do
  endif

!--------------------------------------------------------------------------------
! NOTE: Commented out because there are no solution data. This part should be
!       uncommented if this is used in a solver or if solution data are available
!       and one would like to plot them.
!
! Field data (e.g., density, pressure, velocity)are added here for visualization.

!   write(8,*) 'POINT_DATA   ',nnodes

!  !            FIELD  dataName    # of arrays (variables to plot) <--This is a commnet.
!   write(8,*) 'FIELD  FlowField ', 4

!   write(8,*) 'Density   ',  1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,1)
!   end do

!   write(8,*) 'X-velocity ', 1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,2)
!   end do

!   write(8,*) 'Y-veloiity ', 1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,3)
!   end do

!   write(8,*) 'Pressure   ', 1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,4)
!   end do

!---------------------------------------------------------------------------

 !Close the output file. <--This is a commnet.
  close(8)


 end subroutine write_vtk_file
!********************************************************************************




 end program twod_rectangular_grid
