!*******************************************************************************
!*
!* Educationally-Designed Unstructured 1D (EDU1D) Code
!*
!*  --- EDU1D oned_third_order_diffusion
!*
!*
!* One-Dimensional Third-Order Diffusion Scheme.
!*
!*        written by Dr. Katate Masatsuka (info[at]cfdbooks.com),
!*
!* the author of useful CFD books, "I do like CFD" (http://www.cfdbooks.com).
!*
!* This is Version 1 (05-26-2018).
!*
!* -05-26-18: A typo corrected for Lr: two**pi -> two*pi
!*  thanks to Sinath at University of Tokyo.
!*
!* This F90 program was written and made available for download
!* for an educational purpose. Comments are welcome.
!*
!* This file may be updated in future.
!*
!* Katate Masatsuka, May 2018. http://www.cfdbooks.com
!*
!*-----------------------------------------------------------------------------
!*
!*  Problem: u_tau = nu*u_{xx} + nu*pi^2*sin(pi*x), u(0)=u(1)=0,
!*
!*           which is solved in the first-order system form:
!*
!*           u_tau = nu*p_x + nu*pi^2*sin(pi*x)
!*           p_tau = (u_x - p)/Tr
!*
!*           The two formulations have the same steady solution.
!*
!* Note: The first-order system is hyperbolic in the pseudo time, tau. 
!*       The eigenvalues (wave speeds) are + nu/Lr, and - nu/Lr.
!*
!* Note: The first-order system is equivalent to the original diffusion equation
!*       in the steady state. The idea is to integrate the first-order system in
!*       time, instead of the original diffusion equation. Thereby, the time step
!*       is O(h), not O(h^2), and accurate solution gradient can be computed
!*       simultaneously. See Ref.[1] for details.
!*
!* Note: Two schemes are implemented:
!*
!*       (1)Third-order diffusion scheme based on the hyperbolic form.
!*          - Pseudo time step O(h)
!*          - Accuracy: error(u,p) = O(h^3)
!*          - Weak boundary condition.
!*          - This is Scheme-II in Ref.[1].
!*          - It is stable with the forward Euler scheme.
!*          - Node-centered scheme with the dual volume around each node.
!*          - Third-order without 2nd-derivatives nor quadratic reconstruction [2].
!*          - Special source quadrature is required for 3rd-order [2].
!*          - This is a 5-point stencil scheme just like the 2nd-order version.
!*          - This scheme leads to very economical 3rd-order scheme in 2D/3D. See Ref.[2].
!*
!*       (2)P1 continuous Galerkin scheme
!*          - Pseudo time step O(h^2)
!*          - Accuracy: error(u  ) = O(h^2), error(p)=O(h).
!*          - Strong boundary condition.
!*
!* Note: Gradients computed from 3rd-order solution are typically 2nd-order
!*       on irregular grids. Third-order scheme constructed by the hyperbolic
!*       method gives 3rd-order accurate gradients. This is one of the advantages
!*       of the hyperbolic method, which leads to a useful 3rd-order scheme
!*       for the Navier-Stokes equations. See discussions for the third-order
!*       accurate Hyperbolic Navier-Stokes (HNS) scheme in AIAA Paper-2091:
!*       http://hiroakinishikawa.com/My_papers/nishikawa_aiaa2014-2091.pdf
!*
!*
!* References:
!*
!* [1] H. Nishikawa, First-, third-, and third-order finite-volume schemes
!*     for diffusion, Journal of Computational Physics, 256, pp. 791-805, 2014
!*     https://doi.org/10.1016/j.jcp.2013.09.024
!*  http://hiroakinishikawa.com/My_papers/nishikawa_jcp2014v256pp791-805_preprint.pdf
!*
!* [2] H. Nishikawa and Y. Liu, " Accuracy-Preserving Source Term Quadrature
!*     for Third-Order Edge-Based Discretization",
!*     Journal of Computational Physics, Volume 344, September 2017, Pages 595-622.
!*  http://hiroakinishikawa.com/My_papers/nishikawa_liu_jcp2017_preprint.pdf
!*
!*******************************************************************************
 program oned_third_order_diffusion_scheme

 implicit none

  integer , parameter :: dp = selected_real_kind(15) ! Double precision

  real(dp), parameter :: zero = 0.0_dp
  real(dp), parameter :: half = 0.5_dp
  real(dp), parameter ::  one = 1.0_dp
  real(dp), parameter ::  two = 2.0_dp
  real(dp), parameter ::   pi = 3.141592653589793238_dp

  real(dp), dimension(:), allocatable ::      x          ! Nodal coordinates
  real(dp), dimension(:), allocatable ::    vol          ! Dual volume at nodes
  real(dp), dimension(:), allocatable ::      u,      p  ! Solution & Gradient
  real(dp), dimension(:), allocatable ::             px  ! Gradients of p
  real(dp), dimension(:), allocatable :: uexact, pexact  ! Exact solutions
  real(dp), dimension(:), allocatable ::  res_u,  res_p  ! Nodal Residuals

  real(dp) :: nu          ! Diffusion coefficient (constant)
  real(dp) :: h           ! Mesh spacing for an uniform mesh.
  real(dp) :: hmin, hmax  ! Min/Max mesh spacing for a perturbed irregular mesh.
  real(dp) :: heff        ! Effective (averaged) mesh spacing.
  real(dp) :: rn          ! Random number
  real(dp) :: Lr, Tr      ! Length scale and relaxation time.
  real(dp) :: dtau        ! Pseudo time step.
  real(dp) :: res_max     ! Maximum residual to check the convergence.
  integer  :: nnodes      ! Total number of nodes
  integer  :: scheme_type ! = 1 : Third-order diffusion scheme.
                          ! = 2 : P1 continuous Galerkin scheme
  integer  :: k, j

  real(dp) ::   f(2)      ! Numerical flux
  real(dp) ::   uL, uR    ! Left and right solutions at interface.
  real(dp) ::   pL, pR    ! Left and right solution gradients at interface.

  real(dp) ::   dxL, dxR    ! Distance to neighbors in quadratic fit.
  real(dp) ::   duL, duR    ! Solution difference to neighbors in quadratic fit.
  real(dp) ::   dpL, dpR    ! Gradient difference to neighbors in quadratic fit.

  real(dp), dimension(2) :: s1, sx1  !Source and its gradient at the  left node.
  real(dp), dimension(2) :: s2, sx2  !Source and its gradient at the right node.
  real(dp), dimension(2) :: sL, sR   !Left and right values for the flux.
  real(dp), dimension(2) :: fs1, fs2 !Source flux for a special quadrature.
  real(dp), dimension(2) :: dx, vjk  !dx=x(j+1)-x(j). vjk = dx/2.
  
  integer  ::   iu, ip

  iu = 1
  ip = 2

!--------------------------------------------------------------------------------
! Initialization (values have no meaning; they will be overwritten later.)

    Lr = one
    Tr = one
  dtau = one

!--------------------------------------------------------------------------------
! Diffusion coefficient

  nu = one

!--------------------------------------------------------------------------------
! Input: Select the scheme.

  do j = 1, 100

  write(*,*) " Type of scheme = ?"
  write(*,*) 
  write(*,*) "   1 -> Third-order scheme solving the hyperbolic system"
  write(*,*) "   2 -> P1 continuous Galerkin scheme"
  read(*,*) scheme_type

  if (scheme_type /= 1 .and. scheme_type /= 2) then
   write(*,*) " Wrong number. Enter 1 or 2."
  else
   exit
  endif

  end do

!--------------------------------------------------------------------------------
! Input: Set the number of nodes.

  write(*,*) " The number of nodes = ?"
  read(*,*) nnodes

!--------------------------------------------------------------------------------
! Allocate arrays.

   allocate(                     x(nnodes)) ! Nodal coordinates.
   allocate(                   vol(nnodes)) ! Dual volume
   allocate(     u(nnodes),      p(nnodes)) ! Solution & Gradient
   allocate(                    px(nnodes)) ! Gradient of p
   allocate(uexact(nnodes), pexact(nnodes)) ! Exact solutions
   allocate( res_u(nnodes),  res_p(nnodes)) ! Nodal Residuals

!--------------------------------------------------------------------------------
! Set up                           h
!          Grid ->  o-------o-----o-----o------o---o-----o---o
!                  j=1            j    j+1                 j=nnodes

 !Generate a uniform mesh first.

     h = one / real(nnodes-1,dp) ! Mesh spacing of uniform grid

   do j = 1, nnodes
         x(j) = real(j-1)*h      ! xj = x-coordinate of j-th node
   end do

 !Perturb the nodal coordinates to generate an irregular grid.

   do j = 2, nnodes-1 !<- Perturb only the interior nodes.
    call random_number(rn)
    x(j) = half*(x(j+1)+x(j-1)) + 0.25_dp*(rn-half) * half*(x(j+1)-x(j-1))
   end do

 !Compute and store the exact solution and the gradient.

   do j = 1, nnodes
    uexact(j) = sin(pi*x(j))     ! Exact solution at xj
    pexact(j) = pi*cos(pi*x(j))  ! Exact gradient at xj
         u(j) = zero             ! Initial solution
         p(j) = zero             ! Initial gradient
   end do

 !Compute and store the dual volume around each node.

   do j = 2, nnodes-1
    vol(j) = half*( x(j+1)-x(j-1) )
   end do
    vol(1     ) = half*( x(2)-x(1) )
    vol(nnodes) = half*( x(nnodes)-x(nnodes-1) )

 !Compute the effective mesh spacing: h = L1(vol).

    heff = zero
   do j = 1, nnodes
    heff = heff + vol(j)
   end do
    heff = heff/real(nnodes,dp)

 !Compute the minimum and maximum mesh spacings.

    hmin = minval(vol)
    hmax = maxval(vol)

!--------------------------------------------------------------------------------
! Compute the pseudo time step.

 !--------------------------
 ! 1. Upwind scheme:
 !--------------------------
  if (scheme_type == 1) then

    Lr = one/(two*pi)               ! Optimal formula for Lr. See Ref.[1].
    Tr = Lr*Lr / nu                 ! Relaxation time. See Ref.[1].

    dtau = 0.99_dp * hmin/(nu/Lr)   ! Pseudo time step (CFL condition).

 !   Note: The time step is O(h), not O(h^2). The number of iterations to reach
 !         the steady state will therefore be proportional to 1/h or equivalently
 !         to nnodes. This is orders of magnitude faster than almost all conventional
 !         diffusion schemes for which the number of iterations increases quadratically.

 !--------------------------
 ! 2. P1 continuous Galerkin scheme
 !--------------------------
  elseif (scheme_type == 2) then

   dtau = 0.99_dp * hmin*hmin/(two*nu) ! Pseudo time step, typical O(h^2)

  endif

!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
! Advance in pseudo time to reach the steady state by the forward Euler scheme:
!  u^{n+1} = u^n + dtau*Residual(u^n).

  pseudo_time_loop : do k = 1, 10000000

 !-------------------------------------------------------------------------------
 ! Residual Computation (compute res_u and/or res_p):
 !-------------------------------------------------------------------------------

 !--------------------------------------------------------------------
 ! Option 1: Third-order diffusion scheme

   scheme_choice : if (scheme_type == 1) then

   !------------------------------------------------------------
   ! Initialize the nodal residual arrays
   !------------------------------------------------------------

      res_u = zero
      res_p = zero

   !------------------------------------------------------------
   ! Compute the gradients by a quadratic fit, which is required
   ! for third-order. This gives the exact gradient for a quadratic
   ! function. But second derivatives are not computed nor
   ! stored since they are not used.
   !
   ! Note: No need to compute ux because we can use p as ux.
   !------------------------------------------------------------

     ! j-th interior node

     do j = 2, nnodes-1

      dxL  = x(j-1) - x(j)  !  left neighbor - node_j
      dpL  = p(j-1) - p(j)  !  left neighbor - node_j
      dxR  = x(j+1) - x(j)  ! right neighbor - node_j
      dpR  = p(j+1) - p(j)  ! right neighbor - node_j
     px(j) = ( dxR**2*dpL - dxL**2*dpR ) / ( dxL*dxR*(dxR-dxL) )

     end do

    !Left boundary node at j=1 (x=0).
         j = 1
      dxL  = x(j+1) - x(j)  ! right neighbor - node_j
      dpL  = p(j+1) - p(j)  ! right neighbor - node_j
      dxR  = x(j+2) - x(j)  ! right of right neighbor - node_j
      dpR  = p(j+2) - p(j)  ! right of right neighbor - node_j
     px(j) = ( dxR**2*dpL - dxL**2*dpR ) / ( dxL*dxR*(dxR-dxL) )

    !Right boundary node at j=nnodes (x=1).
         j = nnodes
      dxL  = x(j-1) - x(j)  ! left neighbor - node_j
      dpL  = p(j-1) - p(j)  ! left neighbor - node_j
      dxR  = x(j-2) - x(j)  ! left of left neighbor - node_j
      dpR  = p(j-2) - p(j)  ! left of left neighbor - node_j
     px(j) = ( dxR**2*dpL - dxL**2*dpR ) / ( dxL*dxR*(dxR-dxL) )

   !------------------------------------------------------------
   ! Loop over the interior faces, compute the numerical flux.
   !------------------------------------------------------------

      interior_face_loop : do j = 1, nnodes-1 ! j-th face between j and j+1.

      ! Left and right states.
      !
      !              |           . |           |
      !              |        .  L |.R         |
      !              |      .      |   .       |
      !              |    .        |      .    |
      !              | .           |        .  |
      !              |             |           |
      !     --o-------------o-------------o---------o---
      !      j-1            j     mid    j+1       j+2
      !               <----------->
      !                Dual volume = ( x(j)-x(j-1) )/2 + ( x(j+1)-x(j) )/2
      !                            = ( x(j+1)-x(j-1) )/2
      !
      ! Note: The dual volume is defined by the midpoints of two adjacent nodes.
      !       The node is not necessarily at the midpoint of the dual volume.
      !       The interface is located at the midpoint of two adjacent nodes.

      ! Note: Linear reconstruction is sufficient to achieve third-order accuracy.
      !       This is a special edge-based third-order scheme, which does not
      !       require second derivatives nor quadratic reconstruction for 3rd-order.
      !       See Ref.[2] for a theoretical background.

        uL = u(j)    +  p(j  ) * half*( x(j+1)-x(j) ) !<- p=ux at convergence.
        pL = p(j)    + px(j  ) * half*( x(j+1)-x(j) )

        uR = u(j+1)  -  p(j+1) * half*( x(j+1)-x(j) ) !<- p=ux at convergence.
        pR = p(j+1)  - px(j+1) * half*( x(j+1)-x(j) )

      ! Compute the numerical flux (upwind flux)

         f(iu) = half*( ( nu*pR + nu*pL )    + nu/Lr*( uR - uL ) )
         f(ip) = half*( (    uR +    uL )/Tr + nu/Lr*( pR - pL ) )

      ! Add to the left and subtract from the right for Res = f_{j+1/2}-f_{j-1/2}.

        res_u(j  ) = res_u(j  ) + f(iu)
        res_p(j  ) = res_p(j  ) + f(ip)

        res_u(j+1) = res_u(j+1) - f(iu)
        res_p(j+1) = res_p(j+1) - f(ip)

      end do interior_face_loop

   !------------------------------------------------------------
   ! Fluxes through the domain boundaries to close the residuals.
   ! Note: Weak boundary condition is applied.
   ! Note: Hyperbolic diffusion describes two waves traveling
   !       to the left and right at the same speed. Need to
   !       specify one value at the boundary.
   !------------------------------------------------------------

     !-------------------------------------------
     ! Left boundary
     !-------------------------------------------

      ! Boundary flux at j=1.
      !     |
      !     v
      !
      !    /|     |
      !    /|     |
      !    L|R    |
      !    /|     |
      !    /|     |
      !    /|     |
      !     o-----------o---
      !    j=1
      !     <----->
      !    Dual volume
      !
         j = 1

        uR = u(j) !<- Right state is the value at j=1.
        pR = p(j) !<- Right state is the value at j=1.

        uL = 0.0  !<- Boundary condition: u(0)=0.
        pL = pR   !<- No boundary condition: copy from the right.

       !Compute the numerical flux (upwind flux) at the boundary.

         f(iu) = half*( ( nu*pR + nu*pL )    + nu/Lr*( uR - uL ) )
         f(ip) = half*( (    uR +    uL )/Tr + nu/Lr*( pR - pL ) )

       !Subtract from the resisual at node 1.

        res_u(j) = res_u(j) - f(iu)
        res_p(j) = res_p(j) - f(ip)

     !-------------------------------------------
     ! Right boundary
     !-------------------------------------------

      !             Boundary flux at j=nnodes.
      !                |
      !                v
      !
      !          |    L|R
      !          |     |\
      !          |     |\
      !          |     |\
      !          |     |\
      !          |     |\
      !  --o-----------o
      !               j=nnodes
      !          <----->
      !         Dual volume

         j = nnodes

        uL = u(j) !<- Left state is the value at j=nnodes.
        pL = p(j) !<- Left state is the value at j=nnodes.

        uR = 0.0  !<- Boundary condition: u(1)=0.
        pR = pL   !<- No boundary condition: copy from the left.

       !Compute the numerical flux (upwind flux) at the boundary.

         f(iu) = half*( ( nu*pR + nu*pL )    + nu/Lr*( uR - uL ) )
         f(ip) = half*( (    uR +    uL )/Tr + nu/Lr*( pR - pL ) )

       !Add it to the resisual at the node 'nnodes'.

        res_u(j) = res_u(j) + f(iu)
        res_p(j) = res_p(j) + f(ip)

   !------------------------------------------------------------
   ! Add source terms, and finish the residual calculation.
   ! Third-order accuracy requires a special quadrature. See Ref.[2].
   !------------------------------------------------------------

      ! Compute the source flux at interior faces.
      ! No boundary closure is needed. See Ref.[2].

      interior_face_loop_source : do j = 1, nnodes-1

      ! Left and right states.
      !
      !              |           . |           |
      !              |        .  L |.R         |
      !              |      .      |   .       |
      !              |    .        |      .    |
      !              | .           |        .  |
      !              |             |           |
      !     --o-------------o-------------o---------o---
      !      j-1            j     mid    j+1       j+2
      !
      !                     1             2   <- local numbers
      !
      !                     <-----> <----->
      !                       vjk     vjk     <- Half the dual volumes.

      ! Distance between the two adjacent nodes.
            dx = x(j+1) - x(j)

      ! Distance to the midpoint.
           vjk = half*dx

      ! Source values at the left node j.
        s1(iu) = nu*pi*pi*sin(pi*x(j))
        s1(ip) = - p(j)/Tr

      ! Source gradients at the left node j.
       sx1(iu) = nu*pi*pi*cos(pi*x(j))*pi ! = ds1(iu)/dx
       sx1(ip) = - px(j)/Tr               ! = ds1(ip)/dx

      ! Source values at the right node j+1.
        s2(iu) = nu*pi*pi*sin(pi*x(j+1))
        s2(ip) = - p(j+1)/Tr

      ! Source gradients at the right node j+1.
       sx2(iu) = nu*pi*pi*cos(pi*x(j+1))*pi ! = ds2(iu)/dx
       sx2(ip) = - px(j+1)/Tr               ! = ds2(ip)/dx

      ! Flux by a special quadrature for third-order accuracy.
      ! This is called the compact formula in Ref.[2].

        !- Special source flux to the left node j:
            sL =  7.0_dp/3.0_dp * s1 + (dx*sx1)/3.0_dp
            sR = -1.0_dp/3.0_dp * s2
           fs1 = half*(sL+sR) * vjk

        !- Special source flux to the right node j+1:
            sL =  7.0_dp/3.0_dp * s2 - (dx*sx2)/3.0_dp
            sR = -1.0_dp/3.0_dp * s1
           fs2 = half*(sL+sR) * vjk

      ! Add to the left and the right nodal residuals.

        res_u(j  ) = res_u(j  ) + fs1(iu)
        res_p(j  ) = res_p(j  ) + fs1(ip)

        res_u(j+1) = res_u(j+1) + fs2(iu)
        res_p(j+1) = res_p(j+1) + fs2(ip)

      end do interior_face_loop_source

 !--------------------------------------------------------------------

   else scheme_choice

 !--------------------------------------------------------------------
 ! Option 2: P1 continuous Galerkin scheme + point source.

    res_p = zero ! Gradient, p, is not computed (will be computed at the end).

    node_loop : do j = 2, nnodes-1 ! j-th interior node

    res_u(j) = nu*(   ( u(j+1)-u(j  ) )/( x(j+1)-x(j  ) )   &
                    - ( u(j  )-u(j-1) )/( x(j  )-x(j-1) ) ) &
               + nu*pi*pi*sin(pi*x(j))*vol(j)

    end do node_loop
 !--------------------------------------------------------------------


   end if scheme_choice
 !--------------------------------------------------------------------

 !-------------------------------------------------------------------------------
 !-------------------------------------------------------------------------------
 !  Check convergence: see if res_u and res_p are small enough.
 !-------------------------------------------------------------------------------
 !-------------------------------------------------------------------------------

!     Compute the maximum nodal residual (to check the convergence)
      res_max = max( maxval(abs(res_u)), maxval(abs(res_p)) )

!     Stop if the tolerance (say, 1.0e-08) is reached.
      if ( res_max < 1.0e-08_dp ) exit pseudo_time_loop

!     Display the max nodal residual at every 100 iterations
      if (mod(k,100) == 0) then
        write(*,'(a5,i10,a20,es12.5)') "Itr =", k, "   Max(nodal res) = ", res_max
      endif

 !-------------------------------------------------------------------------------
 !-------------------------------------------------------------------------------
 ! Update the solution by the forward Euler scheme.
 !-------------------------------------------------------------------------------
 !-------------------------------------------------------------------------------

            u = u + (dtau/heff)*res_u
            p = p + (dtau/heff)*res_p

 !-------------------------------------------------------------------------------
 !-------------------------------------------------------------------------------
 ! Apply strong boundary condition for the P1 continuous Galerkin scheme.
 !-------------------------------------------------------------------------------
 !-------------------------------------------------------------------------------

   if (scheme_type == 2) then
         u(1) = zero ! BC at x=0
    u(nnodes) = zero ! BC at x=1
   endif


  end do pseudo_time_loop
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------

!--------------------------------------------------------------------------------
!  Steady state is reached.
!--------------------------------------------------------------------------------

   write(*,'(a5,i10,a20,es12.5)') "Itr =", k-1, "   Max(nodal res) = ", res_max

!--------------------------------------------------------------------------------
! Display the results
!--------------------------------------------------------------------------------

 !---------------------------------------------------------------
 ! 1. Third-order diffusion scheme
 !---------------------------------------------------------------

  if (scheme_type == 1) then

  ! L_infinity Errors:
   write(*,*)
   write(*,'(a34,es10.3)') " L_infinity( u-uexact), O(h^3) = ", maxval(abs(u-uexact))
   write(*,'(a34,es10.3)') " L_infinity( p-pexact), O(h^3) = ", maxval(abs(p-pexact))
   write(*,*)

  !-----------------------------------------------------
  ! Compute the gradient exactly for quadratic functions at all nodes.

    do j = 2, nnodes-1 ! j-th interior node

     dxL  = x(j-1) - x(j)  ! neighbor - node_j
     duL  = u(j-1) - u(j)  ! neighbor - node_j

     dxR  = x(j+1) - x(j)  ! neighbor - node_j
     duR  = u(j+1) - u(j)  ! neighbor - node_j

     p(j) = ( dxR**2*duL - dxL**2*duR ) / ( dxL*dxR*(dxR-dxL) )

    end do

    !Left boundary node.
     j = 1
     dxL  = x(j+1) - x(j)  ! neighbor - node_j
     duL  = u(j+1) - u(j)  ! neighbor - node_j
     dxR  = x(j+2) - x(j)  ! neighbor - node_j
     duR  = u(j+2) - u(j)  ! neighbor - node_j
     p(j) = ( dxR**2*duL - dxL**2*duR ) / ( dxL*dxR*(dxR-dxL) )

    !Right boundary node.
     j = nnodes
     dxL  = x(j-1) - x(j)  ! neighbor - node_j
     duL  = u(j-1) - u(j)  ! neighbor - node_j
     dxR  = x(j-2) - x(j)  ! neighbor - node_j
     duR  = u(j-2) - u(j)  ! neighbor - node_j
     p(j) = ( dxR**2*duL - dxL**2*duR ) / ( dxL*dxR*(dxR-dxL) )

  !-----------------------------------------------------

   write(*,*) "  --- Gradient computed from uj by a quadratic fit, O(h^2):"
   write(*,'(a34,es10.3)') " L_infinity(ux-pexact), O(h^2) = ", maxval(abs(p-pexact))

   write(*,*)
   write(*,*) "  heff = ", heff
   write(*,*) "  hmin = ", hmin
   write(*,*) "  hmax = ", hmax
   write(*,*)

   write(*,*) "-- Run again with nnodes = ", 2*(nnodes-1) + 1
   write(*,*) "   The errors will be about 1/8 of the above (i.e.,3rd-order accurate)."
   write(*,*) "   The number of iterations will be around 2 times the above = ",2*(k-1),")"
   write(*,*)

 !---------------------------------------------------------------
 ! 2. P1 continuous Galerkin scheme
 !---------------------------------------------------------------

  elseif (scheme_type == 2) then

  !-----------------------------------------------------
  ! Compute the gradient exactly for quadratic functions at all nodes.

    do j = 2, nnodes-1 ! j-th interior node

     dxL  = x(j-1) - x(j)  ! neighbor - node_j
     duL  = u(j-1) - u(j)  ! neighbor - node_j

     dxR  = x(j+1) - x(j)  ! neighbor - node_j
     duR  = u(j+1) - u(j)  ! neighbor - node_j

     p(j) = ( dxR**2*duL - dxL**2*duR ) / ( dxL*dxR*(dxR-dxL) )

    end do

    !Left boundary node.
     j = 1
     dxL  = x(j+1) - x(j)  ! neighbor - node_j
     duL  = u(j+1) - u(j)  ! neighbor - node_j
     dxR  = x(j+2) - x(j)  ! neighbor - node_j
     duR  = u(j+2) - u(j)  ! neighbor - node_j
     p(j) = ( dxR**2*duL - dxL**2*duR ) / ( dxL*dxR*(dxR-dxL) )

    !Right boundary node.
     j = nnodes
     dxL  = x(j-1) - x(j)  ! neighbor - node_j
     duL  = u(j-1) - u(j)  ! neighbor - node_j
     dxR  = x(j-2) - x(j)  ! neighbor - node_j
     duR  = u(j-2) - u(j)  ! neighbor - node_j
     p(j) = ( dxR**2*duL - dxL**2*duR ) / ( dxL*dxR*(dxR-dxL) )

  !-----------------------------------------------------

!  L_infinity Errors:
   write(*,*)
   write(*,'(a33,es10.3)') " L_infinity(u -uexact ), O(h^2) = ", maxval(abs(u-uexact))
   write(*,'(a33,es10.3)') " L_infinity(ux-uxexact), O(h  ) = ", maxval(abs(p-pexact))

   write(*,*)
   write(*,*) "  heff = ", heff
   write(*,*) "  hmin = ", hmin
   write(*,*) "  hmax = ", hmax
   write(*,*)

   write(*,*) "-- Run again with nnodes = ", 2*(nnodes-1) + 1
   write(*,*) "   The errors will be about 1/4 of the above (i.e.,2nd-order accurate)."
   write(*,*) "   The number of iterations will be around 4 times the above = ",4*(k-1),")"
   write(*,*) "   Or try the upwind scheme for the same number of nodes (much faster!)."
   write(*,*)

  endif

!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------

 stop

 end program oned_third_order_diffusion_scheme

