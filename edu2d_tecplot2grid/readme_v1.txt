######################################################################
# Running edu2d_tecplot2grid_v1.f90
#
# Follow the steps to run the code.
# (Or just run this script by typing "source readme_v1.txt".)
#
# NOTE: The following is for intel fortran. If you use other compiler,
#       replace "gfortran" by your compiler (ifort, g95, etc).
#
# Katate Masatsuka, http://www.cfdbooks.com
#####################################################################

#####################################################################
# 1. Compile and run
#####################################################################

gfortran -o edu2d_tecplot2grid edu2d_tecplot2grid_v1.f90

./edu2d_tecplot2grid < sample_input


# It takes the following input
#
# - tria_rankine_grid_tecplot.data
#
# which is created by modyfying the header info of tria_rankine_grid_tecplot.dat,
#
# and generates the following files:
#
# - tria_rankine_grid_tecplot.grid
# - tria_rankine_grid_tecplot.bcmap



