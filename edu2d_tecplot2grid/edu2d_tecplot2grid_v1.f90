!*******************************************************************************
! Educationally-Designed Unstructured 2D (EDU2D) Code
!
!  --- EDU2D-Tecplot2Grid
!
! This code reads a 2D tecplot grid file, generates boundary information,
! and writes a 2D grid file. It can be used to learn how to generate
! boundary information for a given 2D grid.
!
!-------------------------------------------------------------------------------
!
!  Input:
!
!   project.data  ! Tecplot data file without the header information.
!       tec_type  ! =1 for DATAPACKING=BLOCK, =2 for DATAPACKING=POINT.
!
!    NOTE: Currently, it accepts triangular grids only.
!    NOTE: Tecplot file needs to be edited before you run this program.
!          For example, the following header information,
!
!          > title = "grid"
!          > variables = "x","y"
!          > zone N=1218 ,E=2280 ,ET=quadrilateral,F=FEPOINT
!
!          needs to be modified as
!
!          > 1218
!          > 2280
!
!
!  Output:
!
!   project.grid                 ! 2D grid file with boundary node info.
!   project.bcmap                ! 2D boundary info (You need to edit this manually!)
!
!    NOTE: .grid and .bcmap files are used in EDU2D solvers.
!
!-------------------------------------------------------------------------------
!
!        written by Dr. Katate Masatsuka (info[at]cfdbooks.com),
!
! the author of useful CFD books, "I do like CFD" (http://www.cfdbooks.com).
!
! This is Version 1 (December 2016).
! The program may be updated in future.
!
!
! This F90 code is written and made available for an educational purpose.
! This file may be updated in future.
!
! Katate Masatsuka, http://www.cfdbooks.com
!*******************************************************************************
 program edu2d_tec2grid

 implicit none

!-------------------------------------------------------------------------------
! Parameters

  integer , parameter ::    dp = selected_real_kind(P=15)
  real(dp), parameter ::  zero = 0.0_dp

!-------------------------------------------------------------------------------
! Custom data types for 2D input grid

  type elm_data2d
   integer,   dimension(:), pointer :: vtx      !list of vertices
   integer                          :: nvtx
  end type elm_data2d

  type node_data2d
   real(dp)                         :: x, y !Coordinates in xy space
  end type node_data2d

  type bnode_data2d
   character(80)                    :: bc_type !type of boundary condition
   integer                          :: nbnodes !# of boundary nodes
   integer,   dimension(:), pointer :: bnode   !list of boundary nodes
  end type bnode_data2d

!-------------------------------------------------------------------------------
! Local variables

 !2D input grid
  type( node_data2d)  , dimension(:), pointer ::  node2d
  type(  elm_data2d)  , dimension(:), pointer ::  elmt2d
  type(bnode_data2d)  , dimension(:), pointer :: bound2d
  integer :: nnodes2d, nelmts2d, ntria2d, nquad2d, nbound2d

 !Local variables
 character(80) :: datafile_grid_in

!-------------------------------------------------------------------------------
! Input parameters

 character(80) :: project_name
 integer       :: tec_type ! Tecplot data packing type

!-------------------------------------------------------------------------------
! Output

 character(80) :: datafile_bcmap_out, datafile_grid_out

!*******************************************************************************
! Define input parameters.
!*******************************************************************************

   write(*,*)
   write(*,*) " project_name = ? "
   read(*,*)    project_name
   write(*,*)

   write(*,*) " tec_type = ? (integer)"
   write(*,*) "  Note: Tecplot data packing type: "
   write(*,*) "        = 1 for DATAPACKING=BLOCK  "
   write(*,*) "        = 2 for DATAPACKING=POINT  "
   write(*,*)
    read(*,*)   tec_type

 !Input
   datafile_grid_in   = trim(project_name) // ".data"   ! 2D input Tecplot grid file name

   write(*,*) " OK, will read the file ", trim(datafile_grid_in)

 !Output
   datafile_grid_out  = trim(project_name) // ".grid"   ! 
  datafile_bcmap_out  = trim(project_name) // ".bcmap"  ! 

!*******************************************************************************
! Read a modified Tecplot file, and generate .grid file and .bcmap file.
!*******************************************************************************

 call read_tecplot_write_grid

 write(*,*)
 write(*,*) " Successfully finished."
 write(*,*)

 stop

 contains

!********************************************************************************
!* Read a 2D grid file.
!*
!* ------------------------------------------------------------------------------
!*  Input: datafile_grid_in  = filename of a modified Tecplot file
!*
!* Output: datafile_grid_out  = .grid file with boundary node info.
!*         datafile_bcmap_out =  boundary info (You need to edit this manually!)
!* ------------------------------------------------------------------------------
!*
!*  In the .grid file:
!* 
!*   NOTE: Will add the first node to the end node if the segment is closed
!*         (e.g., airfoil) The number of nodes will be the actual number + 1
!*         in that case.
!*
!*   NOTE: Boundary nodes will be ordered such that the domain is on the left.
!*
!********************************************************************************
 subroutine read_tecplot_write_grid

 implicit none

!Local variables

 integer  :: i, j, k, os,  inode, ielm, k_of_i, kl, kr, ii, jj

 integer,   dimension(:  ), pointer :: n2n_nnghbrs
 integer,   dimension(:,:), pointer :: n2n_nghbr
 integer,   dimension(:,:), pointer :: n2n_nghbr_origin

 integer,   dimension(:  ), pointer :: n2e_nnghbrs
 integer,   dimension(:,:), pointer :: n2e_nghbr

 integer                            :: nbnodes_g
 integer,   dimension(:),   pointer :: bnode_g

 integer,   dimension(:)  , pointer :: bmark
 integer,   dimension(:)  , pointer :: bnode_flag
 integer,   dimension(:,:), pointer :: bnode_nghbr_g

 integer                            :: ncorners
 integer,   dimension(100)          :: corner

 real(dp),  dimension(2)            :: vR, vL
 real(dp)                           :: mag, vL_dot_vR

 integer                            :: nbnodes_temp, ib, in
 logical                            :: nodes_unprocessed
 character(80)                      :: bid, filename
 logical                            :: already_added

 integer                            :: nvR, nvL, ivL, ivR
 real(dp)                           :: dmin
 integer,  dimension(2)             :: imin
 integer,  dimension(20)            :: vRs, vLs

!--------------------------------------------------------------------------------
! 1. Read grid file>: datafile_grid_in
!--------------------------------------------------------------------------------

  write(*,*) "-----------------------------------------------------------"
  write(*,*) " Reading a Tecplot data file....", datafile_grid_in

  write(*,*)
  write(*,*) " (NOTE: No header information; just # of nodes and elements.)"
  write(*,*) " (NOTE: Only triangular grids at the moment.)"
  write(*,*)

!  Open the input file.
   open(unit=1, file=datafile_grid_in, status="unknown", iostat=os)

 ! READ: Get the size of the grid.
   read(1,*) nnodes2d
    write(*,*) "      nodes = ", nnodes2d
   read(1,*) ntria2d
    write(*,*) "  triangles = ", ntria2d
   nelmts2d = ntria2d
   nquad2d = 0

 !  Allocate node and element arrays.
   allocate(node2d(nnodes2d))
   allocate(elmt2d(nelmts2d))

 ! READ: Read the nodal coordinates, assuming DATAPACKING=BLOCK in the Tecplot file:

  !1. DATAPACKING=BLOCK
   if     (tec_type == 1) then

    read(1,*) ( node2d(i)%x, i= 1, nnodes2d )
    read(1,*) ( node2d(i)%y, i= 1, nnodes2d )

  !1. DATAPACKING=POINT
   elseif (tec_type == 2) then

    do i = 1, nnodes2d
     read(1,*) node2d(i)%x, node2d(i)%y
    end do

   endif

 ! Read element-connectivity information

 ! Triangles: assumed that the vertices are ordered counterclockwise
 !
 !         v3
 !         /\
 !        /  \
 !       /    \
 !      /      \
 !     /        \
 !    /__________\
 !   v1           v2

 ! READ: read connectivity info for triangles
    do i = 1, ntria2d
     elmt2d(i)%nvtx = 3
     allocate(elmt2d(i)%vtx(3))
     read(1,*) elmt2d(i)%vtx(1), elmt2d(i)%vtx(2), elmt2d(i)%vtx(3)
    end do


  write(*,*)

!--------------------------------------------------------------------------------
! 2. Create the boundary grid data
!--------------------------------------------------------------------------------

  write(*,*) "-------------------------------------------------------"
  write(*,*) " Creating the boundary grid data........."
  write(*,*)

  allocate(n2n_nnghbrs(nnodes2d))
  allocate(n2e_nnghbrs(nelmts2d))

  ! Let's just assume maximum of 30 neighbors... Hopefully large enough.
  allocate(n2n_nghbr(nnodes2d,30))
  allocate(n2e_nghbr(nelmts2d,30))

  allocate(n2n_nghbr_origin(nnodes2d,30))

!-----------------------------------------------------------------------------
! Construct a list of elements sharing the same node.

  write(*,*) " >>> Construct a list of elements sharing the same node."
  write(*,*)

     n2e_nnghbrs = 0
   do ielm = 1, ntria2d
    do k = 1, elmt2d(ielm)%nvtx
     inode = elmt2d(ielm)%vtx(k)
     n2e_nnghbrs(inode) = n2e_nnghbrs(inode) + 1
     n2e_nghbr(inode,n2e_nnghbrs(inode)) = ielm
    end do
   end do

!-----------------------------------------------------------------------------
! Construct a list of neighbor nodes for each node.

  write(*,*) " >>> Construct a list of neighbor nodes for each node."
  write(*,*)

    n2n_nnghbrs = 0
   do inode = 1, nnodes2d

    n2n_nnghbrs(inode) = 0

   !Loop over surrounding elements.
    do j  = 1, n2e_nnghbrs(inode)
     ielm = n2e_nghbr(inode,j)

     do k = 1, 3
      if (inode == elmt2d(ielm)%vtx(k)) then
       k_of_i = k
       exit
      endif
     end do

   ! Set (k_of_i, kr, kl) = (1,2,3), (2,3,1), (3,1,2).
     kr = k_of_i + 1 - 3*max(0,k_of_i + 1 -3)
     kl = kr     + 1 - 3*max(0,kr     + 1 -3)

   ! There are 2 candidate neighbor nodes: kl and kr.
   !         kl
   !         /\
   !        /  \
   !       /    \
   !      /      \
   !     /        \
   !    /__________\
   !  inode         kr


   ! First time, add both nodes as neighbors of inode.
     if (j==1) then

      n2n_nnghbrs(inode) = 2
      n2n_nghbr(inode,1) = elmt2d(ielm)%vtx(kr)
      n2n_nghbr(inode,2) = elmt2d(ielm)%vtx(kl)

     !Keep the record on whether the neighbor originates from kl or kr.
     !This is useful for the orientation of the boundary node list.
      n2n_nghbr_origin(inode,1) = 1 ! right node
      n2n_nghbr_origin(inode,2) = 2 ! left  node

   ! Add a node only if it is not already added.
     else

     !Check the node on the right.
       already_added = .false.
       do ii = 1, n2n_nnghbrs(inode)
        if ( elmt2d(ielm)%vtx(kr) == n2n_nghbr(inode,ii)      ) then
         already_added = .true.
        endif
       end do
       if(.not.already_added) then
         n2n_nnghbrs(inode) = n2n_nnghbrs(inode) + 1
         n2n_nghbr(inode,n2n_nnghbrs(inode)) = elmt2d(ielm)%vtx(kr)
         n2n_nghbr_origin(inode,n2n_nnghbrs(inode)) = 1
       endif

     !Check the node on the left.
       already_added = .false.
       do ii = 1, n2n_nnghbrs(inode)
        if ( elmt2d(ielm)%vtx(kl) == n2n_nghbr(inode,ii)      ) then
         already_added = .true.
        endif
       end do
       if(.not.already_added) then
         n2n_nnghbrs(inode) = n2n_nnghbrs(inode) + 1
         n2n_nghbr(inode,n2n_nnghbrs(inode)) = elmt2d(ielm)%vtx(kl)
         n2n_nghbr_origin(inode,n2n_nnghbrs(inode)) = 2
       endif

     endif

    end do

   end do

!-----------------------------------------------------------------------------


!-----------------------------------------------------------------------------
! Construct a global boundary node list. Just collect boundary nodes in 1D array.

  write(*,*) " >>> Construct a global boundary node list."
  write(*,*)

 ! # of neighbor elements = # of neighbor nodes at interior nodes.
 ! # of neighbor elements < # of neighbor nodes at boundary nodes.

 ! Count the total number of boundary nodes.
   nbnodes_g = 0
   do inode = 1, nnodes2d
    if ( n2n_nnghbrs(inode) /= n2e_nnghbrs(inode) ) then
     nbnodes_g = nbnodes_g + 1
    endif
   end do

 ! Allocate the global boundary node list.
   allocate(bnode_g(nbnodes_g))

 ! Collect the information.
   nbnodes_g = 0
   do inode = 1, nnodes2d
    if ( n2n_nnghbrs(inode) /= n2e_nnghbrs(inode) ) then
     nbnodes_g = nbnodes_g + 1
     bnode_g(nbnodes_g) = inode
    endif
   end do

 ! Mark the boundary nodes: bmark()=1 for boundary nodes, and =0 for interior nodes.
    allocate(bmark(nnodes2d))
    bmark = 0
   do i = 1, nbnodes_g
    k = bnode_g(i)
    bmark(k) = 1
   end do

!-----------------------------------------------------------------------------
! Create an ordered global neighbor list for boundary nodes.

  write(*,*) " >>> Construct an ordered global boundary node list."
  write(*,*)

  !Assume each boundary node has precisely two boundary neighbor nodes.
  allocate(bnode_nghbr_g(nnodes2d,2))
  bnode_nghbr_g = -10

   do i = 1, nbnodes_g
    inode = bnode_g(i)

   !Step 1: Store information of boundary neighbors originated from R and L.
   !        Note: there can be multiple of boundary neighbors. Some are conencted
   !              via interior edges. (e.g., near a geometrical corner.)
     nvR = 0
     nvL = 0
    do k = 1, n2n_nnghbrs(inode)
     j = n2n_nghbr(inode,k)
     if ( bmark(j) == 1 ) then

      if     ( n2n_nghbr_origin(inode,k) == 1) then
       nvR = nvR + 1
       vRs(nvR) = j  !Store the right neighbor
      elseif ( n2n_nghbr_origin(inode,k) == 2) then
       nvL = nvL + 1
       vLs(nvL) = j  !Store the left  neighbor
      else
      endif

     endif
    end do

   !Step 2: Select a pair with the largest angle.

     ivL  = 0
     ivR  = 0
     imin = 0
     dmin = 1000.0_dp

    do ii = 1, nvL
     do jj = 1, nvR
      
      vR(1) = node2d( vRs(jj) )%x - node2d( inode )%x
      vR(2) = node2d( vRs(jj) )%y - node2d( inode )%y
        mag = sqrt( vR(1)**2 + vR(2)**2 )
         vR = vR / mag

      vL(1) = node2d( vLs(ii) )%x - node2d( inode )%x
      vL(2) = node2d( vLs(ii) )%y - node2d( inode )%y
        mag = sqrt( vL(1)**2 + vL(2)**2 )
         vL = vL / mag
 
      vL_dot_vR = vL(1)*vR(1) + vL(2)*vR(2)
      if ( abs(vL_dot_vR+1.0_dp) < dmin ) then ! Smaller vL*vR + 1 -> larger angle (vL*vR = -1, 180 degrees)
       dmin = abs(vL_dot_vR+1.0_dp)
       imin(1) = vLs(ii)
       imin(2) = vRs(jj)
      endif

     end do
    end do

    if (imin(1) ==0 .or. imin(2)==0) then
     write(*,*) inode, imin
     stop
    endif

    !This pair should be the correct neighbors.
       bnode_nghbr_g(inode,1) = imin(2) !Store the right neighbor in bnode_nghbr_g(i,1).
       bnode_nghbr_g(inode,2) = imin(1) !Store the left  neighbor in bnode_nghbr_g(i,2).

    if (bmark( imin(1)) ==0 .or. bmark( imin(2) )==0) then
     write(*,*) " interior node? ", inode, imin
     stop
    endif

   end do

!-----------------------------------------------------------------------------
! Identify geometrical corners.
!
!          bnode_nghbr_g(i,2) 
!                 o
!                  \vL
!                   \     vR      
!                    o--------o bnode_nghbr_g(i,1)
!               bnode_g(i)
!
! The inner product, vL*vR , will be -1.0 if straight, 
! becomes zero if they make the right angle (90 degrees),
! and positive if the angle is les than 90 degrees.
!
  write(*,*) " >>> Detecting geometrical corners."
  write(*,*)

  ncorners = 0

   do i = 1, nbnodes_g
    inode = bnode_g(i)

    vR(1) = node2d( bnode_nghbr_g(inode,1) )%x - node2d( inode )%x
    vR(2) = node2d( bnode_nghbr_g(inode,1) )%y - node2d( inode )%y
    mag = sqrt( vR(1)**2 + vR(2)**2 )
    vR = vR / mag

    vL(1) = node2d( bnode_nghbr_g(inode,2) )%x - node2d( inode )%x
    vL(2) = node2d( bnode_nghbr_g(inode,2) )%y - node2d( inode )%y
    mag = sqrt( vL(1)**2 + vL(2)**2 )
    vL = vL / mag

    vL_dot_vR = vL(1)*vR(1) + vL(2)*vR(2)

    !Cornder if the dot product of the two adjacent edges makes
    !a large angle. For a smooth grid, abs(vL_dot_vR + 1.0_dp)
    !should be close to zero; close to 1.0 if the angle is close to 90 degrees.
    !The value 0.6 is just an empirical value.

    if ( abs(vL_dot_vR + 1.0_dp) > 0.6_dp) then
     ncorners = ncorners + 1
     corner(ncorners) = i
     bmark( inode ) = 2
     write(*,*) " vL_dot_vR  = ", vL_dot_vR 
    endif

   end do

   write(*,*) " ncorners = ", ncorners

   do i = 1, ncorners
    write(*,*) i, bnode_g( corner(i) ), node2d(bnode_g(corner(i)))%x, node2d(bnode_g(corner(i)))%y
   end do

!-----------------------------------------------------------------------------
! Construct boundary parts data.

  write(*,*)
  write(*,*) " >>> Construct boundary parts data."
  write(*,*)

  allocate(bnode_flag(nnodes2d))
  bnode_flag = 0

  !Assume 50 boundary parts at maximum.
  allocate(bound2d(50))

  ib = 0

!(1)Use corners to identify boundary parts:

  write(*,*)
  write(*,*) " (1)Use corners to identify boundary parts"
  write(*,*)

 do i = 1, ncorners

  write(*,*)
  write(*,*) " This is the boundary ", ib+1
  write(*,*)
  write(*,*) "  -- From a corner node ", bnode_g( corner(i) )

   ii = bnode_g( corner(i) )

   ib = ib + 1

 !Count the number of nodes from a corner to another.
   nbnodes_temp = 1
  do
   k = bnode_nghbr_g(ii ,1)
    nbnodes_temp = nbnodes_temp + 1

   if ( bmark(k) == 2 ) exit
   ii = k
  end do

 !We can now allocate the node list.
  bound2d(ib)%nbnodes = nbnodes_temp
  allocate(bound2d(ib)%bnode(nbnodes_temp))
 
 !Let's collect and store the node list.
   ii = bnode_g( corner(i) )
   bound2d(ib)%bnode(1) = ii
   bnode_flag( ii ) = 1
   
   nbnodes_temp = 1

   do
    k = bnode_nghbr_g(ii,1)
     nbnodes_temp = nbnodes_temp + 1
    bound2d(ib)%bnode( nbnodes_temp ) = k
    bnode_flag( k ) = 1
    if ( bmark(k) == 2 ) then
     write(*,*) "  --   To a corner node ", k
     exit
    endif
    ii = k
   end do

  write(*,*)

 end do

! Boundaries with corner nodes have been processed.
! Now, consider boundaries without corners, or
! closed boundaries.

!(2)Boundaries with no corners.

  write(*,*)
  write(*,*) " (2)Boundaries with no corners."
  write(*,*)

 !Check if there are boundary nodes that are not processed yet.

    nodes_unprocessed = .true.

  do
   if (.not.nodes_unprocessed) exit

    nodes_unprocessed = .false.
   do i = 1, nbnodes_g
    inode = bnode_g(i)
    if ( bnode_flag( inode ) == 0 ) then
     nodes_unprocessed = .true.
     in = i
     exit
    endif
   end do

  !--------------------------------------------------------------
  ! Construct a boundary part list if unprocessed nodes are found.
   if (nodes_unprocessed) then

    ii = in !Starting node found from the above.

    ib = ib + 1

    write(*,*) "     A boundary found: This is the boundary ", ib

   !Count the number of nodes in the boundary part.
     nbnodes_temp = 1
    do
     k = bnode_nghbr_g(ii,1)
      nbnodes_temp = nbnodes_temp + 1
      if ( k == in ) exit !Returned to the starting node (closed boundary!).
     ii = k
    end do

   !Allocate and fill the node list.
    bound2d(ib)%nbnodes = nbnodes_temp
    allocate(bound2d(ib)%bnode(nbnodes_temp))

     ii = in
     bound2d(ib)%bnode(1) = bnode_g(ii)
     bnode_flag( bnode_g(ii) ) = 1

     nbnodes_temp = 1

     do
      k = bnode_nghbr_g(ii,1)
       nbnodes_temp = nbnodes_temp + 1
      bound2d(ib)%bnode( nbnodes_temp ) = k
      bnode_flag( k ) = 1
      if ( k == in ) exit !Returned to the starting node (closed boundary!).
      ii = k
     end do

   endif
  !--------------------------------------------------------------

  end do

  ! Number of total boundary parts:
   nbound2d = ib

   !Print the boundary grid data.
    write(*,*)
    write(*,*) " --- Boundary list:"
    write(*,*)
    write(*,*) " Number of boudnaries = ", nbound2d
    write(*,*)
     do i = 1, nbound2d
      write(*,'(a10,i3,2(a11,i5))') "  boundary", i, "  bnodes = ", bound2d(i)%nbnodes, &
                                                     "  bfaces = ", bound2d(i)%nbnodes-1
     end do
    write(*,*)

  close(1)

  !--------------------------------------------------------------'
  ! Write Tecplot files for each boundary part.
  ! Use these files to identify exactly which domain boundary is
  ! given by which boundary grid. 

   write(*,*) "-------------------------------------------------------"
   write(*,*) " Writing Tecplot files for each boundary part..."
   write(*,*)

   do ib = 1, nbound2d

     write( bid  , '(i0)' ) ib
     filename = trim(project_name) // trim("_boundary_tec") // trim(".") // trim(bid) // '.dat'

   write(*,*) ">>> Boundary part, ", ib, " : ", trim(filename)

   open(unit=10, file=filename, status="unknown", iostat=os)

    write(10,*) 'variables = "x", "y"'
    write(10,*) 'ZONE t="', trim("Boundary.") // trim(bid) ,'" I =', bound2d(ib)%nbnodes, ', DATAPACKING=POINT'

    do i = 1, bound2d(ib)%nbnodes
     k = bound2d(ib)%bnode(i)
     write(10,*) node2d(k)%x, node2d(k)%y
    end do

   close(10)

   end do

   write(*,*)
   write(*,*) "NOTE: I have no idea which part of the domain boudnary   "
   write(*,*) "      each of the above represents. Load these data files"
   write(*,*) "      in Tecplot, and assign different colors in scatter plot"
   write(*,*) "      to see which domain boudnary each of the above represents."
   write(*,*) "      And set up BCs, in the .bcmap file that will be generated below. "
   write(*,*)

   write(*,*)

!--------------------------------------------------------------------------------
! 3. Write the .grid file, containing the boundary information.
!--------------------------------------------------------------------------------

   write(*,*) "-------------------------------------------------------"
   write(*,*) " Writing .grid file: ", trim(datafile_grid_out)

   open(unit=20, file=datafile_grid_out, status="unknown", iostat=os)

   write(20,*) nnodes2d, ntria2d, 0

   do i = 1, nnodes2d
    write(20,*) node2d(i)%x, node2d(i)%y
   end do

   do i = 1, ntria2d
    write(20,*) elmt2d(i)%vtx(1), elmt2d(i)%vtx(2), elmt2d(i)%vtx(3)
   end do

   write(20,*) nbound2d

   do ib = 1, nbound2d
    write(20,*) bound2d(ib)%nbnodes
   end do

   do ib = 1, nbound2d
    do k = 1, bound2d(ib)%nbnodes
     write(20,*) bound2d(ib)%bnode(k)
    end do
   end do

   close(20)

   write(*,*)

!--------------------------------------------------------------------------------
! 3. Write the .bcmap file, which can be used for a boundary condition list.
!--------------------------------------------------------------------------------

   write(*,*) "-------------------------------------------------------"
   write(*,*) " Writing .bcmap file: ", trim(datafile_bcmap_out)
   write(*,*)
   write(*,*) "NOTE: This is a BC file. Since I don't know the geomery, "
   write(*,*) "      all BCs are indicated as 'Dirichlet'. Please modify it."

   open(unit=10, file=datafile_bcmap_out, status="unknown", iostat=os)

    write(10,*) 'Boundary Segment  Boundary Condition'

    do ib = 1, nbound2d
     write(10,'(i10,a25)') ib, '   Dirichlet'
    end do

   close(10)

   write(*,*)



! End of read_tecplot_write_grid
!--------------------------------------------------------------------------------

 write(*,*)
 write(*,*) "NOTE: Visualize the boundary, and see where these boundaries are,"
 write(*,*) "      and then overwrite 'Dirichlet' by a suitable BC name for your solver."
 write(*,*)

 end subroutine read_tecplot_write_grid

 end program edu2d_tec2grid




