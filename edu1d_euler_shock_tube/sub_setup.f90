MODULE SETUPM
 USE EULER1D_DATA_TYPE; USE C_ARRAYS
CONTAINS
!*******************************************************************************************
!* Set up the problem and the method. 
!* 
!* Note(1): "11 Roe with MUSCL" is a 2nd order extension of the Roe scheme as described
!*           in LeVeque's book with the minmod limiter. 
!* Note(2): Hancock's 2nd order scheme is as described in AE623 project 1.
!*******************************************************************************************
 SUBROUTINE SETUP
 IMPLICIT NONE
 INTEGER :: NP,I
 CHARACTER(10) :: hancock,yes="y",no="n"
1 WRITE(*,*) "***************************************************************************"
  WRITE(*,*) "*  Choose a Problem"
  WRITE(*,*) "***************************************************************************"
  WRITE(*,*) "*  1  Sod's Standard Shock Tube Problem     " 
  WRITE(*,*) "*  2  No Expansion Wave to the left" 
  WRITE(*,*) "*  3  Severe Contact Discontinuity " 
  WRITE(*,*) "*  4  Sonic Rarefaction            " 
  WRITE(*,*) "*  5  Slowly Moving Shock          " 
  WRITE(*,*) "***************************************************************************"
  WRITE(*,'(A23)',ADVANCE="NO") "  The Problem Number = "; READ(*,*) NP; WRITE(*,*)
    IF (NP/=1 .AND. NP/=2 .AND. NP/=3 .AND. NP/=4 .AND. NP/=5) THEN; WRITE(*,*)
    WRITE(*,*) "The number is invalid!!! "; WRITE(*,*); GOTO 1; ENDIF
2 WRITE(*,*)  "***************************************************************************"
  WRITE(*,*)  "*  1 Lax-Friedrichs          1-STEP EVALUATION     : LxF    , 1st Order"
  WRITE(*,*)  "*  2 Richtmyer               2-STEP EVALUATION     : LW-type, 2nd Order"
  WRITE(*,*)  "*  3 MacCormack              2-STEP EVALUATION     : LW-type, 2nd Order"
  WRITE(*,*)  "*  4 Steger-Warming Flux     Flux Vector Splitting : Up-Wind, 1st Order"
  WRITE(*,*)  "*  5 Van Leer                Flux Vector Splitting : Up-Wind, 1st Order"
  WRITE(*,*)  "*  6 Liou-Steffen            Flux Vector Splitting : Up-Wind, 1st Order"
  WRITE(*,*)  "*  7 Zha-Bilgen              Flux Vector Splitting : Up-Wind, 1st Order"
  WRITE(*,*)  "*  8 Godunov                  Exact  Riemann Solver: Up-Wind, 1st Order"
  WRITE(*,*)  "*  9 Osher flux              Approx. Riemann Solver: Up-Wind, 1st Order"
  WRITE(*,*)  "* 10 Roe                     Approx. Riemann Solver: Up-Wind, 1st Order"
  WRITE(*,*)  "* 11 Roe with MUSCL          Approx. Riemann Solver: Up-Wind, 2nd Order"
  WRITE(*,*)  "* 12 Chernousov              Approx. Riemann Solver: Up-Wind, 1st Order"
  WRITE(*,*)  "* 13 Nishikawa               ??????????????????????: Up-Wind, 1st Order"
  WRITE(*,*)  "***************************************************************************"
  WRITE(*,'(A22)',ADVANCE="NO") "  The Scheme Number = "; READ(*,*) NS; WRITE(*,*)
   IF (NS/=1  .AND. NS/=2  .AND. NS/=3  .AND. NS/=4 .AND. NS/=5  .AND. &
       NS/=6  .AND. NS/=7  .AND. NS/=8  .AND. NS/=9 .AND. NS/=10 .AND. &
       NS/=11 .AND. NS/=12 .AND. NS/=13 ) THEN; WRITE(*,*)
       WRITE(*,*) "The number is invalid!!! "; WRITE(*,*); GOTO 2;  ENDIF
 IF (NS/=2 .AND. NS/=3 .AND. NS/=11) THEN
 WRITE(*,*) "***************************************************************************"
 WRITE(*,*) "* Use Hancock's Predictor Corrector Scheme to make it 2nd order? "
 WRITE(*,*) "***************************************************************************"  
 WRITE(*,'(A15)',ADVANCE="NO")  "  y or n? = "; READ(*,*) hancock
  IF (hancock==yes) THEN;  NH=2;  WRITE(*,*)
   WRITE(*,*)  "***************************************************************************"
   WRITE(*,*)  "*  Variables to be limited"
   WRITE(*,*)  "***************************************************************************"
   WRITE(*,*)  "*  1 Primitive Variables "
   WRITE(*,*)  "*  2 Characteristic Variables"
   WRITE(*,*)  "***************************************************************************"
   WRITE(*,'(A26)',ADVANCE="NO")  "  The variable number = "; READ(*,*) NRCON; WRITE(*,*)
   WRITE(*,*)  "***************************************************************************"
   WRITE(*,*)  "*  Choose a limiter"
   WRITE(*,*)  "***************************************************************************"
   WRITE(*,*)  "*  0 Do not use a limiter "
   WRITE(*,*)  "*  1 Minmod "
   WRITE(*,*)  "*  2 Double Minmod"
   WRITE(*,*)  "*  3 Symmetric Minmod"
   WRITE(*,*)  "*  4 Superbee"
   WRITE(*,*)  "*  5 Harmonic"
   WRITE(*,*)  "***************************************************************************"
   WRITE(*,'(A23)',ADVANCE="NO")  "  The limiter number = "; READ(*,*) NLim
  ELSE; NH=1; ENDIF; WRITE(*,*)
 ENDIF
 WRITE(*,*)  "***************************************************************************"
 WRITE(*,*)  "*  Choose the CFL Number, etc" 
 WRITE(*,*)  "***************************************************************************"
 WRITE(*,'(A12)',ADVANCE="NO")  "      CFL = "; READ(*,*) CFL; 
 WRITE(*,'(A12)',ADVANCE="NO")  "  N_Cells = "; READ(*,*)  NC;  ALLOCATE(Cell(0:NC+1)); WRITE(*,*)
 WRITE(*,*)  "***************************************************************************"
!*******************************************************************************************
!* PARAMETERS
!*******************************************************************************************
   xmin=-5.0D0; xmax=5.0D0; 
!**  NC CELLS IN THE DOMAIN AND 2 GHOST CELLS j=0,NC+1 (EVEN)
   dx=(xmax-xmin)/DBLE(NC);  NTmax=50000;
!*******************************************************************************************
!* INITIAL CONDITIONS:  RIEMANN PROBLEMS (DIAPHRAM AT x=0)
!*******************************************************************************************
    IF (NP==1) THEN
       rL=1.0D0    ; PL=1.0D+00   ; uL=0.0D0     ; 
       rR=0.125D0  ; PR=1.0D-01   ; uR=0.0D0     ; Tf=1.7D0;
    ELSEIF (NP==2) THEN
       rL=1.0D0    ; PL=2.0D0   ; uL=0.620174D0;     !** No expansion to the left.
       rR=1.0D0    ; PR=1.0D0   ; uR=0.0D0     ; Tf=1.7D0;
    ELSEIF (NP==3) THEN
       rL= 0.445D0 ; PL=3.528D0 ; uL=0.698D0   ;      !** severe contact disconti
       rR=0.5D0    ; PR=0.571D0 ; uR=0.0D0     ; Tf=1.16D0; 
    ELSEIF (NP==4) THEN
       rL= 3.857D0 ; PL=10.333D0; uL=0.92D0    ;      !** Sonic rarefaction
       rR=1.0D0    ; PR=1.0D0   ; uR=3.55D0    ; Tf=0.7D0; 
    ELSEIF (NP==5) THEN
       rL=3.86D0   ; PL=10.33D0 ; uL=-0.81D0   ; !** Slowly Moving Shock
       rR=1.0D0    ; PR=1.0D0   ; uR=-3.44D0   ; Tf=1.7D0;
    ENDIF
!*******************************************************************************************
!* STORE THE SOLUTIONS
!*******************************************************************************************
!*** NO CELL-AVERAGING IS NECESSARY HERE.
   DO I=0,NC+1; 
      IF (I<=NC/2) THEN;
         Cell(I)%W(1)=rL; Cell(I)%W(2)=uL; Cell(I)%W(3)=pL;
      ELSE
         Cell(I)%W(1)=rR; Cell(I)%W(2)=uR; Cell(I)%W(3)=pR;
      ENDIF
     Cell(I)%xc=xmin+DBLE(I-1)*dx;  CALL WtoU(Cell,I)
   END DO
 END SUBROUTINE SETUP

!*******************************************************************************************
!* 
!*******************************************************************************************
END MODULE SETUPM




