MODULE EXACT_RIEMANN_M
 USE EULER1D_DATA_TYPE; USE C_ARRAYS
CONTAINS
!*******************************************************************************************
!*  EXACT RIEMANN SOLVER
!*******************************************************************************************
 SUBROUTINE EXACT(t)
 IMPLICIT NONE
 DOUBLE PRECISION :: r(2),P(2),u(2),C(2),rm(2),CW1(3),CW2(3),Pm,PmI,um,t;
 INTEGER :: cav=0,N=200 
 WRITE(*,*)
 WRITE(*,*) "COMPUTING EXACT SOLUTION ................................."
!****************************************************************************
!* ENTER INITIAL STATES. (If cavitation occurs, stop.)
!****************************************************************************/
   r(1)=rL; u(1)=UL; P(1)=PL; r(2)=rR; u(2)=UR; P(2)=PR; 
   CALL initial_state(r,P,u,C,cav);  IF (cav==1) WRITE(*,*) "Cavit created!! " 
!****************************************************************************
!* ESTIMATE THE INITIAL GUESS OF Pm BY COMPUTING THE UPPER AND LOWER BOUNDS
!****************************************************************************/
   PmI=initial_P(r,P,u,C);
!****************************************************************************
!* COMPUTE Pm by FIXED-POINT ITERATION STARTING FROM PI(ALSO OTHER QUANTITIES)
!****************************************************************************/
   Pm=iterate_FP(r,rm,P,u,C,um,PmI);
!****************************************************************************
!* GET INFORMATION ABOUT WAVES. (GET CW1 and CW2, WAVE SPEEDS)
!****************************************************************************/
   CALL waves(r,rm,P,u,C,um,Pm,CW1,CW2);
!****************************************************************************
!* SAMPLE THE SOLUTIONS AT time t (Pointwise at the cell center)
!****************************************************************************/
   CALL sample_cell(N,t,CW1,CW2,r,rm,P,u,um,Pm,C); 
   WRITE(*,*) "COMPUTING EXACT SOLUTION DONE ............................."
!****************************************************************************
!* END OF PROGRAM
!****************************************************************************/
 END SUBROUTINE EXACT
!*******************************************************************************************

!****************************************************************************
!* ENTER THE INITIAL STATES
!****************************************************************************/
 SUBROUTINE initial_state(r,P,u,C,cav)
 IMPLICIT NONE
 DOUBLE PRECISION :: r(2),P(2),u(2),C(2)
 INTEGER :: cav
  C(1)=sqrt(gamma*P(1)/r(1)); C(2)=sqrt(gamma*P(2)/r(2))
!***  TEST FOR CAVITY : CAVITY -> cav=1 else cav=0
  IF (u(1)+2.0D0/(gamma-1.0D0)*C(1)<u(2)-2.0D0/(gamma-1.0D0)*C(2)) THEN
     cav=1; 
     WRITE(*,*) "Cavitation occurs. No solutions for this Riemann Problem." 
     WRITE(*,*)
  ENDIF
 END SUBROUTINE initial_state
!****************************************************************************
!* FIXED-POINT ITERATION
!****************************************************************************/
  FUNCTION iterate_FP(r,rm,P,u,C,um,PI)
  IMPLICIT NONE
  DOUBLE PRECISION :: r(2),P(2),u(2),C(2),rm(2),um,PI,gam,tol=1.0D-10
  DOUBLE PRECISION :: rL,rR,uL,uR,PL,PR,CL,CR,Pm1,Pm2,mL,mR,dP,iterate_FP
  INTEGER :: I,NI,NIMAX=200
  gam=(gamma+1.0D0)/(gamma-1.0D0)
  rL=r(1);rR=r(2);uL=u(1);uR=u(2);PL=P(1);PR=P(2);CL=C(1);CR=C(2); Pm1=PI;
!*** FIXED-POINT ITERATION
   DO
     mL=massflux(rL,CL,PL,Pm1); mR=massflux(rR,CR,PR,Pm1);
     Pm2=(mL*PR+mR*PL-mL*mR*(uR-uL))/(mL+mR); dP=abs(Pm2-Pm1);
     NI=NI+1; IF (NI>NIMAX) EXIT; Pm1=Pm2; IF (dP<tol) EXIT; 
   END DO
     mL=massflux(rL,CL,PL,Pm2); mR=massflux(rR,CR,PR,Pm2); 
     um=(mL*uL+mR*uR-(PR-PL))/(mL+mR);
   DO I=1,2
        IF (Pm2/P(I)>=1.0D0) THEN
         rm(I)=r(I)*(1.0D0+gam*Pm2/P(I))/(gam+Pm2/P(I));
        ELSE
         rm(I)=r(I)*(Pm2/P(I))**(1.0D0/gamma); 
        ENDIF
   END DO
!      WRITE(*,*) NI," ITERATIONS by FIXED-POINT ITR";
    iterate_FP=Pm2
  END FUNCTION iterate_FP
!****************************************************************************
!* INITIAL GUESS
!****************************************************************************/
 FUNCTION initial_P(r,P,u,C)
 IMPLICIT NONE
  DOUBLE PRECISION :: r(2),P(2),u(2),C(2),mL,mR,PL,PR,uL,uR,rL,rR,CL,CR,PUP,PLW,z,gam,initial_P
  rL=r(1);rR=r(2);uL=u(1);uR=u(2);PL=P(1);PR=P(2);CL=C(1);CR=C(2); 
  gam=0.5D0*(gamma-1.0D0)/gamma
 !**LINEAR FLUXES
    mL=rL*CL; mR=rR*CR; 
 !**LOWER BOUND
    PLW=(mL*PR+mR*PL-mL*mR*(uR-uL))/(mL+mR); 
    IF (PLW<0.0D0) PLW=1.0D-03;    
 !**UPPER BOUND 
    z=(0.5D0*(uL-uR)*(gamma-1)+CL+CR)/(CL*(PL)**((1.0D0-gam)/gam*0.5D0) &
                                  & +CR*(PR)**((1-gam)/gam*0.5D0));
    PUP=z**(1.0D0/gam);
  initial_P=PUP
 END FUNCTION initial_P
!***************************************************************************
!* WAVE INFORMATION
!*   CW1[2]=1 IF 1-SHOCK : CW2[2]=1 IF 3-SHOCK  
!*   CW1[2]=0 IF 1-EXPN  : CW2[2]=0 IF 3-EXPN
!*   HEAD SPEED = CW1[0] : HEAD SPEED = CW2[0] :
!*   TAIL SPEED = CW1[1] : TAIL SPEED = CW2[1] :
!* NOTE: CW1,2[0] GIVE, in both cases, the lowest and the fastest speeds.
!****************************************************************************/
 SUBROUTINE waves(r,rm,P,u,C,um,Pm,CW1,CW2)
 IMPLICIT NONE
 DOUBLE PRECISION :: r(2),P(2),u(2),C(2),rm(2),um,Pm,CW1(3),CW2(3)

!** LEFT STATE
   IF (P(1)<Pm) THEN 
       CW1(1)=u(1)+(gamma*P(1)/r(1))*(Pm/P(1)-1.0D0)/(gamma*(um-u(1)));
       CW1(2)=CW1(1); CW1(3)=1.0D0;
   ELSE
       CW1(1)=u(1)-sqrt(gamma*P(1)/r(1)); CW1(2)=um-sqrt(gamma*Pm/rm(1));
       CW1(3)=0.0D0;
   ENDIF
!** RIGHT STATE
   IF (P(2)<Pm) THEN
       CW2(1)=u(2)+(gamma*P(2)/r(2))*(Pm/P(2)-1.0D0)/(gamma*(um-u(2)));
       CW2(2)=CW2(1);CW2(3)=1.0D0;
   ELSE
       CW2(1)=u(2)+sqrt(gamma*P(2)/r(2)); CW2(2)=um+sqrt(gamma*Pm/rm(2));
       CW2(3)=0.0D0; 
   ENDIF
 END SUBROUTINE waves
!****************************************************************************
!* SAMPLE
!****************************************************************************/
 SUBROUTINE sample_cell(N,t,CW1,CW2,r,rm,P,u,um,Pm,C)
 IMPLICIT NONE
 DOUBLE PRECISION :: xc,xi,xmid,xiL,xis,uxc,Pxc,rxc,CW1(3),CW2(3)
 DOUBLE PRECISION :: r(2),P(2),u(2),C(2),rm(2),Pm,um,t
 INTEGER :: N,I,OS
 OPEN(UNIT=1, FILE ="sample_cell.dat", STATUS="UNKNOWN", IOSTAT=OS)
 xc = 0.0D0; !rxc=0.0D0; uxc=0.0D0; Pxc=0.0D0
 DO I=1,N
!***** AT A CEL CENTER ******************************************************/
      IF (I==1) THEN 
        xc = xmin+( (xmax-xmin)/DBLE(N)*0.5D0 ) ; xi = xc/t;
      ELSE
        xc = xc+( (xmax-xmin)/DBLE(N) );  xi = xc/t;
      ENDIF
!*****FIND THE SOLUTION  ****************************************************/
!** Left State
    IF (xi <= CW1(1)) THEN
       rxc=r(1); uxc=u(1); Pxc=P(1);
!** Right State
    ELSEIF (xi >= CW2(1)) THEN
       rxc=r(2); uxc=u(2); Pxc=P(2); 
!** Intermediate State
    ELSEIF (xi <= CW2(2) .AND. xi >= CW1(2)) THEN
      uxc=um; Pxc=Pm; 
          IF (xi < um) THEN
            rxc=rm(1);
          ELSE
            rxc=rm(2); 
          ENDIF
!** 1-Rarefaction
    ELSEIF (xi > CW1(1) .AND. xi < CW1(2) ) THEN
        xiL=CW1(1); xis=CW1(2);
        uxc=u(1)*(xi-xis)/(xiL-xis)+um*(xi-xiL)/(xis-xiL);
        Pxc=P(1)*((uxc-xi)/C(1))**(2.0D0*gamma/(gamma-1.0D0));
        rxc=r(1)*((uxc-xi)/C(1))**(2.0D0/(gamma-1.0D0)); 
!** 3-Rarefaction
    ELSE
        xis=CW2(1); xiL=CW2(2);
        uxc=um*(xi-xis)/(xiL-xis)+u(2)*(xi-xiL)/(xis-xiL);
        Pxc=P(2)*((xi-uxc)/C(2))**(2.0D0*gamma/(gamma-1.0D0));
        rxc=r(2)*((xi-uxc)/C(2))**(2.0D0/(gamma-1.0D0)); 
    ENDIF
!***** OUTPUT *************************************************************/ 
   WRITE(1,'(5ES15.7)') xc,rxc,uxc,Pxc,LOG(Pxc*rxc**(-gamma))/(gamma-1.0D0);
 END DO; CLOSE(1)
 END SUBROUTINE sample_cell
!****************************************************************************
!* MASS FLUX for Godunov's Flux Function
!****************************************************************************/
  FUNCTION massflux(r,c,PQ,Pm)
  IMPLICIT NONE
  DOUBLE PRECISION :: r,c,PQ,Pm,gam1,gam2,massflux
   gam1=0.5D0*(gamma+1.0D0)/gamma; gam2=0.5D0*(gamma-1.0D0)/gamma;
   IF (Pm/PQ>=1-1.0D-07) THEN
      massflux=(r*c)*sqrt( 1.0D0+gam1*(Pm/PQ-1.0D0) ); RETURN;
   ELSE
      massflux=(r*c)*gam2*(1.0D0-Pm/PQ)/( 1.0D0-(Pm/PQ)**(gam2) )
   ENDIF
  END FUNCTION massflux
!****************************************************************************
END MODULE EXACT_RIEMANN_M
