MODULE FLUXM
 USE EULER1D_DATA_TYPE; USE C_ARRAYS;
CONTAINS
!****************************************************************************************************  
!* 1D EULER FLUX VECTOR
!****************************************************************************************************  
 FUNCTION Euler_f(u)
 IMPLICIT NONE; 
 DOUBLE PRECISION, INTENT( IN) :: u(3)
 DOUBLE PRECISION :: Euler_f(3),q,gm1,u2o1,gm1u3q
  q=u(2)*u(2)/u(1)/2.0D0; gm1=gamma-1.0D0; u2o1=u(2)/u(1); gm1u3q=gm1*(u(3)-q)
  Euler_f(1)=u(2)
  Euler_f(2)=u(2)*u2o1 +gm1u3q
  Euler_f(3)=u2o1*(u(3)+gm1u3q)
 END FUNCTION Euler_f
!****************************************************************************************************  
!* FLUX FUNCTIONS
!****************************************************************************************************  
 SUBROUTINE FLUX(F,JL,JR,NS)
 IMPLICIT NONE
 DOUBLE PRECISION :: F(3),UL(3),UR(3),WL(3),WR(3),cL,cR,HL,HR
 INTEGER :: JL,JR,NS
!****************************************************************************************************  
!* Variables on the left and the right.
!****************************************************************************************************
 IF (NH==2) THEN
  IF (JL/=0) THEN
   WL=Cell(JL)%WR;  CALL UfromW(UL,cL,HL,WL)
  ELSE
   UL=Cell(JL)%U; WL=Cell(JL)%W; cL=Cell(JL)%c;  HL=Cell(JL)%H
  ENDIF
  IF (JR/=NC+1) THEN
   WR=Cell(JR)%WL; CALL UfromW(UR,cR,HR,WR)
  ELSE
   UR=Cell(JR)%U; WR=Cell(JR)%W; cR=Cell(JR)%c;  HR=Cell(JR)%H
  ENDIF
 ELSE
  UL=Cell(JL)%U; WL=Cell(JL)%W; UR=Cell(JR)%U; WR=Cell(JR)%W
  cL=Cell(JL)%c; cR=Cell(JR)%c; HL=Cell(JL)%H; HR=Cell(JR)%H
 ENDIF
!****************************************************************************************************  
!* CHOOSE A FLUX FUNCTION
!****************************************************************************************************  
 IF     (NS==1) THEN
  CALL LaxFriedrichs(F,UL,UR,WL,WR,cL,cR,HL,HR)
 ELSEIF (NS==2) THEN
  CALL Richtmyer(F,UL,UR,WL,WR,cL,cR,HL,HR)
 ELSEIF (NS==3) THEN
  CALL MacCormack(F,UL,UR,WL,WR,cL,cR,HL,HR)
 ELSEIF (NS==4) THEN
  CALL StegerWarming(F,UL,UR,WL,WR,cL,cR,HL,HR)
 ELSEIF (NS==5) THEN
  CALL VanLeer(F,UL,UR,WL,WR,cL,cR,HL,HR)
 ELSEIF (NS==6) THEN
  CALL AUSM(F,UL,UR,WL,WR,cL,cR,HL,HR)
 ELSEIF (NS==7) THEN
  CALL ZHA_BILGEN(F,UL,UR,WL,WR,cL,cR,HL,HR)
 ELSEIF (NS==8) THEN
  CALL Godunov(F,UL,UR,WL,WR,cL,cR,HL,HR)
 ELSEIF (NS==9) THEN
  CALL Osher(F,UL,UR,WL,WR,cL,cR,HL,HR)
 ELSEIF (NS==10) THEN
  CALL Roe(F,UL,UR,WL,WR,cL,cR,HL,HR)
 ELSEIF (NS==11) THEN
  CALL Roe2nd(F,UL,UR,WL,WR,cL,cR,HL,HR,JL)
 ELSEIF (NS==12) THEN
  CALL Chernousov(F,UL,UR,WL,WR,cL,cR,HL,HR)
 ELSEIF (NS==13) THEN
  CALL Nishikawa(F,UL,UR,WL,WR,cL,cR,HL,HR)
 ENDIF
 END SUBROUTINE FLUX
!**********************************************************************************************************    
!* FLUX FUNCTIONS
!********************************************************************************************************** 

!*************************************************************************************************    
!* Nishikawa's Scheme
!*************************************************************************************************    
 SUBROUTINE Nishikawa(F,UL,UR,WL,WR,cL,cR,HL,HR)
 IMPLICIT NONE
 DOUBLE PRECISION :: F(3),UL(3),UR(3),WL(3),WR(3),cL,cR,HL,HR
 DOUBLE PRECISION :: vL,vR,rL,rR,PL,PR,RT,rhoo,uo,Ho,ao,eso,po,Eo,U1r,U2r,U3r,u,p,pu,a,rho,H,E,es,esr
 DOUBLE PRECISION :: U1,U2,U3,s,sqre,lp1,lp2,du,dp
 INTEGER :: J,K
 rL=WL(1); rR=WR(1); vL=WL(2); vR=WR(2); PL=WL(3); PR=WR(3);
!**** First Compute the Roe Averages **************************
   RT=sqrt(rR/rL);
   rhoo=RT*rL; uo=(vL+RT*vR)/(1.0D0+RT); Ho=(HL+RT*HR)/(1.0D0+RT);
   ao=sqrt( (gamma-1.0D0)*(Ho-0.5D0*uo*uo) ); pr=ao*ao*rhoo/gamma; Eo=Ho-po/rhoo;
   eso=Eo-uo*uo/2.0; U1r=rhoo; U2r=rhoo*uo; U3r=rhoo*Eo;

     u=0.5D0*( vL+vR ); p=0.5D0*( PL+PR ); pu=0.5D0*( vL*PL+vR*PR ); a=0.5D0*( cL+cR );
   rho=0.5D0*( rL+rR ); H=0.5D0*(HL+HR);      E=p/(gamma-1.0D0)+0.5D0*u*u  ; es=E-0.5D0*u*u;

   U1=0.5*( UL(1)+UR(1) ); U2=0.5*(UL(2)+UR(2)) ; U3=0.5*(UL(3)+UR(3));
!**** Compute the FLUX **************************************************************
   !     if (u>0.0) { F[0]= rho*u; F[1]= WL2*UL2; F[2]= WL2*UL3; }
   !     else       { F[0]= rho*u; F[1]= WR2*UR2; F[2]= WR2*UR3; }

     IF  (u>0.0D0) THEN;   F(1)= u*UL(1); F(2)= u*UL(2); F(3)= u*UL(3);
     ELSE              ;   F(1)= u*UR(1); F(2)= u*UR(2); F(3)= u*UR(3); ENDIF

!     //      F[0]= 0.5*(WL2*UL1+WR2*UR1) - 0.5*( fabs(ur)*U1r ); 
!     // F[1]= 0.5*(WL2*UL2+WR2*UR2) - 0.5*( fabs(ur)*U2r ); 
!!     // F[2]= 0.5*(WL2*UL3+WR2*UR3) - 0.5*( fabs(ur)*U3r ); !

!     //if (K=1) { F[1]=F[1]+p; F[2]=F[2]+1.0*pu; }
!     //else     { F[1]=F[1]+p; F[2]=F[2]+1.0*pu; }

      du=vR-vL; dp=PR-PL; sqre=sqrt(esr);
      s=(gamma-1.0D0)*sqre; lp1=0.5D0*(rhoo*du+dp/s); lp2=0.5*(rhoo*du-dp/s);

       F(2)=F(2) + 0.5D0*(PL+PR)       - 0.5D0*( s*lp1*1.0D0     + s*lp2*1.0D0     );
       F(3)=F(3) + 0.5D0*(vL*PL+vR*PR) - 0.5D0*( s*lp1*(uo+sqre) + s*lp2*(uo-sqre) );
 END SUBROUTINE Nishikawa
     
!**********************************************************************************************************      
!* Lax-Friedrichs's Flux Function: F(U,J); Flux between J and (J+1). 1-STEP
!**********************************************************************************************************      
 SUBROUTINE LaxFriedrichs(F,UL,UR,WL,WR,cL,cR,HL,HR)
 IMPLICIT NONE
 DOUBLE PRECISION :: F(3),UL(3),UR(3),WL(3),WR(3),cL,cR,HL,HR
   F=0.5D0*( dx/dt*(UL-UR) + ( Euler_f(UR)+Euler_f(UL) ) );
 END SUBROUTINE LaxFriedrichs
!**********************************************************************************************************      
!* Richtmyer's Flux Function: F(U,J); Flux between J and (J+1).  2-STEPS
!**********************************************************************************************************      
 SUBROUTINE Richtmyer(F,UL,UR,WL,WR,cL,cR,HL,HR)
 IMPLICIT NONE
 DOUBLE PRECISION :: F(3),UL(3),UR(3),WL(3),WR(3),cL,cR,HL,HR,US(3)
!**** First Compute the Intermediate State ******************************************
   US=0.5*( (UL+UR)-dt/dx*(Euler_f(UR)-Euler_f(UL)) );
!**** Second Compute the FLUX *******************************************************
   F=Euler_f(US)
 END SUBROUTINE Richtmyer
!**********************************************************************************************************      
!* MacCormack's Flux Function: F(U,J); Flux between J and (J+1). 2-STEPS
!**********************************************************************************************************      
 SUBROUTINE MacCormack(F,UL,UR,WL,WR,cL,cR,HL,HR)
 IMPLICIT NONE
 DOUBLE PRECISION :: F(3),UL(3),UR(3),WL(3),WR(3),cL,cR,HL,HR,US(3)
!**** First Compute the Intermediate State *******************************************
   US=UL-dt/dx*( Euler_f(UR)-Euler_f(UL) );
!**** Second Compute the FLUX ********************************************************
   F=0.5*( Euler_f(UR)+Euler_f(US) ); 
 END SUBROUTINE MacCormack
!**********************************************************************************************************      
!* Steger-Warming's Flux Function : F(U,J); Flux between J and (J+1).
!**********************************************************************************************************       
 SUBROUTINE StegerWarming(F,UL,UR,WL,WR,cL,cR,HL,HR)
 IMPLICIT NONE
 DOUBLE PRECISION :: F(3),UL(3),UR(3),WL(3),WR(3),cL,cR,HL,HR,ML,MR,maxML,minMR,Fp(3),Fn(3),Z,Q
 INTEGER :: K
 ML=WL(2)/cL; MR=WR(2)/cR;  maxML=max(0.0D0,ML); minMR=min(0.0D0,MR); 
!**** Positive Part of Flux evaluated in L****************************************//
 IF (ML*ML>1.0D0) THEN
     Fp(1)=WL(1)*cL                                      * maxML;
     Fp(2)=Fp(1)*cL/(gamma*ML)*(gamma*ML*ML+1.0D0)       * maxML;
     Fp(3)=Fp(1)*cL*cL*0.5D0*(2.0D0/(gamma-1.0D0)+ML*ML) * maxML;
 ELSE 
     Z=0.5*WL(1)*cL/gamma*(ML+1.0D0); Q=WL(1)*cL*(gamma-1.0D0)/gamma;
     Fp(1)=Z                                              +Q                  *maxML; 
     Fp(2)=Z*cL*(ML+1)                                    +Q*cL*ML            *maxML; 
     Fp(3)=Z*0.5D0*cL*cL*((ML+1.0D0)*(ML+1.0D0)+(3.0D0-gamma)/(gamma-1.0D0))+Q*0.5D0*cL*cL*ML*ML*maxML;     
 ENDIF
!**** Negative Part of Flux evaluated in R****************************************
 IF (MR*MR>1.0D0) THEN
     Fn(1)=WR(1)*cR                              * minMR;
     Fn(2)=Fn(1)*cR/(gamma*MR)*(gamma*MR*MR+1.0D0) * minMR;
     Fn(3)=Fn(1)*cR*cR*0.5D0*(2.0D0/(gamma-1.0D0)+MR*MR) * minMR;
 ELSE
     Z=0.5D0*WR(1)*cR/gamma*(MR-1.0D0); Q=WR(1)*cR*(gamma-1.0D0)/gamma;
     Fn(1)=Z                                              +Q                *minMR; 
     Fn(2)=Z*cR*(MR-1)                                    +Q*CR*MR          *minMR; 
     Fn(3)=Z*0.5D0*cR*cR*((MR-1.0D0)*(MR-1.0D0)+(3.0D0-gamma)/(gamma-1.0D0))+Q*0.5D0*cR*cR*MR*MR*minMR;
 ENDIF  
!**** Compute the FLUX ***********************************************************
   F=Fp+Fn
 END SUBROUTINE StegerWarming
!*********************************************************************************************************        
!* Van Leer's Flux Function : F(U,J); Flux between J and (J+1).
!*********************************************************************************************************        
 SUBROUTINE VanLeer(F,UL,UR,WL,WR,cL,cR,HL,HR)
 IMPLICIT NONE
 DOUBLE PRECISION :: F(3),UL(3),UR(3),WL(3),WR(3),cL,cR,HL,HR,ML,MR,Fp(3),Fn(3)
 INTEGER :: K
 ML=WL(2)/cL; MR=WR(2)/cR;  
!**** Positive Part of Flux ***********************************
 Fp(1)=0.25D0*WL(1)*cL*(ML+1.0D0)*(ML+1.0D0);
 Fp(2)=Fp(1)*2.0D0*cL*(1.0D0+0.5D0*(gamma-1.0D0)*ML)/gamma;
 Fp(3)=Fp(1)*2.0D0*cL*cL*(1.0D0+0.5D0*(gamma-1.0D0)*ML)*(1.0D0+0.5D0*(gamma-1.0D0)*ML)/(gamma*gamma-1.0D0);
!**** Negative Part of Flux ***********************************
 Fn(1)=-0.25D0*WR(1)*cR*(MR-1.0D0)*(MR-1.0D0);
 Fn(2)=Fn(1)*2.0D0*cR*(-1.0D0+0.5D0*(gamma-1.0D0)*MR)/gamma;
 Fn(3)=Fn(1)*2.0D0*cR*cR*(1.0D0-0.5D0*(gamma-1.0D0)*MR)*(1.0D0-0.5D0*(gamma-1.0D0)*MR)/(gamma*gamma-1.0D0);
!**** Compute the FLUX ****************************************
   F=Fp+Fn
 END SUBROUTINE VanLeer
!*************************************************************************************************    
!* AUSM Flux Function : F(U,J); Flux between J and (J+1).
!*************************************************************************************************    
 SUBROUTINE AUSM(F,UL,UR,WL,WR,cL,cR,HL,HR)
 IMPLICIT NONE
 DOUBLE PRECISION :: F(3),UL(3),UR(3),WL(3),WR(3),cL,cR,HL,HR,ML,MR,Fp(3),Fn(3)
 DOUBLE PRECISION :: Pp,Pm,Mp,Mm,PL,PR
 INTEGER :: K
 ML=WL(2)/cL; MR=WR(2)/cR;  PL=WL(3); PR=WR(3)
!**** Positive Part of Flux ***********************************
 IF (ML<=-1.0D0) THEN; Mp=0.0D0; Pp=0.0D0;
 ELSEIF (ML<1.0) THEN 
        Mp = 0.25D0*(ML+1.0D0)*(ML+1.0D0);
!	 Pp = 0.5D0*(1.0D0+ML)*PL;     
        Pp = 0.25D0*PL*(1.0D0+ML)*(1.0D0+ML)*(2.0D0-ML); 
 ELSE; Mp = ML; Pp = PL; ENDIF
!**** Negative Part of Flux ***********************************
 IF   (MR<=-1.0D0) THEN; Mm=MR; Pm=PR;
 ELSEIF (MR<1.0D0) THEN
     Mm = -0.25D0*(MR-1.0D0)*(MR-1.0D0);
!    Pm = 0.5D0*(1.0D0-MR)*PR;      
     Pm = 0.25D0*PR*(1.0D0-MR)*(1.0D0-MR)*(2.0D0+MR); 
 ELSE; Mm = 0.0D0; Pm = 0.0D0; ENDIF
!**** Positive Part of Flux ***********************************
 Fp(1)=max(0.0D0,Mp+Mm)*WL(1)*cL;
 Fp(2)=max(0.0D0,Mp+Mm)*WL(1)*cL*WL(2) + Pp;
 Fp(3)=max(0.0D0,Mp+Mm)*WL(1)*cL*HL;
!**** Negative Part of Flux ***********************************
 Fn(1)=min(0.0D0,Mp+Mm)*WR(1)*cR;
 Fn(2)=min(0.0D0,Mp+Mm)*WR(1)*cR*WR(2) + Pm;
 Fn(3)=min(0.0D0,Mp+Mm)*WR(1)*cR*HR;
!**** Compute the FLUX ****************************************
    F=Fp+Fn
 END SUBROUTINE AUSM
!*************************************************************************************************    
!* ZHA-BILGEN Flux Function : F(U,J); Flux between J and (J+1).
!*************************************************************************************************    
 SUBROUTINE ZHA_BILGEN(F,UL,UR,WL,WR,cL,cR,HL,HR)
 IMPLICIT NONE
 DOUBLE PRECISION :: F(3),UL(3),UR(3),WL(3),WR(3),cL,cR,HL,HR,ML,MR,Fp(3),Fn(3)
 DOUBLE PRECISION :: Pp,Pm,Mp,Mm,PL,PR,Pup,Pum
 INTEGER :: K
 ML=WL(2)/cL; MR=WR(2)/cR;  PL=WL(3); PR=WR(3); 
!**** Positive Part of Flux ***********************************
 IF   (ML<=-1.0D0) THEN;  Pup=0.0; Pp=0.0;
 ELSEIF (ML<1.0D0) THEN 
     Pup = 0.5D0*(WL(2)+cL)*PL;
	 Pp = 0.5D0*(1.0D0+ML)*PL;  
	! Pp = 0.25D0*PL*(1.0D0+ML)*(1.0D0+ML)*(2.0D0-ML); 
 ELSE; Pup = PL*WL(2); Pp = PL; ENDIF
!**** Negative Part of Flux ***********************************
 IF (MR<=-1.0D0) THEN;  Pum=PR*WR(2); Pm=PR;
 ELSEIF (MR<1.0) THEN 
     Pum = 0.5D0*(WR(2)-CR)*PR;
	 Pm = 0.5D0*(1.0D0-MR)*PR;  
	! Pm = 0.25D0*PR*(1.0D0-MR)*(1.0D0-MR)*(2.0D0+MR); 
 ELSE;  Pum = 0.0D0; Pm = 0.0D0; ENDIF
!**** Positive Part of Flux ***********************************
 Fp(1)=max(0.0D0,WL(2))*WL(1);
 Fp(2)=max(0.0D0,WL(2))*WL(1)*WL(2)  + Pp;
 Fp(3)=max(0.0D0,WL(2))*UL(3)        + Pup;
!**** Negative Part of Flux ***********************************
 Fn(1)=min(0.0D0,WR(2))*WR(1);
 Fn(2)=min(0.0D0,WR(2))*WR(1)*WR(2)  + Pm;
 Fn(3)=min(0.0D0,WR(2))*UR(3)        + Pum;
!**** Compute the FLUX ****************************************
  F=Fp+Fn
 END SUBROUTINE ZHA_BILGEN
!*************************************************************************************************    
!* Godunov's Flux Function : F(U,J); Flux between J and (J+1).
!*************************************************************************************************    
 SUBROUTINE Godunov(F,UL,UR,WL,WR,cL,cR,HL,HR)
 IMPLICIT NONE
 DOUBLE PRECISION :: F(3),UL(3),UR(3),WL(3),WR(3),cL,cR,HL,HR
 DOUBLE PRECISION :: vL,vR,gam,rL,rR,PL,PR,tol,Pm1,Pm2,mL,mR,dP,um,P(2),rm(2),r(2),rmI,cmL,cmR
 DOUBLE PRECISION :: AmL,AmR,Um2,Um3
 INTEGER :: K,Kmax
 rL=WL(1); rR=WR(1); vL=WL(2); vR=WR(2); PL=WL(3); PR=WR(3); gam=(gamma+1.0D0)/(gamma-1.0D0); 
 tol=1.0D-08; K=0; Kmax=1000; r(1)=rL; r(2)=rR; P(1)=PL; P(2)=PR;
!*************************************************************************************************    
 IF     (vL/cL>=1.0D0) THEN; !*** Supersonic Flow to the RIGHT
  F=Euler_f(UL)
!*************************************************************************************************    
 ELSEIF (vR/cR<=-1.0D0) THEN; !*** Supersonic Flow to the LEFT
  F=Euler_f(UR)
!*************************************************************************************************    
 ELSE
   Pm1=( (0.5D0*(vL-vR)*(gamma-1.0D0)+cL+cR)/(cL*PL**((1.0D0-gamma)/gamma*0.5D0) &
             &             +cR*PR**((1.0D0-gamma)/gamma*0.5D0) )  )**((1.0D0-gamma)/gamma*0.5D0);
!*** FIXED-POINT ITERATION
 DO;  mL=massflux(rL,cL,PL,Pm1); mR=massflux(rR,cR,PR,Pm1);
     Pm2=(mL*PR+mR*PL-mL*mR*(vR-vL))/(mL+mR); dP=abs(Pm2-Pm1); K=K+1; 
     !IF  (K>Kmax) EXIT;  
     Pm1=Pm2; IF (dP<=tol) EXIT
 END DO
!*************************************************************************************************    
     mL=massflux(rL,cL,PL,Pm2); mR=massflux(rR,cR,PR,Pm2); um=(mL*vL+mR*vR-(PR-PL))/(mL+mR);
!*************************************************************************************************    
 DO K=1,2
  IF (Pm2/P(K)>=1.0D0) THEN; rm(K)=r(K)*(1.0D0+gam*Pm2/P(K))/(gam+Pm2/P(K));
  ELSE;                      rm(K)=r(K)*( Pm2/P(K) )**(1.0D0/gamma)        ; ENDIF
 END DO; IF (um>=0) THEN; rmI=rm(1); ELSE; rmI=rm(2); ENDIF
!**** Solution at x/t=0 ****************************************
    cmL=sqrt(gamma*Pm2/rm(1)); cmR=sqrt(gamma*Pm2/rm(2)); AmL=um-cmL; AmR=um+cmR;
   IF      (AmL<=0.0D0 .AND. AmR>=0.0D0) THEN 
         Um2=rmI*um; Um3=Pm2/(gamma-1.0D0)+0.5*rmI*um*um;
   ELSEIF  (AmL>0.0D0 .AND. vL-CL<0.0D0) THEN
         CALL sonic(rmI,Um2,Um3,vL,CL,PL,um,cmL,vL-CL,AmL);
   ELSEIF  (AmR<0.0D0 .AND. vR+CR>0.0D0) THEN
         CALL sonic(rmI,Um2,Um3,vR,CR,PR,um,cmR,vR+CR,AmR);
   ENDIF
!****** Compute the flux ***********************************************************/
  F(1)=f1(rmI,Um2,Um3);F(2)=f2(rmI,Um2,Um3); F(3)=f3(rmI,Um2,Um3);
 ENDIF 
 END SUBROUTINE Godunov
!*************************************************************************************************    
!* Osher's Flux Function : F(U,J); Flux between J and (J+1). (See my Math671 report for details)
!*************************************************************************************************    
 SUBROUTINE Osher(F,UL,UR,WL,WR,cL,cR,HL,HR)
 IMPLICIT NONE
 DOUBLE PRECISION :: F(3),UL(3),UR(3),WL(3),WR(3),cL,cR,HL,HR
 DOUBLE PRECISION :: vL,vR,gam,rL,rR,PL,PR,PLm,rLm,uLm,cLm,PRm,uRm,rRm,cRm,sgnU,ULm1,ULm2,ULm3,URm1
 DOUBLE PRECISION :: URm2,URm3,US1,US2,US3,ALm,AL,AR,ARm,sgnUmC,sgnUpC,URma(3),ULma(3),US(3); 
 INTEGER :: JR,J
 rL=WL(1); rR=WR(1); vL=WL(2); vR=WR(2); PL=WL(3); PR=WR(3);
!**** First Compute the intermediate states ***************************************************   
 !*** Lm (or j+1/3)****
  PLm=( (0.5D0*(gamma-1.0D0)*(vR-vL)+(cR+cL)) &
      &   /( cL*(1.0D0+sqrt(rL/rR*(PR/PL)**(1.0D0/gamma)))) )**(2.0D0*gamma/(gamma-1.0D0))*PL;
  rLm=(PLm/PL)**(1.0D0/gamma)*rL; cLm=sqrt(gamma*PLm/rLm);   uLm=vL-2/(gamma-1)*(cL-cLm);
  ULm1=rLm; ULm2=rLm*uLm; ULm3= PLm/(gamma-1.0D0)+0.5D0*rLm*uLm*uLm ;
 !*** Rm (or j+2/3)****
  PRm=PLm; uRm=uLm; rRm=(PRm/PR)**(1.0D0/gamma)*rR; cRm=sqrt(gamma*PRm/rRm);
  URm1=rRm; URm2=rRm*uRm; URm3= PRm/(gamma-1.0D0)+0.5D0*rRm*uRm*uRm ;

  URma=(/ URm1,URm2,URm3 /); ULma=(/ ULm1,ULm2,ULm3 /) 
!**** CENTERED FLUX ****************************************************************************   
   F=0.5D0*( Euler_f(UL) + Euler_f(UR) );
!**** WAVE SPEEDS *******************************************************************************   
   IF   (uRm<0.0D0) THEN;   sgnU=-1.0D0; ELSEIF (uRm>0.0D0) THEN;   sgnU=1.0D0; ELSE;   sgnU=0.0D0; ENDIF
   IF (vR-cR<0.0D0) THEN; sgnUmC=-1.0D0; ELSEIF   (vR-cR>0) THEN; sgnUmC=1.0D0; ELSE; sgnUmC=0.0D0; ENDIF
   IF (vL+cL<0.0D0) THEN; sgnUpC=-1.0D0; ELSEIF   (vL+cL>0) THEN; sgnUpC=1.0D0; ELSE; sgnUpC=0.0D0; ENDIF
!************************************************************************************************  
   F=F -0.5D0*(-sgnUpC*Euler_f(UL)+sgnUmC*Euler_f(UR));
   F=F -0.5D0*( ( sgnU-sgnUmC)*Euler_f(URma) );
   F=F -0.5D0*( (-sgnU+sgnUpC)*Euler_f(ULma) );
!**** SONIC CASES **********************************************************
  !*** The first Path: u+c    FUpC
      ALm=uLm+cLm; AL=vL+cL;  !*** wave speeds0.25*WL1*CL*(ML+1)*(ML+1);
   IF (AL*ALm<0.0D0) THEN
       CALL sonic(US1,US2,US3,uLm,cLm,PLm,vL,cL,ALm,AL); US=(/ US1,US2,US3 /) 
    F=F-0.5D0*(2.0D0*sgnUpC*(Euler_f(US)-Euler_f(ULma)));
    
   ENDIF
  !*** The third Path: u-c    FUmC
     ARm=uRm-cRm; AR=vR-cR;  !*** wave speeds
   IF (ARm*AR<0.0D0) THEN
       CALL sonic(US1,US2,US3,vR,cR,PR,uRm,cRm,AR,ARm); US=(/ US1,US2,US3 /) 
    F=F-0.5D0*(2.0D0*sgnUmC*(Euler_f(URma)-Euler_f(US)));
   ENDIF
 END SUBROUTINE Osher
!*************************************************************************************************    
!* Roe's Flux Function : F(U,J); Flux between J and (J+1). (Implemented in an inefficient way.)
!*************************************************************************************************    
 SUBROUTINE Roe(F,UL,UR,WL,WR,cL,cR,HL,HR)
 IMPLICIT NONE
 DOUBLE PRECISION :: F(3),UL(3),UR(3),WL(3),WR(3),cL,cR,HL,HR
 DOUBLE PRECISION :: vL,vR,rL,rR,PL,PR,RT,rho,u,H,a,drho,du,dP,dV(3),lm(3),Da,R(3,3)
 INTEGER :: J,K
 rL=WL(1); rR=WR(1); vL=WL(2); vR=WR(2); PL=WL(3); PR=WR(3);
!**** First Compute the Roe Averages **************************
   RT=sqrt(rR/rL);
   rho=RT*rL; u=(vL+RT*vR)/(1.0D0+RT); H=(HL+RT*HR)/(1.0D0+RT);
   a=sqrt( (gamma-1.0D0)*(H-0.5D0*u*u) );
!**** Differences in Primitive variables **********************
   drho=rR-rL;   du=vR-vL;   dP=PR-PL;
!**** Wave Strength (Characteristic Variables) ****************
   dV(1)=0.5D0*(dP-rho*a*du)/a/a; dV(2)=-(dP/a/a-drho);
   dV(3)=0.5D0*(dP+rho*a*du)/a/a;
!**** Wave Speeds (Eigenvalues) *******************************
   lm(1)=abs(u-a); lm(2)=abs(u); lm(3)=abs(u+a);
!**** Modified Wave Speeds (remove expansion shock) Harten's **********
   Da=max(0.0D0, 4.0D0*((vR-cR)-(vL-cL)) ); 
   IF (lm(1)<0.5D0*Da) lm(1)=lm(1)*lm(1)/Da+0.25D0*Da;
   Da=max(0.0D0, 4.0D0*((vR+cR)-(vL+cL)) ); 
   IF (lm(3)<0.5D0*Da) lm(3)=lm(3)*lm(3)/Da+0.25D0*Da;
!****Right Eigenvectors ***************************************
   R(1,1)=1.0D0;  R(2,1)=1.0D0     ; R(3,1)=1.0D0;
   R(1,2)=u-a  ;  R(2,2)=u         ; R(3,2)=u+a  ;
   R(1,3)=H-u*a;  R(2,3)=0.5D0*u*u ; R(3,3)=H+u*a;
!**** Compute the average FLUX ****************************************
   F=0.5D0*( Euler_f(UL) + Euler_f(UR) );
!**** Finally Compute the FLUX ******************************************
  DO J=1,3; DO K=1,3; F(J)=F(J)-0.5D0*lm(K)*dV(K)*R(K,J); END DO; END DO
 END SUBROUTINE Roe
!*************************************************************************************************    
!* Roe's Flux Function 2nd Order : F(U,J); Flux between J and (J+1).
!*************************************************************************************************    
 SUBROUTINE Roe2nd(F,UL,UR,WL,WR,cL,cR,HL,HR,J)
 IMPLICIT NONE
 DOUBLE PRECISION :: F(3),UL(3),UR(3),WL(3),WR(3),cL,cR,HL,HR
 DOUBLE PRECISION :: vL,vR,rL,rR,PL,PR,RT,rho,u,H,a,drho,du,dP,dV(3),lm(3),Da,R(3,3)
 DOUBLE PRECISION :: arC(3),rho2,u2,H2,a2,drho2,du2,dP2,arLL(3),rho3,u3,H3,a3,drho3,du3,dP3,arRR(3),S(3),sgnlm
 INTEGER :: I,J,K
 rL=WL(1); rR=WR(1); vL=WL(2); vR=WR(2); PL=WL(3); PR=WR(3);
!**** First Compute the Roe Averages **************************
   RT=sqrt(rR/rL);
   rho=RT*rL; u=(vL+RT*vR)/(1.0D0+RT); H=(HL+RT*HR)/(1.0D0+RT);
   a=sqrt( (gamma-1.0D0)*(H-0.5D0*u*u) );
!**** Differences in Primitive variables **********************
   drho=rR-rL;   du=vR-vL;   dP=PR-PL;
!**** Wave Strength (Characteristic Variables) ****************
   dV(1)=0.5D0*(dP-rho*a*du)/a/a; dV(2)=-(dP/a/a-drho);
   dV(3)=0.5D0*(dP+rho*a*du)/a/a;
!**** Wave Speeds (Eigenvalues) *******************************
   lm(1)=(u-a); lm(2)=(u); lm(3)=(u+a);
!****Right Eigenvectors ***************************************
   R(1,1)=1.0D0;  R(2,1)=1.0D0     ; R(3,1)=1.0D0;
   R(1,2)=u-a  ;  R(2,2)=u         ; R(3,2)=u+a  ;
   R(1,3)=H-u*a;  R(2,3)=0.5D0*u*u ; R(3,3)=H+u*a;
!**** Compute the average FLUX ****************************************
   F(1)= f1(UL(1),UL(2),UL(3))
   F(2)= f2(UL(1),UL(2),UL(3))
   F(3)= f3(UL(1),UL(2),UL(3))
!**** Finally Compute the 1st Order FLUX ******************************************
  DO I=1,3; DO K=1,3; 
   IF (lm(K)<=0.0D0)  F(I)=F(I)+lm(K)*dV(K)*R(K,I); 
  END DO; END DO

!**** 2nd Order FLUX **************************************************************
   IF (J>=1 .AND. J<=NC) THEN
!**** Compute sigma ****************************************
   arC=dV
!**** First Compute the Roe Averages between (J-1)th and Jth CELLS***************
   RT=sqrt(rL/Cell(J-1)%W(1))
   rho2=RT*Cell(J-1)%W(1); u2=(Cell(J-1)%W(2)+RT*vL)/(1.0D0+RT); H2=(Cell(J-1)%H+RT*HL)/(1.0D0+RT);
   a2=sqrt( (gamma-1.0D0)*(H2-0.5D0*u2*u2) );  
   drho2=rL-Cell(J-1)%W(1);   du2=vL-Cell(J-1)%W(2);   dP2=PL-Cell(J-1)%W(3);
!**** Characteristic Variables **************************
    arLL(1)=0.5D0*(dP2-rho2*a2*du2)/a2/a2; arLL(2)=-(dP2/a2/a2-drho2);
    arLL(3)=0.5D0*(dP2+rho2*a2*du2)/a2/a2;
!**** Second Compute the Roe Averages between JRth and (JR+1)th CELLS***************
   RT=sqrt(Cell(J+1)%W(1)/rR);
   rho3=RT*rR; u3=(vR+RT*Cell(J+1)%W(2))/(1.0D0+RT); H3=(HR+RT*Cell(J+1)%H)/(1.0D0+RT);
   a3=sqrt( (gamma-1.0D0)*(H3-0.5D0*u3*u3) );  
   drho3=Cell(J+1)%W(1)-rR;   du3=Cell(J+1)%W(2)-vR;   dP3=Cell(J+1)%W(3)-PR;
!**** Characteristic Variables **************************
    arRR(1)=0.5D0*(dP3-rho3*a3*du3)/a3/a3; arRR(2)=-(dP3/a3/a3-drho3);
    arRR(3)=0.5D0*(dP3+rho3*a3*du3)/a3/a3; 
!**** NOW LIMIT THE SLOPE(DIFFERENCIAL OF CHAR. VAR.)
   IF (lm(1)>=0) THEN; S(1)=minmod(arLL(1), arC(1)); 
   ELSE              ; S(1)=minmod( arC(1),arRR(1)); ENDIF
   IF (lm(2)>=0) THEN; S(2)=minmod(arLL(2), arC(2)); 
   ELSE              ; S(2)=minmod( arC(2),arRR(2)); ENDIF
   IF (lm(3)>=0) THEN; S(3)=minmod(arLL(3), arC(3)); 
   ELSE              ; S(3)=minmod( arC(3),arRR(3)); ENDIF
!**** Finally Compute 2nd Order FLUX ******************************************
  DO I=1,3;
   DO K=1,3;
      IF (lm(K)<0.0D0) THEN; sgnlm=-1.0D0; ELSE;  sgnlm=1.0D0; ENDIF 
      F(I)=F(I)+0.5D0*lm(K)*(sgnlm-lm(K)*dt/dx)*S(K)*R(K,I)
   END DO; 
  END DO;
  ENDIF;
 END SUBROUTINE Roe2nd
!*************************************************************************************************    
!* Chernousov's Approximate Riemann Solver: F(U,J); Flux between J and (J+1).
!*************************************************************************************************    
 SUBROUTINE Chernousov(F,UL,UR,WL,WR,cL,cR,HL,HR)
 IMPLICIT NONE
 DOUBLE PRECISION :: F(3),UL(3),UR(3),WL(3),WR(3),cL,cR,HL,HR
 DOUBLE PRECISION :: vL,vR,rL,rR,PL,PR,JpL,JmL,JoL,JpR,JmR,JoR,Ua,Ca,Sp,So,Sm,Pi,ui,Ri,ap,ao,am,Jo,Jp,Jm
 INTEGER :: J,K
 rL=WL(1); rR=WR(1); vL=WL(2); vR=WR(2); PL=WL(3); PR=WR(3);
!**** Compute Riemann Invariants *****************************************************
 JpL=vL+PL/(rL*cL); JpR=vR+PR/(rR*cR); 
 JmL=vL-PL/(rL*cL); JmR=vR-PR/(rR*cR); 
 JoL=PL-cL*cL*rL  ; JoR=PR-cR*cR*rR   ;
!**** Compute Average Char. Speed ****************************************************
 Ua=0.5D0*(vL+vR); Ca=0.5D0*(cL+cR);
 Sp=Ua+Ca; So=Ua; Sm=Ua-Ca;
!**** Choose the Invariants  ****************************************************
 IF (Sp>0.0D0) THEN; Jp=JpL; ap= 1.0D0/(rL*cL);
 ELSE              ; Jp=JpR; ap= 1.0D0/(rR*cR); ENDIF
 IF (Sm>0.0D0) THEN; Jm=JmL; am=-1.0D0/(rL*cL);
 ELSE              ; Jm=JmR; am=-1.0D0/(rR*cR); ENDIF
 IF (So>0.0D0) THEN; Jo=JoL; ao=-cL*cL         ;
 ELSE              ; Jo=JoR; ao=-cR*cR         ; ENDIF
!**** Compute the interefafe states  ****************************************************
 Pi=(Jp-Jm)/(ap-am); ui=Jp-ap*Pi; Ri=(Jo-Pi)/ao;
!**** Compute the flux  ****************************************************
   F(1)=f1(Ri,Ri*ui,Pi/(gamma-1.0D0)+0.5D0*Ri*ui*ui);
   F(2)=f2(Ri,Ri*ui,Pi/(gamma-1.0D0)+0.5D0*Ri*ui*ui);
   F(3)=f3(Ri,Ri*ui,Pi/(gamma-1.0D0)+0.5D0*Ri*ui*ui); 
 END SUBROUTINE Chernousov





!****************************************************************************
!* MASS FLUX for Godunov's Flux Function
!****************************************************************************/
  FUNCTION massflux(r,c,PQ,Pm)
  IMPLICIT NONE
  DOUBLE PRECISION :: r,c,PQ,Pm,gam1,gam2,massflux
   gam1=0.5D0*(gamma+1.0D0)/gamma; gam2=0.5D0*(gamma-1.0D0)/gamma;
   IF (Pm/PQ>=1-1.0D-07) THEN
      massflux=(r*c)*sqrt( 1.0D0+gam1*(Pm/PQ-1.0D0) ); RETURN;
   ELSE
      massflux=(r*c)*gam2*(1.0D0-Pm/PQ)/( 1.0D0-(Pm/PQ)**(gam2) )
   ENDIF
  END FUNCTION massflux
!****************************************************************************
!* Solutions at Sonic points; Used for Godunov's and Osher's fluxes;
!****************************************************************************/
  SUBROUTINE sonic(US1,US2,US3,u1,c1,P1,u2,c2,a1,a2)
  IMPLICIT NONE
  DOUBLE PRECISION :: US1,US2,US3,u1,c1,P1,u2,c2,a1,a2,us,cs,Ps,rs,R1,R2
  R1=a2/(a2-a1); R2=-a1/(a2-a1);
  us=R1*u1+R2*u2; cs=R1*c1+R2*c2;
  Ps=(cs/c1)**(2.0D0*gamma/(gamma-1))*P1; rs=gamma*Ps/(cs*cs);
  US1=rs; US2=rs*us; US3= Ps/(gamma-1.0D0)+0.5D0*rs*us*us;  
  END SUBROUTINE sonic
!****************************************************************************
!* MINMOD LIMITER
!****************************************************************************/
 FUNCTION minmod(a,b)
 IMPLICIT NONE
  DOUBLE PRECISION :: a,b,minmod
      IF (a*b<=0.0D0)    THEN; minmod=0.0D0; RETURN; 
  ELSEIF (abs(a)<abs(b)) THEN; minmod=a    ; RETURN;
  ELSEIF (abs(b)<abs(a)) THEN; minmod=b    ; RETURN;
  ELSE                       ; minmod=b    ; RETURN; ENDIF
 END FUNCTION minmod
!****************************************************************************
!* MINMOD LIMITER
!****************************************************************************/






 FUNCTION f1(u1,u2,u3)
 IMPLICIT NONE; DOUBLE PRECISION :: f1,u1,u2,u3
  f1=u2
 END FUNCTION f1
!****************************************************************************************************  
 FUNCTION f2(u1,u2,u3)
 IMPLICIT NONE; DOUBLE PRECISION :: f2,u1,u2,u3
  f2=u2*u2/u1+(gamma-1.0D0)*(u3-u2*u2/u1/2.0D0)
 END FUNCTION f2
!****************************************************************************************************  
 FUNCTION f3(u1,u2,u3)
 IMPLICIT NONE; DOUBLE PRECISION :: f3,u1,u2,u3
  f3=u2*(u3+(gamma-1.0)*(u3-u2*u2/u1/2.0))/u1
 END FUNCTION f3


END MODULE FLUXM
