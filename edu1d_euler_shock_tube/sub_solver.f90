MODULE SOLVERM
 USE EULER1D_DATA_TYPE; USE C_ARRAYS; USE FLUXM
CONTAINS
!*******************************************************************************************
!* ADVANCE IN TIME: UPDATE THE SOLUTIONS j=1~NC.
!*******************************************************************************************
 SUBROUTINE SOLVER
 IMPLICIT NONE
 DOUBLE PRECISION :: FL(3),FR(3),FRsave(3)
 INTEGER :: J,K
!*******************************************************************************************
!* LOOP OVER INTERIOR CELLS; Compute flux only at the right.
!*******************************************************************************************
  CALL FLUX(FL,0,1,NS); CALL FLUX(FR,1,2,NS); FRsave=FR;
     Cell(1)%Unew=Cell(1)%U-(dt/dx)*(FR-FL)
 DO J=2,NC
             FL=FRsave; CALL FLUX(FR,J,J+1,NS); FRsave=FR;
     Cell(J)%Unew=Cell(J)%U-(dt/dx)*(FR-FL);
 END DO
!*******************************************************************************************
!* UPDATE THE SOLUTIONS
!*******************************************************************************************
 DO J=1,NC; Cell(J)%U=Cell(J)%Unew; CALL UtoW(Cell,J); END DO
 END SUBROUTINE SOLVER
!*******************************************************************************************
!* ADVANCE IN TIME: UPDATE THE SOLUTIONS j=1~NC.
!*******************************************************************************************

END MODULE SOLVERM
