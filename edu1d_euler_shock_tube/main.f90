!*******************************************************************************************
!* 1D Euler Code for Shock Tube Problems.
!*
!*
!*
!* Hiroaki Nishikawa, October 2000
!*******************************************************************************************
 PROGRAM EULER_1D
!*******************************************************************************************
 USE EULER1D_DATA_TYPE; USE C_ARRAYS; USE SETUPM; USE SOLVERM; USE EXACT_RIEMANN_M 
 USE RECONSTRUCTION_M
!*******************************************************************************************
 IMPLICIT NONE
 DOUBLE PRECISION :: t=0.0D0
 INTEGER :: I,NTS=0
 gamma=1.4D0
!*******************************************************************************************
!* Set up the problem to solve and the method to use.
!*******************************************************************************************
  CALL SETUP
!*******************************************************************************************
!* Main Computation until t exceeds the specified time tf.
!*******************************************************************************************
 DO I=1,NTmax; IF (t>Tf) EXIT; dt=TIMESTEP(); t=t+dt; NTS=NTS+1; CALL GHOST; 
  IF (NH==2) CALL RECONSTRUCTION !*** Hancock's Predictor Corrector Scheme
  CALL SOLVER
 END DO
!*******************************************************************************************
!* OUTPUT
!*******************************************************************************************
 WRITE(*,*)
 WRITE(*,*) "Final time t(sec) = ",t," by ",NTS," TIME STEPS"
 CALL EXACT(t); CALL OUTPUT
!*******************************************************************************************
 STOP






CONTAINS
!*******************************************************************************************
!* Compute the time step dt that keeps CFL number constant.
!*******************************************************************************************
 FUNCTION TIMESTEP()
 IMPLICIT NONE
 DOUBLE PRECISION :: timestep,a,u,c,ua
 INTEGER :: I
  ua=-1.0D0
  DO I=1,NC;  u=Cell(I)%W(2); a=Cell(I)%c
     ua=MAX(ua,abs(u)+a)
  END DO
    timestep=CFL*abs(dx/ua)
 END FUNCTION TIMESTEP
!*******************************************************************************************
!* Update solutions at ghost cells. (just copying)
!*******************************************************************************************
 SUBROUTINE GHOST
 IMPLICIT NONE
 INTEGER :: K
 DO K=1,3
     Cell(0)%U(K)=Cell(1)%U(K) ;    Cell(0)%W(K)=Cell(1)%W(K);
  Cell(NC+1)%U(K)=Cell(NC)%U(K); Cell(NC+1)%W(K)=Cell(NC)%W(K);
 END DO 
     Cell(0)%H=Cell(1)%H  ;    Cell(0)%c=Cell(1)%c ; Cell(0)%S=Cell(1)%S ; 
  Cell(NC+1)%H=Cell(NC)%H ; Cell(NC+1)%c=Cell(NC)%c ; Cell(NC+1)%S=Cell(NC)%S ;
 END SUBROUTINE GHOST
!*******************************************************************************************
!* Output File
!*******************************************************************************************
 SUBROUTINE OUTPUT
 IMPLICIT NONE
 INTEGER :: K,OS
 OPEN(UNIT=1, FILE = "solution.dat", STATUS="UNKNOWN", IOSTAT=OS)
 DO K=1,NC
  WRITE(1,'(5ES25.15)') Cell(K)%xc,Cell(K)%W(1),Cell(K)%W(2),Cell(K)%W(3),Cell(K)%S
 END DO 
 CLOSE(1)
 END SUBROUTINE OUTPUT
!*******************************************************************************************




 END PROGRAM EULER_1D
!*******************************************************************************************


