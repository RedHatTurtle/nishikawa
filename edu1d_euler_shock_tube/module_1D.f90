!*****************************************************************************
!* MODULE FOR 1DEULER GRID DATA.
!*    ***** ARRAY TYPE *****
!*****************************************************************************
  MODULE EULER1D_DATA_TYPE
!*****************************************************************************
!  PRIVATE :: WtoU,UtoW
!*****************************************************************************
!* DERIVED DATA TYPES: PUBLIC
!*****************************************************************************
  TYPE CELLS
    DOUBLE PRECISION :: U(3),Unew(3),W(3),WL(3),WR(3),dW(3),H,c,S,xc
  END TYPE CELLS
!*****************************************************************************
!* INTERFACES: 
!*****************************************************************************
!    INTERFACE WtoU !(I) 
!      MODULE PROCEDURE WtoU
!    END INTERFACE 
!*****************************************************************************
!* SUBPROGRAMS: PRIVATE and PUBLIC
!*****************************************************************************
  CONTAINS
!*****************************************************************************
!* PRIVATE SUBPROGRAMS:
!*****************************************************************************
!*****************************************************************************
!* Compute the conservative variables from the primitive.
!*****************************************************************************
  SUBROUTINE WtoU(Cell,I)
  IMPLICIT NONE
  INTEGER    , INTENT(IN)    :: I
  TYPE(CELLS) , INTENT(INOUT) :: Cell(0:)
  DOUBLE PRECISION :: gamma
  gamma=1.4D0
  Cell(I)%U(1)=Cell(I)%W(1);  
  Cell(I)%U(2)=Cell(I)%W(1)*Cell(I)%W(2);  
  Cell(I)%U(3)=Cell(I)%W(3)/(gamma-1.0D0)+Cell(I)%W(1)*Cell(I)%W(2)**2/2.0D0
  Cell(I)%H=( Cell(I)%U(3) + Cell(I)%W(3) )/Cell(I)%U(1)
  Cell(I)%c=sqrt(gamma*Cell(I)%W(3)/Cell(I)%W(1))
  Cell(I)%S=LOG(Cell(I)%W(3)*Cell(I)%W(1)**(-gamma))/(gamma-1.0D0) 
  END SUBROUTINE WtoU
!*****************************************************************************
!* Compute the primitive variables from the conservative.
!*****************************************************************************
  SUBROUTINE UtoW(Cell,I)
  IMPLICIT NONE
  INTEGER    , INTENT(IN)    :: I
  TYPE(CELLS) , INTENT(INOUT) :: Cell(0:)
  DOUBLE PRECISION :: gamma
  gamma=1.4D0
  Cell(I)%W(1)=Cell(I)%U(1);  
  Cell(I)%W(2)=Cell(I)%U(2)/Cell(I)%U(1);  
  Cell(I)%W(3)=(gamma-1.0D0)*(Cell(I)%U(3)-0.5D0*Cell(I)%W(1)*Cell(I)%W(2)**2)
  IF (Cell(I)%W(3)<0.0D0) THEN; WRITE(*,*) "Negative Pressure!"; STOP; ENDIF
  Cell(I)%H=( Cell(I)%U(3) + Cell(I)%W(3) )/Cell(I)%U(1)
  Cell(I)%c=sqrt(gamma*Cell(I)%W(3)/Cell(I)%W(1))
  Cell(I)%S=LOG(Cell(I)%W(3)*Cell(I)%W(1)**(-gamma))/(gamma-1.0D0) 
  END SUBROUTINE UtoW
!*****************************************************************************

!*****************************************************************************
!* Compute the conservative variables from the primitive.
!*****************************************************************************
  SUBROUTINE UfromW(U,c,H,W)
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(IN)  :: W(3)
  DOUBLE PRECISION, INTENT(OUT) :: U(3),c,H
  DOUBLE PRECISION :: gamma,S
  gamma=1.4D0
  U(1)=W(1);  
  U(2)=W(1)*W(2);  
  U(3)=W(3)/(gamma-1.0D0)+W(1)*W(2)**2/2.0D0
  H=( U(3) + W(3) )/U(1)
  c=sqrt(gamma*W(3)/W(1))
  S=LOG(W(3)*W(1)**(-gamma))/(gamma-1.0D0) 
  END SUBROUTINE UfromW
!*****************************************************************************

!*****************************************************************************
!* Compute the char. variables from the primitive.
!* rhoa,ua,ca are linearrization constants.
!*****************************************************************************
  SUBROUTINE WCfromW(Wc,rhoa,ua,ca,rho,u,p)
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(IN)  :: rhoa,ua,ca,rho,u,p
  DOUBLE PRECISION, INTENT(OUT) :: Wc(3)
  DOUBLE PRECISION :: S
  S=p/(ca*ca)
  Wc(1)=0.5D0*(-rhoa/ca*u+S); 
  Wc(2)=rho-S
  Wc(3)=0.5D0*( rhoa/ca*u+S); 
  END SUBROUTINE WCfromW
!*****************************************************************************

!*****************************************************************************
!* Compute the char. variables from the primitive.
!* rhoa,ua,ca are linearrization constants.
!*****************************************************************************
  FUNCTION Wc_fromW(rhoa,ua,ca,rho,u,p)
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(IN)  :: rhoa,ua,ca,rho,u,p
  DOUBLE PRECISION :: Wc_fromW(3),Wc(3)
  DOUBLE PRECISION :: S
  S=p/(ca*ca)
!  Wc(1)=0.5D0*(-rhoa/ca*u+S); Wc(2)=rho-S; Wc(3)=0.5D0*( rhoa/ca*u+S); 
!  Wc_fromW = (/ Wc(1),Wc(2),Wc(3) /)
!   Wc_fromW = Wc
   Wc_fromW(1)=0.5D0*(-rhoa/ca*u+S); Wc_fromW(2)=rho-S; Wc_fromW(3)=0.5D0*( rhoa/ca*u+S); 
  END FUNCTION Wc_fromW
!*****************************************************************************
!*****************************************************************************
!* Compute the char. variables from the primitive.
!* rhoa,ua,ca are linearrization constants.
!*****************************************************************************
  FUNCTION W_fromWc(rhoa,ua,ca,Wc)
  IMPLICIT NONE
  DOUBLE PRECISION, INTENT(IN)  :: rhoa,ua,ca,Wc(3)
  DOUBLE PRECISION :: W_fromWc(3),W(3)
   W(1)=Wc(1)+Wc(2)+Wc(3);
   W(2)=ca/rhoa*(Wc(3)-Wc(1));
   W(3)=ca*ca*( Wc(1)+Wc(3) );  
  W_fromWc = (/ W(1),W(2),W(3) /)
  END FUNCTION W_fromWc
!*****************************************************************************


  END MODULE EULER1D_DATA_TYPE

  

!*****************************************************************************
!* MODULE FOR TRIANGULAR GRID DATA (Global Data).
!*    ***** ARRAY TYPE *****
!*****************************************************************************
 MODULE C_ARRAYS; USE EULER1D_DATA_TYPE
  DOUBLE PRECISION :: xmin,xmax,dx,Tf,CFL,dt,rL,uL,PL,rR,uR,PR,gamma
  INTEGER :: NTmax,NS,NC,NH,NLim,NRCON
  TYPE(CELLS), ALLOCATABLE :: Cell(:)
 END MODULE C_ARRAYS
!*****************************************************************************









