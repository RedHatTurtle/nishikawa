MODULE RECONSTRUCTION_M
 USE EULER1D_DATA_TYPE; USE C_ARRAYS;
CONTAINS


!*******************************************************************************************
!* Reconstruct the solutions. Hancock's Method.
!*******************************************************************************************
 SUBROUTINE RECONSTRUCTION
 IMPLICIT NONE
 DOUBLE PRECISION :: dWL(3),dWR(3),dW(3),W_half(3),Chlf,u,rho,p,c,du,drho,dp
 DOUBLE PRECISION :: WcJ(3),WcL(3),WcR(3),ua,rhoa,pa,ca,dwc(3),lambda(3)
 INTEGER :: K,J
 Chlf=0.5D0*dt/dx
!*******************************************************************************************
!* RECONSTRUCTION BASED ON PRIMITIVE VARIABLES
!*
!*******************************************************************************************
 IF (NRCON==1) THEN
!*******************************************************************************************
!* LOOP OVER CELLS: Compute the slope in primitive variables for each cell.
!*******************************************************************************************
 DO J=1,NC
!***** Unlimited slopes in primitive variables.
   dWL=Cell(J)%W-Cell(J-1)%W; dWR=Cell(J+1)%W-Cell(J)%W;
!***** Apply a slope Limiter 
   IF     (NLim==1) THEN
     DO K=1,3; Cell(J)%dW(K)=           minmod(dWL(K),dWR(K)); END DO
   ELSEIF (NLim==2) THEN
     DO K=1,3; Cell(J)%dW(K)=    double_minmod(dWL(K),dWR(K)); END DO
   ELSEIF (NLim==3) THEN
     DO K=1,3; Cell(J)%dW(K)= symmetric_minmod(dWL(K),dWR(K)); END DO
   ELSEIF (NLim==4) THEN
     DO K=1,3; Cell(J)%dW(K)=         superbee(dWL(K),dWR(K)); END DO
   ELSEIF (NLim==5) THEN
     DO K=1,3; Cell(J)%dW(K)=         harmonic(dWL(K),dWR(K)); END DO
   ELSEIF (NLim==6) THEN
     DO K=1,3; Cell(J)%dW(K)=        vanalbada(dWL(K),dWR(K)); END DO
   ELSEIF(NLim==0) then
     ! Do nothing. No limiter.
   ENDIF
 END DO
!*******************************************************************************************
!* LOOP OVER CELLS: Advance a half time step and get left and right values (promitive).
!*******************************************************************************************
 DO J=1,NC 
    rho=Cell(J)%W(1) ;  u=Cell(J)%W(2) ;  p=Cell(J)%W(3);
   drho=Cell(J)%dW(1); du=Cell(J)%dW(2); dp=Cell(J)%dW(3); c=Cell(J)%c
!***** Advance dt/2
   W_half(1)=Cell(J)%W(1) - Chlf*(u*drho+rho*du ) ;
   W_half(2)=Cell(J)%W(2) - Chlf*(u*du+dp/rho   ) ;
   W_half(3)=Cell(J)%W(3) - Chlf*(rho*c*c*du+u*dp) ;
!***** Left and Right Values
   Cell(J)%WL=W_half-0.5D0*Cell(J)%dW; Cell(J)%WR=W_half+0.5D0*Cell(J)%dW
 END DO




!*******************************************************************************************
!* RECONSTRUCTION BASED ON CHARACTERISTIC VARIABLES
!*
!*******************************************************************************************
 ELSEIF (NRCON==2) THEN
!*******************************************************************************************
!* LOOP OVER CELLS: Compute the slope in char. variables for each cell.
!*******************************************************************************************
 DO J=1,NC
!***** Linearization constants and Char. variables.
   rhoa=Cell(J)%W(1); ua=Cell(J)%W(2); pa=Cell(J)%W(3); ca=Cell(J)%c
   WcL=Wc_fromW(rhoa,ua,ca,Cell(J-1)%W(1),Cell(J-1)%W(2),Cell(J-1)%W(3))
   WcJ=Wc_fromW(rhoa,ua,ca,Cell(J  )%W(1),Cell(J  )%W(2),Cell(J  )%W(3))
   WcR=Wc_fromW(rhoa,ua,ca,Cell(J+1)%W(1),Cell(J+1)%W(2),Cell(J+1)%W(3))
!***** Unlimited slopes in char. variables.
   dWL=WcJ-WcL; dWR=WcR-WcJ; 
!***** Apply a slope Limiter 
   IF     (NLim==1) THEN
     DO K=1,3; Cell(J)%dW(K)=           minmod(dWL(K),dWR(K)); END DO
   ELSEIF (NLim==2) THEN
     DO K=1,3; Cell(J)%dW(K)=    double_minmod(dWL(K),dWR(K)); END DO
   ELSEIF (NLim==3) THEN
     DO K=1,3; Cell(J)%dW(K)= symmetric_minmod(dWL(K),dWR(K)); END DO
   ELSEIF (NLim==4) THEN
     DO K=1,3; Cell(J)%dW(K)=         superbee(dWL(K),dWR(K)); END DO
   ELSEIF (NLim==5) THEN
     DO K=1,3; Cell(J)%dW(K)=         harmonic(dWL(K),dWR(K)); END DO
   ELSEIF (NLim==6) THEN
     DO K=1,3; Cell(J)%dW(K)=        vanalbada(dWL(K),dWR(K)); END DO
   ENDIF
 END DO
!*******************************************************************************************
!* LOOP OVER CELLS: Advance a half time step and get left and right values (primitive).
!*******************************************************************************************
 DO J=1,NC 
!***** Cell-average values at J
   rho=Cell(J)%W(1) ;  u=Cell(J)%W(2) ;  p=Cell(J)%W(3); c=Cell(J)%c
!***** Linearization constants and Char. variables.
   dwc(1)=Cell(J)%dW(1); dwc(2)=Cell(J)%dW(2); dwc(3)=Cell(J)%dW(3);
   rhoa=Cell(J)%W(1); ua=Cell(J)%W(2); pa=Cell(J)%W(3); ca=Cell(J)%c
   lambda(1)=ua-ca;  lambda(2)=ua;  lambda(3)=ua+ca; 
   WcJ=Wc_fromW(rhoa,ua,ca,rho,u,p)
!***** Advance dt/2
    W_half=WcJ - Chlf*lambda*dwc ;
!***** Left and Right Values in the cell J
    WcL=W_half-0.5D0*Cell(J)%dW; WcR=W_half+0.5D0*Cell(J)%dW
!**** Back to primitive variables; See module_1D.f90 for "W_fromWc".
    Cell(J)%WL = W_fromWc(rhoa,ua,ca,WcL)
    Cell(J)%WR = W_fromWc(rhoa,ua,ca,WcR)
 END DO
!*******************************************************************************************

 ENDIF
 END SUBROUTINE RECONSTRUCTION





!****************************************************************************
!* MINMOD LIMITER (Roe's minmod)
!****************************************************************************/
 FUNCTION minmod(a,b)
 IMPLICIT NONE
  DOUBLE PRECISION :: a,b,minmod
      IF (a*b<=0.0D0)    THEN; minmod=0.0D0; RETURN; 
  ELSEIF (abs(a)<abs(b)) THEN; minmod=a    ; RETURN;
  ELSEIF (abs(b)<abs(a)) THEN; minmod=b    ; RETURN;
  ELSE                       ; minmod=b    ; RETURN; ENDIF
 END FUNCTION minmod
!****************************************************************************
!* MINMOD LIMITER
!****************************************************************************/

!****************************************************************************
!* SYMMETRIC MINMOD LIMITER
!****************************************************************************/
 FUNCTION symmetric_minmod(a,b)
 IMPLICIT NONE
  DOUBLE PRECISION :: a,b,symmetric_minmod
  IF (abs(a)<abs(b)) THEN; symmetric_minmod=a    ; RETURN;
  ELSEIF (abs(b)<abs(a)) THEN; symmetric_minmod=b    ; RETURN;
  ELSE                       ; symmetric_minmod=b    ; RETURN; ENDIF
 END FUNCTION symmetric_minmod
!****************************************************************************
!* SYMMETRIC MINMOD LIMITER
!****************************************************************************/



!****************************************************************************
!* MAXMOD LIMITER
!****************************************************************************/
 FUNCTION maxmod(a,b)
 IMPLICIT NONE
  DOUBLE PRECISION :: a,b,maxmod
      IF (a*b<=0.0D0)    THEN; maxmod=0.0D0; RETURN; 
  ELSEIF (abs(a)<abs(b)) THEN; maxmod=b    ; RETURN;
  ELSEIF (abs(b)<abs(a)) THEN; maxmod=a    ; RETURN;
  ELSE                       ; maxmod=b    ; RETURN; ENDIF
 END FUNCTION maxmod
!****************************************************************************
!* MAXMOD LIMITER
!****************************************************************************/

!****************************************************************************
!* DOUBLE MINMOD LIMITER
!****************************************************************************/
 FUNCTION double_minmod(a,b)
 IMPLICIT NONE
  DOUBLE PRECISION :: a,b,ave,double_minmod; ave=0.5D0*(a+b)
      IF (a*b<=0.0D0) THEN; double_minmod=0.0D0; RETURN; 
  ELSEIF ( abs(ave)<abs(2.0D0*a) .and. abs(ave)<abs(2.0D0*b)) THEN; 
    double_minmod=ave      ; RETURN; 
  ELSEIF ( abs(2.0D0*a)<abs(ave) .and. abs(2.0D0*a)<abs(2.0D0*b)) THEN; 
    double_minmod=  2.0D0*a; RETURN; 
  ELSE                                                
    double_minmod=  2.0D0*b; RETURN; ENDIF
 END FUNCTION double_minmod
!****************************************************************************
!* DOUBLE MINMOD LIMITER
!****************************************************************************/


!****************************************************************************
!* Superbee (or super-B)
!****************************************************************************/
 FUNCTION superbee(a,b)
 IMPLICIT NONE
  DOUBLE PRECISION :: a,b,maxm,minm,superbee; 
  maxm=maxmod(a,b); minm=minmod(2.0D0*a,2.0D0*b)
  IF (a*b<=0.0D0) THEN; superbee=0.0D0; RETURN; 
  ELSE
     superbee=minmod(maxm,minm); RETURN; 
  ENDIF
 END FUNCTION superbee
!****************************************************************************
!* Superbee
!****************************************************************************/

!****************************************************************************
!* Van Leer's Harmonic Mean
!****************************************************************************/
 FUNCTION harmonic(a,b)
 IMPLICIT NONE
  DOUBLE PRECISION :: a,b,harmonic; 
  IF (a*b<=0.0D0) THEN; harmonic=0.0D0; RETURN; 
  ELSE
     harmonic=2.0D0*a*b/(a+b+1.0D-20); RETURN; 
  ENDIF
 END FUNCTION harmonic
!****************************************************************************
!* Van Leer
!****************************************************************************/


!****************************************************************************
!* Van Albada
!****************************************************************************/
 FUNCTION vanalbada(a,b)
 IMPLICIT NONE
  DOUBLE PRECISION :: a,b,vanalbada;
      vanalbada = a*(a+b)/(a*a+b*b+1.0D-20); RETURN; 
 END FUNCTION vanalbada
!****************************************************************************
!* Van Albada
!****************************************************************************/


END MODULE RECONSTRUCTION_M
