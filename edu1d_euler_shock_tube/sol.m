gamma=1.4;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GET THE DATA OF THE SOLUTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fid=fopen('solution.dat','r');
format long e  
p1=fscanf(fid,'%le %le %le %le %le',[5 inf]);
x = p1(1,:); r = p1(2,:); u = p1(3,:); P = p1(4,:); s=p1(5,:);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GET THE DATA OF THE EXACT SOLUTIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fid=fopen('sample_cell.dat','r');
format long e  
p1=fscanf(fid,'%le %le %le %le %le',[5 inf]);
xe = p1(1,:); re = p1(2,:); ue = p1(3,:); Pe = p1(4,:); se=p1(5,:);
%se = log(Pe.*re.^(-gamma))./(gamma-1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure(1)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot(2,2,1)
hold on; axis('square'); hold on; grid; title('Density');
xlabel('x'); ylabel('Density \rho'); plot(x,r,'ko'); plot(xe,re,'b')
%text(0.01,0.96,'Van Leer');text(0.01,0.80,'T=1.74423');
%text(0.01,3.9,'Osher');text(0.01,3.7,'29 time Steps');
%text(0.01,3.5,'CFL=0.95');  %S-W t=0.70471
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot(2,2,2)
hold on; axis('square'); hold on; grid; title('Velocity')
xlabel('x'); ylabel('Velocity u'); plot(x,u,'ko'); plot(xe,ue,'b')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot(2,2,3)
hold on; axis('square'); hold on; grid; title('Pressure')
xlabel('x'); ylabel('Pressure P'); plot(x,P,'ko'); plot(xe,Pe,'b')
%text(0.01,14.5,'Osher');text(0.01,13.3,'29 time Steps');
%text(0.01,12,'CFL=0.95');  %S-W t=0.7022
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subplot(2,2,4)
hold on; axis('square'); hold on; grid; title('Entropy/R_{gas}')
xlabel('x'); ylabel('Entropy/R_{gas}'); plot(x,s,'ko'); plot(xe,se,'b')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
