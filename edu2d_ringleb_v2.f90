!********************************************************************************
!* Subroutines for generating grids and computing exact solutions for the famous
!* (often used for code verification of Euler codes) Ringleb's flow [F. Ringleb,
!*  Exakte Losungen der Differentialgleichungen einer adiabatischen Gasstromung,
!*  Z.A.M.M., 20(4), pp.185-198, 1940].
!*
!*        written by Dr. Katate Masatsuka (info[at]cfdbooks.com),
!*
!* the author of useful CFD books, "I do like CFD" (http://www.cfdbooks.com).
!*
!* This is Version 2 (2019).
!* 
!* These F90 routines were written and made available for download
!* for an educational purpose. Also, this is intended to provide all CFD
!* students and researchers with a tool for their code verification.
!*
!* This file may be updated in future. (to improve the boundary point 
!*                                      distribution, extend the domain, etc.)
!*
!* Katate Masatsuka, March 2009. http://www.cfdbooks.com
!********************************************************************************

!********************************************************************************
!* --- Driver code for Ringleb's Flow ---
!* 
!* 1. Generate boundary points for a specified domain in Ringleb's flow.
!*    : The data is written in the file `ringleb_boundary_nodes.dat'.
!*      It can be used as an input for other grid generation programs.
!*
!* 2. Generate structured grids over the domain.
!*    : The grid data is written as a Tecplot file:
!*         ringleb_grid_soln_tec.dat     for quadrilateral grid,
!*         ringleb_grid_soln_tri_tec.dat for triangular grid.       
!*
!* 3. Compute the exact solution on the grid points.
!*    : To demonstrate the ability of the subroutine 'exact_solution_Ringleb()'
!*      to compute the exact solution at a given point location in a physical
!*      domain.
!*
!* Input ---------------------------------------------------
!*   psi_min = right boundary streamline.
!*   psi_max =  left boundary streamline.
!*   Vmin    =  upper boundary (constant speed inflow).
!*       nX  = # of cells in the horizontal direction.
!*       nY  = # of cells in the vertical direction.
!* 
!* Output --------------------------------------------------
!*  ringleb_boundary_nodes.dat: boundary point data file
!*  ringleb_grid_soln_tec.dat : Tecplot grid data file for quadrilateral grid
!*                              (with exact solutions)
!*  ringleb_grid_soln_tri_tec.dat: Tecplot grid data file for triangular grid
!*                                 (with exact solutions)
!*
!*  Quad-grid files = ringleb_quad.grid
!*                    ringleb_quad.grid
!*                    ringleb_quad.su2
!*                    ringleb_quad.vtk
!*                    ringleb_quad_tec.dat
!*                    ringleb_quad_tec_b.dat_boundary.1.dat
!*                    ringleb_quad_tec_b.dat_boundary.2.dat
!*                    ringleb_quad_tec_b.dat_boundary.3.dat
!*                    ringleb_quad_tec_b.dat_boundary.4.dat
!*
!*  Tria-grid files = ringleb_tria.grid
!*                    ringleb_tria.grid
!*                    ringleb_tria.su2
!*                    ringleb_tria.vtk
!*                    ringleb_tria_tec.dat
!*                    ringleb_tria_tec_b.dat_boundary.1.dat
!*                    ringleb_tria_tec_b.dat_boundary.2.dat
!*                    ringleb_tria_tec_b.dat_boundary.3.dat
!*                    ringleb_tria_tec_b.dat_boundary.4.dat
!*
!*
!* 07-23-2019: Added .grid, .bcmap, .su2, .vkt, and tecplot files.
!* 12-29-2010: Bugs fixed.
!*
!* Katate Masatsuka, July 2019. http://www.cfdbooks.com
!********************************************************************************
 program Ringleb
 implicit none
!Parameters
 integer , parameter :: sp = kind(1.0)
 integer , parameter :: dp = selected_real_kind(2*precision(1.0_sp))

  real(dp), parameter ::  zero = 0.0_dp
  real(dp), parameter ::   one = 1.0_dp

!Local
 integer :: i, j

!Variables and parameters (Grid generation part)
 real(dp), dimension(:,:), allocatable :: x,y, V,psi,rho,p,vx,vy,theta
 real(dp)                              :: psi_min, psi_max, V_min
 integer                               :: nx,ny

!Variables and parameters (Exact solution computation part)
 real(dp), dimension(:,:), allocatable :: Vp,psip,rhop,pp,vxp,vyp,thetap
 real(dp) :: xp,yp, error(7), error_inf(7)


 integer                               :: ntria, nquad, nnodes, inode
 integer , dimension(:,:), allocatable :: tria, quad
 real(dp), dimension(  :), allocatable :: xn, yn

 !- Boundary data requied by a solver.
 !   These are needed for .grid and .su2 files.
 integer                                :: ib, ic
 integer                                :: nb      !# of boundaries
 integer      , dimension(:  ), pointer :: nbnodes !# of nodes in each boundary
 integer      , dimension(:,:), pointer :: bnode   !List of boundary nodes
 character(80), dimension(:  ), pointer :: bnames  !Boundary names

 character(80) :: filename_grid
 character(80) :: filename_bcmap
 character(80) :: filename_su2
 character(80) :: filename_tec
 character(80) :: filename_tec_b
 character(80) :: filename_vtk


!Input parameters
! This defines the domain.
  psi_min = 0.69_dp ! suggested value = 0.69_dp
  psi_max = 1.20_dp ! suggested value = 1.20_dp
    V_min = 0.50_dp ! suggested value = 0.50_dp
! This defines the grid size.
       nx = 30      ! the number of nodes in psi-direction
       ny = 60      ! the number of nodes in V-direction

!Allocate arrays
! Array for grid generation
  allocate(     x(nx+1, ny+1) )
  allocate(     y(nx+1, ny+1) )
  allocate(     V(nx+1, ny+1) )
  allocate(   psi(nx+1, ny+1) )
  allocate(   rho(nx+1, ny+1) )
  allocate(     p(nx+1, ny+1) )
  allocate(    vx(nx+1, ny+1) )
  allocate(    vy(nx+1, ny+1) )
  allocate( theta(nx+1, ny+1) )
! Arrays for exact solution computation
  allocate(    Vp(nx+1, ny+1) )
  allocate(  psip(nx+1, ny+1) )
  allocate(  rhop(nx+1, ny+1) )
  allocate(    pp(nx+1, ny+1) )
  allocate(   vxp(nx+1, ny+1) )
  allocate(   vyp(nx+1, ny+1) )
  allocate(thetap(nx+1, ny+1) )

! 1. Grid generation:
!    Generate boundary points, structured quadrilateral and triangular grids.
     call generate_grid_Ringleb(x,y,V,psi,rho,p,vx,vy,theta, &
                                psi_min,psi_max,V_min,nx,ny  )

!-----------------------------------------------------------
!-----------------------------------------------------------
!-----------------------------------------------------------
!-----------------------------------------------------------

  !Node data
   nnodes = (nx+1)*(ny+1)
   allocate( xn(nnodes), yn(nnodes) )

   do j = 1, ny+1
    do i = 1, nx+1
     inode = i + (j-1)*(nx+1)
       xn(inode) = x(i,j)
       yn(inode) = y(i,j)
    end do
   end do

  !Boundary data
   nb = 4
   allocate( bnode( max(ny+1,nx+1), nb) )
   allocate( nbnodes(nb) )
   allocate(  bnames(nb) )

  !(1)Bottom boundary: j = 1

      ib = 1
      bnames( ib) = "bottom"
      nbnodes(ib) = nx+1
                j = 1
               ic = 0

    do i = 1, nx+1
            inode = i + (j-1)*(nx+1)
               ic = ic + 1
     bnode(ic,ib) = inode
    end do

  !(2)Right boundary: i = nx+1

      ib = 2
      bnames( ib) = "right"
      nbnodes(ib) = ny+1
                i = nx+1
               ic = 0

    do j = 1, ny+1
            inode = i + (j-1)*(nx+1)
               ic = ic + 1
     bnode(ic,ib) = inode
    end do

  !(3)Top boundary: j = ny+1

      ib = 3
      bnames( ib) = "top"
      nbnodes(ib) = nx+1
                j = ny+1
               ic = 0

    do i = nx+1, 1, -1
            inode = i + (j-1)*(nx+1)
               ic = ic + 1
     bnode(ic,ib) = inode
    end do

  !(4)Left boundary: i = 1

      ib = 4
      bnames( ib) = "left"
      nbnodes(ib) = ny+1
                i = 1
               ic = 0

    do j = ny+1, 1, -1
            inode = i + (j-1)*(nx+1)
               ic = ic + 1
     bnode(ic,ib) = inode
    end do

   filename_bcmap = "ringleb_quad.bcmap"
   call write_bcmap_file(filename_bcmap, nb, bnames)

   filename_bcmap = "ringleb_tria.bcmap"
   call write_bcmap_file(filename_bcmap, nb, bnames)

 !-------------------------------------------------
 ! Quadrilateral grid

   write(*,*) " ------------------------------------- "
   write(*,*) "  Quadrilateral grid "
   write(*,*)


   ntria = 0
   nquad = 0
   allocate( tria(    1,3) )
   allocate( quad(nx*ny,4) )

   do j = 1, ny
     do i = 1, nx
       nquad = nquad + 1
       quad(nquad,1) = i   + (j-1)*(nx+1)
       quad(nquad,2) = i+1 + (j-1)*(nx+1)
       quad(nquad,3) = i+1 + (j  )*(nx+1)
       quad(nquad,4) = i   + (j  )*(nx+1)
     end do
   end do

   write(*,*) " nquad = ", nquad, "  nx*ny = ", nx*ny

   filename_grid  = "ringleb_quad.grid"
   call write_grid_file(filename_grid,nnodes,tria,ntria,quad,nquad,xn,yn, &
                                                       nb,nbnodes,bnode )
   filename_su2   = "ringleb_quad.su2"
   call write_su2_file(filename_su2,nnodes,tria,ntria,quad,nquad,xn,yn, &
                                              nb,nbnodes,bnode,bnames )
   filename_tec   = "ringleb_quad_tec.dat"
   call write_tecplot_file(filename_tec, nnodes,xn,yn, ntria,tria, &
                                                     nquad,quad  )

   filename_tec_b = "ringleb_quad_tec_b.dat"
   call write_tecplot_boundary_file(filename_tec_b,xn,yn,nb,nbnodes,bnode)

   filename_vtk   = "ringleb_quad.vtk"
   call write_vtk_file(filename_vtk, nnodes,xn,yn, ntria,tria, &
                                                 nquad,quad  )

   deallocate( tria, quad )

   write(*,*)
   write(*,*) " ------------------------------------- "

 !-------------------------------------------------
 ! Triangular grid

   write(*,*) " ------------------------------------- "
   write(*,*) "  Triangular grid "
   write(*,*)


   ntria = 0
   nquad = 0
   allocate( tria(2*nx*ny,3) )
   allocate( quad(      1,4) )

   do j = 1, ny
     do i = 1, nx

!    Split the quad
!      quad(nquad,1) = i   + (j-1)*(nx+1) !1
!      quad(nquad,2) = i+1 + (j-1)*(nx+1) !2
!      quad(nquad,3) = i+1 + (j  )*(nx+1) !3
!      quad(nquad,4) = i   + (j  )*(nx+1) !4
!    into two triangles as below

       ntria = ntria + 1
       tria(ntria,1) = i   + (j-1)*(nx+1) !1
       tria(ntria,2) = i+1 + (j-1)*(nx+1) !2
       tria(ntria,3) = i   + (j  )*(nx+1) !4

       ntria = ntria + 1
       tria(ntria,1) = i+1 + (j-1)*(nx+1) !2
       tria(ntria,2) = i+1 + (j  )*(nx+1) !3
       tria(ntria,3) = i   + (j  )*(nx+1) !4

     end do
   end do

   write(*,*) " ntria = ", ntria, "  2*nx*ny = ", 2*nx*ny

   filename_grid  = "ringleb_tria.grid"
   call write_grid_file(filename_grid,nnodes,tria,ntria,quad,nquad,xn,yn, &
                                                       nb,nbnodes,bnode )
   filename_su2   = "ringleb_tria.su2"
   call write_su2_file(filename_su2,nnodes,tria,ntria,quad,nquad,xn,yn, &
                                              nb,nbnodes,bnode,bnames )
   filename_tec   = "ringleb_tria_tec.dat"
   call write_tecplot_file(filename_tec, nnodes,xn,yn, ntria,tria, &
                                                     nquad,quad  )

   filename_tec_b = "ringleb_tria_tec_b.dat"
   call write_tecplot_boundary_file(filename_tec_b,xn,yn,nb,nbnodes,bnode)

   filename_vtk   = "ringleb_tria.vtk"
   call write_vtk_file(filename_vtk, nnodes,xn,yn, ntria,tria, &
                                                 nquad,quad  )

   deallocate( tria, quad )

   write(*,*)
   write(*,*) " ------------------------------------- "

!-----------------------------------------------------------
!-----------------------------------------------------------
!-----------------------------------------------------------
!-----------------------------------------------------------   

! 2. Exact solution computation on the grid generated above: 
!    Compute the exact solution at each grid point
!    from the coordinates (xp, yp).
   write(*,*) "Computing exact solutions and errors ......."
   error     =        0.0D0
   error_inf = -1000000.0D0

!    Check if the exact solutions (those with p) match those used to generate
!    the grid.
  do j = 1, ny+1
   do i = 1, nx+1

           xp = x(i,j)
           yp = y(i,j)
!        Compute exact solutions at (xp,yp).
         call exact_solution_Ringleb(Vp(i,j),rhop(i,j),pp(i,j),psip(i,j), &
                             thetap(i,j),vxp(i,j),vyp(i,j),  xp,yp)

     error(1) = error(1) + abs(     Vp(i,j) -     V(i,j) )
     error(2) = error(2) + abs( thetap(i,j) - theta(i,j) )
     error(3) = error(3) + abs(   psip(i,j) -   psi(i,j) )
     error(4) = error(4) + abs(   rhop(i,j) -   rho(i,j) )
     error(5) = error(5) + abs(     pp(i,j) -     p(i,j) )
     error(6) = error(6) + abs(    vxp(i,j) -    vx(i,j) )
     error(7) = error(7) + abs(    vyp(i,j) -    vy(i,j) )

     error_inf(1) = max( error_inf(1), abs(     Vp(i,j) -     V(i,j) ) )
     error_inf(2) = max( error_inf(2), abs( thetap(i,j) - theta(i,j) ) )
     error_inf(3) = max( error_inf(3), abs(   psip(i,j) -   psi(i,j) ) )
     error_inf(4) = max( error_inf(4), abs(   rhop(i,j) -   rho(i,j) ) )
     error_inf(5) = max( error_inf(5), abs(     pp(i,j) -     p(i,j) ) )
     error_inf(6) = max( error_inf(6), abs(    vxp(i,j) -    vx(i,j) ) )
     error_inf(7) = max( error_inf(7), abs(    vyp(i,j) -    vy(i,j) ) )

   end do
  end do

!   Errors between exact solutions computed for given grid points and those
!   from which the grid was generated. All errors should be zero.
    error = error / real((nx+1)*(ny+1))
    write(*,'(A14,7ES9.2)') "   L1 Errors: ", (error(i)    , i=1,7)
    write(*,'(A14,7ES9.2)') "L_inf Errors: ", (error_inf(i), i=1,7)

 stop
!--------------------------------------------------------------------------------
!* End of Main Program
!--------------------------------------------------------------------------------

 contains

!********************************************************************************
!* This generates boundary points and a computational (structured quadrilateral
!* and triangular) grid for a domain of Ringleb's flow, based on the algorithm
!* described in the book,
!*    "I do like CFD, VOL.1" by Katate Masatuka (http://www.cfdbooks.com).
!*
!* Input ------------------------------------------------------------
!*       nx: the number of grid points in x (in psi, to be precise)
!*       ny: the number of grid points in y (in  V , to be precise)
!*  psi_min:  left boundary coordinate (suggested value = 0.69)
!*  psi_max: right boundary coordinate (suggested value = 1.20)
!*    V_min:   top boundary coordinate (suggested value = 0.50)
!*
!* (The physical domain is defined by the last three parameters: psi_min, 
!*  psi_max, V_min. See Figure 7.11.12 in "I do like CFD, VOL.1".)
!*
!* Output -----------------------------------------------------------
!*          x(i,j), y(i,j)    : grid points (i=1,nx+1, j=1,ny+1)
!*          V(i,j), psi(i,j)  : flow speed and stream function
!*        rho(i,j),   p(i,j)  : density and pressure
!*         vx(i,j),  vy(i,j)  : velocity components
!*                 theta(i,j) : flow angle
!*       ringleb_boundary.dat : boundary point data (ordered counter-clockwise)
!*  ringleb_grid_soln_tec.dat : Tecplot file for a quadrilateral grid
!*                              (grid and exact solution)
!*  ringleb_grid_soln_tri_tec.dat : Tecplot file for triangular grid
!*                                  (grid and exact solution)
!*
!*
!* See "I do like CFD, VOL.1" for details.
!*
!* Katate Masatsuka, March 2009. http://www.cfdbooks.com
!********************************************************************************
 subroutine generate_grid_Ringleb( x, y, V, psi, rho, p, vx, vy, theta, &
                                        psi_min, psi_max, V_min, nx, ny)
 implicit none
!Parameters
 integer , parameter :: sp = kind(1.0)
 integer , parameter :: dp = selected_real_kind(2*precision(1.0_sp))
 real(dp), parameter :: fifth=0.2_dp, half=0.5_dp
!Input and Output
 real(dp), intent(in) :: psi_min, psi_max, V_min ! Input
 integer , intent(in) :: nx, ny                  ! Input
 real(dp), dimension(nx+1, ny+1), intent(out) :: x,y,V,psi         ! Output
 real(dp), dimension(nx+1, ny+1), intent(out) :: rho,p,vx,vy,theta ! Output
!Local variables
 integer  :: i, j
 real(dp) :: V_max, b

!******************************************************************
! 1. Generate a structured grid: (psi,V) -> (x,y)
!    (See "I do like CFD, VOL.1", p.190.)
!******************************************************************
 write(*,*) "Generating boundary points ......."

  !*** Right boundary: i = nx+1
     V_max = one / psi_min
    do j = 1, ny+1
     psi(nx+1,j) = psi_min
     call V_from_theta(psi_min, V_min, V_max, j, ny, V(nx+1,j),theta(nx+1,j))
!       V(nx+1,j) = V_from_theta(psi_min, V_min, V_max, j, ny,theta(nx+1,j))
     call xy_from_psi_V(x(nx+1,j), y(nx+1,j),  psi_min, V(nx+1,j))
    end do
    write(*,*) "Right boundary done."

  !*** Left boundary: i = 1
     V_max = one / psi_max
    do j = 1, ny+1
        psi(1,j) = psi_max
      call V_from_theta(psi_max, V_min, V_max, j, ny,  V(1,j),theta(1,j))
!          V(1,j) = V_from_theta(psi_max, V_min, V_max, j, ny,theta(1,j))
     call xy_from_psi_V(x(1, j), y(1, j),  psi_max, V(1,j))
    end do
    write(*,*) "left boundary done."

  !*** Top boundary: j = ny+1
    do i = 1, nx+1
     psi(i,ny+1) = psi_max + (psi_min-psi_max)/real(nx)*real(i-1)
       V(i,ny+1) = V_min
     call xy_from_psi_V(x(i, ny+1), y(i, ny+1),  psi(i,ny+1), V_min)
    end do
    write(*,*) "Top boundary done."

  !*** Bottom boundary: j = 1
    do i = 2, nx
        psi(i,1) = psi_max + (psi_min-psi_max)/real(nx)*real(i-1)
           V_max = one / psi(i,1)
          V(i,1) = V_max
     call xy_from_psi_V(x(i,1), y(i,1),  psi(i,1), V_max)
    end do
    write(*,*) "Bottom boundary done."

! Write out a boundary node data: (x,y) in counter-clockwise order.
  write(*,*) "Writing boundary node data ......."
  open(unit=1, file ="ringleb_boundary_nodes.dat", status="unknown")

   bottom : do i = 1, nx   ! left to right
    write(1,*) x(i, 1), y(i, 1)
   end do bottom

   right : do j = 1, ny    ! bottom to top
    write(1,*) x(nx+1, j), y(nx+1, j)
   end do right

   top : do i = nx, 1, -1  ! right to left
    write(1,*) x(i,ny+1), y(i,ny+1)
   end do top

   left : do j = ny, 1, -1 ! top to bottom
    write(1,*) x(1,j), y(1,j)
   end do left

  close(1)

!Generate interior nodes to construct a structured mesh.
   write(*,*) "Generating interior points ......."
   do i = 2, nx
     do j = 2, ny
       psi(i,j) = psi_max + (psi_min-psi_max)/real(nx)*real(i-1)
          V_max = one / psi(i,j)
       call V_from_theta(psi(i,j), V_min, V_max, j, ny,  V(i,j),theta(i,j))
!         V(i,j) = V_from_theta(psi(i,j), V_min, V_max, j, ny,theta(i,j))
       call xy_from_psi_V(x(i,j), y(i,j),  psi(i,j), V(i,j))
     end do
   end do

   write(*,*) "Interior Points done."

!******************************************************************
! 2. Compute exact solutions from the grid generation data, i.e.
!    from the known values of psi and V at every grid point.
!    ( Iterations are not required here; the grid was generated
!      from the solution. See "I do like CFD, VOL.1", p.191. )
!******************************************************************
 write(*,*) "Computing exact solutions at grid points..."
  do j = 1, ny+1
   do i = 1, nx+1

   if ( abs(psi(i,j)*V(i,j)-one) < 1.0e-15_dp ) then
    theta(i,j) = half*3.141592653589793238_dp ! Do not rely on `asin' here.
   else
    theta(i,j) = asin(psi(i,j)*V(i,j)) ! Eq.(7.11.65)
   endif

       vx(i,j) = -V(i,j)*cos(theta(i,j))
       vy(i,j) = -V(i,j)*sin(theta(i,j))
             b = sqrt(one-fifth*V(i,j)*V(i,j))
      rho(i,j) = b**5 ! Eq.(7.11.68)
        p(i,j) = b**7
   end do
  end do

!******************************************************************
! 3. Write a Tecplot file of a structured grid which includes the
!    exact solution values.
!******************************************************************
 write(*,*) "Writing a Tecplot file (quadrilateral grid)..."
 open(unit=10, file ="ringleb_grid_soln_tec.dat", status="unknown")
 write(10,*) "TITLE = GRID" 
 write(10,*) "VARIABLES = x, y, vx, vy, rho, p, V, theta, Mach"
 write(10,*) "ZONE  N=",(nx+1)*(ny+1), ",E=" , nx*ny, &
             " ,ET=QUADRILATERAL, F=FEPOINT"

! nodes: coordinates and exact solutions.
  do j = 1, ny+1
   do i = 1, nx+1
    write(10,'(9ES20.10)') x(i,j),y(i,j),vx(i,j),vy(i,j),rho(i,j),p(i,j), &
        V(i,j),theta(i,j), V(i,j)/sqrt(abs(1.4_dp*p(i,j)/rho(i,j)))*sqrt(1.4_dp)
   end do
  end do

! elements
  do j = 0, ny-1
    do i = 0, nx-1
      write(10,*) (nx+1)*j+i+1        , (nx+1)*j+(i+1)+1, &
                  (nx+1)*(j+1)+(i+1)+1, (nx+1)*(j+1)+i+1
    end do
  end do

 close(10)

!******************************************************************
! 4. Write a Tecplot file of a triangular grid which includes the
!    exact solution values.
!******************************************************************
 write(*,*) "Writing a Tecplot file (triangular grid)..."
 open(unit=11, file ="ringleb_grid_soln_tri_tec.dat", status="unknown")
 write(11,*) "TITLE = GRID" 
 write(11,*) "VARIABLES = x, y, vx, vy, rho, p, V, theta, Mach"
 write(11,*) "ZONE  N=",(nx+1)*(ny+1), ",E=" , 2*nx*ny, &
             " ,ET=TRIANGLE, F=FEPOINT"

! nodes: coordinates and exact solutions.
  do j = 1, ny+1
   do i = 1, nx+1
    write(11,'(9ES20.10)') x(i,j),y(i,j),vx(i,j),vy(i,j),rho(i,j),p(i,j), &
        V(i,j),theta(i,j), V(i,j)/sqrt(abs(1.4_dp*p(i,j)/rho(i,j)))*sqrt(1.4_dp)
   end do
  end do

! elements
  do j = 0, ny-1
    do i = 0, nx-1
      write(11,*) (nx+1)*j+i+1, (nx+1)*j+(i+1)+1    , (nx+1)*(j+1)+(i+1)+1
      write(11,*) (nx+1)*j+i+1, (nx+1)*(j+1)+(i+1)+1, (nx+1)*(j+1)+i+1
    end do
  end do

 close(11)

 end subroutine generate_grid_Ringleb
!--------------------------------------------------------------------------------



!********************************************************************************
!* This computes exact solutions at a given location (xp,yp).
!*
!* This is a useful subroutine when you need exact solutions at a point
!* on a grid which is not generated from exact solutions ( other than those
!* generated by the subtourine generate_grid_soln_Ringleb() ).
!*
!* See "I do like CFD, VOL.1" for details.
!*
!* Katate Masatsuka, March 2009. http://www.cfdbooks.com
!********************************************************************************
 subroutine exact_solution_Ringleb(V,rho,p,psi,theta,vx,vy,  xp,yp)
 implicit none
!Parameters
 integer , parameter :: sp = kind(1.0)
 integer , parameter :: dp = selected_real_kind(2*precision(1.0_sp))
 real(dp), parameter :: zero=0.0_dp, fifth=0.2_dp, half=0.5_dp
 real(dp), parameter ::  one=1.0_dp, three=3.0_dp, five=5.0_dp
!Input and Output

 real(dp), intent( in) :: xp,yp                    ! Input
 real(dp), intent(out) :: V,rho,p,psi,theta,vx,vy  ! Output
!Local variables
 real(dp) :: b, L, Vpsi

     V = zero
   rho = zero
     p = zero
   psi = zero
 theta = zero
    vx = zero
    vy = zero

!Start computing the exact solution by finding V from (xp,yp)
      V = V_from_xy_fpitr(xp, yp, 0.0_dp, 2.0_dp)

!Once V is obtained, everything else can be computed.
      b = sqrt(one - fifth*V*V)
    rho = b**five
      p = b**7
      L = one/b + one/(three*b**three) + one/(five*b**five) &
        - half*log( (one+b) / (one-b) )
    psi = sqrt( half/V**2-(xp-half*L)*rho )

   Vpsi = sqrt( half-V*V*(xp-half*L)*rho )

 if ( abs( Vpsi - one ) < 1.0e-15_dp  ) then
  theta = half*3.141592653589793238_dp ! Do not rely on `asin' here.
 else
  theta = asin( psi*V )
 endif

     vx = -V*cos(theta)
     vy = -V*sin(theta)

 end subroutine exact_solution_Ringleb


!********************************************************************************
!* This computes (x,y) from psi and V.
!*
!* See "I do like CFD, VOL.1" for details.
!*
!* Katate Masatsuka, March 2009. http://www.cfdbooks.com
!********************************************************************************
 subroutine xy_from_psi_V(x,y, psi,V)
 implicit none
!Parameters
 integer , parameter :: sp = kind(1.0)
 integer , parameter :: dp = selected_real_kind(2*precision(1.0_sp))
 real(dp), parameter :: zero=0.0_dp, fifth=0.2_dp, half=0.5_dp
 real(dp), parameter :: one=1.0_dp, three=3.0_dp, five=5.0_dp
!Input and Output
 real(dp), intent( in) :: psi, V ! Input
 real(dp), intent(out) :: x, y   ! Output
!Local variables
 real(dp) :: b, rho, L

   b  = sqrt((one - fifth*V*V))
  rho = b**five
   L  =   one/b + one/(three*b**three) + one/(five*b**five) &
        - half*log((one+b)/(one-b))
   x  = (one/rho)*(half/V/V- psi*psi) + half*L ! Eq.(7.11.66)
   if ( abs(psi*V - one) < 1.0e-15_dp ) then
    y = zero   ! to avoid sqrt(negative) below.
   else
    y  = psi/(rho*V)*sqrt(one-psi*psi*V*V) ! Eq.(7.11.67)
   endif

 end subroutine xy_from_psi_V
!--------------------------------------------------------------------------------


!********************************************************************************
!* Adjust V through theta along the streamline psi.
!*
!* This is to generate a smooth node distribution on a boundary.
!* See "I do like CFD, VOL.1" by Katate Masatuka (http://www.cfdbooks.com)
!* for details. It computes and returns theta also which is based on a uniform
!* spacing in theta..
!*
!* Katate Masatsuka, March 2009. http://www.cfdbooks.com
!********************************************************************************
 subroutine V_from_theta(psi, V_min, V_max, j, ny, V, theta)
 implicit none
!Parameters
 integer , parameter :: sp = kind(1.0)
 integer , parameter :: dp = selected_real_kind(2*precision(1.0_sp))
!Input and output
 real(dp), intent(in)  :: psi, V_min, V_max   ! Input
 integer , intent(in)  :: j, ny               ! Input
 real(dp), intent(out) :: V, theta            ! Output
!Local variables
 real(dp) :: th_min, th_max
 
           th_min = asin(psi*V_min)
           th_max = asin(psi*V_max)
            theta = th_max + (th_min-th_max)/real(ny)*real(j-1)
                V = sin(theta) / psi

 end subroutine V_from_theta
!--------------------------------------------------------------------------------

!********************************************************************************
!* This solves F(V) = (x-L/2)^2 + y^2 - (1/2/rho/V^2)^2 = 0 for V by the 
!* fixed-point iteration.
!*
!* See "I do like CFD, VOL.1" for details.
!*
!* Katate Masatsuka, March 2009. http://www.cfdbooks.com
!********************************************************************************
 function V_from_xy_fpitr(x, y, Vmin, Vmax)
 implicit none
!Parameters
 integer , parameter :: sp = kind(1.0)
 integer , parameter :: dp = selected_real_kind(2*precision(1.0_sp))
 real(dp), parameter :: fifth=0.2_dp, half=0.5_dp
 real(dp), parameter :: one=1.0_dp, two=2.0_dp, three=3.0_dp, five=5.0_dp
!Input and output
 real(dp), intent(in) :: x, y, Vmin, Vmax ! Iput
 real(dp)             :: V_from_xy_fpitr  ! Output
!Local variables
 integer  :: i, imax
 real(dp) :: b, rho,L, V, VP, tol

 imax = 500
  tol = 1.0e-15_dp

!Initial guess
 Vp = half*( Vmin + Vmax )

!Start iterations
 fp_itertions : do i=1, imax

   b  = sqrt(one - fifth*Vp*Vp)
  rho = b**five
   L  =   one/b + one/(three*b**three) + one/(five*b**five) &
        - half*log((one+b)/(one-b))
   V  = sqrt(one/sqrt( abs((x-half*L)**two + y**two) )/two/rho) ! Eq.(7.11.76)

  if (abs(V-Vp) < tol) exit
  if (i == imax) then
    write(*,*) "did not converge... Sorry.", i
    stop
  endif

   Vp = V

 end do fp_itertions

  V_from_xy_fpitr = V

 end function V_from_xy_fpitr
!--------------------------------------------------------------------------------




!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------
!--------------------------------------------------------------------------------


!*******************************************************************************
! This subroutine writes a Tecplot file for the grid.
!*******************************************************************************
 subroutine write_tecplot_file(filename, nnodes,x,y, ntria,tria, nquad,quad  )

  implicit none

  character(80),                 intent(in) :: filename
  integer      ,                 intent(in) :: nnodes, ntria, nquad
  real(dp)     , dimension(:  ), intent(in) :: x, y
  integer      , dimension(:,:), intent(in) :: tria
  integer      , dimension(:,:), intent(in) :: quad

!Local variables
  integer :: i, os

 write(*,*)
 write(*,*) ' Writing a Tecplot file = ', trim(filename)
 write(*,*)

  open(unit=9, file=filename, status="unknown", iostat=os)

  write(9,*) 'TITLE = "Grid"'
  write(9,*) 'VARIABLES = "x","y"'
!---------------------------------------------------------------------------
! zone

   write(9,*) 'zone t=', '"grid"'
   write(9,*)'  n=', nnodes,',e=', ntria+nquad,' , et=quadrilateral, f=fepoint'

   do i = 1, nnodes
     write(9,'(2es25.15)') x(i), y(i)
   end do

  if (ntria > 0) then
   do i = 1, ntria
    write(9,'(4i10)') tria(i,1), tria(i,2), tria(i,3), tria(i,3)
   end do
  endif

  if (nquad > 0) then
   do i = 1, nquad
    write(9,'(4i10)') quad(i,1), quad(i,2), quad(i,3), quad(i,4)
   end do
  endif

!---------------------------------------------------------------------------

 close(9)

 end subroutine write_tecplot_file
!********************************************************************************


!********************************************************************************
! This subroutine writes boundary grid files.
!********************************************************************************
 subroutine write_tecplot_boundary_file(filename,x,y,nb,nbnodes,bnode )

 implicit none

 character(80),                 intent(in) :: filename
 real(dp)     , dimension(:)  , intent(in) :: x, y
 integer                      , intent(in) :: nb
 integer      , dimension(:  ), intent(in) :: nbnodes
 integer      , dimension(:,:), intent(in) :: bnode

!Local variables
 character(80) :: filename_loc
 character(80) :: bid
 integer       :: ib, os

  do ib = 1, nb

     write( bid  , '(i0)' ) ib
     filename_loc = trim(filename) // trim("_boundary") // trim(".") // trim(bid) // '.dat'

   write(*,*)
   write(*,*) ' Writing a Tecplot file for boundary = ', ib , trim(filename_loc)
   write(*,*)

   open(unit=10, file=filename_loc, status="unknown", iostat=os)

    write(10,*) 'variables = "x", "y"'
    write(10,*) 'ZONE t="', trim("Boundary.") // trim(bid) ,'" I =', nbnodes(ib), ', DATAPACKING=POINT'

    do i = 1, nbnodes(ib)
     write(10,*) x( bnode(i,ib) ), y( bnode(i,ib) )
    end do

   close(10)

  end do

 end subroutine write_tecplot_boundary_file

!********************************************************************************
! This subroutine writes a grid file to be read by a solver.
! NOTE: Unlike the tecplot file, this files contains boundary info.
!********************************************************************************
 subroutine write_grid_file(filename,nnodes,tria,ntria,quad,nquad,x,y, &
                                                      nb,nbnodes,bnode )

 implicit none

 character(80),                 intent(in) :: filename
 integer                      , intent(in) :: nnodes, ntria, nquad
 integer      , dimension(:,:), intent(in) :: tria, quad
 real(dp)     , dimension(:)  , intent(in) :: x, y
 integer                      , intent(in) :: nb
 integer      , dimension(:  ), intent(in) :: nbnodes
 integer      , dimension(:,:), intent(in) :: bnode

!Local variables
 integer  :: os, i, j

   write(*,*)
   write(*,*) " Writing .grid file: ", trim(filename)

!--------------------------------------------------------------------------------
 open(unit=1, file=filename, status="unknown", iostat=os)

!--------------------------------------------------------------------------------
! Grid size: # of nodes, # of triangles, # of quadrilaterals
  write(1,*) nnodes, ntria, nquad

!--------------------------------------------------------------------------------
! Node data
  do i = 1, nnodes
   write(1,*) x(i), y(i)
  end do

!--------------------------------------------------------------------------------
! Triangle connectivity
  if (ntria > 0) then
   do i = 1, ntria
    write(1,*) tria(i,1), tria(i,2), tria(i,3)
   end do
  endif

  if (nquad > 0) then
   do i = 1, nquad
    write(1,*) quad(i,1), quad(i,2), quad(i,3), quad(i,4)
   end do
  endif

!--------------------------------------------------------------------------------
! Boundary data:
!
! The number of boundary segments
  write(1,*) nb

! The number of nodes in each segment:
  do i = 1, nb
   write(1,*) nbnodes(i)
  end do

  write(1,*)

! List of boundary nodes
  do i = 1, nb

   write(*,*) " write_grid_file: nbnodes(i) = ", nbnodes(i), i

   do j = 1, nbnodes(i)
    write(1,*) bnode(j,i)
   end do
    write(1,*)

  end do

!--------------------------------------------------------------------------------
 close(1)

 end subroutine write_grid_file
!********************************************************************************



!*******************************************************************************
! This subroutine writes a .bcmap for the grid.
!*******************************************************************************
 subroutine write_bcmap_file(filename, nb, bnames)

  implicit none

 character(80),                 intent(in) :: filename
 integer                      , intent(in) :: nb
 character(80),   dimension(:), intent(in) :: bnames

!Local variables
  integer :: i, os

 write(*,*)
 write(*,*) ' Writing a .bcmap file = ', trim(filename)
 write(*,*)

  open(unit=9, file=filename, status="unknown", iostat=os)


  write(9,*) "      Boundary Part          Boundary Condition"

  do i = 1, nb
   write(9,'(i20,a28)') i, '"' // trim(bnames(i)) // '"'
  end do

  close(9)

 end subroutine write_bcmap_file
!********************************************************************************

!*******************************************************************************
! This subroutine writes a su2 grid file.
!
! Note: Nodes -> i = 0,1,2,...; Elements -> i = 0,1,2,...
!
!
!  Identifier:
!  Line 	 3
!  Triangle 	 5
!  Quadrilateral 9
!  Tetrahedral 	10
!  Hexahedral 	12
!  Prism 	13
!  Pyramid 	14
!
!
!*******************************************************************************
 subroutine write_su2_file(filename,nnodes,tria,ntria,quad,nquad,x,y, &
                                              nb,nbnodes,bnode,bnames )

 character(80),                 intent(in) :: filename
 integer                      , intent(in) :: nnodes, ntria, nquad
 integer      , dimension(:,:), intent(in) :: tria, quad
 real(dp)     , dimension(:)  , intent(in) :: x, y
 integer                      , intent(in) :: nb
 integer      , dimension(:  ), intent(in) :: nbnodes
 integer      , dimension(:,:), intent(in) :: bnode
 character(80), dimension(:)  , intent(in) :: bnames

!Local variables
 integer :: i, ib, j, os

   write(*,*)
   write(*,*) " Writing .su2 file: ", trim(filename)

  open(unit=7, file=filename, status="unknown", iostat=os)

  write(7,*) "%"
  write(7,*) "% Problem dimension"
  write(7,*) "%"
  write(7,5) 2
5 format('NDIME= ',i12)

   write(7,*) "%"
   write(7,*) "% Inner element connectivity"
   write(7,10) ntria + nquad
10 format('NELEM= ',i12)

 !-------------------------------------------------------------------------
 ! Elements

  if (ntria > 0) then
   do i = 1, ntria
    write(7,'(4i20)') 5, tria(i,1)-1, tria(i,2)-1, tria(i,3)-1
   end do
  endif

  if (nquad > 0) then
   do i = 1, nquad
    write(7,'(5i20)') 9, quad(i,1)-1, quad(i,2)-1, quad(i,3)-1, quad(i,4)-1
   end do
  endif

 !--------------------------------------------------------------------------
 ! Nodes

   write(7,*) "%"
   write(7,*) "% Node coordinates"
   write(7,*) "%"
   write(7,20) nnodes
20 format('NPOIN= ', i12)

  ! Nodes
    do i = 1, nnodes
     write(7,'(2es26.15)') x(i), y(i)
    end do

 !--------------------------------------------------------------------------
 ! Boundary

30  format('NMARK= ',i12)
40  format('MARKER_TAG= ',a)
50  format('MARKER_ELEMS= ', i12)

    write(7,*) "%"
    write(7,*) "% Boundary elements"
    write(7,*) "%"
    write(7,30) nb !# of boundary parts.

   do ib = 1, nb

   !-------------------------
   !Just to print on screen
    write(*,*)
    write(*,40) trim(bnames(ib)) !ib-th boundary-part name, e.g., "farfield".
    write(*,50) nbnodes(ib)-1    !# of boundary elements (edges)
   !-------------------------

    write(7,40) trim(bnames(ib)) !ib-th boundary-part name, e.g., "farfield".
    write(7,50) nbnodes(ib)-1    !# of boundary elements (edges)

    do j = 1, nbnodes(ib)-1
     write(7,'(3i20)') 3, bnode(j,ib)-1, bnode(j+1,ib)-1
    end do

   end do

  close(7)

 end subroutine write_su2_file
!********************************************************************************

!*******************************************************************************
! This subroutine writes a .vtk file for the grid whose name is defined by
! filename_vtk.
!
! Use Paraview to read .vtk and visualize it.  https://www.paraview.org
!
! Search in Google for 'vkt format' to learn .vtk file format.
!*******************************************************************************
 subroutine write_vtk_file(filename, nnodes,x,y, ntria,tria, nquad,quad  )

  implicit none

  character(80),                 intent(in) :: filename
  integer      ,                 intent(in) :: nnodes, ntria, nquad
  real(dp)     , dimension(:  ), intent(in) :: x, y
  integer      , dimension(:,:), intent(in) :: tria
  integer      , dimension(:,:), intent(in) :: quad

!Local variables
  integer :: i, j, os

 !------------------------------------------------------------------------------
 !------------------------------------------------------------------------------
 !------------------------------------------------------------------------------

  write(*,*)
  write(*,*) ' Writing .vtk file = ', trim(filename)
  write(*,*)

 !Open the output file.
  open(unit=8, file=filename, status="unknown", iostat=os)

!---------------------------------------------------------------------------
! Header information

  write(8,'(a)') '# vtk DataFile Version 3.0'
  write(8,'(a)') filename
  write(8,'(a)') 'ASCII'
  write(8,'(a)') 'DATASET UNSTRUCTURED_GRID'

!---------------------------------------------------------------------------
! Nodal information
!
! Note: These nodes i=1,nnodes are interpreted as i=0,nnodes-1 in .vtk file.
!       So, later below, the connectivity list for tria and quad will be
!       shifted by -1.

   write(8,*) 'POINTS ', nnodes, ' double'

   do j = 1, nnodes
    write(8,'(3es25.15)') x(j), y(j), zero
   end do

!---------------------------------------------------------------------------
! Cell information.

  !CELLS: # of total cells (tria+quad), total size of the cell list.

  write(8,'(a,i12,i12)') 'CELLS ',ntria+nquad, (3+1)*ntria + (4+1)*nquad

  ! Note: The latter is the number of integer values written below as data.
  !           4 for triangles (# of vertices + 3 vertices), and
  !           5 for quads     (# of vertices + 4 vertices).

  !---------------------------------
  ! 2.1 List of triangles (counterclockwise vertex ordering)

   if (ntria > 0) then
                         ! (# of vertices = 3), 3 vertices in counterclockwise
    do i = 1, ntria
     write(8,'(a,4i12)') '3', tria(i,1)-1, tria(i,2)-1, tria(i,3)-1
                         ! -1 since VTK reads the nodes as 0,1,2,3,..., not 1,2,3,..
    end do
   endif

  !---------------------------------
  ! 2.2 List of quads (counterclockwise vertex ordering)

   if (nquad > 0) then
                         ! (# of vertices = 4), 4 vertices in counterclockwise
    do i = 1, nquad
     write(8,'(a,4i12)') '4', quad(i,1)-1, quad(i,2)-1, quad(i,3)-1, quad(i,4)-1
                         ! -1 since VTK reads the nodes as 0,1,2,3,..., not 1,2,3,..
    end do
   endif

!---------------------------------------------------------------------------
! Cell type information.

                                   !# of all cells
  write(8,'(a,i11)') 'CELL_TYPES ', ntria+nquad

  !Triangle is classified as the cell type 5 in the .vtk format.

  if (ntria > 0) then
   do i = 1, ntria
    write(8,'(i3)') 5
   end do
  endif

  !Triangle is classified as the cell type 9 in the .vtk format.

  if (nquad > 0) then
   do i = 1, nquad
    write(8,'(i3)') 9
   end do
  endif

!--------------------------------------------------------------------------------
! NOTE: Commented out because there are no solution data. This part should be
!       uncommented if this is used in a solver or if solution data are available
!       and one would like to plot them.
!
! Field data (e.g., density, pressure, velocity)are added here for visualization.

!   write(8,*) 'POINT_DATA   ',nnodes

!  !            FIELD  dataName    # of arrays (variables to plot) <--This is a commnet.
!   write(8,*) 'FIELD  FlowField ', 4

!   write(8,*) 'Density   ',  1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,1)
!   end do

!   write(8,*) 'X-velocity ', 1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,2)
!   end do

!   write(8,*) 'Y-veloiity ', 1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,3)
!   end do

!   write(8,*) 'Pressure   ', 1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,4)
!   end do

!---------------------------------------------------------------------------

 !Close the output file. <--This is a commnet.
  close(8)


 end subroutine write_vtk_file
!********************************************************************************





 end program Ringleb







