!********************************************************************************
!
! This program generates a grid over a cylinder with grid liness clustered
! in the wake region, suitable for vortex shedding simulations.
!
!
!        written by Dr. Katate Masatsuka (info[at]cfdbooks.com),
!
! the author of useful CFD books, "I do like CFD" (http://www.cfdbooks.com).
!
! This is Version 3 (2019).
!
!
! Version 3, 05/06/2019:
! - Added a new input variable "nq", which is the number of quad elements
!   in the radial direction for the mixed grid case. Before, all inner region
!   elements were quads. Now you can specify the number of quad layers.
!
!
! Version 2, 05/02/2019:
! - Added 2 outer boundary parts. The split is determined by
!   an inflow angle specified in the input file.
!
! These F90 routines were written and made available for download
! for an educational purpose. Also, this is intended to provide all CFD
! students and researchers with a tool for their code verification.
!
! This file may be updated in future.
!
! Katate Masatsuka, http://www.cfdbooks.com
!********************************************************************************

!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!
!  Input parameter module
!
!-------------------------------------------------------------------------------
!
!  Sample input file: 'input.nml' to generate a sample grid.
!  ------------------------------------------------------
!   &input_parameters
!                      debug_mode = F
!                             dr1 = 1.0e-03
!                  dr_growth_rate = 1.05
!                      igrid_type = 2
!                      R_cylinder = 0.5
!                      R_outer    = 30.0
!           cluster_outer_pts_sf  = 5.0
!           switching_n1_to_n2_sf = -0.8
!                             nc  = 80
!                             nr1 = 60
!                             nr2 = 50
!              generate_grid_file = T
!              generate_su2_file  = T
!              generate_tec_file  = T
!              generate_vtk_file  = T
!   /
!  ------------------------------------------------------
!
!   Note: No need to specify all namelist variables.
!         Those not specified are given their default values
!         as defined below.
!
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
 module input_parameter_module

  implicit none

  integer , parameter ::    dp = selected_real_kind(P=15)
  real(dp), parameter ::  zero = 0.0_dp
  real(dp), parameter ::   one = 1.0_dp
  real(dp), parameter ::   two = 2.0_dp
  real(dp), parameter :: three = 3.0_dp
  real(dp), parameter ::  half = 0.5_dp
  real(dp), parameter ::    pi = 3.14159265358979323846_dp

  public

!----------------------------
! Default input values
!----------------------------

!----------------------------
! debug_mode = T: for debug use only.
!              F: Default
!
  logical :: debug_mode = .false.

!----------------------------
! The first-off-the-wall mesh spacing at cylinder.

  real(dp) :: dr1 = 1.0e-03_dp

!----------------------------
! Growth rate of the radial spacing in the inner region.

  real(dp) :: dr_growth_rate = 1.05_dp

!----------------------------
! igrid_type = Element type as listed below.
!
!                           1 = Quads
!                           2 = Trias
!                           3 = Mixed
!
! Mixed grid will have quads in the inner region and
! triangles in the outer region.

  integer :: igrid_type = 1

!----------------------------
! Radius of the cylinder

  real(dp) :: R_cylinder   = 0.5_dp

!----------------------------
! Distance to the outer-boundary.

  real(dp) :: R_outer   = 30.0_dp

!----------------------------
! To cluster points near the right center of the outer
! boundary for higher resolution of the wake.
! Larger value will pull more points towards there.

  real(dp) :: cluster_outer_pts_sf = 2.0_dp

!----------------------------
! To control the switch from the surface normal
! to the straight radial direction to outer boundary points.
! Larger value will let the direction quickly switch the direction.
! This value should be negative.

  real(dp) :: switching_n1_to_n2_sf   = 2.0_dp

!----------------------------
! Split the outer boundary into two parts: inflow, outflow if =2.
! If two_outer_boundary_parts = 1, the outer boundary is
! treated as a single boundary part.
! You can visualize the boundary points with the boundary
! datafile (tecplot): assign different colors for different boundary pts.

  integer :: n_outer_boundary_parts = 2

!----------------------------
! Only if n_outer_boundary_parts = 2.
! This defines the split of the outer boundary in
! inflow and outflow part. Uniform flow at this angle of
! attack is used to split it into two parts.

  real(dp) :: angle_of_attack = 0.0_dp

!----------------------------
! # of elements in different directions.
! nc  = around cylinder
! nr1 = in inner region from cylinder.
! nr2 = in outer region from the last point in inner region to farfield.
!
  integer :: nc  = 40 !# of elms around the cylinder
  integer :: nr1 = 40 !# of elms in radial direction in inner region.
  integer :: nr2 = 20 !# of elms in radial direction in outer region.

  integer :: nq  = 40 !# of quads in the radial direction (mixed)

!----------------------------
! generate_tec_file = T to write a Tecplot file.

  logical :: generate_grid_file = .true.

!----------------------------
! generate_su2_file = T to write .su2 file
!                         F not to write.

  logical :: generate_su2_file = .true.

!----------------------------
! generate_tec_file = T to write a Tecplot file.

  logical :: generate_tec_file = .true.

!----------------------------
! generate_vtk_file = T towWrite a .vtk file.

  logical :: generate_vtk_file = .true.

!----------------------------
! End of Default input values
!----------------------------

! Below is the list of all input parameters available:

  namelist / input_parameters /   &
                      debug_mode, &
                             dr1, &
                  dr_growth_rate, &
                      igrid_type, &
                      R_cylinder, &
                         R_outer, &
            cluster_outer_pts_sf, &
           switching_n1_to_n2_sf, &
          n_outer_boundary_parts, &
                 angle_of_attack, &
                             nc , &
                             nr1, &
                             nr2, &
                             nq , &
              generate_grid_file, &
              generate_su2_file , &
              generate_tec_file, &
              generate_vtk_file

 contains

!*****************************************************************************
!* Read input_parameters in the input file: file name = namelist_file
!*****************************************************************************
  subroutine read_nml_input_parameters(namelist_file)

  implicit none
  character(9), intent(in) :: namelist_file
  integer :: os

  write(*,*) "**************************************************************"
  write(*,*) " List of namelist variables and their values"
  write(*,*)

  open(unit=10,file=namelist_file,form='formatted',status='old',iostat=os)
  read(unit=10,nml=input_parameters)

  write(*,nml=input_parameters) ! Print the namelist variables.
  close(10)

  write(*,*) "**************************************************************"
  write(*,*)

  end subroutine read_nml_input_parameters

 end module input_parameter_module
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!
!  End of input parameter module
!
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!-------------------------------------------------------------------------------
!********************************************************************************
! Main program begins here.
!
!
!
! Katate Masatsuka, May 2019. http://www.cfdbooks.com
!********************************************************************************

 program edu2d_cgrid_generation_cylinder

 use input_parameter_module

 implicit none

!Output file names
 character(80) :: filename_tecplot0
 character(80) :: filename_grid
 character(80) :: filename_su2
 character(80) :: filename_tec
 character(80) :: filename_vtk

!Grid data to be constructed:

 !- Basic grid data enough for visualizing the grid.
 !   These are needed by all output files.
 integer                                :: ntria, nquad
 integer      , dimension(:,:), pointer ::  tria,  quad
 integer                                :: nnodes
 real(dp)     , dimension(:), pointer   :: x, y

 !- Boundary data requied by a solver.
 !   These are needed for .grid and .su2 files.
 integer                                :: nb      !# of boundaries
 integer      , dimension(:  ), pointer :: nbnodes !# of nodes in each boundary
 integer      , dimension(:,:), pointer :: bnode   !List of boundary nodes
 character(80), dimension(:  ), pointer :: bnames  !Boundary names

!Temporary variables.
 integer                           :: i, k, itr, stat, nnodes1
 integer                           :: i1, i2, i3,i4
 real(dp)                          :: dr, theta, dtheta, dr_last, r_last, r, gr

 real(dp)                          :: sf, sf_initial, xi, final_x

 real(dp)                          :: s, xo, yo, ro, xp, yp, nx, ny
 real(dp)                          :: nx1, ny1, nx2, ny2

 integer, dimension(:)  , pointer  :: r_last_right
 integer                           :: ib

 real(dp)                          :: ui, vi, dot
 integer                           :: v1, v2, v1s, v2s, v1_start, v2_start, &
                                      v1_end, v2_end, istart, iend

 logical :: still_positive_dot

 real(dp)                          :: ss

!-------------------------------------------------------------------------------
!
! Read the input parameters, defined in the file named as 'input.nml'.
!
!-------------------------------------------------------------------------------

   write(*,*) "Reading the input file: input.nml..... "
   write(*,*)
   call read_nml_input_parameters('input.nml')
   write(*,*)

  if    (igrid_type == 1) then

   filename_tec  = "grid_quad_tec.dat"
   filename_grid = "grid_quad.grid"
   filename_su2  = "grid_quad.su2"
   filename_vtk  = "grid_quad.vtk"

  elseif(igrid_type == 2) then

   filename_tec  = "grid_tria_tec.dat"
   filename_grid = "grid_tria.grid"
   filename_su2  = "grid_tria.su2"
   filename_vtk  = "grid_tria.vtk"

  elseif(igrid_type == 3) then

   filename_tec  = "grid_mixd_tec.dat"
   filename_grid = "grid_mixd.grid"
   filename_su2  = "grid_mixd.su2"
   filename_vtk  = "grid_mixd.vtk"

  endif

   filename_tecplot0 = "debug_grid0_tec.dat"

!-------------------------------------------------------------------------------
!
! Figure out the grid size and allocate arrays.
!
!-------------------------------------------------------------------------------

   if (n_outer_boundary_parts == 1) then
    nb = 2
    allocate(bnode(nc+1,nb))
    allocate(nbnodes(nb))
    allocate(bnames(nb))
   elseif (n_outer_boundary_parts == 2) then
    nb = 3
    allocate(bnode(nc+1,nb))
    allocate(nbnodes(nb))
    allocate(bnames(nb))
   endif

  nnodes = (nr1+nr2+1)*nc
  allocate( x(nnodes), y(nnodes) )

  if    (igrid_type == 1) then

    ntria = 0
    nquad = (nr1 + nr2)*nc

    allocate( tria(1    ,3) )
    allocate( quad(nquad,4) )

  elseif(igrid_type == 2) then

    ntria = 2*(nr1 + nr2)*nc
    nquad = 0

    allocate( tria(ntria,3) )
    allocate( quad(1    ,4) )

  elseif(igrid_type == 3) then

    ntria = 2*(nr2+nr1-nq)*nc
    nquad =   (nq)*nc

    allocate( tria(ntria,3) )
    allocate( quad(nquad,4) )

  else

   write(*,*) "Invalid input: igrid_type = ", igrid_type
   stop

  endif

  write(*,*) "  Predicted dimension:"
  write(*,*) "      nnodes = ", nnodes
  write(*,*) "      nquad  = ", nquad
  write(*,*) "      ntria  = ", ntria

  nnodes = 0
  nquad  = 0
  ntria  = 0

!-------------------------------------------------------------------------------
!
! Generate a grid around the cylinder: inner region.
!
!-------------------------------------------------------------------------------

  write(*,*)
  write(*,*) "  Generating the inner-region grid....."

    allocate( r_last_right(nc/2+1) )

!- Generate nodes:

    dtheta = two*pi/real(nc,dp)

  do i = 1, nc

     theta = 0.0_dp + dtheta*real(i-1,dp)

   do k = 1, nr1+1

    nnodes = nnodes + 1

    if     (k==1) then

     x( k+(i-1)*(nr1+1) ) = R_cylinder*cos(theta)
     y( k+(i-1)*(nr1+1) ) = R_cylinder*sin(theta)

    elseif (k==2) then

                  dr = dr1
     x( k+(i-1)*(nr1+1) ) = x( k-1+(i-1)*(nr1+1) ) + dr*cos(theta)
     y( k+(i-1)*(nr1+1) ) = y( k-1+(i-1)*(nr1+1) ) + dr*sin(theta)

    else

                  dr = dr * dr_growth_rate
     x( k+(i-1)*(nr1+1) ) = x( k-1+(i-1)*(nr1+1) ) + dr*cos(theta)
     y( k+(i-1)*(nr1+1) ) = y( k-1+(i-1)*(nr1+1) ) + dr*sin(theta)

      if (k==nr1+1) dr_last = dr
      if (k==nr1+1)  r_last = sqrt( x(k+(i-1)*(nr1+1))**2 + y(k+(i-1)*(nr1+1))**2 )

    endif

   end do
  end do


!Boundary nodes (oriented so as to see the interior domain on the left).

   ib = 1
   bnames( ib) = "cylinder"
   nbnodes(ib) = nc+1
  do i = nc, 1, -1
   bnode(nc-i+1,ib) = 1+(i-1)*(nr1+1)
  end do
   bnode(nc+1  ,ib) = 1+(nc-1)*(nr1+1)


  nnodes1 = nnodes

!- Generate elements:

  do i = 1, nc
   do k = 1, nr1

    if (i==nc) then
     i1 =  k  +(i-1)*(nr1+1)
     i2 =  k+1+(i-1)*(nr1+1)
     i3 =  k+1
     i4 =  k
    else
     i1 =  k  +(i-1)*(nr1+1)
     i2 =  k+1+(i-1)*(nr1+1)
     i3 =  k+1+(i  )*(nr1+1)
     i4 =  k  +(i  )*(nr1+1)
    endif

   !Triangular grid
    if    (igrid_type == 2) then

             ntria = ntria + 1
     tria(ntria,1) = i1
     tria(ntria,2) = i2
     tria(ntria,3) = i4

             ntria = ntria + 1
     tria(ntria,1) = i4
     tria(ntria,2) = i2
     tria(ntria,3) = i3

   !Mixed grid case
    elseif (igrid_type == 3) then

    !Triangles
     if (k > nq) then

             ntria = ntria + 1
     tria(ntria,1) = i1
     tria(ntria,2) = i2
     tria(ntria,3) = i4

             ntria = ntria + 1
     tria(ntria,1) = i4
     tria(ntria,2) = i2
     tria(ntria,3) = i3

    !Quads only "nq" layers.
     else

             nquad = nquad + 1
     quad(nquad,1) = i1
     quad(nquad,2) = i2
     quad(nquad,3) = i3
     quad(nquad,4) = i4

     endif

   !Quadrilateral grid
    else

             nquad = nquad + 1
     quad(nquad,1) = i1
     quad(nquad,2) = i2
     quad(nquad,3) = i3
     quad(nquad,4) = i4

    endif

   end do
  end do

  if (debug_mode) call write_tecplot_file(filename_tecplot0, nnodes,x,y, ntria,tria, &
                                                                         nquad,quad  )

!-------------------------------------------------------------------------------
!
! Generate a grid in the outer region.
!
! NOTE: The last point may not land precisely on the outer circle.
!
!-------------------------------------------------------------------------------

  write(*,*)
  write(*,*) "  Generating the outer-region grid....."

 !Compute the stretching factor sf for a smooth transition.
    sf_initial = 5.0_dp
             r = r_last + real(1,dp)*( R_outer - r_last )/real(nr2,dp)
            xi = ( r - r_last ) / ( R_outer - r_last )
    call compute_stretching_factor(dr_last,R_outer-r_last,xi,sf_initial, final_x,sf,itr,stat )

 !Equal spacing in theta, which is used as a parameter.
    dtheta = two*pi/real(nc,dp)

  do i = 1, nc

  !Theta is defined over [0,pi] for both upper and lower parts.
                   theta = dtheta*real(i-1,dp)
   if (theta > pi) theta = theta - 2.0_dp*pi

   !This is to further adjust the stretching parameter:
   !  cluster_outer_pts_sf -> r*cluster_outer_pts_sf
      r = one + 0.15_dp*(one-exp(1.2_dp*xi))/(one-exp(1.2_dp))

   !Parameter that goes from 0 to 1.
      s = abs(theta)/pi ! = [0,1]
     xi = (one-exp(r*cluster_outer_pts_sf*s))/(one-exp(r*cluster_outer_pts_sf))

   !Corresponding original outer boundary point: (xo,yo).
      xo = R_outer*cos(xi*theta)
      yo = R_outer*sin(xi*theta)
      ro = sqrt( xo**2 + yo**2 )
    !Direction from the origin to (xo,yo).
     nx2 = xo / ro
     ny2 = yo / ro

   !Corresponding inner boundary point over the cylinder: (xp,yp).
      xp = x( nr1+1 + (i-1)*(nr1+1) )
      yp = y( nr1+1 + (i-1)*(nr1+1) )
       s = sqrt( xp**2 + yp**2 )
    !Direction of surface normal at (xp,yp).
     nx1 = xp / s
     ny1 = yp / s

 !Generate points from the surface point to an outer boundary point.
     ss = one
   do k = 1, nr2

             r = r_last + real(k,dp)*( R_outer - r_last )/real(nr2,dp)
            xi = ( r - r_last ) / ( R_outer - r_last )

            !This is to further adjust the stretching parameter: sf -> s*sf.
             ss = ss + 0.1_dp*(one-exp(5.5_dp*xi))/(one-exp(5.5_dp))

            gr = (one-exp(ss*sf*xi))/(one-exp(ss*sf))
             r = r_last + ( R_outer - r_last )*gr

     nnodes = nnodes + 1

    !Blend the two direction.
     xi = (one-exp(switching_n1_to_n2_sf*xi))/(one-exp(switching_n1_to_n2_sf))
     nx = (1.0_dp-xi)*nx1 + xi*nx2
     ny = (1.0_dp-xi)*ny1 + xi*ny2

    !Blended direction, in which a new node is created.
      s = sqrt( nx**2 + ny**2 )
     nx = nx / s
     ny = ny / s

      x( nnodes1 + k+(i-1)*(nr2) ) = xp + (r-r_last)*nx
      y( nnodes1 + k+(i-1)*(nr2) ) = yp + (r-r_last)*ny

    !Note: Generating nodes this way, there is no guaranetee that the last point
    !      ends up at (xo,yo). So, the outer boundary is not a perfect circle.
    !      But that should not be a problem, shouldn't it?

   end do

  end do

!Boundary nodes (oriented so as to see the interior domain on the left).

 !------------------------------------------------------
 !(1)Outer boundary is a single part, e.g., farfield.

   if (n_outer_boundary_parts == 1) then

      ib = 2
      bnames(ib) = "farfield"
      nbnodes(ib) = nc+1
     do i = 1, nc
      bnode(i,ib) = nnodes1 + nr2+(i-1)*(nr2)
     end do
      bnode(i,ib) = nnodes1 + nr2+(1-1)*(nr2)

 !------------------------------------------------------
 !(2)Outer boundary is split into two: inflow and outflow.
 !   Split is based on a specified inflow direction (ui,vi).

   elseif (n_outer_boundary_parts == 2) then

   !First find the split points.

    !This defines the direction of inflow.
       ui = cos(angle_of_attack)
       vi = sin(angle_of_attack)

    !Search begins with the point (~R_outer,0).
     still_positive_dot = .true.
     find_split : do i = 1, nc

      if (i < nc) then
       v1 = nnodes1 + nr2+(i-1)*(nr2)
       v2 = nnodes1 + nr2+(i  )*(nr2)
      elseif (i==nc) then
       v1 = nnodes1 + nr2+(i-1)*(nr2)
       v2 = nnodes1 + nr2
      endif

     !Dot product is positive = outflow side
     !Dot product is negative =  inflow side

        xo = half*( x(v1) + x(v2) )
        yo = half*( y(v1) + y(v2) )
       dot = ui*xo + vi*yo

       if (still_positive_dot .and. dot < zero) then
          istart = i
        v1_start = v1
        v2_start = v2
        still_positive_dot = .false.
       endif

       if (.not.still_positive_dot .and. dot > zero) then
          iend = i
        v1_end = v1s
        v2_end = v2s
        exit find_split
       endif

        v1s = v1
        v2s = v2

     end do find_split

      ib = 2
      bnames(ib) = "inflow"
      nbnodes(ib) = 0
     do i = istart, iend
      nbnodes(ib) = nbnodes(ib) + 1
      bnode(nbnodes(ib),ib) = nnodes1 + nr2+(i-1)*(nr2)
      if (debug_mode) write(*,*) " ib=2", " j = ", bnode(nbnodes(ib),ib), nbnodes(ib)
     end do


      ib = 3
      bnames(ib) = "outflow"
      nbnodes(ib) = 0
     do i = iend, nc
      nbnodes(ib) = nbnodes(ib) + 1
      bnode(nbnodes(ib),ib) = nnodes1 + nr2+(i-1)*(nr2)
      if (debug_mode) write(*,*) " ib=3 (1)", " j = ", bnode(nbnodes(ib),ib), nbnodes(ib)
     end do
     do i = 1, istart
      nbnodes(ib) = nbnodes(ib) + 1
      bnode(nbnodes(ib),ib) = nnodes1 + nr2+(i-1)*(nr2)
      if (debug_mode) write(*,*) " ib=3 (2)", " j = ", bnode(nbnodes(ib),ib), nbnodes(ib)
     end do

    write(*,*) " nbnodes2 = ", nbnodes(2)
    write(*,*) " nbnodes3 = ", nbnodes(3)
    write(*,*) "  sum + 2 = ", nbnodes(2) + nbnodes(3)
    write(*,*) "       nc = ", nc+2

   endif

!- Generate elements:

  do i = 1, nc
   do k = 1, nr2

    if (k==1) then

     if (i==nc) then
      i1 =           nr1+1  +(i-1)*(nr1+1)
      i2 = nnodes1 + k  +(i-1)*(nr2)
      i3 = nnodes1 + k  
      i4 =           nr1+1
     else
      i1 =           nr1+1  +(i-1)*(nr1+1)
      i2 = nnodes1 + 1+(i-1)*(nr2)
      i3 = nnodes1 + 1+(i  )*(nr2)
      i4 =           nr1+1  +   i*(nr1+1)
     endif

    else

     if (i==nc) then
      i1 = nnodes1 + k-1+(i-1)*(nr2)
      i2 = nnodes1 + k+(i-1)*(nr2)
      i3 = nnodes1 + k
      i4 = nnodes1 + k-1
     else
      i1 = nnodes1 + k-1+(i-1)*(nr2)
      i2 = nnodes1 + k  +(i-1)*(nr2)
      i3 = nnodes1 + k  +(i  )*(nr2)
      i4 = nnodes1 + k-1+(i  )*(nr2)
     endif

    endif

 !Triangles for triangular and mixed grids.
    if    (igrid_type == 2 .or. igrid_type == 3) then

             ntria = ntria + 1
     tria(ntria,1) = i1
     tria(ntria,2) = i2
     tria(ntria,3) = i4

             ntria = ntria + 1
     tria(ntria,1) = i4
     tria(ntria,2) = i2
     tria(ntria,3) = i3

 !Quads for a quad grid.
    else

             nquad = nquad + 1
     quad(nquad,1) = i1
     quad(nquad,2) = i2
     quad(nquad,3) = i3
     quad(nquad,4) = i4

    endif

   end do
  end do

  write(*,*)
  write(*,*) "  Resulting dimension:"
  write(*,*) "      nnodes = ", nnodes
  write(*,*) "      nquad  = ", nquad
  write(*,*) "      ntria  = ", ntria
  write(*,*)

!-------------------------------------------------------------------------------
!
! Generate a .grid file for a 2D solver.
!
!-------------------------------------------------------------------------------

  if (generate_grid_file) then

   call write_grid_file(filename_grid,nnodes,tria,ntria,quad,nquad,x,y, &
                                                       nb,nbnodes,bnode )
  endif

!-------------------------------------------------------------------------------
!
! Generate a .grid file for a 2D solver.
!
!-------------------------------------------------------------------------------

  if (generate_su2_file) then

   call write_su2_file(filename_su2,nnodes,tria,ntria,quad,nquad,x,y, &
                                              nb,nbnodes,bnode,bnames )
  endif

!-------------------------------------------------------------------------------
!
! Generate a Tecplot file.
!
!-------------------------------------------------------------------------------

  if (generate_tec_file) then

   call write_tecplot_file(filename_tec, nnodes,x,y, ntria,tria, &
                                                     nquad,quad  )

   call write_tecplot_boundary_file(filename_tec,x,y,nb,nbnodes,bnode )

  endif

!-------------------------------------------------------------------------------
!
! Generate a .vtk file
!
!-------------------------------------------------------------------------------

  if (generate_vtk_file) then

   call write_vtk_file(filename_vtk, nnodes,x,y, ntria,tria, &
                                                 nquad,quad  )
  endif

!-------------------------------------------------------------------------------


 contains


!*******************************************************************************
! This subroutine writes a Tecplot file for the grid.
!*******************************************************************************
 subroutine write_tecplot_file(filename, nnodes,x,y, ntria,tria, nquad,quad  )

  implicit none

  character(80),                 intent(in) :: filename
  integer      ,                 intent(in) :: nnodes, ntria, nquad
  real(dp)     , dimension(:  ), intent(in) :: x, y
  integer      , dimension(:,:), intent(in) :: tria
  integer      , dimension(:,:), intent(in) :: quad

!Local variables
  integer :: i, os

 write(*,*)
 write(*,*) ' Writing a Tecplot file = ', trim(filename)
 write(*,*)

  open(unit=9, file=filename, status="unknown", iostat=os)

  write(9,*) 'TITLE = "Grid"'
  write(9,*) 'VARIABLES = "x","y"'
!---------------------------------------------------------------------------
! zone

   write(9,*) 'zone t=', '"grid"'
   write(9,*)'  n=', nnodes,',e=', ntria+nquad,' , et=quadrilateral, f=fepoint'

   do i = 1, nnodes
     write(9,'(2es25.15)') x(i), y(i)
   end do

  if (ntria > 0) then
   do i = 1, ntria
    write(9,'(4i10)') tria(i,1), tria(i,2), tria(i,3), tria(i,3)
   end do
  endif

  if (nquad > 0) then
   do i = 1, nquad
    write(9,'(4i10)') quad(i,1), quad(i,2), quad(i,3), quad(i,4)
   end do
  endif

!---------------------------------------------------------------------------

 close(9)

 end subroutine write_tecplot_file
!********************************************************************************


!********************************************************************************
! This subroutine writes boundary grid files.
!********************************************************************************
 subroutine write_tecplot_boundary_file(filename,x,y,nb,nbnodes,bnode )

 implicit none

 character(80),                 intent(in) :: filename
 real(dp)     , dimension(:)  , intent(in) :: x, y
 integer                      , intent(in) :: nb
 integer      , dimension(:  ), intent(in) :: nbnodes
 integer      , dimension(:,:), intent(in) :: bnode

!Local variables
 character(80) :: filename_loc
 character(80) :: bid
 integer       :: ib, os

  do ib = 1, nb

     write( bid  , '(i0)' ) ib
     filename_loc = trim(filename) // trim("_boundary_") // trim(".") // trim(bid) // '.dat'

   write(*,*)
   write(*,*) ' Writing a Tecplot file for boundary = ', ib , trim(filename_loc)
   write(*,*)

   open(unit=10, file=filename_loc, status="unknown", iostat=os)

    write(10,*) 'variables = "x", "y"'
    write(10,*) 'ZONE t="', trim("Boundary.") // trim(bid) ,'" I =', nbnodes(ib), ', DATAPACKING=POINT'

    do i = 1, nbnodes(ib)
     write(10,*) x( bnode(i,ib) ), y( bnode(i,ib) )
    end do

   close(10)

  end do

 end subroutine write_tecplot_boundary_file

!********************************************************************************
! This subroutine writes a grid file to be read by a solver.
! NOTE: Unlike the tecplot file, this files contains boundary info.
!********************************************************************************
 subroutine write_grid_file(filename,nnodes,tria,ntria,quad,nquad,x,y, &
                                                      nb,nbnodes,bnode )

 implicit none

 character(80),                 intent(in) :: filename
 integer                      , intent(in) :: nnodes, ntria, nquad
 integer      , dimension(:,:), intent(in) :: tria, quad
 real(dp)     , dimension(:)  , intent(in) :: x, y
 integer                      , intent(in) :: nb
 integer      , dimension(:  ), intent(in) :: nbnodes
 integer      , dimension(:,:), intent(in) :: bnode

!Local variables
 integer  :: os, i, j

   write(*,*)
   write(*,*) " Writing .grid file: ", trim(filename)

!--------------------------------------------------------------------------------
 open(unit=1, file=filename, status="unknown", iostat=os)

!--------------------------------------------------------------------------------
! Grid size: # of nodes, # of triangles, # of quadrilaterals
  write(1,*) nnodes, ntria, nquad

!--------------------------------------------------------------------------------
! Node data
  do i = 1, nnodes
   write(1,*) x(i), y(i)
  end do

!--------------------------------------------------------------------------------
! Triangle connectivity
  if (ntria > 0) then
   do i = 1, ntria
    write(1,*) tria(i,1), tria(i,2), tria(i,3)
   end do
  endif

  if (nquad > 0) then
   do i = 1, nquad
    write(1,*) quad(i,1), quad(i,2), quad(i,3), quad(i,4)
   end do
  endif

!--------------------------------------------------------------------------------
! Boundary data:
!
! The number of boundary segments
  write(1,*) nb

! The number of nodes in each segment:
  do i = 1, nb
   write(1,*) nbnodes(i)
  end do

  write(1,*)

! List of boundary nodes
  do i = 1, nb

   if (debug_mode) write(*,*) " write_grid_file: nbnodes(i) = ", nbnodes(i), i

   do j = 1, nbnodes(i)
    write(1,*) bnode(j,i)
   end do
    write(1,*)

  end do

!--------------------------------------------------------------------------------
 close(1)

 end subroutine write_grid_file
!********************************************************************************

!*******************************************************************************
! This subroutine writes a su2 grid file.
!
! Note: Nodes -> i = 0,1,2,...; Elements -> i = 0,1,2,...
!
!
!  Identifier:
!  Line 	 3
!  Triangle 	 5
!  Quadrilateral 9
!  Tetrahedral 	10
!  Hexahedral 	12
!  Prism 	13
!  Pyramid 	14
!
!
!*******************************************************************************
 subroutine write_su2_file(filename,nnodes,tria,ntria,quad,nquad,x,y, &
                                              nb,nbnodes,bnode,bnames )

 character(80),                 intent(in) :: filename
 integer                      , intent(in) :: nnodes, ntria, nquad
 integer      , dimension(:,:), intent(in) :: tria, quad
 real(dp)     , dimension(:)  , intent(in) :: x, y
 integer                      , intent(in) :: nb
 integer      , dimension(:  ), intent(in) :: nbnodes
 integer      , dimension(:,:), intent(in) :: bnode
 character(80), dimension(:)  , intent(in) :: bnames

!Local variables
 integer :: i, ib, j, os

   write(*,*)
   write(*,*) " Writing .su2 file: ", trim(filename)

  open(unit=7, file=filename, status="unknown", iostat=os)

  write(7,*) "%"
  write(7,*) "% Problem dimension"
  write(7,*) "%"
  write(7,5) 2
5 format('NDIME= ',i12)

   write(7,*) "%"
   write(7,*) "% Inner element connectivity"
   write(7,10) ntria + nquad
10 format('NELEM= ',i12)

 !-------------------------------------------------------------------------
 ! Elements

  if (ntria > 0) then
   do i = 1, ntria
    write(7,'(4i20)') 5, tria(i,1)-1, tria(i,2)-1, tria(i,3)-1
   end do
  endif

  if (nquad > 0) then
   do i = 1, nquad
    write(7,'(5i20)') 9, quad(i,1)-1, quad(i,2)-1, quad(i,3)-1, quad(i,4)-1
   end do
  endif

 !--------------------------------------------------------------------------
 ! Nodes

   write(7,*) "%"
   write(7,*) "% Node coordinates"
   write(7,*) "%"
   write(7,20) nnodes
20 format('NPOIN= ', i12)

  ! Nodes
    do i = 1, nnodes
     write(7,'(2es26.15)') x(i), y(i)
    end do

 !--------------------------------------------------------------------------
 ! Boundary

30  format('NMARK= ',i12)
40  format('MARKER_TAG= ',a)
50  format('MARKER_ELEMS= ', i12)

    write(7,*) "%"
    write(7,*) "% Boundary elements"
    write(7,*) "%"
    write(7,30) nb !# of boundary parts.

   do ib = 1, nb

   !-------------------------
   !Just to print on screen
    write(*,*)
    write(*,40) trim(bnames(ib)) !ib-th boundary-part name, e.g., "farfield".
    write(*,50) nbnodes(ib)-1    !# of boundary elements (edges)
   !-------------------------

    write(7,40) trim(bnames(ib)) !ib-th boundary-part name, e.g., "farfield".
    write(7,50) nbnodes(ib)-1    !# of boundary elements (edges)

    do j = 1, nbnodes(ib)-1
     write(7,'(3i20)') 3, bnode(j,ib)-1, bnode(j+1,ib)-1
    end do

   end do

  close(7)

 end subroutine write_su2_file
!********************************************************************************

!*******************************************************************************
! This subroutine writes a .vtk file for the grid whose name is defined by
! filename_vtk.
!
! Use Paraview to read .vtk and visualize it.  https://www.paraview.org
!
! Search in Google for 'vkt format' to learn .vtk file format.
!*******************************************************************************
 subroutine write_vtk_file(filename, nnodes,x,y, ntria,tria, nquad,quad  )

  implicit none

  character(80),                 intent(in) :: filename
  integer      ,                 intent(in) :: nnodes, ntria, nquad
  real(dp)     , dimension(:  ), intent(in) :: x, y
  integer      , dimension(:,:), intent(in) :: tria
  integer      , dimension(:,:), intent(in) :: quad

!Local variables
  integer :: i, j, os

 !------------------------------------------------------------------------------
 !------------------------------------------------------------------------------
 !------------------------------------------------------------------------------

  write(*,*)
  write(*,*) ' Writing .vtk file = ', trim(filename)
  write(*,*)

 !Open the output file.
  open(unit=8, file=filename, status="unknown", iostat=os)

!---------------------------------------------------------------------------
! Header information

  write(8,'(a)') '# vtk DataFile Version 3.0'
  write(8,'(a)') filename
  write(8,'(a)') 'ASCII'
  write(8,'(a)') 'DATASET UNSTRUCTURED_GRID'

!---------------------------------------------------------------------------
! Nodal information
!
! Note: These nodes i=1,nnodes are interpreted as i=0,nnodes-1 in .vtk file.
!       So, later below, the connectivity list for tria and quad will be
!       shifted by -1.

   write(8,*) 'POINTS ', nnodes, ' double'

   do j = 1, nnodes
    write(8,'(3es25.15)') x(j), y(j), zero
   end do

!---------------------------------------------------------------------------
! Cell information.

  !CELLS: # of total cells (tria+quad), total size of the cell list.

  write(8,'(a,i12,i12)') 'CELLS ',ntria+nquad, (3+1)*ntria + (4+1)*nquad

  ! Note: The latter is the number of integer values written below as data.
  !           4 for triangles (# of vertices + 3 vertices), and
  !           5 for quads     (# of vertices + 4 vertices).

  !---------------------------------
  ! 2.1 List of triangles (counterclockwise vertex ordering)

   if (ntria > 0) then
                         ! (# of vertices = 3), 3 vertices in counterclockwise
    do i = 1, ntria
     write(8,'(a,4i12)') '3', tria(i,1)-1, tria(i,2)-1, tria(i,3)-1
                         ! -1 since VTK reads the nodes as 0,1,2,3,..., not 1,2,3,..
    end do
   endif

  !---------------------------------
  ! 2.2 List of quads (counterclockwise vertex ordering)

   if (nquad > 0) then
                         ! (# of vertices = 4), 4 vertices in counterclockwise
    do i = 1, nquad
     write(8,'(a,4i12)') '4', quad(i,1)-1, quad(i,2)-1, quad(i,3)-1, quad(i,4)-1
                         ! -1 since VTK reads the nodes as 0,1,2,3,..., not 1,2,3,..
    end do
   endif

!---------------------------------------------------------------------------
! Cell type information.

                                   !# of all cells
  write(8,'(a,i11)') 'CELL_TYPES ', ntria+nquad

  !Triangle is classified as the cell type 5 in the .vtk format.

  if (ntria > 0) then
   do i = 1, ntria
    write(8,'(i3)') 5
   end do
  endif

  !Triangle is classified as the cell type 9 in the .vtk format.

  if (nquad > 0) then
   do i = 1, nquad
    write(8,'(i3)') 9
   end do
  endif

!--------------------------------------------------------------------------------
! NOTE: Commented out because there are no solution data. This part should be
!       uncommented if this is used in a solver or if solution data are available
!       and one would like to plot them.
!
! Field data (e.g., density, pressure, velocity)are added here for visualization.

!   write(8,*) 'POINT_DATA   ',nnodes

!  !            FIELD  dataName    # of arrays (variables to plot) <--This is a commnet.
!   write(8,*) 'FIELD  FlowField ', 4

!   write(8,*) 'Density   ',  1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,1)
!   end do

!   write(8,*) 'X-velocity ', 1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,2)
!   end do

!   write(8,*) 'Y-veloiity ', 1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,3)
!   end do

!   write(8,*) 'Pressure   ', 1 , nnodes, ' double'
!   do j = 1, nnodes
!    write(8,'(es25.15)') w(j,4)
!   end do

!---------------------------------------------------------------------------

 !Close the output file. <--This is a commnet.
  close(8)


 end subroutine write_vtk_file
!********************************************************************************


!********************************************************************************
! This solves target_x - cx*(one-exp(sf*xi))/(one-exp(sf)) for sf.
!
! E.g., If one wants to find the stretching factor sf such that
!        (rmax-rmin)*(one-exp(sf*xi))/(one-exp(sf)) = desired spacing,
!       where xi = 1*(rmax-rmin)/n (=1st increment in the uniform spacing),
!       Set cx = rmax-rmin, and target_x=desired spacing.
!
!********************************************************************************
 subroutine compute_stretching_factor(target_x,cx,xi,sf_initial, final_x,sf,itr,stat )
 implicit none

 real(dp), intent( in) :: target_x, cx, xi, sf_initial
 real(dp), intent(out) :: final_x, sf
 integer , intent(out) :: itr, stat

 real(dp)              :: res, dres, gr

 
  stat = 0
   itr = 0
    sf = sf_initial

 do

   !Equation to sovle: res = 0.0.

       gr = (one-exp(sf*xi))/(one-exp(sf))
      res =  target_x - cx*gr

      final_x = gr*cx

     if (debug_mode) write(*,'(a,i6,a,es10.2,a,es10.2,a,es10.2,a,es10.2)') &
       "   itr=", itr, " current_x = ", final_x, " target_x = ", target_x, &
       " sf=",sf, " res=", abs(res/target_x)

     if (abs(res)/target_x < 1.0e-08_dp) exit !<--Smaller the better.

   ! The following doesn't work. This creates a trivial solution sf=0
   ! and Newton's method converges to it.
   !     res =  dr1*(one-exp(sf)) - rmax*(one-exp(sf*xi))
   !    dres = -dr1*     exp(sf)  +   xi*rmax*exp(sf*xi)

   !So, we solve directly: target_x - cx*(one-exp(sf*xi))/(one-exp(sf))
   !Its derivative is 
    dres = - cx*( -xi*exp(sf*xi)*(one-exp(sf)) + exp(sf)*(one-exp(sf*xi)) )/(one-exp(sf))**2

   !Newton iteration:
      sf = sf - res/dres

  itr = itr + 1
  if (itr > 500) then
    stat = 1
    exit
  endif

 end do

   if (sf < 0.0_dp) stat = 2 !Negative stretching factor.

 end subroutine compute_stretching_factor
!********************************************************************************


 end program edu2d_cgrid_generation_cylinder
